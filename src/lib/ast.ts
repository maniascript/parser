// Loosely follow the estree syntax

import { type Token } from 'antlr4ng'
import {
  ProgramContext,
  ProgramContentContext,
  RequireContextDirectiveContext,
  ExtendsDirectiveContext,
  IncludeDirectiveContext,
  SettingDirectiveContext,
  CommandDirectiveContext,
  StructDirectiveContext,
  StructDeclarationContext,
  StructMemberContext,
  StructAliasingContext,
  ConstDirectiveContext,
  ConstDeclarationContext,
  ConstAliasingContext,
  TypeContext,
  InitializerTypeContext,
  BaseTypeContext,
  ArrayTypeContext,
  SimpleTypeContext,
  EnumTypeContext,
  CustomTypeContext,
  TextLiteralContext,
  LiteralContext,
  TextContext,
  IntegerContext,
  RealContext,
  BooleanContext,
  NullContext,
  NullIdContext,
  EnumContext,
  ExpressionContext,
  VectorContext,
  ArrayContext,
  ArrayExpressionContext,
  StructureContext,
  StructureMemberInitializationContext,
  FunctionCallExpressionContext,
  FunctionCallContext,
  LiteralExpressionContext,
  FunctionDeclarationContext,
  LabelDeclarationContext,
  StorageSpecifierContext,
  BlockContext,
  ParameterContext,
  StatementContext,
  MainStatementContext,
  ExpressionStatementContext,
  AsExpressionContext,
  IsExpressionContext,
  DotAccessExpressionContext,
  IndexAccessExpressionContext,
  UnaryMinusExpressionContext,
  NotExpressionContext,
  MultiplicativeExpressionContext,
  AdditiveExpressionContext,
  ConcatenationExpressionContext,
  RelationalExpressionContext,
  EqualityExpressionContext,
  OrExpressionContext,
  AndExpressionContext,
  TextInterpolationExpressionContext,
  DumptypeExpressionContext,
  DumpExpressionContext,
  CastExpressionContext,
  IdentifierExpressionContext,
  VectorExpressionContext,
  ThisExpressionContext,
  NowExpressionContext,
  ParenthesisExpressionContext,
  AssignmentStatementContext,
  ManiaScriptParser,
  ReturnStatementContext,
  LabelStatementContext,
  AssertStatementContext,
  ForeachStatementContext,
  ForStatementContext,
  WhileStatementContext,
  MeanwhileStatementContext,
  BreakStatementContext,
  ContinueStatementContext,
  SwitchStatementContext,
  SwitchCaseContext,
  SwitchDefaultContext,
  SwitchtypeStatementContext,
  SwitchtypeCaseContext,
  SwitchtypeDefaultContext,
  ConditionalStatementContext,
  IfBranchContext,
  ElseIfBranchContext,
  ElseBranchContext,
  LogStatementContext,
  SleepStatementContext,
  TuningstartStatementContext,
  TuningendStatementContext,
  TuningmarkStatementContext,
  WaitStatementContext,
  YieldStatementContext,
  StructureExpressionContext,
  VariableDeclarationWithTypeContext,
  VariableDeclarationWithoutTypeContext,
  VariableDeclarationLetContext
} from '../antlr/ManiaScriptParser.js'
import { SourceLocationRange } from './position.js'

interface AST {
  program?: Program
}

type VisitCallback = ((node: Node) => void) | null

enum Kind {
  Program = 'Program',
  SimpleType = 'SimpleType',
  ClassType = 'ClassType',
  EnumType = 'EnumType',
  CustomType = 'CustomType',
  InvalidType = 'InvalidType',
  ArrayType = 'ArrayType',
  InvalidLiteral = 'InvalidLiteral',
  TextLiteral = 'TextLiteral',
  IntegerLiteral = 'IntegerLiteral',
  RealLiteral = 'RealLiteral',
  BooleanLiteral = 'BooleanLiteral',
  NullLiteral = 'NullLiteral',
  NullIdLiteral = 'NullIdLiteral',
  EnumLiteral = 'EnumLiteral',
  Identifier = 'Identifier',
  InvalidIdentifier = 'InvalidIdentifier',
  RequireContextDirective = 'RequireContextDirective',
  InvalidDirective = 'InvalidDirective',
  ExtendsDirective = 'ExtendsDirective',
  IncludeDirective = 'IncludeDirective',
  InvalidSettingValue = 'InvalidSettingValue',
  SettingDirective = 'SettingDirective',
  CommandDirective = 'CommandDirective',
  StructMemberDeclaration = 'StructMemberDeclaration',
  StructDeclaration = 'StructDeclaration',
  StructAliasing = 'StructAliasing',
  StructDirective = 'StructDirective',
  InvalidConstValue = 'InvalidConstValue',
  ConstDeclaration = 'ConstDeclaration',
  ConstAliasing = 'ConstAliasing',
  ConstDirective = 'ConstDirective',
  InvalidDeclaration = 'InvalidDeclaration',
  VariableDeclaration = 'VariableDeclaration',
  FunctionParameterDeclaration = 'FunctionParameterDeclaration',
  FunctionDeclaration = 'FunctionDeclaration',
  LabelDeclaration = 'LabelDeclaration',
  BlockStatement = 'BlockStatement',
  InvalidStatement = 'InvalidStatement',
  ExpressionStatement = 'ExpressionStatement',
  InvalidExpression = 'InvalidExpression',
  VectorExpression = 'VectorExpression',
  ArrayExpression = 'ArrayExpression',
  StructMemberExpression = 'StructMemberExpression',
  StructExpression = 'StructExpression',
  FunctionCallExpression = 'FunctionCallExpression',
  AsExpression = 'AsExpression',
  IsExpression = 'IsExpression',
  DotAccessExpression = 'DotAccessExpression',
  IndexAccessExpression = 'IndexAccessExpression',
  UnaryExpression = 'UnaryExpression',
  BinaryExpression = 'BinaryExpression',
  LogicalExpression = 'LogicalExpression',
  TemplateTextLiteral = 'TemplateTextLiteral',
  TemplateTextElement = 'TemplateTextElement',
  DumptypeExpression = 'DumptypeExpression',
  DumpExpression = 'DumpExpression',
  CastExpression = 'CastExpression',
  ThisExpression = 'ThisExpression',
  NowExpression = 'NowExpression',
  AssignmentStatement = 'AssignmentStatement',
  ReturnStatement = 'ReturnStatement',
  LabelStatement = 'LabelStatement',
  AssertStatement = 'AssertStatement',
  ForeachStatement = 'ForeachStatement',
  ForStatement = 'ForStatement',
  WhileStatement = 'WhileStatement',
  MeanwhileStatement = 'MeanwhileStatement',
  BreakStatement = 'BreakStatement',
  ContinueStatement = 'ContinueStatement',
  SwitchStatement = 'SwitchStatement',
  SwitchCase = 'SwitchCase',
  SwitchtypeStatement = 'SwitchtypeStatement',
  SwitchtypeCase = 'SwitchtypeCase',
  ConditionalStatement = 'ConditionalStatement',
  ConditionalBranch = 'ConditionalBranch',
  LogStatement = 'LogStatement',
  SleepStatement = 'SleepStatement',
  TuningstartStatement = 'TuningstartStatement',
  TuningendStatement = 'TuningendStatement',
  TuningmarkStatement = 'TuningmarkStatement',
  WaitStatement = 'WaitStatement',
  YieldStatement = 'YieldStatement',
  Main = 'Main'
}

function isNullish (value: unknown): value is (undefined | null) {
  return value === undefined || value === null
}

class ASTBuildError extends Error {}

abstract class Node {
  abstract kind: Kind
  source: SourceLocationRange
  parent?: Node

  constructor (parent: Node | undefined, start: Token | null, stop?: Token | null) {
    if (start === null) {
      throw new ASTBuildError('Failed to build node. Missing start token')
    } else {
      this.source = new SourceLocationRange(start, stop)
    }
    this.parent = parent
  }

  enter (enterCb: VisitCallback): void {
    if (enterCb !== null) enterCb(this)
  }

  exit (exitCb: VisitCallback): void {
    if (exitCb !== null) exitCb(this)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.exit(exitCb)
  }
}

class Program extends Node {
  kind = Kind.Program
  directives: Directive[]
  declarations: Declaration[]
  main?: Main

  constructor (program: ProgramContentContext) {
    super(undefined, program.start, program.stop)
    this.directives = []
    this.declarations = []
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    for (const directives of this.directives) {
      directives.visit(enterCb, exitCb)
    }
    for (const declaration of this.declarations) {
      declaration.visit(enterCb, exitCb)
    }
    this.main?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

enum TypeName {
  Boolean = 'Boolean',
  Ident = 'Ident',
  Int2 = 'Int2',
  Int3 = 'Int3',
  Integer = 'Integer',
  Real = 'Real',
  Text = 'Text',
  Vec2 = 'Vec2',
  Vec3 = 'Vec3',
  Void = 'Void',
  Invalid = 'Invalid'
}

type BaseType = (
  SimpleType |
  ClassType |
  EnumType |
  CustomType |
  InvalidType
)

function isBaseType (node: Node): node is BaseType {
  return (
    node.kind === Kind.SimpleType ||
    node.kind === Kind.ClassType ||
    node.kind === Kind.EnumType ||
    node.kind === Kind.CustomType
  )
}

type KeyType = (
  BaseType |
  'EmptyKey'
)

type InitializerType = (
  SimpleType |
  ClassType |
  ArrayType |
  InvalidType
)

function isInitializerType (node: Node): node is InitializerType {
  return (
    node.kind === Kind.ArrayType ||
    node.kind === Kind.SimpleType ||
    node.kind === Kind.ClassType
  )
}

type Type = (
  BaseType |
  ArrayType
)

function isType (node: Node): node is Type {
  return (
    node.kind === Kind.ArrayType ||
    isBaseType(node)
  )
}

class SimpleType extends Node {
  kind = Kind.SimpleType
  name: TypeName

  constructor (parent: Node, simpleType: SimpleTypeContext) {
    super(parent, simpleType.start, simpleType.stop)

    if (simpleType.TYPE_BOOLEAN() !== null) {
      this.name = TypeName.Boolean
    } else if (simpleType.TYPE_IDENT() !== null) {
      this.name = TypeName.Ident
    } else if (simpleType.TYPE_INT2() !== null) {
      this.name = TypeName.Int2
    } else if (simpleType.TYPE_INT3() !== null) {
      this.name = TypeName.Int3
    } else if (simpleType.TYPE_INTEGER() !== null) {
      this.name = TypeName.Integer
    } else if (simpleType.TYPE_REAL() !== null) {
      this.name = TypeName.Real
    } else if (simpleType.TYPE_TEXT() !== null) {
      this.name = TypeName.Text
    } else if (simpleType.TYPE_VEC2() !== null) {
      this.name = TypeName.Vec2
    } else if (simpleType.TYPE_VEC3() !== null) {
      this.name = TypeName.Vec3
    } else if (simpleType.TYPE_VOID() !== null) {
      this.name = TypeName.Void
    } else {
      this.name = TypeName.Invalid
    }
  }
}

class ClassType extends Node {
  kind = Kind.ClassType
  name: string

  constructor (parent: Node, className: Token) {
    super(parent, className)
    this.name = className.text ?? ''
  }
}

class EnumType extends Node {
  kind = Kind.EnumType
  class?: ClassType
  name: Identifier

  constructor (parent: Node, enumType: EnumTypeContext) {
    super(parent, enumType.start, enumType.stop)

    const classToken = enumType.className()?.CLASS().symbol
    if (classToken !== undefined) {
      this.class = new ClassType(this, classToken)
    }
    this.name = new Identifier(this, enumType.enumName().IDENTIFIER().symbol)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.class?.visit(enterCb, exitCb)
    this.name.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class CustomType extends Node {
  kind = Kind.CustomType
  name: Identifier

  constructor (parent: Node, customType: CustomTypeContext) {
    super(parent, customType.start, customType.stop)
    this.name = new Identifier(this, customType.structName().IDENTIFIER().symbol, customType.namespaceName()?.IDENTIFIER().symbol)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class InvalidType extends Node {
  kind = Kind.InvalidType
  error: Error | null

  constructor (parent: Node | undefined, start: Token | null, stop?: Token | null, error?: Error | null) {
    super(parent, start, stop)

    if (error instanceof Error) {
      this.error = error
    } else {
      this.error = null
    }
  }
}

class ArrayType extends Node {
  kind = Kind.ArrayType
  value: BaseType
  keys: KeyType[]

  constructor (parent: Node, arrayType: ArrayTypeContext) {
    super(parent, arrayType.start, arrayType.stop)
    this.value = createBaseTypeNode(this, arrayType.baseType())
    this.keys = []
    for (const keyType of arrayType.arrayTypeDimension()) {
      const baseType = keyType.baseType()
      if (baseType !== null) {
        this.keys.push(createBaseTypeNode(this, baseType))
      } else {
        this.keys.push('EmptyKey')
      }
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.value.visit(enterCb, exitCb)
    for (const key of this.keys) {
      if (key !== 'EmptyKey') {
        key.visit(enterCb, exitCb)
      }
    }
    this.exit(exitCb)
  }
}

function createBaseTypeNode (parent: Node, baseType: BaseTypeContext): BaseType {
  try {
    const simpleType = baseType.simpleType()
    const classType = baseType.className()
    const enumType = baseType.enumType()
    const customType = baseType.customType()
    if (simpleType != null) {
      return new SimpleType(parent, simpleType)
    } else if (classType !== null) {
      return new ClassType(parent, classType.CLASS().symbol)
    } else if (enumType !== null) {
      return new EnumType(parent, enumType)
    } else if (customType !== null) {
      return new CustomType(parent, customType)
    } else {
      return new InvalidType(parent, baseType.start, baseType.stop)
    }
  } catch (error) {
    if (error instanceof Error) {
      return new InvalidType(parent, baseType.start, baseType.stop, error)
    } else {
      return new InvalidType(parent, baseType.start, baseType.stop, new ASTBuildError('Failed to build `BaseType` node. Something went wrong while reading `BaseTypeContext`.'))
    }
  }
}

function createInitializerTypeNode (parent: Node, type: InitializerTypeContext): InitializerType {
  try {
    const arrayType = type.arrayType()
    const simpleType = type.simpleType()
    const classType = type.className()
    if (arrayType !== null) {
      return new ArrayType(parent, arrayType)
    } else if (simpleType !== null) {
      return new SimpleType(parent, simpleType)
    } else if (classType !== null) {
      return new ClassType(parent, classType.CLASS().symbol)
    } else {
      return new InvalidType(parent, type.start, type.stop)
    }
  } catch (error) {
    if (error instanceof Error) {
      return new InvalidType(parent, type.start, type.stop, error)
    } else {
      return new InvalidType(parent, type.start, type.stop, new ASTBuildError('Failed to build `InitializerType` node. Something went wrong while reading `BaseTypeContext`.'))
    }
  }
}

function createTypeNode (parent: Node, type: TypeContext): Type {
  try {
    const arrayType = type.arrayType()
    const baseType = type.baseType()
    if (arrayType !== null) {
      return new ArrayType(parent, arrayType)
    } else if (baseType !== null) {
      return createBaseTypeNode(parent, baseType)
    } else {
      return new InvalidType(parent, type.start, type.stop)
    }
  } catch (error) {
    if (error instanceof Error) {
      return new InvalidType(parent, type.start, type.stop, error)
    } else {
      return new InvalidType(parent, type.start, type.stop, new ASTBuildError('Failed to build `Type` node. Something went wrong while reading `TypeContext`.'))
    }
  }
}

type Literal = (
  TextLiteral |
  IntegerLiteral |
  RealLiteral |
  BooleanLiteral |
  NullLiteral |
  NullIdLiteral |
  EnumLiteral |
  InvalidLiteral
)

function isLiteral (node: Node): node is Literal {
  return (
    node.kind === Kind.TextLiteral ||
    node.kind === Kind.IntegerLiteral ||
    node.kind === Kind.RealLiteral ||
    node.kind === Kind.BooleanLiteral ||
    node.kind === Kind.NullLiteral ||
    node.kind === Kind.NullIdLiteral ||
    node.kind === Kind.EnumLiteral
  )
}

class InvalidLiteral extends Node {
  kind = Kind.InvalidLiteral
  error: Error | null

  constructor (parent: Node | undefined, start: Token | null, stop?: Token | null, error?: Error | null) {
    super(parent, start, stop)

    if (error instanceof Error) {
      this.error = error
    } else {
      this.error = null
    }
  }
}

class TextLiteral extends Node {
  kind = Kind.TextLiteral
  isTranslated: boolean
  isMultiline: boolean
  value: string
  raw: string

  constructor (parent: Node, textLiteral: TextLiteralContext) {
    super(parent, textLiteral.start, textLiteral.stop)

    this.isMultiline = false
    this.value = ''
    this.raw = ''

    const textTranslatedLiteral = textLiteral.textTranslatedLiteral()
    this.isTranslated = textTranslatedLiteral !== null

    let textBaseLiteral = textLiteral.textBaseLiteral()
    if (textTranslatedLiteral !== null) {
      textBaseLiteral = textTranslatedLiteral.textBaseLiteral()
      this.raw = textTranslatedLiteral.getText()
    }

    if (textBaseLiteral !== null) {
      const textSingleQuoteLiteral = textBaseLiteral.textSingleQuoteLiteral()
      const textTripleQuoteLiteral = textBaseLiteral.textTripleQuoteLiteral()
      if (textSingleQuoteLiteral !== null) {
        this.value = textSingleQuoteLiteral.getText().slice(textSingleQuoteLiteral.OPERATOR_OPEN_TEXT_SINGLE().getText().length, -textSingleQuoteLiteral.OPERATOR_CLOSE_TEXT_SINGLE().getText().length)
        if (this.raw === '') this.raw = textSingleQuoteLiteral.getText()
      } else if (textTripleQuoteLiteral !== null) {
        this.isMultiline = true
        this.value = textTripleQuoteLiteral.getText().slice(textTripleQuoteLiteral.OPERATOR_OPEN_TEXT_TRIPLE().getText().length, -textTripleQuoteLiteral.OPERATOR_CLOSE_TEXT_TRIPLE().getText().length)
        if (this.raw === '') this.raw = textTripleQuoteLiteral.getText()
      }
    }
  }
}

class IntegerLiteral extends Node {
  kind = Kind.IntegerLiteral
  value: number
  raw: string

  constructor (parent: Node, integerLiteral: IntegerContext) {
    super(parent, integerLiteral.start, integerLiteral.stop)

    this.raw = integerLiteral.LITERAL_INTEGER().getText()
    if (integerLiteral.OPERATOR_MINUS() !== null) {
      this.raw = `-${this.raw}`
    }

    this.value = Number(this.raw)
  }
}

class RealLiteral extends Node {
  kind = Kind.RealLiteral
  value: number
  raw: string

  constructor (parent: Node, realLiteral: RealContext) {
    super(parent, realLiteral.start, realLiteral.stop)

    this.raw = realLiteral.LITERAL_REAL().getText()
    if (realLiteral.OPERATOR_MINUS() !== null) {
      this.raw = `-${this.raw}`
    }

    this.value = Number(this.raw)
  }
}

class BooleanLiteral extends Node {
  kind = Kind.BooleanLiteral
  value: boolean

  constructor (parent: Node, booleanLiteral: BooleanContext) {
    super(parent, booleanLiteral.start, booleanLiteral.stop)

    if (booleanLiteral.LITERAL_BOOLEAN().getText() === 'True') {
      this.value = true
    } else {
      this.value = false
    }
  }
}

class NullLiteral extends Node {
  kind = Kind.NullLiteral

  constructor (parent: Node, nullLiteral: LiteralContext) {
    super(parent, nullLiteral.start, nullLiteral.stop)
  }
}

class NullIdLiteral extends Node {
  kind = Kind.NullIdLiteral

  constructor (parent: Node, nullIdLiteral: LiteralContext) {
    super(parent, nullIdLiteral.start, nullIdLiteral.stop)
  }
}

class EnumLiteral extends Node {
  kind = Kind.EnumLiteral
  class?: ClassType | Identifier
  name: Identifier
  value: Identifier

  constructor (parent: Node, enumLiteral: EnumContext) {
    super(parent, enumLiteral.start, enumLiteral.stop)

    const name = enumLiteral.enumLiteral().enumName().IDENTIFIER().symbol
    const value = enumLiteral.enumLiteral().enumValue().IDENTIFIER().symbol
    const classClass = enumLiteral.enumLiteral().className()?.CLASS().symbol
    const classLib = enumLiteral.enumLiteral().namespaceName()?.IDENTIFIER().symbol

    if (classClass !== undefined) {
      this.class = new ClassType(this, classClass)
    } else if (classLib !== undefined) {
      this.class = new Identifier(this, classLib)
    }
    this.name = new Identifier(this, name)
    this.value = new Identifier(this, value)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.class?.visit(enterCb, exitCb)
    this.name.visit(enterCb, exitCb)
    this.value.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

function createLiteralNode (parent: Node, literal: LiteralContext): Literal {
  try {
    if (literal instanceof TextContext) {
      return new TextLiteral(parent, literal.textLiteral())
    } else if (literal instanceof IntegerContext) {
      return new IntegerLiteral(parent, literal)
    } else if (literal instanceof RealContext) {
      return new RealLiteral(parent, literal)
    } else if (literal instanceof BooleanContext) {
      return new BooleanLiteral(parent, literal)
    } else if (literal instanceof NullContext) {
      return new NullLiteral(parent, literal)
    } else if (literal instanceof NullIdContext) {
      return new NullIdLiteral(parent, literal)
    } else if (literal instanceof EnumContext) {
      return new EnumLiteral(parent, literal)
    } else {
      return new InvalidLiteral(parent, literal.start, literal.stop)
    }
  } catch (error) {
    if (error instanceof Error) {
      return new InvalidLiteral(parent, literal.start, literal.stop, error)
    } else {
      return new InvalidLiteral(parent, literal.start, literal.stop, new ASTBuildError('Failed to build `Literal` node. Something went wrong while reading `LiteralContext`.'))
    }
  }
}

class Identifier extends Node {
  kind: Kind
  namespace?: string
  name: string
  isExpression: boolean

  constructor (parent: Node, identifier: Token, namespace?: Token | null, isExpression?: boolean) {
    if (namespace !== undefined && namespace !== null) {
      super(parent, namespace, identifier)
      this.namespace = namespace.text
    } else {
      super(parent, identifier)
    }

    this.kind = Kind.Identifier
    this.name = identifier.text ?? ''
    this.isExpression = isExpression ?? false
  }
}

class InvalidIdentifier extends Node {
  kind = Kind.InvalidIdentifier
}

type Directive = (
  RequireContextDirective |
  ExtendsDirective |
  IncludeDirective |
  SettingDirective |
  CommandDirective |
  StructDirective |
  ConstDirective |
  InvalidDirective
)

function isDirective (node: Node): node is Directive {
  return (
    node.kind === Kind.RequireContextDirective ||
    node.kind === Kind.ExtendsDirective ||
    node.kind === Kind.IncludeDirective ||
    node.kind === Kind.SettingDirective ||
    node.kind === Kind.CommandDirective ||
    node.kind === Kind.StructDirective ||
    node.kind === Kind.ConstDirective
  )
}

class InvalidDirective extends Node {
  kind = Kind.InvalidDirective
  error: Error | null

  constructor (parent: Node | undefined, start: Token | null, stop?: Token | null, error?: Error | null) {
    super(parent, start, stop)

    if (error instanceof Error) {
      this.error = error
    } else {
      this.error = null
    }
  }
}

class RequireContextDirective extends Node {
  kind = Kind.RequireContextDirective
  class: ClassType

  constructor (parent: Node, requireContextDirective: RequireContextDirectiveContext) {
    super(parent, requireContextDirective.start, requireContextDirective.stop)
    this.class = new ClassType(this, requireContextDirective.className().CLASS().symbol)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.class.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class ExtendsDirective extends Node {
  kind = Kind.ExtendsDirective
  path: TextLiteral

  constructor (parent: Node, extendsDirective: ExtendsDirectiveContext) {
    super(parent, extendsDirective.start, extendsDirective.stop)
    this.path = new TextLiteral(this, extendsDirective.extendsPath().textLiteral())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.path.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class IncludeDirective extends Node {
  kind = Kind.IncludeDirective
  path: TextLiteral
  alias?: Identifier

  constructor (parent: Node, includeDirective: IncludeDirectiveContext) {
    super(parent, includeDirective.start, includeDirective.stop)
    this.path = new TextLiteral(this, includeDirective.includePath().textLiteral())
    const identifier = includeDirective.includeAlias()?.namespaceDefinition().IDENTIFIER().symbol
    if (identifier !== undefined) {
      this.alias = new Identifier(this, identifier)
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.path.visit(enterCb, exitCb)
    this.alias?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class InvalidSettingValue extends Node {
  kind = Kind.InvalidSettingValue
}

class SettingDirective extends Node {
  kind = Kind.SettingDirective
  name: Identifier
  value: Literal | VectorExpression | InvalidSettingValue
  description?: TextLiteral

  constructor (parent: Node, settingDirective: SettingDirectiveContext) {
    super(parent, settingDirective.start, settingDirective.stop)
    this.name = new Identifier(this, settingDirective.settingName().IDENTIFIER().symbol)
    const literal = settingDirective.literal()
    const vector = settingDirective.vector()
    if (literal !== null) {
      this.value = createLiteralNode(this, literal)
    } else if (vector !== null) {
      this.value = new VectorExpression(this, vector)
    } else {
      this.value = new InvalidSettingValue(this, settingDirective.start, settingDirective.stop)
    }
    const descriptionAlias = settingDirective.textAlias()?._alias
    if (descriptionAlias !== undefined) {
      this.description = new TextLiteral(this, descriptionAlias)
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    this.value.visit(enterCb, exitCb)
    this.description?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class CommandDirective extends Node {
  kind = Kind.CommandDirective
  name: Identifier
  type: Type
  description?: TextLiteral

  constructor (parent: Node, commandDirective: CommandDirectiveContext) {
    super(parent, commandDirective.start, commandDirective.stop)
    this.name = new Identifier(this, commandDirective.commandName().IDENTIFIER().symbol)
    this.type = createTypeNode(this, commandDirective.type())
    const alias = commandDirective.textAlias()?._alias
    if (alias !== undefined) {
      this.description = new TextLiteral(this, alias)
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    this.type.visit(enterCb, exitCb)
    this.description?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class StructMemberDeclaration extends Node {
  kind = Kind.StructMemberDeclaration
  type: Type
  name: Identifier

  constructor (parent: Node, structMember: StructMemberContext) {
    super(parent, structMember.start, structMember.stop)
    this.type = createTypeNode(this, structMember.type())
    this.name = new Identifier(this, structMember.IDENTIFIER().symbol)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.type.visit(enterCb, exitCb)
    this.name.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class StructDeclaration extends Node {
  kind = Kind.StructDeclaration
  name: Identifier
  members: StructMemberDeclaration[]

  constructor (parent: Node, structDeclaration: StructDeclarationContext) {
    super(parent, structDeclaration.start, structDeclaration.stop)
    this.name = new Identifier(this, structDeclaration.IDENTIFIER().symbol)
    this.members = []
    const structMembers = structDeclaration.structMembers()?.structMember()
    if (structMembers !== undefined) {
      for (const structMember of structMembers) {
        this.members.push(new StructMemberDeclaration(this, structMember))
      }
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    for (const member of this.members) {
      member.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class StructAliasing extends Node {
  kind = Kind.StructAliasing
  name: Identifier
  alias: Identifier

  constructor (parent: Node, structAliasing: StructAliasingContext) {
    super(parent, structAliasing.start, structAliasing.stop)
    const structName = structAliasing.structName().IDENTIFIER().symbol
    const structNamespace = structAliasing.namespaceName().IDENTIFIER().symbol
    const structAlias = structAliasing.identifierAlias().IDENTIFIER().symbol
    this.name = new Identifier(this, structName, structNamespace)
    this.alias = new Identifier(this, structAlias)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    this.alias.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class StructDirective extends Node {
  kind = Kind.StructDirective
  declaration?: StructDeclaration
  aliasing?: StructAliasing

  constructor (parent: Node, structDirective: StructDirectiveContext) {
    super(parent, structDirective.start, structDirective.stop)
    const structDeclaration = structDirective.structDeclaration()
    const structAliasing = structDirective.structAliasing()
    if (structDeclaration !== null) {
      this.declaration = new StructDeclaration(this, structDeclaration)
    }
    if (structAliasing !== null) {
      this.aliasing = new StructAliasing(this, structAliasing)
    }
    if (this.declaration === undefined && this.aliasing === undefined) {
      throw new ASTBuildError('Failed to build `StructDirective` node. `declaration` and `aliasing` cannot both be `undefined`.')
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.declaration?.visit(enterCb, exitCb)
    this.aliasing?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class InvalidConstValue extends Node {
  kind = Kind.InvalidConstValue
}

class ConstDeclaration extends Node {
  kind = Kind.ConstDeclaration
  name: Identifier
  value: Literal | VectorExpression | ArrayExpression | StructExpression | InvalidConstValue

  constructor (parent: Node, constDeclaration: ConstDeclarationContext) {
    super(parent, constDeclaration.start, constDeclaration.stop)
    this.name = new Identifier(this, constDeclaration.IDENTIFIER().symbol)

    const literal = constDeclaration.literal()
    const vector = constDeclaration.vector()
    const array = constDeclaration.array()
    const structure = constDeclaration.structure()

    if (literal !== null) {
      this.value = createLiteralNode(this, literal)
    } else if (vector !== null) {
      this.value = new VectorExpression(this, vector)
    } else if (array !== null) {
      this.value = new ArrayExpression(this, array)
    } else if (structure !== null) {
      this.value = new StructExpression(this, structure)
    } else {
      this.value = new InvalidConstValue(this, constDeclaration.start, constDeclaration.stop)
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    this.value.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class ConstAliasing extends Node {
  kind = Kind.ConstAliasing
  name: Identifier
  alias: Identifier

  constructor (parent: Node, constAliasing: ConstAliasingContext) {
    super(parent, constAliasing.start, constAliasing.stop)
    const constName = constAliasing.constName().IDENTIFIER().symbol
    const constNamespace = constAliasing.namespaceName().IDENTIFIER().symbol
    const constAlias = constAliasing.identifierAlias().IDENTIFIER().symbol
    this.name = new Identifier(this, constName, constNamespace)
    this.alias = new Identifier(this, constAlias)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    this.alias.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class ConstDirective extends Node {
  kind = Kind.ConstDirective
  declaration?: ConstDeclaration
  aliasing?: ConstAliasing

  constructor (parent: Node, constDirective: ConstDirectiveContext) {
    super(parent, constDirective.start, constDirective.stop)
    const constDeclaration = constDirective.constDeclaration()
    const constAliasing = constDirective.constAliasing()
    if (constDeclaration !== null) {
      this.declaration = new ConstDeclaration(this, constDeclaration)
    }
    if (constAliasing !== null) {
      this.aliasing = new ConstAliasing(this, constAliasing)
    }
    if (this.declaration === undefined && this.aliasing === undefined) {
      throw new ASTBuildError('Failed to build `ConstDirective` node. `declaration` and `aliasing` cannot both be `undefined`.')
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.declaration?.visit(enterCb, exitCb)
    this.aliasing?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

type Declaration = (
  VariableDeclaration |
  FunctionDeclaration |
  LabelDeclaration |
  InvalidDeclaration
)

function isDeclaration (node: Node): node is Declaration {
  return (
    node.kind === Kind.VariableDeclaration ||
    node.kind === Kind.FunctionDeclaration ||
    node.kind === Kind.LabelDeclaration
  )
}

enum Storage {
  cloud = 'cloud',
  metadata = 'metadata',
  netread = 'netread',
  netwrite = 'netwrite',
  persistent = 'persistent',
  invalid = 'invalid'
}

enum InitializerSign {
  '=' = '=',
  '<=>' = '<=>',
  invalid = 'invalid'
}

function toStorageEnum (storage: StorageSpecifierContext): Storage {
  if (storage.STORAGESPECIFIER_CLOUD() !== null) {
    return Storage.cloud
  } else if (storage.STORAGESPECIFIER_METADATA() !== null) {
    return Storage.metadata
  } else if (storage.STORAGESPECIFIER_NETREAD() !== null) {
    return Storage.netread
  } else if (storage.STORAGESPECIFIER_NETWRITE() !== null) {
    return Storage.netwrite
  } else if (storage.STORAGESPECIFIER_PERSISTENT() !== null) {
    return Storage.persistent
  } else {
    return Storage.invalid
  }
}

function toInitializerSign (sign: Token): InitializerSign {
  if (sign.type === ManiaScriptParser.OPERATOR_ASSIGN) {
    return InitializerSign['=']
  } else if (sign.type === ManiaScriptParser.OPERATOR_ARROWASSIGN) {
    return InitializerSign['<=>']
  } else {
    return InitializerSign.invalid
  }
}

class InvalidDeclaration extends Node {
  kind = Kind.InvalidDeclaration
  error: Error | null

  constructor (parent: Node | undefined, start: Token | null, stop?: Token | null, error?: Error | null) {
    super(parent, start, stop)

    if (error instanceof Error) {
      this.error = error
    } else {
      this.error = null
    }
  }
}

class VariableDeclaration extends Node {
  kind = Kind.VariableDeclaration
  isGlobal: boolean
  isLet: boolean
  storage?: Storage
  type?: Type
  name: Identifier
  alias?: Identifier
  forTarget?: Expression
  initializerSign?: InitializerSign
  initializerExpression?: Expression
  initializerType?: InitializerType

  constructor (parent: Node, variableDeclaration: VariableDeclarationWithTypeContext | VariableDeclarationWithoutTypeContext | VariableDeclarationLetContext, isGlobal: boolean) {
    super(parent, variableDeclaration.start, variableDeclaration.stop)

    this.isGlobal = isGlobal
    this.isLet = variableDeclaration instanceof VariableDeclarationLetContext

    if (variableDeclaration instanceof VariableDeclarationWithTypeContext) {
      const storageSpecifier = variableDeclaration.storageSpecifier()
      if (storageSpecifier !== null) {
        this.storage = toStorageEnum(storageSpecifier)
      }

      this.type = createTypeNode(this, variableDeclaration.type())
      this.name = new Identifier(this, variableDeclaration.variableName().IDENTIFIER().symbol)

      const identifierAlias = variableDeclaration.identifierAlias()
      if (identifierAlias !== null) {
        this.alias = new Identifier(this, identifierAlias.IDENTIFIER().symbol)
      }

      if (variableDeclaration._forTarget !== undefined) {
        this.forTarget = createExpressionNode(this, variableDeclaration._forTarget)
      }

      const initializer = variableDeclaration.initializer()
      if (initializer !== null) {
        if (isNullish(initializer._sign)) {
          throw new ASTBuildError('Failed to build `VariableDeclaration` node. Missing initializer sign.')
        }
        this.initializerSign = toInitializerSign(initializer._sign)
        const initializerExpression = initializer.expression()
        const initializerType = initializer.initializerType()
        if (initializerExpression !== null) {
          this.initializerExpression = createExpressionNode(this, initializerExpression)
        } else if (initializerType !== null) {
          this.initializerType = createInitializerTypeNode(this, initializerType)
        }
      }
    } else if (variableDeclaration instanceof VariableDeclarationWithoutTypeContext) {
      const storageSpecifier = variableDeclaration.storageSpecifier()
      if (storageSpecifier !== null) {
        this.storage = toStorageEnum(storageSpecifier)
      }

      this.name = new Identifier(this, variableDeclaration.variableName().IDENTIFIER().symbol)

      const identifierAlias = variableDeclaration.identifierAlias()
      if (identifierAlias !== null) {
        this.alias = new Identifier(this, identifierAlias.IDENTIFIER().symbol)
      }

      if (variableDeclaration._forTarget !== undefined) {
        this.forTarget = createExpressionNode(this, variableDeclaration._forTarget)
      }

      const initializer = variableDeclaration.initializer()
      if (isNullish(initializer._sign)) {
        throw new ASTBuildError('Failed to build `VariableDeclaration` node. Missing initializer sign.')
      }
      this.initializerSign = toInitializerSign(initializer._sign)
      const initializerExpression = initializer.expression()
      const initializerType = initializer.initializerType()
      if (initializerExpression !== null) {
        this.initializerExpression = createExpressionNode(this, initializerExpression)
      } else if (initializerType !== null) {
        this.initializerType = createInitializerTypeNode(this, initializerType)
      }
    } else {
      this.name = new Identifier(this, variableDeclaration.IDENTIFIER().symbol)
      this.initializerSign = toInitializerSign(variableDeclaration.OPERATOR_ASSIGN().symbol)
      const initializerExpression = variableDeclaration.expression()
      const intializerArrayType = variableDeclaration.arrayType()
      if (initializerExpression !== null) {
        this.initializerExpression = createExpressionNode(this, initializerExpression)
      } else if (intializerArrayType !== null) {
        this.initializerType = new ArrayType(this, intializerArrayType)
      }
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.type?.visit(enterCb, exitCb)
    this.name.visit(enterCb, exitCb)
    this.alias?.visit(enterCb, exitCb)
    this.forTarget?.visit(enterCb, exitCb)
    this.initializerExpression?.visit(enterCb, exitCb)
    this.initializerType?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class FunctionParameterDeclaration extends Node {
  kind = Kind.FunctionParameterDeclaration
  type: Type
  name: Identifier

  constructor (parent: Node, parameter: ParameterContext) {
    super(parent, parameter.start, parameter.stop)
    this.type = createTypeNode(this, parameter.type())
    this.name = new Identifier(this, parameter.IDENTIFIER().symbol)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.type.visit(enterCb, exitCb)
    this.name.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class FunctionDeclaration extends Node {
  kind = Kind.FunctionDeclaration
  type: Type
  name: Identifier
  parameters: FunctionParameterDeclaration[]
  body: BlockStatement

  constructor (parent: Node, functionDeclaration: FunctionDeclarationContext) {
    super(parent, functionDeclaration.start, functionDeclaration.stop)

    this.type = createTypeNode(this, functionDeclaration.type())
    this.name = new Identifier(this, functionDeclaration.functionName().IDENTIFIER().symbol)
    this.parameters = []
    const parameters = functionDeclaration.parameters()
    if (parameters !== null) {
      for (const parameter of parameters.parameter()) {
        this.parameters.push(new FunctionParameterDeclaration(this, parameter))
      }
    }
    this.body = new BlockStatement(this, functionDeclaration.block())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.type.visit(enterCb, exitCb)
    this.name.visit(enterCb, exitCb)
    for (const parameter of this.parameters) {
      parameter.visit(enterCb, exitCb)
    }
    this.body.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class LabelDeclaration extends Node {
  kind = Kind.LabelDeclaration
  name: Identifier
  body: Statement[]

  constructor (parent: Node, labelDeclaration: LabelDeclarationContext) {
    super(parent, labelDeclaration.start, labelDeclaration.stop)
    this.name = new Identifier(this, labelDeclaration.labelName().IDENTIFIER().symbol)
    this.body = []
    for (const statement of labelDeclaration.statements().statement()) {
      this.body.push(createStatementNode(this, statement))
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    for (const statement of this.body) {
      statement.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

type Statement = (
  BlockStatement |
  InvalidStatement |
  VariableDeclaration |
  ExpressionStatement |
  AssignmentStatement |
  ReturnStatement |
  LabelStatement |
  AssertStatement |
  ForeachStatement |
  ForStatement |
  WhileStatement |
  MeanwhileStatement |
  BreakStatement |
  ContinueStatement |
  SwitchStatement |
  SwitchtypeStatement |
  ConditionalStatement |
  ConditionalBranch |
  LogStatement |
  SleepStatement |
  TuningstartStatement |
  TuningendStatement |
  TuningmarkStatement |
  WaitStatement |
  YieldStatement
)

function isStatement (node: Node): node is Statement {
  return (
    node.kind === Kind.BlockStatement ||
    node.kind === Kind.VariableDeclaration ||
    node.kind === Kind.ExpressionStatement ||
    node.kind === Kind.AssignmentStatement ||
    node.kind === Kind.ReturnStatement ||
    node.kind === Kind.LabelStatement ||
    node.kind === Kind.AssertStatement ||
    node.kind === Kind.ForeachStatement ||
    node.kind === Kind.ForStatement ||
    node.kind === Kind.WhileStatement ||
    node.kind === Kind.MeanwhileStatement ||
    node.kind === Kind.BreakStatement ||
    node.kind === Kind.ContinueStatement ||
    node.kind === Kind.SwitchStatement ||
    node.kind === Kind.SwitchtypeStatement ||
    node.kind === Kind.ConditionalStatement ||
    node.kind === Kind.ConditionalBranch ||
    node.kind === Kind.LogStatement ||
    node.kind === Kind.SleepStatement ||
    node.kind === Kind.TuningstartStatement ||
    node.kind === Kind.TuningendStatement ||
    node.kind === Kind.TuningmarkStatement ||
    node.kind === Kind.WaitStatement ||
    node.kind === Kind.YieldStatement
  )
}

class BlockStatement extends Node {
  kind = Kind.BlockStatement
  body: Statement[]

  constructor (parent: Node, block: BlockContext) {
    super(parent, block.start, block.stop)
    this.body = []
    const statements = block.statements()?.statement()
    if (statements !== undefined) {
      for (const statement of statements) {
        this.body.push(createStatementNode(this, statement))
      }
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    for (const statement of this.body) {
      statement.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class InvalidStatement extends Node {
  kind = Kind.InvalidStatement
  error: Error | null

  constructor (parent: Node | undefined, start: Token | null, stop?: Token | null, error?: Error | null) {
    super(parent, start, stop)
    if (error instanceof Error) {
      this.error = error
    } else {
      this.error = null
    }
  }
}

class ExpressionStatement extends Node {
  kind = Kind.ExpressionStatement
  expression: Expression

  constructor (parent: Node, expressionStatement: ExpressionStatementContext) {
    super(parent, expressionStatement.start, expressionStatement.stop)
    this.expression = createExpressionNode(this, expressionStatement.expression())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.expression.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

enum AssignmentOperator {
  '=' = '=',
  '<=>' = '<=>',
  '*=' = '*=',
  '/=' = '/=',
  '+=' = '+=',
  '-=' = '-=',
  '%=' = '%=',
  '^=' = '^='
}

class AssignmentStatement extends Node {
  kind = Kind.AssignmentStatement
  left: Expression
  right: Expression | InitializerType
  operator: AssignmentOperator
  isInitializedByType: boolean

  constructor (parent: Node, assignmentStatement: AssignmentStatementContext) {
    super(parent, assignmentStatement.start, assignmentStatement.stop)

    if (assignmentStatement._left === undefined) {
      throw new ASTBuildError('Failed to build `AssignmentStatement` node. Missing left part of the assignment.')
    }
    this.left = createExpressionNode(this, assignmentStatement._left)
    const initializerType = assignmentStatement.initializerType()
    if (initializerType !== null) {
      this.right = createInitializerTypeNode(this, initializerType)
      this.isInitializedByType = true
    } else {
      if (assignmentStatement._right === undefined) {
        throw new ASTBuildError('Failed to build `AssignmentStatement` node. Missing right part of the assignment.')
      }
      this.right = createExpressionNode(this, assignmentStatement._right)
      this.isInitializedByType = false
    }
    const assignmentOperator = assignmentStatement.assignmentOperator()
    if (assignmentOperator !== null) {
      if (assignmentOperator.OPERATOR_ARROWASSIGN() !== null) {
        this.operator = AssignmentOperator['<=>']
      } else if (assignmentOperator.OPERATOR_CONCATASSIGN() !== null) {
        this.operator = AssignmentOperator['^=']
      } else if (assignmentOperator.OPERATOR_DIVIDEASSIGN() !== null) {
        this.operator = AssignmentOperator['/=']
      } else if (assignmentOperator.OPERATOR_MINUSASSIGN() !== null) {
        this.operator = AssignmentOperator['-=']
      } else if (assignmentOperator.OPERATOR_MODULOASSIGN() !== null) {
        this.operator = AssignmentOperator['%=']
      } else if (assignmentOperator.OPERATOR_MULTIPLYASSIGN() !== null) {
        this.operator = AssignmentOperator['*=']
      } else if (assignmentOperator.OPERATOR_PLUSASSIGN() !== null) {
        this.operator = AssignmentOperator['+=']
      } else {
        this.operator = AssignmentOperator['=']
      }
    } else if (assignmentStatement.OPERATOR_ARROWASSIGN() !== null) {
      this.operator = AssignmentOperator['<=>']
    } else {
      this.operator = AssignmentOperator['=']
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.left.visit(enterCb, exitCb)
    this.right.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class ReturnStatement extends Node {
  kind = Kind.ReturnStatement
  argument?: Expression | InitializerType
  isReturningType: boolean

  constructor (parent: Node, returnStatement: ReturnStatementContext) {
    super(parent, returnStatement.start, returnStatement.stop)

    this.isReturningType = false

    const initializerType = returnStatement.initializerType()
    const returnExpression = returnStatement.expression()
    if (initializerType !== null) {
      this.argument = createInitializerTypeNode(this, initializerType)
      this.isReturningType = true
    } else if (returnExpression !== null) {
      this.argument = createExpressionNode(this, returnExpression)
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.argument?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class LabelStatement extends Node {
  kind = Kind.LabelStatement
  isOverwrite: boolean
  name: Identifier | InvalidIdentifier

  constructor (parent: Node, labelStatement: LabelStatementContext) {
    super(parent, labelStatement.start, labelStatement.stop)

    const labelInsert = labelStatement.labelInsert()
    const labelOverwrite = labelStatement.labelOverwrite()
    if (labelInsert !== null) {
      this.isOverwrite = false
      this.name = new Identifier(this, labelInsert.IDENTIFIER().symbol)
    } else if (labelOverwrite !== null) {
      this.isOverwrite = true
      this.name = new Identifier(this, labelOverwrite.IDENTIFIER().symbol)
    } else {
      this.isOverwrite = false
      this.name = new InvalidIdentifier(this, labelStatement.start, labelStatement.stop)
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class AssertStatement extends Node {
  kind = Kind.AssertStatement
  test: Expression
  message?: Expression

  constructor (parent: Node, assertStatement: AssertStatementContext) {
    super(parent, assertStatement.start, assertStatement.stop)

    if (assertStatement._test === undefined) {
      throw new ASTBuildError('Failed to build `AssertStatement` node. Missing assert test.')
    }
    this.test = createExpressionNode(this, assertStatement._test)
    if (assertStatement._message !== undefined) {
      this.message = createExpressionNode(this, assertStatement._message)
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.test.visit(enterCb, exitCb)
    this.message?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class ForeachStatement extends Node {
  kind = Kind.ForeachStatement
  key?: Identifier
  value: Identifier
  expression: Expression
  body: Statement

  constructor (parent: Node, foreachStatement: ForeachStatementContext) {
    super(parent, foreachStatement.start, foreachStatement.stop)
    if (!isNullish(foreachStatement._key)) {
      this.key = new Identifier(this, foreachStatement._key)
    }
    if (isNullish(foreachStatement._value)) {
      throw new ASTBuildError('Failed to build `ForeachStatement` node. Missing loop output value.')
    }
    this.value = new Identifier(this, foreachStatement._value)
    if (foreachStatement._source === undefined) {
      throw new ASTBuildError('Failed to build `ForeachStatement` node. Missing loop input expression.')
    }
    this.expression = createExpressionNode(this, foreachStatement._source)
    this.body = createStatementNode(this, foreachStatement.statement())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.key?.visit(enterCb, exitCb)
    this.value.visit(enterCb, exitCb)
    this.expression.visit(enterCb, exitCb)
    this.body.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class ForStatement extends Node {
  kind = Kind.ForStatement
  value: Identifier
  loopStart: Expression
  loopStop: Expression
  increment?: Expression
  body: Statement

  constructor (parent: Node, forStatement: ForStatementContext) {
    super(parent, forStatement.start, forStatement.stop)
    if (isNullish(forStatement._value)) {
      throw new ASTBuildError('Failed to build `ForStatement` node. Missing loop value.')
    }
    this.value = new Identifier(this, forStatement._value)
    if (forStatement._loopStart === undefined) {
      throw new ASTBuildError('Failed to build `ForStatement` node. Missing loop start value.')
    }
    this.loopStart = createExpressionNode(this, forStatement._loopStart)
    if (forStatement._loopStop === undefined) {
      throw new ASTBuildError('Failed to build `ForStatement` node. Missing loop stop value.')
    }
    this.loopStop = createExpressionNode(this, forStatement._loopStop)
    if (forStatement._increment !== undefined) {
      this.increment = createExpressionNode(this, forStatement._increment)
    }
    this.body = createStatementNode(this, forStatement.statement())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.value.visit(enterCb, exitCb)
    this.loopStart.visit(enterCb, exitCb)
    this.loopStop.visit(enterCb, exitCb)
    this.increment?.visit(enterCb, exitCb)
    this.body.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class WhileStatement extends Node {
  kind = Kind.WhileStatement
  test: Expression
  body: Statement

  constructor (parent: Node, whileStatement: WhileStatementContext) {
    super(parent, whileStatement.start, whileStatement.stop)
    this.test = createExpressionNode(this, whileStatement.expression())
    this.body = createStatementNode(this, whileStatement.statement())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.test.visit(enterCb, exitCb)
    this.body.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class MeanwhileStatement extends Node {
  kind = Kind.MeanwhileStatement
  test: Expression
  body: Statement

  constructor (parent: Node, meanwhileStatement: MeanwhileStatementContext) {
    super(parent, meanwhileStatement.start, meanwhileStatement.stop)
    this.test = createExpressionNode(this, meanwhileStatement.expression())
    this.body = createStatementNode(this, meanwhileStatement.statement())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.test.visit(enterCb, exitCb)
    this.body.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class BreakStatement extends Node {
  kind = Kind.BreakStatement

  constructor (parent: Node, breakStatement: BreakStatementContext) {
    super(parent, breakStatement.start, breakStatement.stop)
  }
}

class ContinueStatement extends Node {
  kind = Kind.ContinueStatement

  constructor (parent: Node, continueStatement: ContinueStatementContext) {
    super(parent, continueStatement.start, continueStatement.stop)
  }
}

class SwitchCase extends Node {
  kind = Kind.SwitchCase
  tests: Expression[]
  consequent: Statement

  constructor (parent: Node, switchCase: SwitchCaseContext | SwitchDefaultContext) {
    super(parent, switchCase.start, switchCase.stop)

    this.tests = []
    if (switchCase instanceof SwitchCaseContext) {
      for (const test of switchCase._tests) {
        this.tests.push(createExpressionNode(this, test))
      }
    }

    this.consequent = createStatementNode(this, switchCase.statement())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    for (const test of this.tests) {
      test.visit(enterCb, exitCb)
    }
    this.consequent.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class SwitchStatement extends Node {
  kind = Kind.SwitchStatement
  discriminant: Expression
  cases: SwitchCase[]

  constructor (parent: Node, switchStatement: SwitchStatementContext) {
    super(parent, switchStatement.start, switchStatement.stop)
    if (switchStatement._discriminant === undefined) {
      throw new ASTBuildError('Failed to build `SwitchStatement` node. Missing switch discriminant.')
    }
    this.discriminant = createExpressionNode(this, switchStatement._discriminant)
    this.cases = []
    for (const switchClause of switchStatement.switchClauseList().switchCase()) {
      this.cases.push(new SwitchCase(this, switchClause))
    }
    for (const switchClause of switchStatement.switchClauseList().switchDefault()) {
      this.cases.push(new SwitchCase(this, switchClause))
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.discriminant.visit(enterCb, exitCb)
    for (const switchCase of this.cases) {
      switchCase.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class SwitchtypeCase extends Node {
  kind = Kind.SwitchtypeCase
  tests: ClassType[]
  consequent: Statement

  constructor (parent: Node, switchtypeCase: SwitchtypeCaseContext | SwitchtypeDefaultContext) {
    super(parent, switchtypeCase.start, switchtypeCase.stop)

    this.tests = []
    if (switchtypeCase instanceof SwitchtypeCaseContext) {
      for (const caseClass of switchtypeCase.className()) {
        this.tests.push(new ClassType(this, caseClass.CLASS().symbol))
      }
    }

    this.consequent = createStatementNode(this, switchtypeCase.statement())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    for (const test of this.tests) {
      test.visit(enterCb, exitCb)
    }
    this.consequent.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class SwitchtypeStatement extends Node {
  kind = Kind.SwitchtypeStatement
  discriminant: Expression
  alias?: Identifier
  cases: SwitchtypeCase[]

  constructor (parent: Node, switchtypeStatement: SwitchtypeStatementContext) {
    super(parent, switchtypeStatement.start, switchtypeStatement.stop)
    if (switchtypeStatement._discriminant === undefined) {
      throw new ASTBuildError('Failed to build `SwitchtypeStatement` node. Missing switchtype discriminant.')
    }
    this.discriminant = createExpressionNode(this, switchtypeStatement._discriminant)
    const discriminantAlias = switchtypeStatement.identifierAlias()?.IDENTIFIER().symbol
    if (discriminantAlias !== undefined) {
      this.alias = new Identifier(this, discriminantAlias)
    }
    this.cases = []
    for (const switchtypeClause of switchtypeStatement.switchtypeClauseList().switchtypeCase()) {
      this.cases.push(new SwitchtypeCase(this, switchtypeClause))
    }
    for (const switchClause of switchtypeStatement.switchtypeClauseList().switchtypeDefault()) {
      this.cases.push(new SwitchtypeCase(this, switchClause))
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.discriminant.visit(enterCb, exitCb)
    for (const switchCase of this.cases) {
      switchCase.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class ConditionalBranch extends Node {
  kind = Kind.ConditionalBranch
  test?: Expression
  consequent: Statement

  constructor (parent: Node, branch: IfBranchContext | ElseIfBranchContext | ElseBranchContext) {
    super(parent, branch.start, branch.stop)

    if (branch instanceof IfBranchContext || branch instanceof ElseIfBranchContext) {
      if (branch._test === undefined) {
        throw new ASTBuildError('Failed to build `ConditinalBranch` node. Missing branch test.')
      }
      this.test = createExpressionNode(this, branch._test)
    }
    if (branch._consequent === undefined) {
      throw new ASTBuildError('Failed to build `ConditionalBranch` node. Missing branch consequent.')
    }
    this.consequent = createStatementNode(this, branch._consequent)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.test?.visit(enterCb, exitCb)
    this.consequent.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class ConditionalStatement extends Node {
  kind = Kind.ConditionalStatement
  branches: ConditionalBranch[]

  constructor (parent: Node, conditionalStatement: ConditionalStatementContext) {
    super(parent, conditionalStatement.start, conditionalStatement.stop)
    this.branches = []
    this.branches.push(new ConditionalBranch(this, conditionalStatement.ifBranch()))
    for (const elseIfBranch of conditionalStatement.elseIfBranch()) {
      this.branches.push(new ConditionalBranch(this, elseIfBranch))
    }
    const elseBranch = conditionalStatement.elseBranch()
    if (elseBranch !== null) {
      this.branches.push(new ConditionalBranch(this, elseBranch))
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    for (const branch of this.branches) {
      branch.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class LogStatement extends Node {
  kind = Kind.LogStatement
  expression: Expression

  constructor (parent: Node, logStatement: LogStatementContext) {
    super(parent, logStatement.start, logStatement.stop)
    this.expression = createExpressionNode(this, logStatement.expression())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.expression.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class SleepStatement extends Node {
  kind = Kind.SleepStatement
  expression: Expression

  constructor (parent: Node, sleepStatement: SleepStatementContext) {
    super(parent, sleepStatement.start, sleepStatement.stop)
    this.expression = createExpressionNode(this, sleepStatement.expression())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.expression.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class TuningstartStatement extends Node {
  kind = Kind.TuningstartStatement

  constructor (parent: Node, tuningstartStatement: TuningstartStatementContext) {
    super(parent, tuningstartStatement.start, tuningstartStatement.stop)
  }
}

class TuningendStatement extends Node {
  kind = Kind.TuningendStatement

  constructor (parent: Node, tuningendStatement: TuningendStatementContext) {
    super(parent, tuningendStatement.start, tuningendStatement.stop)
  }
}

class TuningmarkStatement extends Node {
  kind = Kind.TuningmarkStatement
  label: TextLiteral

  constructor (parent: Node, tuningmarkStatement: TuningmarkStatementContext) {
    super(parent, tuningmarkStatement.start, tuningmarkStatement.stop)
    this.label = new TextLiteral(this, tuningmarkStatement.textLiteral())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.label.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class WaitStatement extends Node {
  kind = Kind.WaitStatement
  expression: Expression

  constructor (parent: Node, waitStatement: WaitStatementContext) {
    super(parent, waitStatement.start, waitStatement.stop)
    this.expression = createExpressionNode(this, waitStatement.expression())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.expression.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class YieldStatement extends Node {
  kind = Kind.YieldStatement

  constructor (parent: Node, yieldStatement: YieldStatementContext) {
    super(parent, yieldStatement.start, yieldStatement.stop)
  }
}

function createStatementNode (parent: Node, statement: StatementContext | BlockContext): Statement {
  try {
    if (statement instanceof BlockContext) {
      return new BlockStatement(parent, statement)
    } else {
      const blockStatement = statement.block()
      const variableDeclaration = statement.variableDeclaration()
      const expressionStatement = statement.expressionStatement()
      const assignmentStatement = statement.assignmentStatement()
      const returnStatement = statement.returnStatement()
      const labelStatement = statement.labelStatement()
      const assertStatement = statement.assertStatement()
      const foreachStatement = statement.foreachStatement()
      const forStatement = statement.forStatement()
      const whileStatement = statement.whileStatement()
      const meanwhileStatement = statement.meanwhileStatement()
      const breakStatement = statement.breakStatement()
      const continueStatement = statement.continueStatement()
      const switchStatement = statement.switchStatement()
      const switchtypeStatement = statement.switchtypeStatement()
      const conditionalStatement = statement.conditionalStatement()
      const logStatement = statement.logStatement()
      const sleepStatement = statement.sleepStatement()
      const tuningstartStatement = statement.tuningstartStatement()
      const tuningendStatement = statement.tuningendStatement()
      const tuningmarkStatement = statement.tuningmarkStatement()
      const waitStatement = statement.waitStatement()
      const yieldStatement = statement.yieldStatement()
      if (blockStatement !== null) {
        return new BlockStatement(parent, blockStatement)
      } else if (variableDeclaration !== null) {
        if (
          variableDeclaration instanceof VariableDeclarationWithTypeContext ||
          variableDeclaration instanceof VariableDeclarationWithoutTypeContext ||
          variableDeclaration instanceof VariableDeclarationLetContext
        ) {
          return new VariableDeclaration(parent, variableDeclaration, false)
        } else {
          return new InvalidStatement(parent, statement.start, statement.stop)
        }
      } else if (expressionStatement !== null) {
        return new ExpressionStatement(parent, expressionStatement)
      } else if (assignmentStatement !== null) {
        return new AssignmentStatement(parent, assignmentStatement)
      } else if (returnStatement !== null) {
        return new ReturnStatement(parent, returnStatement)
      } else if (labelStatement !== null) {
        return new LabelStatement(parent, labelStatement)
      } else if (assertStatement !== null) {
        return new AssertStatement(parent, assertStatement)
      } else if (foreachStatement !== null) {
        return new ForeachStatement(parent, foreachStatement)
      } else if (forStatement !== null) {
        return new ForStatement(parent, forStatement)
      } else if (whileStatement !== null) {
        return new WhileStatement(parent, whileStatement)
      } else if (meanwhileStatement !== null) {
        return new MeanwhileStatement(parent, meanwhileStatement)
      } else if (breakStatement !== null) {
        return new BreakStatement(parent, breakStatement)
      } else if (continueStatement !== null) {
        return new ContinueStatement(parent, continueStatement)
      } else if (switchStatement !== null) {
        return new SwitchStatement(parent, switchStatement)
      } else if (switchtypeStatement !== null) {
        return new SwitchtypeStatement(parent, switchtypeStatement)
      } else if (conditionalStatement !== null) {
        return new ConditionalStatement(parent, conditionalStatement)
      } else if (logStatement !== null) {
        return new LogStatement(parent, logStatement)
      } else if (sleepStatement !== null) {
        return new SleepStatement(parent, sleepStatement)
      } else if (tuningstartStatement !== null) {
        return new TuningstartStatement(parent, tuningstartStatement)
      } else if (tuningendStatement !== null) {
        return new TuningendStatement(parent, tuningendStatement)
      } else if (tuningmarkStatement !== null) {
        return new TuningmarkStatement(parent, tuningmarkStatement)
      } else if (waitStatement !== null) {
        return new WaitStatement(parent, waitStatement)
      } else if (yieldStatement !== null) {
        return new YieldStatement(parent, yieldStatement)
      } else {
        return new InvalidStatement(parent, statement.start, statement.stop)
      }
    }
  } catch (error) {
    if (error instanceof Error) {
      return new InvalidStatement(parent, statement.start, statement.stop, error)
    } else {
      return new InvalidStatement(parent, statement.start, statement.stop, new ASTBuildError('Failed to build `Statement` node. Something went wrong while reading `StatementContext` | `BlockContext`.'))
    }
  }
}

type Expression = (
  VectorExpression |
  ArrayExpression |
  StructExpression |
  Literal |
  AsExpression |
  IsExpression |
  DotAccessExpression |
  IndexAccessExpression |
  UnaryExpression |
  BinaryExpression |
  LogicalExpression |
  TemplateTextLiteral |
  DumptypeExpression |
  DumpExpression |
  CastExpression |
  Identifier |
  ThisExpression |
  NowExpression |
  InvalidExpression
)

function isExpression (node: Node): node is Expression {
  return (
    node.kind === Kind.VectorExpression ||
    node.kind === Kind.ArrayExpression ||
    node.kind === Kind.StructExpression ||
    node.kind === Kind.AsExpression ||
    node.kind === Kind.IsExpression ||
    node.kind === Kind.DotAccessExpression ||
    node.kind === Kind.IndexAccessExpression ||
    node.kind === Kind.UnaryExpression ||
    node.kind === Kind.BinaryExpression ||
    node.kind === Kind.LogicalExpression ||
    node.kind === Kind.TemplateTextLiteral ||
    node.kind === Kind.DumptypeExpression ||
    node.kind === Kind.DumpExpression ||
    node.kind === Kind.CastExpression ||
    (node.kind === Kind.Identifier && (node as Identifier).isExpression) ||
    node.kind === Kind.ThisExpression ||
    node.kind === Kind.NowExpression ||
    isLiteral(node)
  )
}

class InvalidExpression extends Node {
  kind = Kind.InvalidExpression
  error: Error | null

  constructor (parent: Node | undefined, start: Token | null, stop?: Token | null, error?: Error | null) {
    super(parent, start, stop)
    if (error instanceof Error) {
      this.error = error
    } else {
      this.error = null
    }
  }
}

class VectorExpression extends Node {
  kind = Kind.VectorExpression
  values: Expression[]

  constructor (parent: Node, vector: VectorContext) {
    super(parent, vector.start, vector.stop)
    this.values = []
    for (const expression of vector.expression()) {
      this.values.push(createExpressionNode(this, expression))
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    for (const expression of this.values) {
      expression.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class ArrayExpression extends Node {
  kind = Kind.ArrayExpression
  values: { key?: Expression, value: Expression }[]
  isAssociative: boolean

  constructor (parent: Node, array: ArrayContext | ArrayExpressionContext) {
    if (array instanceof ArrayExpressionContext) {
      array = array.array()
    }

    super(parent, array.start, array.stop)

    this.values = []
    this.isAssociative = false

    const associativeArray = array.associativeArray()
    const simpleArray = array.simpleArray()
    if (associativeArray !== null) {
      this.isAssociative = true
      for (let i = 0; i < associativeArray._value.length; i++) {
        this.values.push({
          key: createExpressionNode(this, associativeArray._key[i]),
          value: createExpressionNode(this, associativeArray._value[i])
        })
      }
    }
    if (simpleArray !== null) {
      for (const value of simpleArray._value) {
        this.values.push({
          value: createExpressionNode(this, value)
        })
      }
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    for (const value of this.values) {
      value.key?.visit(enterCb, exitCb)
      value.value.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class StructMemberExpression extends Node {
  kind = Kind.StructMemberExpression
  name: Identifier
  value: Expression

  constructor (parent: Node, structureMemberInitialization: StructureMemberInitializationContext) {
    super(parent, structureMemberInitialization.start, structureMemberInitialization.stop)

    this.name = new Identifier(this, structureMemberInitialization.structMemberName().IDENTIFIER().symbol)
    this.value = createExpressionNode(this, structureMemberInitialization.expression())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    this.value.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class StructExpression extends Node {
  kind = Kind.StructExpression
  name: Identifier
  members: StructMemberExpression[]

  constructor (parent: Node, structure: StructureContext | StructureExpressionContext) {
    if (structure instanceof StructureExpressionContext) {
      structure = structure.structure()
    }
    super(parent, structure.start, structure.stop)

    const structName = structure.structIdentifier().structName().IDENTIFIER().symbol
    this.name = new Identifier(this, structName, structure.structIdentifier().namespaceName()?.IDENTIFIER().symbol)
    this.members = []

    const structureMemberInitializationList = structure.structureMemberInitializationList()
    if (structureMemberInitializationList !== null) {
      for (const member of structureMemberInitializationList.structureMemberInitialization()) {
        this.members.push(new StructMemberExpression(this, member))
      }
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    for (const member of this.members) {
      member.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class FunctionCallExpression extends Node {
  kind = Kind.FunctionCallExpression
  name: Identifier
  arguments: (Expression | InitializerType)[]

  constructor (parent: Node, functionCall: FunctionCallExpressionContext | FunctionCallContext) {
    super(parent, functionCall.start, functionCall.stop)
    this.arguments = []
    let fnArguments
    if (functionCall instanceof FunctionCallExpressionContext) {
      this.name = new Identifier(this, functionCall.functionCall().functionName().IDENTIFIER().symbol, functionCall.namespaceName()?.IDENTIFIER().symbol)
      fnArguments = functionCall.functionCall().arguments()
    } else {
      this.name = new Identifier(this, functionCall.functionName().IDENTIFIER().symbol)
      fnArguments = functionCall.arguments()
    }
    if (fnArguments !== null) {
      for (const argument of fnArguments.argument()) {
        const argExpression = argument.expression()
        const argInitializerType = argument.initializerType()
        if (argExpression !== null) {
          this.arguments.push(createExpressionNode(this, argExpression))
        } else if (argInitializerType !== null) {
          this.arguments.push(createInitializerTypeNode(this, argInitializerType))
        }
      }
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.name.visit(enterCb, exitCb)
    for (const argument of this.arguments) {
      argument.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class AsExpression extends Node {
  kind = Kind.AsExpression
  expression: Expression
  class: ClassType

  constructor (parent: Node, asExpression: AsExpressionContext) {
    super(parent, asExpression.start, asExpression.stop)
    this.expression = createExpressionNode(this, asExpression.expression())
    this.class = new ClassType(this, asExpression.className().CLASS().symbol)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.expression.visit(enterCb, exitCb)
    this.class.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class IsExpression extends Node {
  kind = Kind.IsExpression
  expression: Expression
  class: ClassType

  constructor (parent: Node, isExpression: IsExpressionContext) {
    super(parent, isExpression.start, isExpression.stop)
    this.expression = createExpressionNode(this, isExpression.expression())
    this.class = new ClassType(this, isExpression.className().CLASS().symbol)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.expression.visit(enterCb, exitCb)
    this.class.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class DotAccessExpression extends Node {
  kind = Kind.DotAccessExpression
  object: Expression
  functionCall?: FunctionCallExpression
  member?: Identifier

  constructor (parent: Node, dotAccessExpression: DotAccessExpressionContext) {
    super(parent, dotAccessExpression.start, dotAccessExpression.stop)
    if (dotAccessExpression._left === undefined) {
      throw new ASTBuildError('Failed to build `DotAccessExpression` node. Missing left part of the expression.')
    }
    this.object = createExpressionNode(this, dotAccessExpression._left)
    const functionCall = dotAccessExpression.functionCall()
    if (functionCall !== null) {
      this.functionCall = new FunctionCallExpression(this, functionCall)
    }
    const member = dotAccessExpression.objectMemberName()?.IDENTIFIER() ?? dotAccessExpression.KEYWORD_NOW()
    if (member !== null) {
      this.member = new Identifier(this, member.symbol)
    }
    if (this.functionCall === undefined && this.member === undefined) {
      throw new ASTBuildError('Failed to build `DotAccessExpression` node. `functionCall` and `member` cannot both be `undefined`.')
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.object.visit(enterCb, exitCb)
    this.functionCall?.visit(enterCb, exitCb)
    this.member?.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class IndexAccessExpression extends Node {
  kind = Kind.IndexAccessExpression
  array: Expression
  index: Expression

  constructor (parent: Node, indexAccessExpression: IndexAccessExpressionContext) {
    super(parent, indexAccessExpression.start, indexAccessExpression.stop)

    if (indexAccessExpression._left === undefined) {
      throw new ASTBuildError('Failed to build `IndexAccessExpression` node. Missing left part of the expression.')
    }
    this.array = createExpressionNode(this, indexAccessExpression._left)
    if (indexAccessExpression._index === undefined) {
      throw new ASTBuildError('Failed to build `IndexAccessExpression` node. Missing the index.')
    }
    this.index = createExpressionNode(this, indexAccessExpression._index)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.array.visit(enterCb, exitCb)
    this.index.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

enum UnaryOperator {
  '-' = '-',
  '!' = '!'
}

class UnaryExpression extends Node {
  kind = Kind.UnaryExpression
  operator: UnaryOperator
  argument: Expression

  constructor (parent: Node, unaryExpression: UnaryMinusExpressionContext | NotExpressionContext) {
    super(parent, unaryExpression.start, unaryExpression.stop)

    if (unaryExpression instanceof UnaryMinusExpressionContext) {
      this.operator = UnaryOperator['-']
    } else {
      this.operator = UnaryOperator['!']
    }
    this.argument = createExpressionNode(this, unaryExpression.expression())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.argument.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

enum BinaryOperator {
  '*' = '*',
  '/' = '/',
  '%' = '%',
  '+' = '+',
  '-' = '-',
  '^' = '^',
  '<' = '<',
  '>' = '>',
  '<=' = '<=',
  '>=' = '>=',
  '!=' = '!=',
  '==' = '=='
}

class BinaryExpression extends Node {
  kind = Kind.BinaryExpression
  operator: BinaryOperator
  left: Expression
  right: Expression

  constructor (parent: Node, binaryExpression: MultiplicativeExpressionContext | AdditiveExpressionContext | ConcatenationExpressionContext | RelationalExpressionContext | EqualityExpressionContext) {
    super(parent, binaryExpression.start, binaryExpression.stop)

    if (binaryExpression instanceof MultiplicativeExpressionContext) {
      if (binaryExpression.OPERATOR_MULTIPLY() !== null) {
        this.operator = BinaryOperator['*']
      } else if (binaryExpression.OPERATOR_DIVIDE() !== null) {
        this.operator = BinaryOperator['/']
      } else {
        this.operator = BinaryOperator['%']
      }
    } else if (binaryExpression instanceof AdditiveExpressionContext) {
      if (binaryExpression.OPERATOR_PLUS() !== null) {
        this.operator = BinaryOperator['+']
      } else {
        this.operator = BinaryOperator['-']
      }
    } else if (binaryExpression instanceof ConcatenationExpressionContext) {
      this.operator = BinaryOperator['^']
    } else if (binaryExpression instanceof RelationalExpressionContext) {
      if (binaryExpression.OPERATOR_LESSTHAN() !== null) {
        this.operator = BinaryOperator['<']
      } else if (binaryExpression.OPERATOR_MORETHAN() !== null) {
        this.operator = BinaryOperator['>']
      } else if (binaryExpression.OPERATOR_LESSTHANEQUAL() !== null) {
        this.operator = BinaryOperator['<=']
      } else {
        this.operator = BinaryOperator['>=']
      }
    } else {
      if (binaryExpression.OPERATOR_NOTEQUAL() !== null) {
        this.operator = BinaryOperator['!=']
      } else {
        this.operator = BinaryOperator['==']
      }
    }

    if (binaryExpression._left === undefined) {
      throw new ASTBuildError('Failed to build `BinaryExpression` node. Missing the left side of the expression.')
    }
    this.left = createExpressionNode(this, binaryExpression._left)
    if (binaryExpression._right === undefined) {
      throw new ASTBuildError('Failed to build `BinaryExpression` node. Missing the right part of the expression.')
    }
    this.right = createExpressionNode(this, binaryExpression._right)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.left.visit(enterCb, exitCb)
    this.right.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

enum LogicalOperator {
  '||' = '||',
  '&&' = '&&'
}

class LogicalExpression extends Node {
  kind = Kind.LogicalExpression
  operator: LogicalOperator
  left: Expression
  right: Expression

  constructor (parent: Node, logicalExpression: OrExpressionContext | AndExpressionContext) {
    super(parent, logicalExpression.start, logicalExpression.stop)

    if (logicalExpression instanceof OrExpressionContext) {
      this.operator = LogicalOperator['||']
    } else {
      this.operator = LogicalOperator['&&']
    }

    if (logicalExpression._left === undefined) {
      throw new ASTBuildError('Failed to build `LogicalExpression` node. Missing the left side of the expression.')
    }
    this.left = createExpressionNode(this, logicalExpression._left)
    if (logicalExpression._right === undefined) {
      throw new ASTBuildError('Failed to build `LogicalExpression` node. Missing the right side of the expression.')
    }
    this.right = createExpressionNode(this, logicalExpression._right)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.left.visit(enterCb, exitCb)
    this.right.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class TemplateTextElement extends Node {
  kind = Kind.TemplateTextElement
  value: string

  constructor (parent: Node, text: Token) {
    super(parent, text)
    this.value = text.text ?? ''
  }
}

class TemplateTextLiteral extends Node {
  kind = Kind.TemplateTextLiteral
  expressions: Expression[]
  texts: TemplateTextElement[]

  constructor (parent: Node, textInterpolationExpression: TextInterpolationExpressionContext) {
    super(parent, textInterpolationExpression.start, textInterpolationExpression.stop)
    this.expressions = []
    for (const expression of textInterpolationExpression.textInterpolation().expression()) {
      this.expressions.push(createExpressionNode(this, expression))
    }
    this.texts = []
    for (const text of textInterpolationExpression.textInterpolation()._text) {
      this.texts.push(new TemplateTextElement(this, text))
    }
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    for (const expression of this.expressions) {
      expression.visit(enterCb, exitCb)
    }
    for (const text of this.texts) {
      text.visit(enterCb, exitCb)
    }
    this.exit(exitCb)
  }
}

class DumptypeExpression extends Node {
  kind = Kind.DumptypeExpression
  type: Identifier

  constructor (parent: Node, dumptypeExpression: DumptypeExpressionContext) {
    super(parent, dumptypeExpression.start, dumptypeExpression.stop)
    const typeName = dumptypeExpression.structIdentifier().structName().IDENTIFIER().symbol
    this.type = new Identifier(this, typeName, dumptypeExpression.structIdentifier().namespaceName()?.IDENTIFIER().symbol)
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.type.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class DumpExpression extends Node {
  kind = Kind.DumpExpression
  expression: Expression

  constructor (parent: Node, dumpExpression: DumpExpressionContext) {
    super(parent, dumpExpression.start, dumpExpression.stop)
    this.expression = createExpressionNode(this, dumpExpression.expression())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.expression.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class CastExpression extends Node {
  kind = Kind.CastExpression
  class: ClassType
  expression: Expression

  constructor (parent: Node, castExpression: CastExpressionContext) {
    super(parent, castExpression.start, castExpression.stop)
    this.class = new ClassType(this, castExpression.className().CLASS().symbol)
    this.expression = createExpressionNode(this, castExpression.expression())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.class.visit(enterCb, exitCb)
    this.expression.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

class ThisExpression extends Node {
  kind = Kind.ThisExpression

  constructor (parent: Node, thisExpression: ThisExpressionContext) {
    super(parent, thisExpression.start, thisExpression.stop)
  }
}

class NowExpression extends Node {
  kind = Kind.NowExpression

  constructor (parent: Node, nowExpression: NowExpressionContext) {
    super(parent, nowExpression.start, nowExpression.stop)
  }
}

function createExpressionNode (parent: Node, expression: ExpressionContext): Expression {
  try {
    if (expression instanceof FunctionCallExpressionContext) {
      return new FunctionCallExpression(parent, expression)
    } else if (expression instanceof LiteralExpressionContext) {
      return createLiteralNode(parent, expression.literal())
    } else if (expression instanceof ArrayContext || expression instanceof ArrayExpressionContext) {
      return new ArrayExpression(parent, expression)
    } else if (expression instanceof StructureContext || expression instanceof StructureExpressionContext) {
      return new StructExpression(parent, expression)
    } else if (expression instanceof AsExpressionContext) {
      return new AsExpression(parent, expression)
    } else if (expression instanceof IsExpressionContext) {
      return new IsExpression(parent, expression)
    } else if (expression instanceof DotAccessExpressionContext) {
      return new DotAccessExpression(parent, expression)
    } else if (expression instanceof IndexAccessExpressionContext) {
      return new IndexAccessExpression(parent, expression)
    } else if (
      expression instanceof UnaryMinusExpressionContext ||
      expression instanceof NotExpressionContext
    ) {
      return new UnaryExpression(parent, expression)
    } else if (
      expression instanceof MultiplicativeExpressionContext ||
      expression instanceof AdditiveExpressionContext ||
      expression instanceof ConcatenationExpressionContext ||
      expression instanceof RelationalExpressionContext ||
      expression instanceof EqualityExpressionContext
    ) {
      return new BinaryExpression(parent, expression)
    } else if (
      expression instanceof AndExpressionContext ||
      expression instanceof OrExpressionContext
    ) {
      return new LogicalExpression(parent, expression)
    } else if (expression instanceof TextInterpolationExpressionContext) {
      return new TemplateTextLiteral(parent, expression)
    } else if (expression instanceof DumptypeExpressionContext) {
      return new DumptypeExpression(parent, expression)
    } else if (expression instanceof DumpExpressionContext) {
      return new DumpExpression(parent, expression)
    } else if (expression instanceof CastExpressionContext) {
      return new CastExpression(parent, expression)
    } else if (expression instanceof IdentifierExpressionContext) {
      return new Identifier(parent, expression.identifier().variableName().IDENTIFIER().symbol, expression.identifier().namespaceName()?.IDENTIFIER().symbol, true)
    } else if (expression instanceof VectorExpressionContext) {
      return new VectorExpression(parent, expression.vector())
    } else if (expression instanceof ThisExpressionContext) {
      return new ThisExpression(parent, expression)
    } else if (expression instanceof NowExpressionContext) {
      return new NowExpression(parent, expression)
    } else if (expression instanceof ParenthesisExpressionContext) {
      return createExpressionNode(parent, expression.parenthesis().expression())
    } else {
      return new InvalidExpression(parent, expression.start, expression.stop)
    }
  } catch (error) {
    if (error instanceof Error) {
      return new InvalidExpression(parent, expression.start, expression.stop, error)
    } else {
      return new InvalidExpression(parent, expression.start, expression.stop, new ASTBuildError('Failed to build `Expression` node. Something went wrong while reading `ExpressionContext`.'))
    }
  }
}

class Main extends Node {
  kind = Kind.Main
  body: BlockStatement

  constructor (parent: Node, mainStatetement: MainStatementContext) {
    super(parent, mainStatetement.start, mainStatetement.stop)
    this.body = new BlockStatement(this, mainStatetement.block())
  }

  visit (enterCb: VisitCallback, exitCb: VisitCallback): void {
    this.enter(enterCb)
    this.body.visit(enterCb, exitCb)
    this.exit(exitCb)
  }
}

function build (tree: ProgramContext): AST {
  const ast: AST = {}

  ast.program = new Program(tree.programContent())

  const directives = tree.programContent().directives()
  if (directives !== null) {
    for (const directive of directives.directive()) {
      try {
        const requireContextDirective = directive.requireContextDirective()
        const extendsDirective = directive.extendsDirective()
        const includeDirective = directive.includeDirective()
        const settingDirective = directive.settingDirective()
        const commandDirective = directive.commandDirective()
        const structDirective = directive.structDirective()
        const constDirective = directive.constDirective()

        if (requireContextDirective !== null) {
          ast.program.directives.push(new RequireContextDirective(ast.program, requireContextDirective))
        } else if (extendsDirective !== null) {
          ast.program.directives.push(new ExtendsDirective(ast.program, extendsDirective))
        } else if (includeDirective !== null) {
          ast.program.directives.push(new IncludeDirective(ast.program, includeDirective))
        } else if (settingDirective !== null) {
          ast.program.directives.push(new SettingDirective(ast.program, settingDirective))
        } else if (commandDirective !== null) {
          ast.program.directives.push(new CommandDirective(ast.program, commandDirective))
        } else if (structDirective !== null) {
          ast.program.directives.push(new StructDirective(ast.program, structDirective))
        } else if (constDirective !== null) {
          ast.program.directives.push(new ConstDirective(ast.program, constDirective))
        }
      } catch (error) {
        if (error instanceof Error) {
          ast.program.directives.push(new InvalidDirective(ast.program, directive.start, directive.stop, error))
        } else {
          ast.program.directives.push(new InvalidDirective(ast.program, directive.start, directive.stop, new ASTBuildError('Failed to build `Directive` node. Something went wrong while reading `DirectiveContext`.')))
        }
      }
    }
  }

  const declarations = tree.programContent().declarations()
  if (declarations !== null) {
    for (const declaration of declarations.declaration()) {
      try {
        const variableDeclaration = declaration.variableDeclaration()
        const functionDeclaration = declaration.functionDeclaration()
        const labelDeclaration = declaration.labelDeclaration()

        if (variableDeclaration !== null) {
          if (
            variableDeclaration instanceof VariableDeclarationWithTypeContext ||
            variableDeclaration instanceof VariableDeclarationWithoutTypeContext ||
            variableDeclaration instanceof VariableDeclarationLetContext
          ) {
            ast.program.declarations.push(new VariableDeclaration(ast.program, variableDeclaration, true))
          }
        } else if (functionDeclaration !== null) {
          ast.program.declarations.push(new FunctionDeclaration(ast.program, functionDeclaration))
        } else if (labelDeclaration !== null) {
          ast.program.declarations.push(new LabelDeclaration(ast.program, labelDeclaration))
        }
      } catch (error) {
        if (error instanceof Error) {
          ast.program.declarations.push(new InvalidDeclaration(ast.program, declaration.start, declaration.stop, error))
        } else {
          ast.program.declarations.push(new InvalidDeclaration(ast.program, declaration.start, declaration.stop, new ASTBuildError('Failed to build `Declaration` node. Something went wrong while reading `DeclarationContext`.')))
        }
      }
    }
  }

  const mainStatement = tree.programContent().mainStatement()
  if (mainStatement !== null) {
    ast.program.main = new Main(ast.program, mainStatement)
  }

  return ast
}

export {
  Kind,
  SourceLocationRange,
  Node,
  Program,
  TypeName,
  SimpleType,
  ClassType,
  EnumType,
  CustomType,
  InvalidType,
  ArrayType,
  InvalidLiteral,
  TextLiteral,
  IntegerLiteral,
  RealLiteral,
  BooleanLiteral,
  NullLiteral,
  NullIdLiteral,
  EnumLiteral,
  Identifier,
  InvalidIdentifier,
  RequireContextDirective,
  InvalidDirective,
  ExtendsDirective,
  IncludeDirective,
  InvalidSettingValue,
  SettingDirective,
  CommandDirective,
  StructMemberDeclaration,
  StructDeclaration,
  StructAliasing,
  StructDirective,
  InvalidConstValue,
  ConstDeclaration,
  ConstAliasing,
  ConstDirective,
  Storage,
  InitializerSign,
  InvalidDeclaration,
  VariableDeclaration,
  FunctionParameterDeclaration,
  FunctionDeclaration,
  LabelDeclaration,
  BlockStatement,
  InvalidStatement,
  ExpressionStatement,
  InvalidExpression,
  VectorExpression,
  ArrayExpression,
  StructMemberExpression,
  StructExpression,
  FunctionCallExpression,
  AsExpression,
  IsExpression,
  DotAccessExpression,
  IndexAccessExpression,
  UnaryExpression,
  UnaryOperator,
  BinaryExpression,
  BinaryOperator,
  LogicalExpression,
  LogicalOperator,
  TemplateTextLiteral,
  DumptypeExpression,
  DumpExpression,
  CastExpression,
  ThisExpression,
  NowExpression,
  AssignmentStatement,
  AssignmentOperator,
  ReturnStatement,
  LabelStatement,
  AssertStatement,
  ForeachStatement,
  ForStatement,
  WhileStatement,
  MeanwhileStatement,
  BreakStatement,
  ContinueStatement,
  SwitchStatement,
  SwitchCase,
  SwitchtypeStatement,
  SwitchtypeCase,
  ConditionalStatement,
  ConditionalBranch,
  LogStatement,
  SleepStatement,
  TuningstartStatement,
  TuningendStatement,
  TuningmarkStatement,
  WaitStatement,
  YieldStatement,
  Main,
  isBaseType,
  isInitializerType,
  isType,
  isLiteral,
  isDirective,
  isDeclaration,
  isStatement,
  isExpression,
  build
}
export type {
  AST,
  BaseType,
  InitializerType,
  KeyType,
  Type,
  Literal,
  Directive,
  Declaration,
  Statement,
  Expression
}
