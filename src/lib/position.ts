import { type Token } from 'antlr4ng'

class SourcePosition {
  /** Line number 1..n */
  line: number
  /** column number 0..n */
  column: number

  constructor (line: number, column: number) {
    this.line = line
    this.column = column
  }
}

class SourceLocation {
  start: SourcePosition
  end: SourcePosition

  constructor (start: SourcePosition, end: SourcePosition) {
    this.start = start
    this.end = end
  }
}

class SourceRange {
  start: number
  end: number

  constructor (start: number, end: number) {
    this.start = start
    this.end = end
  }
}

class SourceLocationRange {
  loc: SourceLocation
  range: SourceRange
  token: SourceRange

  constructor (start: Token, stop?: Token | null) {
    let stopLine = 0
    let stopColumn = 0
    let rangeEnd = 0
    let tokenEnd = 0

    if (stop !== undefined && stop !== null) {
      let length = 0
      if (stop.stop >= stop.start) {
        rangeEnd = stop.stop
        length = stop.stop + 1 - stop.start
      } else {
        rangeEnd = stop.start
      }
      stopLine = stop.line
      stopColumn = stop.column + length
      tokenEnd = stop.tokenIndex
    } else {
      let length = 0
      if (start.stop >= start.start) {
        rangeEnd = start.stop
        length = start.stop + 1 - start.start
      } else {
        rangeEnd = start.start
      }
      stopLine = start.line
      stopColumn = start.column + length
      tokenEnd = start.tokenIndex
    }

    this.loc = new SourceLocation(
      new SourcePosition(start.line, start.column),
      new SourcePosition(stopLine, stopColumn)
    )

    this.range = new SourceRange(
      start.start,
      rangeEnd
    )

    this.token = new SourceRange(
      start.tokenIndex,
      tokenEnd
    )
  }
}

export {
  SourcePosition,
  SourceLocation,
  SourceRange,
  SourceLocationRange
}
