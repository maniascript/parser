import {
  CommonTokenStream,
  CharStream,
  ATNSimulator,
  Recognizer,
  BailErrorStrategy,
  DefaultErrorStrategy,
  BaseErrorListener,
  Token,
  PredictionMode,
  Lexer,
  Parser,
  ProfilingATNSimulator
} from 'antlr4ng'
import { ManiaScriptLexer } from '../antlr/ManiaScriptLexer.js'
import { ManiaScriptParser, ProgramContext } from '../antlr/ManiaScriptParser.js'
import { build as buildAST, type AST } from './ast.js'
import { ScopeManager } from './scope.js'
import { SourceLocationRange } from './position.js'
import { SymbolTable, build as buildST } from './symbol-table.js'

interface ParseError {
  source: SourceLocationRange
  message: string
}

interface ParseResult {
  success: boolean
  errors: ParseError[]
  tree: ProgramContext
  ast: AST
  scopeManager: ScopeManager
  symbolTable: SymbolTable | null
  chars: CharStream
  lexer: Lexer
  tokens: CommonTokenStream
  parser: Parser
  profiling: ProfileInfo[]
}

interface ParseOptions {
  CustomLexer?: typeof ManiaScriptLexer
  CustomParser?: typeof ManiaScriptParser
  twoStepsParsing?: boolean
  profile?: boolean
  msApiPath?: string
  lexerClasses?: Set<string>
  buildAst?: boolean
  buildScopes?: boolean
  buildSymbolTable?: boolean
}

interface ProfileInfo {
  rule: string
  time: number
  invocations: number
  'lookahead total (SLL)': number
  'lookahead min (SLL)': number
  'lookahead max (SLL)': number
  'ATN transition (SLL)': number
  'DFA transition (SLL)': number
  'LL fallback': number
  'lookahead total (LL)': number
  'lookahead min (LL)': number
  'lookahead max (LL)': number
  'ATN transition (LL)': number
  'DFA transition (LL)': number
  ambiguities: number
  predicates: number
  context: number
  errors: number
}

class CollectLexerErrorListener extends BaseErrorListener {
  errors: ParseError[]

  constructor () {
    super()
    this.errors = []
  }

  syntaxError (
    _recognizer: Recognizer<ATNSimulator>,
    _offendingSymbol: Token | null,
    line: number,
    charPositionInLine: number,
    msg: string
  ): void {
    this.errors.push({
      source: {
        loc: {
          start: {
            line,
            column: charPositionInLine
          },
          end: {
            line: 0,
            column: 0
          }
        },
        range: {
          start: 0,
          end: 0
        },
        token: {
          start: 0,
          end: 0
        }
      },
      message: msg
    })
  }
}

class CollectParserErrorListener extends BaseErrorListener {
  errors: ParseError[]

  constructor () {
    super()
    this.errors = []
  }

  syntaxError (
    recognizer: Recognizer<ATNSimulator>,
    offendingSymbol: Token | null,
    _line: number,
    _charPositionInLine: number,
    msg: string
  ): void {
    if (recognizer instanceof ManiaScriptParser && recognizer.context?.start !== null && recognizer.context?.start !== undefined) {
      this.errors.push({
        source: new SourceLocationRange(recognizer.context.start, offendingSymbol),
        message: msg
      })
    }
  }
}

async function parse (
  input: string,
  {
    CustomLexer = ManiaScriptLexer,
    CustomParser = ManiaScriptParser,
    twoStepsParsing = false,
    profile = false,
    msApiPath = '',
    lexerClasses = new Set(),
    buildAst = false,
    buildScopes = false,
    buildSymbolTable = false
  }: ParseOptions = {}
): Promise<ParseResult> {
  const chars = CharStream.fromString(input)
  const lexer = new CustomLexer(chars)

  // We capture all characters in the lexer, so it should not produce errors
  // The parser will handle any invalid token
  const lexerErrorListener = new CollectLexerErrorListener()
  lexer.removeErrorListeners()
  lexer.addErrorListener(lexerErrorListener)

  if (msApiPath.length > 0) {
    await lexer.setMSApi(msApiPath)
  } else if (lexerClasses.size > 0) {
    lexer.setClasses(lexerClasses)
  }

  const tokens = new CommonTokenStream(lexer)
  const parser = new CustomParser(tokens)

  const parserErrorListener = new CollectParserErrorListener()
  parser.removeErrorListeners()
  let tree: ProgramContext
  const profiling: ProfileInfo[] = []

  if (profile) {
    parser.setProfile(true)
    tree = parser.program()
    const decisionInfos = (parser.interpreter as ProfilingATNSimulator).getDecisionInfo()
    for (const decisionInfo of decisionInfos) {
      if (decisionInfo.timeInPrediction > 0) {
        const decisionState = parser.atn.getDecisionState(decisionInfo.decision)
        const ruleName = decisionState !== null ? (parser.ruleNames.at(decisionState.ruleIndex) ?? '') : ''
        profiling.push({
          rule: ruleName,
          time: decisionInfo.timeInPrediction,
          invocations: decisionInfo.invocations,
          'lookahead total (SLL)': decisionInfo.sllTotalLook,
          'lookahead min (SLL)': decisionInfo.sllMinLook,
          'lookahead max (SLL)': decisionInfo.sllMaxLook,
          'ATN transition (SLL)': decisionInfo.sllATNTransitions,
          'DFA transition (SLL)': decisionInfo.sllDFATransitions,
          'LL fallback': decisionInfo.llFallback,
          'lookahead total (LL)': decisionInfo.llTotalLook,
          'lookahead min (LL)': decisionInfo.llMinLook,
          'lookahead max (LL)': decisionInfo.llMaxLook,
          'ATN transition (LL)': decisionInfo.llATNTransitions,
          'DFA transition (LL)': decisionInfo.llDFATransitions,
          ambiguities: decisionInfo.ambiguities.length,
          predicates: decisionInfo.predicateEvals.length,
          context: decisionInfo.contextSensitivities.length,
          errors: decisionInfo.errors.length
        })
      }
    }
  } else if (twoStepsParsing) {
    // Use fast SLL parsing first
    // Only if it fails try the more advance LL parsing
    // to see if the error can be solved
    parser.interpreter.predictionMode = PredictionMode.SLL
    parser.errorHandler = new BailErrorStrategy()

    try {
      tree = parser.program()
    } catch {
      tokens.seek(0)
      parser.reset()
      parser.interpreter.predictionMode = PredictionMode.LL
      parser.errorHandler = new DefaultErrorStrategy()
      parser.addErrorListener(parserErrorListener)
      tree = parser.program()
    }
  } else {
    parser.addErrorListener(parserErrorListener)
    tree = parser.program()
  }

  const errors = [...lexerErrorListener.errors, ...parserErrorListener.errors]
  let ast: AST = {}
  if (buildAst || buildScopes || buildSymbolTable) {
    try {
      ast = buildAST(tree)
    } catch {
      ast = {}
    }
  }
  const scopeManager = new ScopeManager()
  if (buildScopes) scopeManager.analyze(ast)

  let symbolTable: SymbolTable | null = null
  if (buildSymbolTable) symbolTable = buildST(ast)

  return {
    success: errors.length === 0,
    errors,
    tree,
    ast,
    scopeManager,
    symbolTable,
    chars,
    lexer,
    tokens,
    parser,
    profiling
  }
}

export {
  parse
}
export type {
  ParseError,
  ParseResult,
  ParseOptions,
  ProfileInfo
}
