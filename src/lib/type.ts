import { type ParseTree, ParserRuleContext } from 'antlr4ng'
import {
  TextContext,
  IntegerContext,
  RealContext,
  BooleanContext,
  NullContext,
  NullIdContext,
  EnumContext,
  VectorContext,
  LiteralExpressionContext,
  ArrayContext,
  ArrayTypeContext,
  StructureContext,
  TypeContext,
  BaseTypeContext,
  ConstDeclarationContext,
  VariableDeclarationWithTypeContext
} from '../antlr/ManiaScriptParser.js'

enum TypeKind {
  Unknown,
  Text,
  Integer,
  Real,
  Boolean,
  Ident,
  Vec2,
  Int2,
  Vec3,
  Int3,
  Enum,
  Class,
  Array,
  Structure,
  Void
}

interface Type {
  kind: TypeKind
  name: string
}

function getUnknownType (): Type {
  return Object.assign({}, Object.freeze({ kind: TypeKind.Unknown, name: '' }))
}

function deduceTypeFromContext (ctx: ParserRuleContext | ParseTree | null): Type {
  let type: Type = getUnknownType()
  if (ctx instanceof TextContext) {
    type.kind = TypeKind.Text
  } else if (ctx instanceof IntegerContext) {
    type.kind = TypeKind.Integer
  } else if (ctx instanceof RealContext) {
    type.kind = TypeKind.Real
  } else if (ctx instanceof BooleanContext) {
    type.kind = TypeKind.Boolean
  } else if (ctx instanceof NullContext) {
    type.kind = TypeKind.Class
  } else if (ctx instanceof NullIdContext) {
    type.kind = TypeKind.Ident
  } else if (ctx instanceof EnumContext) {
    type.kind = TypeKind.Enum
  } else if (ctx instanceof VectorContext) {
    const expressions = ctx.expression()
    const vectorType = deduceTypeFromContext(expressions[0])
    if (expressions.length === 2) {
      if (vectorType.kind === TypeKind.Real) {
        type.kind = TypeKind.Vec2
      } else if (vectorType.kind === TypeKind.Integer) {
        type.kind = TypeKind.Int2
      }
    } else if (expressions.length === 3) {
      if (vectorType.kind === TypeKind.Real) {
        type.kind = TypeKind.Vec3
      } else if (vectorType.kind === TypeKind.Integer) {
        type.kind = TypeKind.Int3
      }
    }
  } else if (ctx instanceof ArrayContext || ctx instanceof ArrayTypeContext) {
    type.kind = TypeKind.Array
  } else if (ctx instanceof StructureContext) {
    type.kind = TypeKind.Structure
    type.name = ctx.structIdentifier().getText()
  } else if (ctx instanceof LiteralExpressionContext) {
    type = deduceTypeFromContext(ctx.literal())
  } else if (ctx instanceof TypeContext) {
    if (ctx.arrayType() !== null) {
      type = deduceTypeFromContext(ctx.arrayType())
    } else if (ctx.baseType() !== null) {
      type = deduceTypeFromContext(ctx.baseType())
    }
  } else if (ctx instanceof BaseTypeContext) {
    const simpleType = ctx.simpleType()
    if (simpleType !== null) {
      if (simpleType.TYPE_BOOLEAN() !== null) {
        type.kind = TypeKind.Boolean
      } else if (simpleType.TYPE_IDENT() !== null) {
        type.kind = TypeKind.Ident
      } else if (simpleType.TYPE_INT2() !== null) {
        type.kind = TypeKind.Int2
      } else if (simpleType.TYPE_INT3() !== null) {
        type.kind = TypeKind.Int3
      } else if (simpleType.TYPE_INTEGER() !== null) {
        type.kind = TypeKind.Integer
      } else if (simpleType.TYPE_REAL() !== null) {
        type.kind = TypeKind.Real
      } else if (simpleType.TYPE_TEXT() !== null) {
        type.kind = TypeKind.Text
      } else if (simpleType.TYPE_VEC2() !== null) {
        type.kind = TypeKind.Vec2
      } else if (simpleType.TYPE_VEC3() !== null) {
        type.kind = TypeKind.Vec3
      } else if (simpleType.TYPE_VOID() !== null) {
        type.kind = TypeKind.Void
      }
    }
  } else if (ctx instanceof ConstDeclarationContext) {
    if (ctx.literal() !== null) {
      type = deduceTypeFromContext(ctx.literal())
    } else if (ctx.vector() !== null) {
      type = deduceTypeFromContext(ctx.vector())
    } else if (ctx.array() !== null) {
      type = deduceTypeFromContext(ctx.array())
    } else if (ctx.structure() !== null) {
      type = deduceTypeFromContext(ctx.structure())
    }
  } else if (ctx instanceof VariableDeclarationWithTypeContext) {
    type = deduceTypeFromContext(ctx.type())
  }
  return type
}

export {
  type Type,
  getUnknownType,
  deduceTypeFromContext,
  TypeKind
}
