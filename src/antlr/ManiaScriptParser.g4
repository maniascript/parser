parser grammar ManiaScriptParser;

options {
  tokenVocab=ManiaScriptLexer;
}

@members {

public canBeAccessed(ctx: antlr.ParserRuleContext | undefined): boolean {
  return (
    ctx === undefined || (
      ctx instanceof IdentifierExpressionContext &&
      ctx.identifier().namespaceName()?.IDENTIFIER().symbol === undefined
    ) ||
    ctx instanceof ThisExpressionContext ||
    ctx instanceof FunctionCallExpressionContext ||
    ctx instanceof AsExpressionContext ||
    ctx instanceof ParenthesisExpressionContext ||
    ctx instanceof IndexAccessExpressionContext ||
    ctx instanceof DotAccessExpressionContext
  )
}

public notAnArrayErrorMessage(ctx: antlr.ParserRuleContext | undefined): string {
  return (ctx === undefined) ? 'undefined is not an array' : `${ctx.getText()} is not an array`
}

public notAnObjectErrorMessage(ctx: antlr.ParserRuleContext | undefined): string {
  return (ctx === undefined) ? 'undefined is not an object' : `${ctx.getText()} is not an object`
}

}

// Root
// ----

program
  : programContent EOF
  ;

programContent
  : directives? declarations? mainStatement?
  ;

// Directives
// ----------

directives
  : directive+
  ;

directive
  : requireContextDirective
  | extendsDirective
  | includeDirective
  | settingDirective
  | commandDirective
  | structDirective
  | constDirective
  ;

requireContextDirective
  : DIRECTIVE_REQUIRE_CONTEXT className
  ;

extendsDirective
  : DIRECTIVE_EXTENDS extendsPath
  ;

extendsPath
  : textLiteral
  ;

includeDirective
  : DIRECTIVE_INCLUDE includePath includeAlias?
  ;

includePath
  : textLiteral
  ;

includeAlias
  : KEYWORD_AS namespaceDefinition
  ;

namespaceDefinition
  : IDENTIFIER
  ;

settingDirective
  : DIRECTIVE_SETTING settingName (literal | vector) textAlias?
  ;

settingName
  : IDENTIFIER
  ;

commandDirective
  : DIRECTIVE_COMMAND commandName '(' type ')' textAlias?
  ;

commandName
  : IDENTIFIER
  ;

structDirective
  : structDeclaration
  | structAliasing
  ;

structDeclaration
  : DIRECTIVE_STRUCT IDENTIFIER OPERATOR_OPEN_BRACE structMembers? OPERATOR_CLOSE_BRACE
  ;

structMembers
  : structMember+
  ;

structMember
  : type IDENTIFIER ';'
  ;

structAliasing
  : DIRECTIVE_STRUCT  namespaceName '::' structName identifierAlias
  ;

namespaceName
  : IDENTIFIER
  ;

structName
  : IDENTIFIER
  ;

constDirective
  : constDeclaration
  | constAliasing
  ;

constDeclaration
  : DIRECTIVE_CONST name=IDENTIFIER (literal | vector | array | structure)
  ;

constAliasing
  : DIRECTIVE_CONST namespaceName '::' constName identifierAlias
  ;

constName
  : IDENTIFIER
  ;

// Declarations
// ------------

declarations
  : declaration+
  ;

declaration
  : variableDeclaration
  | functionDeclaration
  | labelDeclaration
  ;

variableDeclaration
  : KEYWORD_DECLARE storageSpecifier? type variableName identifierAlias? (KEYWORD_FOR forTarget=expression)? initializer? ';' # variableDeclarationWithType
  | KEYWORD_DECLARE storageSpecifier? variableName identifierAlias? (KEYWORD_FOR forTarget=expression)? initializer ';' # variableDeclarationWithoutType
  | KEYWORD_LET name=IDENTIFIER OPERATOR_ASSIGN (expression | arrayType) ';' # variableDeclarationLet
  ;

variableName
  : IDENTIFIER
  ;

initializer
  : (sign='=' | sign='<=>') (expression | initializerType)
  ;

functionDeclaration
  : type functionName '(' parameters? ')' block
  ;

functionName
  : IDENTIFIER
  ;

parameters
  : parameter (',' parameter)*
  ;

parameter
  : type IDENTIFIER
  ;

labelDeclaration
  : '***' labelName '***' '***' statements '***'
  ;

labelName
  : IDENTIFIER
  ;

// Statements
// ----------

mainStatement
  : 'main' '(' ')' block
  ;

block
  : OPERATOR_OPEN_BRACE statements? OPERATOR_CLOSE_BRACE
  ;

statements
  : statement+
  ;

statement
  : expressionStatement
  | block
  | variableDeclaration
  | assignmentStatement
  | returnStatement
  | labelStatement
  | assertStatement
  | foreachStatement
  | forStatement
  | whileStatement
  | meanwhileStatement
  | breakStatement
  | continueStatement
  | switchStatement
  | switchtypeStatement
  | conditionalStatement
  | logStatement
  | sleepStatement
  | tuningstartStatement
  | tuningendStatement
  | tuningmarkStatement
  | waitStatement
  | yieldStatement
  ;

expressionStatement
  : expression ';'
  ;

assignmentStatement
  : left=expression assignmentOperator right=expression ';'
  | left=expression ('=' | '<=>') initializerType ';'
  ;

returnStatement
  : KEYWORD_RETURN (expression | initializerType)? ';'
  ;

labelStatement
  : labelInsert
  | labelOverwrite
  ;

labelInsert
  : '+++' IDENTIFIER '+++'
  ;

labelOverwrite
  : '---' IDENTIFIER '---'
  ;

assertStatement
  : KEYWORD_ASSERT '(' test=expression (',' message=expression)? ')' ';'
  ;

foreachStatement
  : KEYWORD_FOREACH '(' (key=IDENTIFIER '=>')? value=IDENTIFIER (',' | 'in' ) source=expression KEYWORD_REVERSE?')' statement
  | KEYWORD_FOR '(' (key=IDENTIFIER '=>')? value=IDENTIFIER 'in' KEYWORD_REVERSE? source=expression ')' statement
  ;

forStatement
  : KEYWORD_FOR '(' value=IDENTIFIER ',' loopStart=expression ',' loopStop=expression (',' increment=expression)? ')' statement
  ;

whileStatement
  : KEYWORD_WHILE '(' expression ')' statement
  ;

meanwhileStatement
  : KEYWORD_MEANWHILE '(' expression ')' statement
  ;

breakStatement
  : KEYWORD_BREAK ';'
  ;

continueStatement
  : KEYWORD_CONTINUE ';'
  ;

switchStatement
  : KEYWORD_SWITCH '(' discriminant=expression ')' OPERATOR_OPEN_BRACE switchClauseList OPERATOR_CLOSE_BRACE
  ;

switchClauseList
  : switchCase* switchDefault+
  | switchCase+
  ;

switchCase
  : KEYWORD_CASE tests+=expression (',' tests+=expression)* ':' statement
  ;

switchDefault
  : KEYWORD_DEFAULT ':' statement
  ;

switchtypeStatement
  : KEYWORD_SWITCHTYPE '(' discriminant=expression identifierAlias? ')' OPERATOR_OPEN_BRACE switchtypeClauseList OPERATOR_CLOSE_BRACE
  ;

switchtypeClauseList
  : switchtypeCase* switchtypeDefault+
  | switchtypeCase+
  ;

switchtypeCase
  : KEYWORD_CASE className (',' className)* ':' statement
  ;

switchtypeDefault
  : KEYWORD_DEFAULT ':' statement
  ;

conditionalStatement
  : ifBranch elseIfBranch* elseBranch?
  ;

ifBranch
  : KEYWORD_IF '(' test=expression ')' consequent=statement
  ;

elseIfBranch
  : KEYWORD_ELSE KEYWORD_IF '(' test=expression ')' consequent=statement
  ;

elseBranch
  : KEYWORD_ELSE consequent=statement
  ;

logStatement
  : KEYWORD_LOG '(' expression ')' ';'
  ;

sleepStatement
  : KEYWORD_SLEEP '(' expression ')' ';'
  ;

tuningstartStatement
  : KEYWORD_TUNINGSTART '(' ')' ';'
  ;

tuningendStatement
  : KEYWORD_TUNINGEND '(' ')' ';'
  ;

tuningmarkStatement
  : KEYWORD_TUNINGMARK '(' textLiteral ')' ';'
  ;

waitStatement
  : KEYWORD_WAIT '(' expression ')' ';'
  ;

yieldStatement
  : KEYWORD_YIELD ';'
  | KEYWORD_YIELD '(' LITERAL_INTEGER ')' ';'
  ;

// Expressions
// -----------
expression
  : (namespaceName '::')? functionCall # functionCallExpression
  | expression KEYWORD_AS className           # asExpression
  | left=expression { this.canBeAccessed($left.ctx) }?<fail={ this.notAnObjectErrorMessage($left.ctx) }> '.' (objectMemberName | KEYWORD_NOW | functionCall) #dotAccessExpression
  | left=expression { this.canBeAccessed($left.ctx) }?<fail={ this.notAnArrayErrorMessage($left.ctx) }> '[' index=expression ']' #indexAccessExpression
  | '-' expression # unaryMinusExpression
  | '!' expression # notExpression
  | left=expression ('*' | '/' | '%') right=expression         # multiplicativeExpression
  | left=expression ('+' | '-') right=expression               # additiveExpression
  | left=expression '^' right=expression                       # concatenationExpression
  | left=expression ('<' | '>' | '<=' | '>=') right=expression # relationalExpression
  | left=expression ('!=' | '==') right=expression # equalityExpression
  | left=expression '&&' right=expression          # andExpression
  | left=expression '||' right=expression          # orExpression
  | expression KEYWORD_IS className     # isExpression
  | KEYWORD_DUMPTYPE '(' structIdentifier ')' # dumptypeExpression
  | KEYWORD_DUMP '(' expression ')'     # dumpExpression
  | KEYWORD_CAST '(' className ',' expression ')' # castExpression
  | structure         # structureExpression
  | array             # arrayExpression
  | identifier        # identifierExpression
  | literal           # literalExpression
  | vector            # vectorExpression
  | textInterpolation # textInterpolationExpression
  | KEYWORD_THIS      # thisExpression
  | KEYWORD_NOW       # nowExpression
  | parenthesis       # parenthesisExpression
  ;

functionCall
  : functionName '(' arguments? ')'
  ;

arguments
  : argument (',' argument)*
  ;

argument
  : expression
  | initializerType
  ;

textInterpolation
  : OPERATOR_OPEN_TEXT_TRIPLE (text+=(PART_TEXT_TRIPLE | PART_TEXT_TRIPLE_BRACE | PART_TEXT_TRIPLE_QUOTE) | (OPERATOR_OPEN_INTERPOLATION expression OPERATOR_CLOSE_INTERPOLATION))+ OPERATOR_CLOSE_TEXT_TRIPLE
  ;

structure
  : structIdentifier OPERATOR_OPEN_BRACE structureMemberInitializationList? OPERATOR_CLOSE_BRACE
  ;

structureMemberInitializationList
  : structureMemberInitialization (',' structureMemberInitialization)*
  ;

structureMemberInitialization
  : OPERATOR_DOT? structMemberName '=' expression
  ;

structMemberName
  : IDENTIFIER
  ;

structIdentifier
  : (namespaceName '::')? structName
  ;

array
  : associativeArray
  | simpleArray
  | emptyArray
  ;

associativeArray
  : '[' key+=expression '=>' value+=expression (',' key+=expression '=>' value+=expression)* ']'
  ;

simpleArray
  : '[' value+=expression (',' value+=expression)* ']'
  ;

emptyArray
  : '[' ']'
  ;

identifier
  : (namespaceName '::')? variableName
  ;

literal
  : '-'? LITERAL_REAL # real
  | '-'? LITERAL_INTEGER # integer
  | LITERAL_BOOLEAN # boolean
  | LITERAL_NULL # null
  | LITERAL_NULLID # nullId
  | textLiteral # text
  | enumLiteral # enum
  ;

textLiteral
  : textTranslatedLiteral
  | textBaseLiteral
  ;

textTranslatedLiteral
  : '_(' textBaseLiteral ')'
  ;

textBaseLiteral
  : textSingleQuoteLiteral
  | textTripleQuoteLiteral
  ;

textSingleQuoteLiteral
  : OPERATOR_OPEN_TEXT_SINGLE PART_TEXT_SINGLE* OPERATOR_CLOSE_TEXT_SINGLE
  ;

textTripleQuoteLiteral
  : OPERATOR_OPEN_TEXT_TRIPLE (PART_TEXT_TRIPLE | PART_TEXT_TRIPLE_BRACE | PART_TEXT_TRIPLE_QUOTE)* OPERATOR_CLOSE_TEXT_TRIPLE
  ;

enumLiteral
  : (className | namespaceName)? '::' enumName '::' enumValue
  ;

vector
  : '<' expression ',' expression '>'
  | '<' expression ',' expression ',' expression '>'
  ;

parenthesis
  : '(' expression ')'
  ;

// Types
// -----

type
  : baseType
  | arrayType
  ;

initializerType
  : simpleType
  | className
  | arrayType
  ;

baseType
  : simpleType
  | className
  | enumType
  | customType
  ;

simpleType
  : TYPE_BOOLEAN
  | TYPE_IDENT
  | TYPE_INT2
  | TYPE_INT3
  | TYPE_INTEGER
  | TYPE_REAL
  | TYPE_TEXT
  | TYPE_VEC2
  | TYPE_VEC3
  | TYPE_VOID
  ;

enumType
  : className? '::' enumName
  ;

// Match structures but also library enum (eg: TimeLib::EDateFormat)
// We cannot make a difference between TimeLib::EDateFormat and MyLib::K_MyStruct without parsing the library
customType
  : (namespaceName '::')? structName
  ;

arrayType
  : baseType arrayTypeDimension+
  ;

arrayTypeDimension
  : '[' ']'
  | '[' baseType ']'
  ;

// Miscellaneous
// -------------

assignmentOperator
  : '='
  | '*='
  | '/='
  | '+='
  | '-='
  | '%='
  | '<=>'
  | '^='
  ;

storageSpecifier
  : STORAGESPECIFIER_CLOUD
  | STORAGESPECIFIER_METADATA
  | STORAGESPECIFIER_NETREAD
  | STORAGESPECIFIER_NETWRITE
  | STORAGESPECIFIER_PERSISTENT
  ;

identifierAlias
  : KEYWORD_AS IDENTIFIER
  ;

textAlias
  : KEYWORD_AS alias=textLiteral
  ;

className
  : CLASS
  ;

objectMemberName
  : IDENTIFIER
  ;

enumName
  : IDENTIFIER
  ;

enumValue
  : IDENTIFIER
  ;
