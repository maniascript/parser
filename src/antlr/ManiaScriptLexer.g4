lexer grammar ManiaScriptLexer;

@lexer::header {
// antlr4ng does not handle the `superClass` option of ANTLR grammar yet
// this line is added by the ManiaScriptLexer.g4 file instead
import { ManiaScriptBaseLexer } from './ManiaScriptBaseLexer.js'
}

options { superClass=ManiaScriptBaseLexer; }

tokens {
  CLASS
}

channels {
  COMMENT
}

// Mode default
// ============

// Comments
// --------
SINGLE_LINE_COMMENT : '//' ~[\r\n\u2028\u2029]* -> channel(COMMENT) ;
MULTI_LINES_COMMENT : '/*' .*? '*/' -> channel(COMMENT) ;

// Keywords
// --------
KEYWORD_ASSERT : 'assert' ;
KEYWORD_AS : 'as' ;
KEYWORD_BREAK : 'break' ;
KEYWORD_CASE : 'case' ;
KEYWORD_CAST : 'cast' ;
KEYWORD_CONTINUE : 'continue' ;
KEYWORD_DECLARE : 'declare' ;
KEYWORD_LET : 'let' ;
KEYWORD_DEFAULT : 'default' ;
KEYWORD_DUMPTYPE : 'dumptype' ;
KEYWORD_DUMP : 'dump' ;
KEYWORD_ELSE : 'else' ;
KEYWORD_FOREACH : 'foreach' ;
KEYWORD_FOR : 'for' ;
KEYWORD_IF : 'if' ;
KEYWORD_IN : 'in' ;
KEYWORD_IS : 'is' ;
KEYWORD_LOG : 'log' ;
KEYWORD_MAIN : 'main' ;
KEYWORD_MEANWHILE : 'meanwhile' ;
KEYWORD_RETURN : 'return' ;
KEYWORD_REVERSE : 'reverse' ;
KEYWORD_SLEEP : 'sleep' ;
KEYWORD_SWITCHTYPE : 'switchtype' ;
KEYWORD_SWITCH : 'switch' ;
KEYWORD_TUNINGEND : 'tuningend' ;
KEYWORD_TUNINGMARK : 'tuningmark' ;
KEYWORD_TUNINGSTART : 'tuningstart' ;
KEYWORD_WAIT : 'wait' ;
KEYWORD_WHILE : 'while' ;
KEYWORD_YIELD : 'yield' ;
KEYWORD_NOW : 'Now' ;
KEYWORD_THIS : 'This' ;

// Labels
// ------
LABEL_PLUS : '+++' ;
LABEL_MINUS : '---' ;
LABEL_STAR : '***' ;

// Operators
// ---------
OPERATOR_ASSIGN : '=' ;
OPERATOR_SEMICOLON : ';' ;
OPERATOR_PLUS : '+' ;
OPERATOR_MINUS : '-' ;
OPERATOR_MULTIPLY : '*' ;
OPERATOR_DIVIDE : '/' ;
OPERATOR_MODULO : '%' ;
OPERATOR_OPEN_TEXT_TRIPLE : '"""' -> pushMode(MODE_TEXT_TRIPLE) ;
OPERATOR_CLOSE_INTERPOLATION : '}}}' { this.interpolatedStringLevel > 0 }? { this.interpolatedStringLevel--} -> popMode ;
OPERATOR_OPEN_INTERPOLATION_ERROR : '{{{' ;
OPERATOR_CLOSE_INTERPOLATION_ERROR : '}}}' ;
OPERATOR_OPEN_TEXT_SINGLE : '"' -> pushMode(MODE_TEXT_SINGLE) ;
OPERATOR_OPEN_PAREN : '(' ;
OPERATOR_CLOSE_PAREN : ')' ;
OPERATOR_OPEN_BRACKET : '[' ;
OPERATOR_CLOSE_BRACKET : ']' ;
OPERATOR_OPEN_BRACE : '{' ;
OPERATOR_CLOSE_BRACE : '}' ;
OPERATOR_CONCAT : '^' ;
OPERATOR_COMMA : ',' ;
OPERATOR_DOUBLECOLON : '::' ;
OPERATOR_COLON : ':' ;
OPERATOR_DOT : '.' ;
OPERATOR_NOT : '!' ;
OPERATOR_LESSTHAN : '<' ;
OPERATOR_MORETHAN : '>' ;
OPERATOR_LESSTHANEQUAL : '<=' ;
OPERATOR_MORETHANEQUAL : '>=' ;
OPERATOR_EQUAL : '==' ;
OPERATOR_NOTEQUAL : '!=' ;
OPERATOR_AND : '&&' ;
OPERATOR_OR : '||' ;
OPERATOR_MULTIPLYASSIGN : '*=' ;
OPERATOR_DIVIDEASSIGN : '/=' ;
OPERATOR_PLUSASSIGN : '+=' ;
OPERATOR_MINUSASSIGN : '-=' ;
OPERATOR_MODULOASSIGN : '%=' ;
OPERATOR_ARROWASSIGN : '<=>' ;
OPERATOR_CONCATASSIGN : '^=' ;
OPERATOR_TEXTTRANSLATE : '_(' ;
OPERATOR_ARROW : '=>' ;

// Types
// -----
TYPE_BOOLEAN : 'Boolean' ;
TYPE_IDENT : 'Ident' ;
TYPE_INT2 : 'Int2' ;
TYPE_INT3 : 'Int3' ;
TYPE_INTEGER : 'Integer' ;
TYPE_REAL : 'Real' ;
TYPE_TEXT : 'Text' ;
TYPE_VEC2 : 'Vec2' ;
TYPE_VEC3 : 'Vec3' ;
TYPE_VOID : 'Void' ;

// Storage specifiers
// ------------------
STORAGESPECIFIER_CLOUD : 'cloud' ;
STORAGESPECIFIER_METADATA : 'metadata' ;
STORAGESPECIFIER_NETREAD : 'netread' ;
STORAGESPECIFIER_NETWRITE : 'netwrite' ;
STORAGESPECIFIER_PERSISTENT : 'persistent' ;

// Literals
// --------
LITERAL_REAL : [0-9]*'.'[0-9]+ EXPONENT? | [0-9]+'.' EXPONENT?;
LITERAL_INTEGER : [0-9]+ EXPONENT? ;
LITERAL_BOOLEAN : 'True' | 'False' ;
LITERAL_NULL : 'Null' ;
LITERAL_NULLID : 'NullId' ;

// Directives
// ----------
DIRECTIVE_REQUIRE_CONTEXT : '#RequireContext' ;
DIRECTIVE_EXTENDS : '#Extends' ;
DIRECTIVE_INCLUDE : '#Include' ;
DIRECTIVE_SETTING : '#Setting' ;
DIRECTIVE_COMMAND : '#Command' ;
DIRECTIVE_STRUCT : '#Struct' ;
DIRECTIVE_CONST : '#Const' ;

// Others
// ------
IDENTIFIER : IDENTIFIER_START IDENTIFIER_PART* { if (this.classes.has(this.text)) this.type = ManiaScriptLexer.CLASS };
WHITESPACES : WHITESPACE+ -> channel(HIDDEN) ;
LINE_TERMINATOR : NEWLINE -> channel(HIDDEN) ;
UNKNOWN: . ;

// Text with """
// ------
mode MODE_TEXT_TRIPLE ;

OPERATOR_CLOSE_TEXT_TRIPLE : '"""' -> popMode ;
OPERATOR_OPEN_INTERPOLATION : '{{{' { this.interpolatedStringLevel++ } -> pushMode(DEFAULT_MODE) ;
PART_TEXT_TRIPLE : ~[{"]+ ;
PART_TEXT_TRIPLE_QUOTE : '"' ;
PART_TEXT_TRIPLE_BRACE : '{' ;

// Text with "
// ------
mode MODE_TEXT_SINGLE ;

OPERATOR_CLOSE_TEXT_SINGLE : '"' -> popMode ;
PART_TEXT_SINGLE : (ESCAPED_CHARACTERS | ~["\n\r\\])+;

// Fragments
// =========
fragment IDENTIFIER_START : [a-zA-Z_] ;
fragment IDENTIFIER_PART : [a-zA-Z0-9_] ;
fragment EXPONENT : [eE] [+-]? [0-9]+ ;
fragment ESCAPED_CHARACTERS : '\\'["\\nrt] ;
fragment WHITESPACE
	: '\u0009' // HORIZONTAL TAB
	| '\u000B' // VERTICAL TAB
	| '\u000C' // FORM FEED
	| '\u0020' // SPACE
	| '\u00A0' // NO_BREAK SPACE
	| '\u1680' // OGHAM SPACE MARK
	| '\u180E' // MONGOLIAN VOWEL SEPARATOR
	| '\u2000' // EN QUAD
	| '\u2001' // EM QUAD
	| '\u2002' // EN SPACE
	| '\u2003' // EM SPACE
	| '\u2004' // THREE_PER_EM SPACE
	| '\u2005' // FOUR_PER_EM SPACE
	| '\u2006' // SIX_PER_EM SPACE
	| '\u2008' // PUNCTUATION SPACE
	| '\u2009' // THIN SPACE
	| '\u200A' // HAIR SPACE
	| '\u202F' // NARROW NO_BREAK SPACE
	| '\u3000' // IDEOGRAPHIC SPACE
	| '\u205F' // MEDIUM MATHEMATICAL SPACE
	;
fragment NEWLINE
	: '\r\n'
  | '\r'
  | '\n'
	| '\u0085' // NEXT LINE
	| '\u2028' // LINE SEPARATOR
	| '\u2029' // PARAGRAPH SEPARATOR
	;
