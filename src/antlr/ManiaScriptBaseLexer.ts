import { Lexer, type CharStream } from 'antlr4ng'
import { api, generateFromFile } from '@maniascript/api'

const classesSet = new Set(api.classNames)

export abstract class ManiaScriptBaseLexer extends Lexer {
  interpolatedStringLevel: number
  classes: Set<string>

  constructor (input: CharStream) {
    super(input)

    this.interpolatedStringLevel = 0
    this.classes = classesSet
  }

  setClasses (classes: string[] | Set<string> = []): void {
    if (Array.isArray(classes)) {
      this.classes = new Set(classes)
    } else if (classes instanceof Set) {
      this.classes = classes
    } else {
      throw new Error('classes parameter must be an Array or Set')
    }
  }

  async setMSApi (path = ''): Promise<void> {
    const api = await generateFromFile(path)
    this.setClasses(api.classNames)
  }
}
