import eslint from '@eslint/js'
import tseslint from 'typescript-eslint'

export default tseslint.config(
  {
    ignores: [
      'src/antlr/ManiaScriptLexer.ts',
      'src/antlr/ManiaScriptParser.ts',
      'src/antlr/ManiaScriptParserListener.ts',
      'src/antlr/ManiaScriptParserVisitor.ts',
      'eslint.config.js',
      'fix-antlr-error.js'
    ]
  },
  eslint.configs.recommended,
  tseslint.configs.strictTypeChecked,
  tseslint.configs.stylisticTypeChecked,
  {
    languageOptions: {
      parserOptions: {
        projectService: true,
        tsconfigRootDir: import.meta.dirname,
      }
    }
  }
)
