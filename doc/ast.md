Description of the different AST nodes produced by the parser. Loosely inspired by the [EStree spec](https://github.com/estree/estree).

[[_TOC_]]

# Node

Every nodes in the AST inherit from this class

```ts
class Node {
  kind: string
  parent: Node | undefined
  source: SourceLocationRange
}
```

```ts
class SourceLocationRange {
  loc: SourceLocation
  range: SourceRange
  token: SourceRange
}
```

```ts
class SourceLocation {
  start: SourcePosition
  end: SourcePosition
}
```

```ts
class SourcePosition {
  line: number
  column: number
}
```

```ts
class SourceRange {
  start: number
  end: number
}
```

`kind` is the type of the node, eg: 'Program', 'ConditionalStatement', 'VariableDeclation', ...

`parent` is the parent of the node in the tree.

`source.loc` is the starting and ending line and column of the node in the source code. `line` starts at 1 and `column` starts at 0.

`source.range` is the index of the first and last character of the node in the source code.

`source.token` is the index of the first and last token of the node in the source code.

```ts
enum Kind {
  Program = 'Program',
  SimpleType = 'SimpleType',
  ClassType = 'ClassType',
  EnumType = 'EnumType',
  CustomType = 'CustomType',
  ArrayType = 'ArrayType',
  TextLiteral = 'TextLiteral',
  IntegerLiteral = 'IntegerLiteral',
  RealLiteral = 'RealLiteral',
  BooleanLiteral = 'BooleanLiteral',
  NullLiteral = 'NullLiteral',
  NullIdLiteral = 'NullIdLiteral',
  EnumLiteral = 'EnumLiteral',
  Identifier = 'Identifier',
  RequireContextDirective = 'RequireContextDirective',
  ExtendsDirective = 'ExtendsDirective',
  IncludeDirective = 'IncludeDirective',
  SettingDirective = 'SettingDirective',
  CommandDirective = 'CommandDirective',
  StructMemberDeclaration = 'StructMemberDeclaration',
  StructDeclaration = 'StructDeclaration',
  StructAliasing = 'StructAliasing',
  StructDirective = 'StructDirective',
  ConstDeclaration = 'ConstDeclaration',
  ConstAliasing = 'ConstAliasing',
  ConstDirective = 'ConstDirective',
  VariableDeclaration = 'VariableDeclaration',
  FunctionParameterDeclaration = 'FunctionParameterDeclaration',
  FunctionDeclaration = 'FunctionDeclaration',
  LabelDeclaration = 'LabelDeclaration',
  BlockStatement = 'BlockStatement',
  ExpressionStatement = 'ExpressionStatement',
  VectorExpression = 'VectorExpression',
  ArrayExpression = 'ArrayExpression',
  StructMemberExpression = 'StructMemberExpression',
  StructExpression = 'StructExpression',
  FunctionCallExpression = 'FunctionCallExpression',
  AsExpression = 'AsExpression',
  IsExpression = 'IsExpression',
  DotAccessExpression = 'DotAccessExpression',
  IndexAccessExpression = 'IndexAccessExpression',
  UnaryExpression = 'UnaryExpression',
  BinaryExpression = 'BinaryExpression',
  LogicalExpression = 'LogicalExpression',
  TemplateTextLiteral = 'TemplateTextLiteral',
  TemplateTextElement = 'TemplateTextElement',
  DumptypeExpression = 'DumptypeExpression',
  DumpExpression = 'DumpExpression',
  CastExpression = 'CastExpression',
  ThisExpression = 'ThisExpression',
  NowExpression = 'NowExpression',
  AssignmentStatement = 'AssignmentStatement',
  ReturnStatement = 'ReturnStatement',
  LabelStatement = 'LabelStatement',
  AssertStatement = 'AssertStatement',
  ForeachStatement = 'ForeachStatement',
  ForStatement = 'ForStatement',
  WhileStatement = 'WhileStatement',
  MeanwhileStatement = 'MeanwhileStatement',
  BreakStatement = 'BreakStatement',
  ContinueStatement = 'ContinueStatement',
  SwitchStatement = 'SwitchStatement',
  SwitchCase = 'SwitchCase',
  SwitchtypeStatement = 'SwitchtypeStatement',
  SwitchtypeCase = 'SwitchtypeCase',
  ConditionalStatement = 'ConditionalStatement',
  ConditionalBranch = 'ConditionalBranch',
  LogStatement = 'LogStatement',
  SleepStatement = 'SleepStatement',
  TuningstartStatement = 'TuningstartStatement',
  TuningendStatement = 'TuningendStatement',
  TuningmarkStatement = 'TuningmarkStatement',
  WaitStatement = 'WaitStatement',
  YieldStatement = 'YieldStatement',
  Main = 'Main'
}
```

All valid `Kind` of nodes.

## Program

```ts
class Program extends Node {
  kind = Kind.Program
  directives: Directive[]
  declarations: Declaration[]
  main?: Main
}
```

A complete program tree. A program start with directives (`#Const`, `#Include`, ...). Followed by global variables, functions and labels declaration. And finally a main statement.

## Main

```ts
class Main extends Node {
  kind = Kind.Main
  body: BlockStatement
}
```

The script entry point. eg: `main() {}`

## Identifier

```ts
class Identifier extends Node {
  kind: Kind
  namespace?: string
  name: string
}
```

An identifier. eg: `AAA::BBB`

- namespace: `AAA`
- name: `BBB`

eg: `CCC`

- namespace: `undefined`
- name: `CCC`

## Types

```ts
type BaseType = (
  SimpleType |
  ClassType |
  EnumType |
  CustomType
)

type KeyType = (
  BaseType |
  'EmptyKey'
)

type InitializerType = (
  SimpleType |
  ClassType |
  ArrayType
)

type Type = (
  BaseType |
  ArrayType
)
```

Unions of different kind of types used by other nodes.

### SimpleType

```ts
enum TypeName {
  Boolean = 'Boolean',
  Ident = 'Ident',
  Int2 = 'Int2',
  Int3 = 'Int3',
  Integer = 'Integer',
  Real = 'Real',
  Text = 'Text',
  Vec2 = 'Vec2',
  Vec3 = 'Vec3',
  Void = 'Void'
}
```

All valid `SimpleType` names.

```ts
class SimpleType extends Node {
  kind = Kind.SimpleType
  name: TypeName
}
```

A simple type. eg: `Integer`

- name: `TypeName.Integer`

### ClassType

```ts
class ClassType extends Node {
  kind = Kind.ClassType
  name: string
}
```

A ManiaScript class. eg: `CSmMode`

- name: `'CSmMode'`

### EnumType

```ts
class EnumType extends Node {
  kind = Kind.EnumType
  class?: ClassType
  name: Identifier
}
```

An enum. eg: `CSmMode::EMedal`

- class: `CSmMode`
- name: `EMedal`

eg: `::EMedal`

- class: `undefined`
- name: `EMedal`

### CustomType

```ts
class CustomType extends Node {
  kind = Kind.CustomType
  name: Identifier
}
```

A custom type aka a structure. eg: `K_StructA`

- name: `K_StructA`

### ArrayType

```ts
class ArrayType extends Node {
  kind = Kind.ArrayType
  value: BaseType
  keys: KeyType[]
}
```

A simple or associative arrays. eg: `Real[Integer][][CSmMode]`

- value: `Real`
- keys: [`Integer`, `'EmptyKey'`, `CSmMode`]

## Literals

```ts
type Literal = (
  TextLiteral |
  IntegerLiteral |
  RealLiteral |
  BooleanLiteral |
  NullLiteral |
  NullIdLiteral |
  EnumLiteral
)
```

All types of literals.

### TextLiteral

```ts
class TextLiteral extends Node {
  kind = Kind.TextLiteral
  isTranslated: boolean
  isMultiline: boolean
  value: string
  raw: string
}
```

A string. eg: `_("A bit of text")`

- isTranslated: `false`
- isMultiline: `false`
- value: `'A bit of text'`
- raw: `'_("A bit of text")'`

### IntegerLiteral

```ts
class IntegerLiteral extends Node {
  kind = Kind.IntegerLiteral
  value: number
  raw: string
}
```

An integer. eg: `123`

- value: `123`
- raw: `'123'`

### RealLiteral

```ts
class RealLiteral extends Node {
  kind = Kind.RealLiteral
  value: number
  raw: string
}
```

A real. eg: `123.456`

- value: `123.456`
- raw: `'123.456'`

eg: `.5`

- value: `0.5`
- raw: `'.5'`

### BooleanLiteral

```ts
class BooleanLiteral extends Node {
  kind = Kind.BooleanLiteral
  value: boolean
}
```

A boolean. eg: `True`

- value: `true`

### NullLiteral

```ts
class NullLiteral extends Node {
  kind = Kind.NullLiteral
}
```

A `Null` value.

### NullIdLiteral

```ts
class NullIdLiteral extends Node {
  kind = Kind.NullIdLiteral
}
```

A `NullId` value.

### EnumLiteral

```ts
class EnumLiteral extends Node {
  kind = Kind.EnumLiteral
  class?: ClassType | Identifier
  name: Identifier
  value: Identifier
}
```

An enum. eg: `CSmMode::EMedal::Gold`

- class: `CSmMode`
- name: `EMedal`
- value: `Gold`

eg: `::EMedal::Gold`

- class: `undefined`
- name: `EMedal`
- value: `Gold`

## Directives

```ts
type Directive = (
  RequireContextDirective |
  ExtendsDirective |
  IncludeDirective |
  SettingDirective |
  CommandDirective |
  StructDirective |
  ConstDirective
)
```

All types of directives. They start with a `#`, eg: `#Const`, `#Include`, ...

### RequireContextDirective

```ts
class RequireContextDirective extends Node {
  kind = Kind.RequireContextDirective
  class: ClassType
}
```

A directive to select the context of execution of the script. eg: `#RequireContext CSmMode`

- class: `CSmMode`

### ExtendsDirective

```ts
class ExtendsDirective extends Node {
  kind = Kind.ExtendsDirective
  path: TextLiteral
}
```

A directive to extend from another script. eg: `#Extends "Path/To/Mode.Script.txt"`

- path: `"Path/To/Lib.Script.txt"`

### IncludeDirective

```ts
class IncludeDirective extends Node {
  kind = Kind.IncludeDirective
  path: TextLiteral
  alias?: Identifier
}
```

A directive to include another script. eg: `#Include "Path/To/Lib.Script.txt" as Lib`

- path: `"Path/To/Lib.Script.txt"`
- alias: `Lib`

eg: `#Include "Path/To/Another/Lib.Script.txt"`

- path: `"Path/To/Another/Lib.Script.txt"`
- alias: `undefined`

### SettingDirective

```ts
class SettingDirective extends Node {
  kind = Kind.SettingDirective
  name: Identifier
  value: Literal | VectorExpression
  description?: TextLiteral
}
```

A directive to create a setting. eg: `#Setting S_SettingName 123 as "Setting description"`

- name: `S_SettingName`
- value: `123`
- description: `"Setting description"`

eg: `#Setting S_AnotherSetting "Value"`

- name: `S_AnotherSetting`
- value: `"Value"`
- description: `undefined`

### CommandDirective

```ts
class CommandDirective extends Node {
  kind = Kind.CommandDirective
  name: Identifier
  type: Type
  description?: TextLiteral
}
```

A directive to create a command. eg: `#Command Command_Name (Integer) as "Command description"`

- name: `Command_Name`
- type: `Integer`
- description: `"Command description"`

eg: `#Command Command_Name (Text)`

- name: `Command_Name`
- type: `Text`
- description: `undefined`

### Structure directive

#### StructMemberDeclaration

```ts
class StructMemberDeclaration extends Node {
  kind = Kind.StructMemberDeclaration
  type: Type
  name: Identifier
}
```

Declare a member of a structure. eg: `#Struct K_StructName { Integer MemberName; }`

- type: `Integer`
- name: `MemberName`

#### StructDeclaration

```ts
class StructDeclaration extends Node {
  kind = Kind.StructDeclaration
  name: Identifier
  members: StructMemberDeclaration[]
}
```

A directive to declare a new structure. eg: `#Struct K_StructName { Integer MemberName; }`

- name: `K_StructName`
- members: [`Integer MemberName;`]

#### StructAliasing

```ts
class StructAliasing extends Node {
  kind = Kind.StructAliasing
  name: Identifier
  alias: Identifier
}
```

A directive to create an alias for an included structure. eg: `#Struct Lib::K_StructNameA as K_StructNameB`

- name: `Lib::K_StructNameA`
- alias: `K_StructNameB`

#### StructDirective

```ts
class StructDirective extends Node {
  kind = Kind.StructDirective
  declaration?: StructDeclaration
  aliasing?: StructAliasing
}
```

A node to wrap a structure declaration or aliasing. eg: `#Struct K_StructName { Integer MemberName; }`

- declaration: `#Struct K_StructName { Integer MemberName; }`
- aliasing: `undefined`

eg: `#Struct Lib::K_StructNameA as K_StructNameB`

- declaration: `undefined`
- aliasing: `#Struct Lib::K_StructNameA as K_StructNameB`


### Constant directive

#### ConstDeclaration

```ts
class ConstDeclaration extends Node {
  kind = Kind.ConstDeclaration
  name: Identifier
  value: Literal | VectorExpression | ArrayExpression | StructExpression
}
```

A directive to declare a new constant. eg: `#Const C_ConstName 123`.

- name: `C_ConstName`
- value: `123`

#### ConstAliasing

```ts
class ConstAliasing extends Node {
  kind = Kind.ConstAliasing
  name: Identifier
  alias: Identifier
}
```

A directive to create an alias for an included constant. eg: `#Const Lib::C_ConstNameA as C_ConstNameB`.

- name: `Lib::C_ConstNameA`
- alias: `C_ConstNameB`

#### ConstDirective

```ts
class ConstDirective extends Node {
  kind = Kind.ConstDirective
  declaration?: ConstDeclaration
  aliasing?: ConstAliasing
}
```

A node to wrap a constant declaration or aliasing. eg: `#Const C_ConstName 123`

- declaration: `#Const C_ConstName 123`
- aliasing: `undefined`

eg: `#Const Lib::C_ConstNameA as C_ConstNameB`

- declaration: `undefined`
- aliasing: `#Const Lib::C_ConstNameA as C_ConstNameB`

## Declarations

```ts
type Declaration = (
  VariableDeclaration |
  FunctionDeclaration |
  LabelDeclaration
)
```

All types of declarations.

### VariableDeclaration

```ts
enum Storage {
  cloud = 'cloud',
  metadata = 'metadata',
  netread = 'netread',
  netwrite = 'netwrite',
  persistent = 'persistent'
}
```

All type of storage for the declared variable.

```ts
enum InitializerSign {
  '=' = '=',
  '<=>' = '<=>'
}
```

The sign used to assign an initial value to the declared variable.

```ts
class VariableDeclaration extends Node {
  kind = Kind.VariableDeclaration
  isGlobal: boolean
  storage?: Storage
  type?: Type
  name: Identifier
  alias?: Identifier
  forTarget?: Expression
  initializerSign?: InitializerSign
  initializerExpression?: Expression
  initializerType?: InitializerType
}
```

A variable declaration. eg: `declare netwrite Integer VarA as VarB for UI = 0;`.

- isGlobal: true
- storage: `netwrite`
- type: `Integer`
- name: `VarA`
- alias: `VarB`
- forTarger: `UI`
- initializerSign: `=`
- initializerExpression: `0`

With an `InitializerType`, which is deprecated. eg: `declare VarA <=> CSmMode;`

- isGlobal: true
- name: `VarA`
- initializerSign: `<=>`
- initializerType: `CSmMode`

### Function declaration

#### FunctionParameterDeclaration

```ts
class FunctionParameterDeclaration extends Node {
  kind = Kind.FunctionParameterDeclaration
  type: Type
  name: Identifier
}
```

A function parameter. eg: `Void FunctionName(Integer _ParameterName) {}`

- type: `Integer`
- name: `_ParameterName`

#### FunctionDeclaration

```ts
class FunctionDeclaration extends Node {
  kind = Kind.FunctionDeclaration
  type: Type
  name: Identifier
  parameters: FunctionParameterDeclaration[]
  body: BlockStatement
}
```

A new function declaration. eg: `Void FunctionName(Integer _ParameterNameA, Real _ParameterNameB) {}`

- type: `Void`
- name: `FunctionName`
- parameters: [`Integer _ParameterNameA`, `Real _ParameterNameB`]
- body: `{}`

### LabelDeclaration

```ts
class LabelDeclaration extends Node {
  kind = Kind.LabelDeclaration
  name: Identifier
  body: Statement[]
}
```

Declare a new label. eg:

```
***LabelName***
***
declare Integer LabelContent;
***
```

- name: `LabelName`
- body: [`declare Integer LabelContent;`]

## Statements

```ts
type Statement = (
  BlockStatement |
  VariableDeclaration |
  ExpressionStatement |
  AssignmentStatement |
  ReturnStatement |
  LabelStatement |
  AssertStatement |
  ForeachStatement |
  ForStatement |
  WhileStatement |
  MeanwhileStatement |
  BreakStatement |
  ContinueStatement |
  SwitchStatement |
  SwitchtypeStatement |
  ConditionalStatement |
  ConditionalBranch |
  LogStatement |
  SleepStatement |
  TuningstartStatement |
  TuningendStatement |
  TuningmarkStatement |
  WaitStatement |
  YieldStatement
)
```

All types of statements.

### BlockStatement

```ts
class BlockStatement extends Node {
  kind = Kind.BlockStatement
  body: Statement[]
}
```

A block statement that contains other statements surrounded by braces. eg:

```
{
  declare Integer A;
  declare Integer B;
}
```

body: [`declare Integer A;`, `declare Integer B;`]

### ExpressionStatement

```ts
class ExpressionStatement extends Node {
  kind = Kind.ExpressionStatement
  expression: Expression
}
```

A statement consisting of one expression. eg: `1 + 1;`

- expression: `1 + 1`

### AssignmentStatement

```ts
enum AssignmentOperator {
  '=' = '=',
  '<=>' = '<=>',
  '*=' = '*=',
  '/=' = '/=',
  '+=' = '+=',
  '-=' = '-=',
  '%=' = '%=',
  '^=' = '^='
}
```

All valid assignment operators.

```ts
class AssignmentStatement extends Node {
  kind = Kind.AssignmentStatement
  left: Expression
  right: Expression | InitializerType
  operator: AssignmentOperator
  isInitializedByType: boolean
}
```

Assign a new value to a variable. eg: `VarA += 1;`

- left: `VarA`
- right: `1`
- operator: `AssignmentOperator['+=']`
- isInitializedByType: `false`

eg: `VarB = CSmMode;`

- left: `VarB`
- right: `CSmMode`
- operator: `AssignmentOperator['=']`
- isInitializedByType: `true`

### ReturnStatement

```ts
class ReturnStatement extends Node {
  kind = Kind.ReturnStatement
  argument?: Expression | InitializerType
  isReturningType: boolean
}
```

Return a value from a function. eg: `return 123;`

- argument: `123`
- isReturningType: `false`

eg: `return Integer;`

- argument: `Integer`
- isReturningType: `true`

### LabelStatement

```ts
class LabelStatement extends Node {
  kind = Kind.LabelStatement
  isOverwrite: boolean
  name: Identifier
}
```

Execute code from a label. eg: `+++LabelNameA+++`

- isOverwrite: `false`
- name: `LabelNameA`

eg: `---LabelNameB---`

- isOverwrite: `true`
- name: `LabelNameB`

### AssertStatement

```ts
class AssertStatement extends Node {
  kind = Kind.AssertStatement
  test: Expression
  message?: Expression
}
```

Check that an expression is true. eg: `assert(True);`

- test: `True`
- message: `undefined`

eg: `assert(1 == 2, "Assert failed");`

- test: `1 == 2`
- message: `"Assert failed"`

### ForeachStatement

```ts
class ForeachStatement extends Node {
  kind = Kind.ForeachStatement
  key?: Identifier
  value: Identifier
  source: Expression
  body: Statement
}
```

Loop over an array. eg: `foreach (ValueA in ArrayA) {}`

- key: `undefined`
- value: `ValueA`
- source: `ArrayA`
- body: `{}`

eg: `foreach (KeyB => ValueB in ArrayB) {}`

- key: `KeyB`
- value: `ValueB`
- source: `ArrayB`
- body: `{}`

### ForStatement

```ts
class ForStatement extends Node {
  kind = Kind.ForStatement
  value: Identifier
  loopStart: Expression
  loopStop: Expression
  increment?: Expression
  body: Statement
}
```

Loop and increment a value. eg: `for (VarA, 0, 10) {}`

- value: `VarA`
- loopStart: `0`
- loopStop: `10`
- increment: `undefined`
- body: `{}`

eg: `for (VarB, 5, 15, -2) {}`

- value: `VarA`
- loopStart: `5`
- loopStop: `15`
- increment: `-2`
- body: `{}`

### WhileStatement

```ts
class WhileStatement extends Node {
  kind = Kind.WhileStatement
  test: Expression
  body: Statement
}
```

Loop until a condition is false. eg: `while (CanStop()) {}`

- test: `CanStop()`
- body: `{}`

### MeanwhileStatement

```ts
class MeanwhileStatement extends Node {
  kind = Kind.MeanwhileStatement
  test: Expression
  body: Statement
}
```

Loop until a condition is false. eg: `meanwhile (CanStop()) {}`

- test: `CanStop()`
- body: `{}`

### BreakStatement

```ts
class BreakStatement extends Node {
  kind = Kind.BreakStatement
}
```

Exit a loop. eg: `break;`

### ContinueStatement

```ts
class ContinueStatement extends Node {
  kind = Kind.ContinueStatement
}
```

Skip loop current step. eg: `continue;`

### Switch statement

#### SwitchCase

```ts
class SwitchCase extends Node {
  kind = Kind.SwitchCase
  tests: Expression[]
  consequent: Statement
}
```

A `switch` statement branch. eg: `case 1, GetCase(): {}`

- tests: [`1`, `GetCase()`]
- consequent: `{}`

eg: `default: DoSomething();`

- tests: []
- consequent: `DoSomething();`

#### SwitchStatement

```ts
class SwitchStatement extends Node {
  kind = Kind.SwitchStatement
  discriminant: Expression
  cases: SwitchCase[]
}
```

A `switch` statement. eg: `switch (Value) { case 1: {} default: {} }`

- discriminant: `Value`
- cases: [`case 1: {}`, `default: {}`]

### Switchtype statement

#### SwitchtypeCase

```ts
class SwitchtypeCase extends Node {
  kind = Kind.SwitchtypeCase
  tests: ClassType[]
  consequent: Statement
}
```

A `switchtype` statement branch. eg: `case CSmMode, CSmPlayer: {}`

- tests: [`CSmMode`, `CSmPlayer`]
- consequent: `{}`

eg: `default: DoSomething();`

- tests: []
- consequent: `DoSomething();`

#### SwitchtypeStatement

```ts
class SwitchtypeStatement extends Node {
  kind = Kind.SwitchtypeStatement
  discriminant: Expression
  cases: SwitchtypeCase[]
}
```

A `switchtype` statement. eg: `switchtype (Value) { case CSmMode: {} default: {} }`

- discriminant: `Value`
- cases: [`case CSmMode: {}`, `default: {}`]

### if / else if / else statement

#### ConditionalBranch

```ts
class ConditionalBranch extends Node {
  kind = Kind.ConditionalBranch
  test?: Expression
  consequent: Statement
}
```

A branch of an `if` / `else if` / `else` statement. eg: `if (ValueA) {}`

- test: `ValueA`
- consequent: `{}`

eg: `else DoSomething();`

- test: `undefined`
- consequent: `DoSomething();`

#### ConditionalStatement

```ts
class ConditionalStatement extends Node {
  kind = Kind.ConditionalStatement
  branches: ConditionalBranch[]
}
```

An `if` / `else if` / `else` statement. eg:

```
if (ValueA) DoSomething();
else if (ValueB) DoAnotherThing();
else {}
```

- branches: [`if (ValueA) DoSomething();`, `else if (ValueB) DoAnotherThing();`, `else {}`]

### LogStatement

```ts
class LogStatement extends Node {
  kind = Kind.LogStatement
  expression: Expression
}
```

Log a value. eg: `log("Something");`

- expression: `"Something"`

### SleepStatement

```ts
class SleepStatement extends Node {
  kind = Kind.SleepStatement
  expression: Expression
}
```

Sleep for a given duration. eg: `sleep(1000);`

- expression: `1000`

### TuningstartStatement

```ts
class TuningstartStatement extends Node {
  kind = Kind.TuningstartStatement
}
```

A `tuningstart();` statement

### TuningendStatement

```ts
class TuningendStatement extends Node {
  kind = Kind.TuningendStatement
}
```

A `tuningend();` statement

### TuningmarkStatement

```ts
class TuningmarkStatement extends Node {
  kind = Kind.TuningmarkStatement
  label: TextLiteral
}
```

Measure time until the next `TuningmarkStatement`. eg: `tuningmark("LabelName");`

- label : `"LabelName"`

### WaitStatement

```ts
class WaitStatement extends Node {
  kind = Kind.WaitStatement
  expression: Expression
}
```

Wait until expression is `True`. eg: `wait(CanStopWaiting());`

- expression: `CanStopWaiting()`

### YieldStatement

```ts
class YieldStatement extends Node {
  kind = Kind.YieldStatement
}
```

Give the control back to the C++. eg: `yield;`

## Expressions

```ts
type Expression = (
  VectorExpression |
  ArrayExpression |
  StructExpression |
  Literal |
  AsExpression |
  IsExpression |
  DotAccessExpression |
  IndexAccessExpression |
  UnaryExpression |
  BinaryExpression |
  LogicalExpression |
  TemplateTextLiteral |
  DumptypeExpression |
  DumpExpression |
  CastExpression |
  Identifier |
  ThisExpression |
  NowExpression
)
```

Union of all valid expressions used by other nodes.

### VectorExpression

```ts
class VectorExpression extends Node {
  kind = Kind.VectorExpression
  values: Expression[]
}
```

A container for `Vec2`, `Vec3`, `Int2` and `Int3` values. eg: `<1., 2.>`

- values: [`1.`, `2.`]

eg: `<GetValue(), 2, 3>`

- values: [`GetValue()`, `2`, `3`]

### ArrayExpression

```ts
class ArrayExpression extends Node {
  kind = Kind.ArrayExpression
  values: Array<{ key?: Expression, value: Expression }>
  isAssociative: boolean
}
```

A simple or associative array. eg: `[1, GetValue(), 3]`

- values: [{ value: `1`}, { value: `GetValue()` }, { value: `3` }]
- isAssociative: `false`

eg: `["KeyA" => 1, "KeyB" => 2]`


- values: [{ key: `"KeyA"`, value: `1` }, { key: `"KeyB"`, value: `2` }]
- isAssociative: `true`

### Structure

#### StructMemberExpression

```ts
class StructMemberExpression extends Node {
  kind = Kind.StructMemberExpression
  name: Identifier
  value: Expression
}
```

A structure member. eg: `MemberName = 2`

- name: `MemberName`
- value: `2`

#### StructExpression

```ts
class StructExpression extends Node {
  kind = Kind.StructExpression
  name: Identifier
  members: StructMemberExpression[]
}
```

A structure. eg: `K_StructName { MemberA = 1, MemberB = 2 }`

- name: `K_StructName`
- members: [`MemberA = 1`, `MemberB = 2`]

### FunctionCallExpression

```ts
class FunctionCallExpression extends Node {
  kind = Kind.FunctionCallExpression
  name: Identifier
  arguments: (Expression | InitializerType)[]
}
```

A function call. eg: `FunctioName(Arg1, Arg2, Integer)`

- name: `FunctioName`
- arguments: [`Arg1`, `Arg2`, `Integer`]

### AsExpression

```ts
class AsExpression extends Node {
  kind = Kind.AsExpression
  expression: Expression
  class: ClassType
}
```

A type cast. eg: `VariableA as CMlLabel`

- expression: `VariableA`
- class: `CMlLabel`

### IsExpression

```ts
class IsExpression extends Node {
  kind = Kind.IsExpression
  expression: Expression
  class: ClassType
}
```

A type check. eg: `VariableB is CSmMode`

- expression: `VariableB`
- class: `CSmMode`

### DotAccessExpression

```ts
class DotAccessExpression extends Node {
  kind = Kind.DotAccessExpression
  object: Expression
  functionCall?: FunctionCallExpression
  member?: Identifier
}
```

Access to a member or method of an object. eg: `SomeObject.MemberA`

- object: `SomeObject`
- functionCall: `undefined`
- member: `MemberA`

eg: `GetObject().MethodA()`

- object: `GetObject()`
- functionCall: `MethodA()`
- member: `undefined`

### IndexAccessExpression

```ts
class IndexAccessExpression extends Node {
  kind = Kind.IndexAccessExpression
  array: Expression
  index: Expression
}
```

Access to an array value. eg: `SomeArray[123]`

- array: `SomeArray`
- index: `123`

### UnaryExpression

```ts
enum UnaryOperator {
  '-' = '-',
  '!' = '!'
}
```

All valid unary operators.

```ts
class UnaryExpression extends Node {
  kind = Kind.UnaryExpression
  operator: UnaryOperator
  argument: Expression
}
```

An unary expression. eg: `-ValueA`

- operator: `UnaryOperator['-']`
- argument: `ValueA`

eg: `!ValueB`

- operator: `UnaryOperator['!']`
- argument: `ValueB`

### BinaryExpression

```ts
enum BinaryOperator {
  '*' = '*',
  '/' = '/',
  '%' = '%',
  '+' = '+',
  '-' = '-',
  '^' = '^',
  '<' = '<',
  '>' = '>',
  '<=' = '<=',
  '>=' = '>=',
  '!=' = '!=',
  '==' = '=='
}
```

All valid binary operators.

```ts
class BinaryExpression extends Node {
  kind = Kind.BinaryExpression
  operator: BinaryOperator
  left: Expression
  right: Expression
}
```

A binary expression. eg: `1 + 2`

- operator: `BinaryOperator['+']`
- left: `1`
- right: `2`

eg: `True != False`

- operator: `BinaryOperator['!=']`
- left: `True`
- right: `False`

### LogicalExpression

```ts
enum LogicalOperator {
  '||' = '||',
  '&&' = '&&'
}
```

All valid logical operators.

```ts
class LogicalExpression extends Node {
  kind = Kind.LogicalExpression
  operator: LogicalOperator
  left: Expression
  right: Expression
}
```

A logical expression. eg: `A && B`

- operator: `LogicalOperator['&&']`
- left: `A`
- right: `B`

eg: `True || False`

- operator: `LogicalOperator['||']`
- left: `True`
- right: `False`

### Template Text

#### TemplateTextElement

```ts
class TemplateTextElement extends Node {
  kind = Kind.TemplateTextElement
  value: string
}
```

Text part of a template Text.

#### TemplateTextLiteral

```ts
class TemplateTextLiteral extends Node {
  kind = Kind.TemplateTextLiteral
  expressions: Expression[]
  texts: TemplateTextElement[]
}
```

A template Text. eg: `"""AAA {{{1 + 1}}} BBB {{{FunctionCall()}}} CCC"""`

- expressions: [`1 + 1`, `FunctionCall()`]
- texts: [`AAA `, ` BBB `, ` CCC`]

### DumptypeExpression

```ts
class DumptypeExpression extends Node {
  kind = Kind.DumptypeExpression
  type: Identifier
}
```

Stringify a structure declaration. eg: `dumptype(S_StructTypeName)`

- type: `S_StructTypeName`

### DumpExpression

```ts
class DumpExpression extends Node {
  kind = Kind.DumpExpression
  expression: Expression
}
```

Stringify an expression value. eg: `dump(1 + 1)`

- expression: `1 + 1`

### CastExpression

```ts
class CastExpression extends Node {
  kind = Kind.CastExpression
  class: ClassType
  expression: Expression
}
```

A type cast. eg: `cast(CSmMode, VariableA)`

- class: `CSmMode`
- expression: `VariableA`

### ThisExpression

```ts
class ThisExpression extends Node {
  kind = Kind.ThisExpression
}
```

eg: `This`

### NowExpression

```ts
class NowExpression extends Node {
  kind = Kind.NowExpression
}
```

eg: `Now`
