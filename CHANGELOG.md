Changelog
=========

Unreleased
----------

- [#66](https://gitlab.com/maniascript/parser/-/issues/66) Added a `kind` enum on symbols to identify them without using `instanceof`
- [#67](https://gitlab.com/maniascript/parser/-/issues/67) Updated node depencies

Version 15.0.0
--------------

- [#62](https://gitlab.com/maniascript/parser/-/issues/62) Adapted parser rules to ease autocompletion
- [#63](https://gitlab.com/maniascript/parser/-/issues/63) Added a symbol table to the parsing result
- [#64](https://gitlab.com/maniascript/parser/-/issues/64) Fixed parsing of empty structure declaration
- [#65](https://gitlab.com/maniascript/parser/-/issues/65) Improved AST generation on invalid code

Version 14.3.0
--------------

- [#60](https://gitlab.com/maniascript/parser/-/issues/60) Improved the performance of template strings parsing
- [#61](https://gitlab.com/maniascript/parser/-/issues/61) Added parser profiling

Version 14.2.0
--------------

- [#59](https://gitlab.com/maniascript/parser/-/issues/59) Fixed `Now` member in AST

Version 14.1.0
--------------

- [#56](https://gitlab.com/maniascript/parser/-/issues/56) Moved the benchmarks into their own project
- [#57](https://gitlab.com/maniascript/parser/-/issues/57) Improved the performance of template strings parsing
- [#58](https://gitlab.com/maniascript/parser/-/issues/58) Added parsing of the old `Now` class member

Version 14.0.0
--------------

- [#55](https://gitlab.com/maniascript/parser/-/issues/55) Add the `Now` keyword

Version 13.0.0
--------------

- [#54](https://gitlab.com/maniascript/parser/-/issues/54) Switch to the new [antlr4ng](https://github.com/mike-lischke/antlr4ng) typescript runtime

Version 12.2.0
--------------

- [#53](https://gitlab.com/maniascript/parser/-/issues/53) Build the ast even if there are errors in the parse tree

Version 12.1.0
--------------

- [#52](https://gitlab.com/maniascript/parser/-/issues/52) Mark variables declared with let in the AST

Version 12.0.0
--------------

- [#45](https://gitlab.com/maniascript/parser/-/issues/45) Update dependencies
- [#46](https://gitlab.com/maniascript/parser/-/issues/46) Use vitest instead of jest to test the package
- [#47](https://gitlab.com/maniascript/parser/-/issues/47) Add support for `yield(X)`
- [#48](https://gitlab.com/maniascript/parser/-/issues/48) Add support for variable declaration with `let`
- [#49](https://gitlab.com/maniascript/parser/-/issues/49) Add support for `for` as an alias of `foreach`
- [#50](https://gitlab.com/maniascript/parser/-/issues/50) Add support for the `reverse` keyword
- [#51](https://gitlab.com/maniascript/parser/-/issues/51) Add support for `.` in structure declaration

Version 11.0.0
--------------

- [#44](https://gitlab.com/maniascript/parser/-/issues/44) Add support for `switchtype (X as Y)`

Version 10.5.0
--------------

- [#43](https://gitlab.com/maniascript/parser/-/issues/42) The parser creates a list of the different scopes of the program and the variables accessible in theses scopes.

Version 10.4.0
--------------

- [#42](https://gitlab.com/maniascript/parser/-/issues/42) Create AST nodes for StructureExpressionContext.

Version 10.3.0
--------------

- [#41](https://gitlab.com/maniascript/parser/-/issues/41) Mark Identifier that are parsed as an Expression.

Version 10.2.0
--------------

- [#40](https://gitlab.com/maniascript/parser/-/issues/40) Make the parent node of each AST node accessible.

Version 10.1.1
--------------

- [#39](https://gitlab.com/maniascript/parser/-/issues/39) Create `ArrayExpression` AST nodes from `ArrayExpressionContext` parse tree nodes.

Version 10.1.0
--------------

- [#38](https://gitlab.com/maniascript/parser/-/issues/38) Mark global variables in the `VariableDeclaration` AST node.

Version 10.0.0
--------------

- [#37](https://gitlab.com/maniascript/parser/-/issues/37) Make the tokens easily accessible from the AST

Version 9.1.0
-------------

- [#36](https://gitlab.com/maniascript/parser/-/issues/36) Ease the check of some union types

Version 9.0.0
-------------

- [#34](https://gitlab.com/maniascript/parser/-/issues/34) Parse cast expression
- [#35](https://gitlab.com/maniascript/parser/-/issues/35) Accept types as function argument

Version 8.1.0
-------------

- [#33](https://gitlab.com/maniascript/parser/-/issues/33) Include the raw value of Integer and Real literal in the AST

Version 8.0.2
-------------

- [#32](https://gitlab.com/maniascript/parser/-/issues/32) Add some missing visit methods

Version 8.0.1
-------------

- Update documentation
- Remove unused code

Version 8.0.0
-------------

- [#31](https://gitlab.com/maniascript/parser/-/issues/31) Give the full source position (location and range) in parse errors

Version 7.2.1
-------------

- [#30](https://gitlab.com/maniascript/parser/-/issues/30) Do not attempt to build an ast if the parser failed

Version 7.2.0
-------------

- [#29](https://gitlab.com/maniascript/parser/-/issues/29) Export the `ParseError` and `ParseOptions` classes

Version 7.1.0
-------------

- Do not bundle the module into a single file anymore
- [#28](https://gitlab.com/maniascript/parser/-/issues/28) Make the AST nodes classes accessible from the root of the module

Version 7.0.0
-------------

- [#26](https://gitlab.com/maniascript/parser/-/issues/26) Generate an AST

Version 6.0.0
-------------

- [!27](https://gitlab.com/maniascript/parser/-/merge_requests/27) Yet another parser improvement. Parsing is now 30% faster.
- [#27](https://gitlab.com/maniascript/parser/-/issues/27) Parse cases with commas

Version 5.3.0
-------------

- [#24](https://gitlab.com/maniascript/parser/-/issues/24) Export everything from the package

Version 5.2.0
-------------

- [#23](https://gitlab.com/maniascript/parser/-/issues/23) Parse structure aliasing

Version 5.1.0
-------------

- [#22](https://gitlab.com/maniascript/parser/-/issues/22) For loops can accept a fourth parameters

Version 5.0.0
-------------

- [#21](https://gitlab.com/maniascript/parser/-/issues/21) Complete rewrite of the module in TypeScript

Version 4.0.0
-------------

- [#19](https://gitlab.com/maniascript/parser/-/issues/19) Parse custom classes provided by an array or a `doc.h` file

Version 3.0.1
-------------

- [#18](https://gitlab.com/maniascript/parser/-/issues/18) Fix 'not' test

Version 3.0.0
-------------

- [#17](https://gitlab.com/maniascript/parser/-/issues/17) Rework parse tree. Improve the parse tree produced by the parser to ease the creation of the AST. It also vastly improves the performances of the parser.

Version 2.2.0
-------------

- [#16](https://gitlab.com/maniascript/parser/-/issues/16) Parse aliased constants

Version 2.1.0
-------------

- [#15](https://gitlab.com/maniascript/parser/-/issues/15) Report the range of parsing errors

Version 2.0.1
-------------

- [#14](https://gitlab.com/maniascript/parser/-/issues/14) Update dependencies

Version 2.0.0
-------------

- [#13](https://gitlab.com/maniascript/parser/-/issues/13) Improve parsing performances

Version 1.6.0
-------------

- [#11](https://gitlab.com/maniascript/parser/-/issues/11) Parse deprecated return statement with default type value
- [#12](https://gitlab.com/maniascript/parser/-/issues/12) Parse cast expression followed by a dot

Version 1.5.0
-------------

- [#3](https://gitlab.com/maniascript/parser/-/issues/3) Rollback to CharStreams
- [#9](https://gitlab.com/maniascript/parser/-/issues/9) Parse deprecated default type value assignment
- [#10](https://gitlab.com/maniascript/parser/-/issues/10) Update dependencies

Version 1.4.0
-------------

- Parse deprecated empty array assignment.

Version 1.3.0
-------------

- The parser can now parse `as` expressions. e.g. `This as CSmMode` or `declare CMlLabel Label_Test <=> (Page.GetFirstChild("label-test") as CMlLabel);`.

Version 1.2.0
-------------

- Parse 'is' keyword

Version 1.1.0
-------------

- Parse enum starting with library alias.

Version 1.0.1
-------------

- Fix wrong `LICENSE` file. Use the correct LGPL license now.

Version 1.0.0
-------------

- Initial release. 🎉
