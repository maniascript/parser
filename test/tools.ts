import {
  CommonTokenStream,
  CharStream,
  Token,
  ATNSimulator,
  BaseErrorListener,
  Recognizer,
  ParserRuleContext
} from 'antlr4ng'
import { ManiaScriptLexer } from '../src/antlr/ManiaScriptLexer'
import { ManiaScriptParser } from '../src/antlr/ManiaScriptParser'

class ThrowErrorListener extends BaseErrorListener {
  syntaxError (
    _recognizer: Recognizer<ATNSimulator>,
    _offendingSymbol: Token | null,
    line: number,
    charPositionInLine: number,
    msg: string
  ): void {
    throw new Error(`ParserError (${line.toString()}:${charPositionInLine.toString()}): ${msg}`)
  }
}

const getParser = (input: string): ManiaScriptParser => {
  const chars = CharStream.fromString(input)
  const lexer = new ManiaScriptLexer(chars)
  lexer.removeErrorListeners()
  lexer.addErrorListener(new ThrowErrorListener())

  const tokens = new CommonTokenStream(lexer)
  const parser = new ManiaScriptParser(tokens)
  parser.removeErrorListeners()
  parser.addErrorListener(new ThrowErrorListener())

  return parser
}

const getParseTree = (parser: ManiaScriptParser, tree: ParserRuleContext): string => {
  return tree.toStringTree(parser)
}

export {
  ThrowErrorListener,
  getParser,
  getParseTree
}
