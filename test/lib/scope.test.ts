import { describe, test, expect } from 'vitest'
import {
  AssignmentStatement,
  BinaryExpression,
  BlockStatement,
  ConditionalStatement,
  ForeachStatement,
  ForStatement,
  FunctionDeclaration,
  Kind,
  LabelDeclaration,
  SwitchStatement,
  SwitchtypeStatement,
  VariableDeclaration,
  WhileStatement
} from '../../src/lib/ast'
import { parse } from '../../src/lib/parser'
import { ScopeType } from '../../src/lib/scope'

describe('lib/score.ts', () => {
  test('find global variables', async () => {
    const result = await parse(`
      declare Integer Var_01;
      declare Real Var_02;
      declare Boolean Var_03;
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.ast.program).toBeDefined()

    if (result.ast.program !== undefined) {
      expect(result.scopeManager.scopes.length).toBe(1)

      const scope = result.scopeManager.scopes[0]
      expect(scope).toBe(result.scopeManager.getScope(result.ast.program))
      expect(scope.type).toBe(ScopeType.Global)

      expect(scope.variables.size).toBe(3)
      expect(scope.getVariable('Var_01')).not.toBeNull()
      expect(scope.getVariable('Var_01')?.name).toBe('Var_01')
      expect(scope.getVariable('Var_01')?.isGlobal).toBe(true)
      expect(scope.getVariable('Var_01')?.node).toBe((result.ast.program.declarations[0] as VariableDeclaration).name)
      expect(scope.getVariable('Var_02')).not.toBeNull()
      expect(scope.getVariable('Var_02')?.name).toBe('Var_02')
      expect(scope.getVariable('Var_02')?.isGlobal).toBe(true)
      expect(scope.getVariable('Var_02')?.node).toBe((result.ast.program.declarations[1] as VariableDeclaration).name)
      expect(scope.getVariable('Var_03')).not.toBeNull()
      expect(scope.getVariable('Var_03')?.name).toBe('Var_03')
      expect(scope.getVariable('Var_03')?.isGlobal).toBe(true)
      expect(scope.getVariable('Var_03')?.node).toBe((result.ast.program.declarations[2] as VariableDeclaration).name)
      expect(scope.getVariable('Var_04')).toBeNull()
    }
  })

  test('find function variables', async () => {
    const result = await parse(`
      Void Function_01(Integer _Param_01, Real _Param_02) {
        declare Integer FunctionVar_01;
        declare Integer FunctionVar_02;
      }
      Void Function_02() {}
      main() {
        declare Integer Var_01;
        declare Integer Var_02;
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.ast.program?.main).toBeDefined()

    if (result.ast.program?.main !== undefined) {
      expect(result.scopeManager.scopes).toHaveLength(4)

      const function01Scope = result.scopeManager.scopes[1]
      const function01Node = (result.ast.program.declarations[0] as FunctionDeclaration)
      expect(function01Scope).toBe(result.scopeManager.getScope(function01Node))
      expect(function01Scope).toBe(result.scopeManager.getScope(function01Node.body))
      expect(function01Scope).toBe(result.scopeManager.getScope(function01Node.name))
      expect(function01Scope).toBe(result.scopeManager.getScope(function01Node.parameters[0]))
      expect(function01Scope).toBe(result.scopeManager.getScope(function01Node.parameters[0].name))
      expect(function01Scope).toBe(result.scopeManager.getScope(function01Node.body.body[0]))
      expect(function01Scope.type).toBe(ScopeType.Function)

      expect(function01Scope.variables.size).toBe(4)
      expect(function01Scope.getVariable('_Param_01')).not.toBeNull()
      expect(function01Scope.getVariable('_Param_01')?.name).toBe('_Param_01')
      expect(function01Scope.getVariable('_Param_01')?.isFunctionParameter).toBe(true)
      expect(function01Scope.getVariable('_Param_01')?.node).toBe(function01Node.parameters[0].name)
      expect(function01Scope.getVariable('_Param_02')).not.toBeNull()
      expect(function01Scope.getVariable('_Param_02')?.name).toBe('_Param_02')
      expect(function01Scope.getVariable('_Param_02')?.isFunctionParameter).toBe(true)
      expect(function01Scope.getVariable('_Param_02')?.node).toBe(function01Node.parameters[1].name)
      expect(function01Scope.getVariable('FunctionVar_01')).not.toBeNull()
      expect(function01Scope.getVariable('FunctionVar_01')?.name).toBe('FunctionVar_01')
      expect(function01Scope.getVariable('FunctionVar_01')?.isFunctionParameter).toBe(false)
      expect(function01Scope.getVariable('FunctionVar_01')?.node).toBe((function01Node.body.body[0] as VariableDeclaration).name)
      expect(function01Scope.getVariable('FunctionVar_02')).not.toBeNull()
      expect(function01Scope.getVariable('FunctionVar_02')?.name).toBe('FunctionVar_02')
      expect(function01Scope.getVariable('FunctionVar_02')?.isFunctionParameter).toBe(false)
      expect(function01Scope.getVariable('FunctionVar_02')?.node).toBe((function01Node.body.body[1] as VariableDeclaration).name)

      const function02Scope = result.scopeManager.scopes[2]
      expect(function02Scope).toBe(result.scopeManager.getScope(result.ast.program.declarations[1]))
      expect(function02Scope.type).toBe(ScopeType.Function)
      expect(function02Scope.variables.size).toBe(0)

      const mainScope = result.scopeManager.scopes[3]
      expect(mainScope).toBe(result.scopeManager.getScope(result.ast.program.main))
      expect(mainScope.type).toBe(ScopeType.Function)

      expect(mainScope.variables.size).toBe(2)
      expect(mainScope.getVariable('Var_01')).not.toBeNull()
      expect(mainScope.getVariable('Var_01')?.name).toBe('Var_01')
      expect(mainScope.getVariable('Var_01')?.isFunctionParameter).toBe(false)
      expect(mainScope.getVariable('Var_01')?.node).toBe((result.ast.program.main.body.body[0] as VariableDeclaration).name)
      expect(mainScope.getVariable('Var_02')).not.toBeNull()
      expect(mainScope.getVariable('Var_02')?.name).toBe('Var_02')
      expect(mainScope.getVariable('Var_02')?.isFunctionParameter).toBe(false)
      expect(mainScope.getVariable('Var_02')?.node).toBe((result.ast.program.main.body.body[1] as VariableDeclaration).name)
    }
  })

  test('find block variables', async () => {
    const result = await parse(`
      main() {
        {
          declare Integer Var_01;
          declare Integer Var_02;
        }
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(3)

    const block = result.ast.program?.main?.body.body[0] as BlockStatement
    expect(block).toBeDefined()
    expect(block.kind).toBe(Kind.BlockStatement)

    const blockScope = result.scopeManager.scopes[2]
    expect(blockScope).toBe(result.scopeManager.getScope(block))
    expect(blockScope).toBe(result.scopeManager.getScope(block.body[0]))
    expect(blockScope).toBe(result.scopeManager.getScope(block.body[1]))
    expect(blockScope.type).toBe(ScopeType.Block)

    expect(blockScope.variables.size).toBe(2)
    expect(blockScope.getVariable('Var_01')).not.toBeNull()
    expect(blockScope.getVariable('Var_01')?.node).toBe((block.body[0] as VariableDeclaration).name)
    expect(blockScope.getVariable('Var_02')).not.toBeNull()
    expect(blockScope.getVariable('Var_02')?.node).toBe((block.body[1] as VariableDeclaration).name)
    expect(blockScope.getVariable('Var_03')).toBeNull()
  })

  test('find for loop value', async () => {
    const result = await parse(`
      main() {
        for (Var_01, 0, 10) {
          declare Integer Var_02;
        }
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(3)

    const forNode = result.ast.program?.main?.body.body[0] as ForStatement
    expect(forNode).toBeDefined()
    expect(forNode.kind).toBe(Kind.ForStatement)

    const forScope = result.scopeManager.scopes[2]
    expect(forScope).toBe(result.scopeManager.getScope(forNode))
    expect(forScope).toBe(result.scopeManager.getScope(forNode.body))
    expect(forScope).toBe(result.scopeManager.getScope((forNode.body as BlockStatement).body[0]))
    expect(forScope.type).toBe(ScopeType.For)

    expect(forScope.variables.size).toBe(2)
    expect(forScope.getVariable('Var_01')).not.toBeNull()
    expect(forScope.getVariable('Var_01')?.isForValue).toBe(true)
    expect(forScope.getVariable('Var_01')?.node).toBe(forNode.value)
    expect(forScope.getVariable('Var_02')).not.toBeNull()
    expect(forScope.getVariable('Var_02')?.isForValue).toBe(false)
    expect(forScope.getVariable('Var_02')?.node).toBe(((forNode.body as BlockStatement).body[0] as VariableDeclaration).name)
    expect(forScope.getVariable('Var_03')).toBeNull()
  })

  test('find foreach loop key and value', async () => {
    const result = await parse(`
      declare Integer[Integer] Var_03;
      main() {
        foreach (Var_01 => Var_02 in Var_03) {
          declare Integer Var_04;
        }
        foreach (Var_05 in Var_03) declare Integer Var_06;
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(4)

    const foreachNode = result.ast.program?.main?.body.body[0] as ForeachStatement
    expect(foreachNode).toBeDefined()
    expect(foreachNode.kind).toBe(Kind.ForeachStatement)

    const foreachScope = result.scopeManager.scopes[2]
    expect(foreachScope).toBe(result.scopeManager.getScope(foreachNode))
    expect(foreachScope).toBe(result.scopeManager.getScope(foreachNode.body))
    expect(foreachScope).toBe(result.scopeManager.getScope((foreachNode.body as BlockStatement).body[0]))
    expect(foreachScope.type).toBe(ScopeType.Foreach)

    expect(foreachScope.variables.size).toBe(3)
    expect(foreachScope.getVariable('Var_01')).not.toBeNull()
    expect(foreachScope.getVariable('Var_01')?.isForeachKey).toBe(true)
    expect(foreachScope.getVariable('Var_01')?.node).toBe(foreachNode.key)
    expect(foreachScope.getVariable('Var_02')).not.toBeNull()
    expect(foreachScope.getVariable('Var_02')?.isForeachValue).toBe(true)
    expect(foreachScope.getVariable('Var_02')?.node).toBe(foreachNode.value)
    expect(foreachScope.getVariable('Var_03')).not.toBeNull()
    expect(foreachScope.getVariable('Var_03')?.isGlobal).toBe(true)
    expect(foreachScope.getVariable('Var_03')?.node).toBe((result.ast.program?.declarations[0] as VariableDeclaration).name)
    expect(foreachScope.getVariable('Var_04')).not.toBeNull()
    expect(foreachScope.getVariable('Var_04')?.isForeachKey).toBe(false)
    expect(foreachScope.getVariable('Var_04')?.isForeachValue).toBe(false)
    expect(foreachScope.getVariable('Var_04')?.node).toBe(((foreachNode.body as BlockStatement).body[0] as VariableDeclaration).name)
    expect(foreachScope.getVariable('Var_05')).toBeNull()
    expect(foreachScope.getVariable('Var_06')).toBeNull()

    const foreachNode2 = result.ast.program?.main?.body.body[1] as ForeachStatement
    expect(foreachNode2).toBeDefined()
    expect(foreachNode2.kind).toBe(Kind.ForeachStatement)

    const foreachScope2 = result.scopeManager.scopes[3]
    expect(foreachScope2.type).toBe(ScopeType.Foreach)
    expect(foreachScope2.variables.size).toBe(2)

    expect(foreachScope2.getVariable('Var_01')).toBeNull()
    expect(foreachScope2.getVariable('Var_02')).toBeNull()
    expect(foreachScope2.getVariable('Var_03')).not.toBeNull()
    expect(foreachScope2.getVariable('Var_03')?.isGlobal).toBe(true)
    expect(foreachScope2.getVariable('Var_04')).toBeNull()
    expect(foreachScope2.getVariable('Var_05')).not.toBeNull()
    expect(foreachScope2.getVariable('Var_05')?.isForeachValue).toBe(true)
    expect(foreachScope2.getVariable('Var_05')?.node).toBe(foreachNode2.value)
    expect(foreachScope2.getVariable('Var_06')).not.toBeNull()
    expect(foreachScope2.getVariable('Var_06')?.isForeachValue).toBe(false)
    expect(foreachScope2.getVariable('Var_06')?.node).toBe((foreachNode2.body as VariableDeclaration).name)
  })

  test('find while variables', async () => {
    const result = await parse(`
      main() {
        while (True) {
          declare Integer Var_01;
        }
        while (True) declare Integer Var_02;
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(4)

    const whileNode = result.ast.program?.main?.body.body[0] as WhileStatement
    expect(whileNode).toBeDefined()
    expect(whileNode.kind).toBe(Kind.WhileStatement)

    const whileScope = result.scopeManager.scopes[2]
    expect(whileScope).toBe(result.scopeManager.getScope(whileNode))
    expect(whileScope).toBe(result.scopeManager.getScope(whileNode.body))
    expect(whileScope).toBe(result.scopeManager.getScope((whileNode.body as BlockStatement).body[0]))
    expect(whileScope.type).toBe(ScopeType.While)

    expect(whileScope.variables.size).toBe(1)
    expect(whileScope.getVariable('Var_01')).not.toBeNull()
    expect(whileScope.getVariable('Var_01')?.node).toBe(((whileNode.body as BlockStatement).body[0] as VariableDeclaration).name)
    expect(whileScope.getVariable('Var_02')).toBeNull()

    const whileNode2 = result.ast.program?.main?.body.body[1] as WhileStatement
    expect(whileNode2).toBeDefined()
    expect(whileNode2.kind).toBe(Kind.WhileStatement)

    const whileScope2 = result.scopeManager.scopes[3]
    expect(whileScope2.type).toBe(ScopeType.While)

    expect(whileScope2.variables.size).toBe(1)
    expect(whileScope2.getVariable('Var_01')).toBeNull()
    expect(whileScope2.getVariable('Var_02')).not.toBeNull()
    expect(whileScope2.getVariable('Var_02')?.node).toBe((whileNode2.body as VariableDeclaration).name)
  })

  test('find meanwhile variables', async () => {
    const result = await parse(`
      main() {
        meanwhile (True) {
          declare Integer Var_01;
        }
        meanwhile (True) declare Integer Var_02;
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(4)

    const meanwhileNode = result.ast.program?.main?.body.body[0] as WhileStatement
    expect(meanwhileNode).toBeDefined()
    expect(meanwhileNode.kind).toBe(Kind.MeanwhileStatement)

    const meanwhileScope = result.scopeManager.scopes[2]
    expect(meanwhileScope).toBe(result.scopeManager.getScope(meanwhileNode))
    expect(meanwhileScope).toBe(result.scopeManager.getScope(meanwhileNode.body))
    expect(meanwhileScope).toBe(result.scopeManager.getScope((meanwhileNode.body as BlockStatement).body[0]))
    expect(meanwhileScope.type).toBe(ScopeType.Meanwhile)

    expect(meanwhileScope.variables.size).toBe(1)
    expect(meanwhileScope.getVariable('Var_01')).not.toBeNull()
    expect(meanwhileScope.getVariable('Var_01')?.node).toBe(((meanwhileNode.body as BlockStatement).body[0] as VariableDeclaration).name)
    expect(meanwhileScope.getVariable('Var_02')).toBeNull()

    const meanwhileNode2 = result.ast.program?.main?.body.body[1] as WhileStatement
    expect(meanwhileNode2).toBeDefined()
    expect(meanwhileNode2.kind).toBe(Kind.MeanwhileStatement)

    const meanwhileScope2 = result.scopeManager.scopes[3]
    expect(meanwhileScope2.type).toBe(ScopeType.Meanwhile)

    expect(meanwhileScope2.variables.size).toBe(1)
    expect(meanwhileScope2.getVariable('Var_01')).toBeNull()
    expect(meanwhileScope2.getVariable('Var_02')).not.toBeNull()
    expect(meanwhileScope2.getVariable('Var_02')?.node).toBe((meanwhileNode2.body as VariableDeclaration).name)
  })

  test('find switch case variables', async () => {
    const result = await parse(`
      declare Integer Var_01;
      main() {
        switch (Var_01) {
          case 0: {
            declare Integer Var_02;
          }
          case 1: declare Integer Var_03;
          default: {
            declare Integer Var_04;
          }
        }
        switchtype (Var_01) {
          case CSmMode: declare Integer Var_05;
          case CSmPlayer: { declare Integer Var_06; }
          default: declare Integer Var_07;
        }
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(8)

    const switchNode = result.ast.program?.main?.body.body[0] as SwitchStatement
    expect(switchNode).toBeDefined()
    expect(switchNode.kind).toBe(Kind.SwitchStatement)

    const switchCase0 = result.scopeManager.scopes[2]
    expect(switchCase0).toBe(result.scopeManager.getScope(switchNode.cases[0]))
    expect(switchCase0).toBe(result.scopeManager.getScope(switchNode.cases[0].consequent))
    expect(switchCase0).toBe(result.scopeManager.getScope((switchNode.cases[0].consequent as BlockStatement).body[0]))
    expect(switchCase0.type).toBe(ScopeType.SwitchCase)

    expect(switchCase0.variables.size).toBe(1)
    expect(switchCase0.getVariable('Var_01')).not.toBeNull()
    expect(switchCase0.getVariable('Var_01')?.node).toBe((result.ast.program?.declarations[0] as VariableDeclaration).name)
    expect(switchCase0.getVariable('Var_02')).not.toBeNull()
    expect(switchCase0.getVariable('Var_02')?.node).toBe(((switchNode.cases[0].consequent as BlockStatement).body[0] as VariableDeclaration).name)
    expect(switchCase0.getVariable('Var_03')).toBeNull()
    expect(switchCase0.getVariable('Var_04')).toBeNull()
    expect(switchCase0.getVariable('Var_05')).toBeNull()
    expect(switchCase0.getVariable('Var_06')).toBeNull()
    expect(switchCase0.getVariable('Var_07')).toBeNull()

    const switchCase1 = result.scopeManager.scopes[3]
    expect(switchCase1).toBe(result.scopeManager.getScope(switchNode.cases[1]))
    expect(switchCase1).toBe(result.scopeManager.getScope(switchNode.cases[1].consequent))
    expect(switchCase1.type).toBe(ScopeType.SwitchCase)

    expect(switchCase1.variables.size).toBe(1)
    expect(switchCase1.getVariable('Var_01')).not.toBeNull()
    expect(switchCase1.getVariable('Var_02')).toBeNull()
    expect(switchCase1.getVariable('Var_03')).not.toBeNull()
    expect(switchCase1.getVariable('Var_03')?.node).toBe((switchNode.cases[1].consequent as VariableDeclaration).name)
    expect(switchCase1.getVariable('Var_04')).toBeNull()
    expect(switchCase1.getVariable('Var_05')).toBeNull()
    expect(switchCase1.getVariable('Var_06')).toBeNull()
    expect(switchCase1.getVariable('Var_07')).toBeNull()

    const switchCase2 = result.scopeManager.scopes[4]
    expect(switchCase2).toBe(result.scopeManager.getScope(switchNode.cases[2]))
    expect(switchCase2).toBe(result.scopeManager.getScope(switchNode.cases[2].consequent))
    expect(switchCase2.type).toBe(ScopeType.SwitchCase)

    expect(switchCase2.variables.size).toBe(1)
    expect(switchCase2.getVariable('Var_01')).not.toBeNull()
    expect(switchCase2.getVariable('Var_02')).toBeNull()
    expect(switchCase2.getVariable('Var_03')).toBeNull()
    expect(switchCase2.getVariable('Var_04')).not.toBeNull()
    expect(switchCase2.getVariable('Var_04')?.node).toBe(((switchNode.cases[2].consequent as BlockStatement).body[0] as VariableDeclaration).name)
    expect(switchCase2.getVariable('Var_05')).toBeNull()
    expect(switchCase2.getVariable('Var_06')).toBeNull()
    expect(switchCase2.getVariable('Var_07')).toBeNull()

    const switchtypeNode = result.ast.program?.main?.body.body[1] as SwitchtypeStatement
    expect(switchtypeNode).toBeDefined()
    expect(switchtypeNode.kind).toBe(Kind.SwitchtypeStatement)

    const switchtypeCase0 = result.scopeManager.scopes[5]
    expect(switchtypeCase0).toBe(result.scopeManager.getScope(switchtypeNode.cases[0]))
    expect(switchtypeCase0).toBe(result.scopeManager.getScope(switchtypeNode.cases[0].consequent))
    expect(switchtypeCase0.type).toBe(ScopeType.SwitchCase)

    expect(switchtypeCase0.variables.size).toBe(1)
    expect(switchtypeCase0.getVariable('Var_01')).not.toBeNull()
    expect(switchtypeCase0.getVariable('Var_02')).toBeNull()
    expect(switchtypeCase0.getVariable('Var_03')).toBeNull()
    expect(switchtypeCase0.getVariable('Var_04')).toBeNull()
    expect(switchtypeCase0.getVariable('Var_05')).not.toBeNull()
    expect(switchtypeCase0.getVariable('Var_05')?.node).toBe((switchtypeNode.cases[0].consequent as VariableDeclaration).name)
    expect(switchtypeCase0.getVariable('Var_06')).toBeNull()
    expect(switchtypeCase0.getVariable('Var_07')).toBeNull()

    const switchtypeCase1 = result.scopeManager.scopes[6]
    expect(switchtypeCase1).toBe(result.scopeManager.getScope(switchtypeNode.cases[1]))
    expect(switchtypeCase1).toBe(result.scopeManager.getScope(switchtypeNode.cases[1].consequent))
    expect(switchtypeCase1).toBe(result.scopeManager.getScope((switchtypeNode.cases[1].consequent as BlockStatement).body[0]))
    expect(switchtypeCase1.type).toBe(ScopeType.SwitchCase)

    expect(switchtypeCase1.variables.size).toBe(1)
    expect(switchtypeCase1.getVariable('Var_01')).not.toBeNull()
    expect(switchtypeCase1.getVariable('Var_02')).toBeNull()
    expect(switchtypeCase1.getVariable('Var_03')).toBeNull()
    expect(switchtypeCase1.getVariable('Var_04')).toBeNull()
    expect(switchtypeCase1.getVariable('Var_05')).toBeNull()
    expect(switchtypeCase1.getVariable('Var_06')).not.toBeNull()
    expect(switchtypeCase1.getVariable('Var_06')?.node).toBe(((switchtypeNode.cases[1].consequent as BlockStatement).body[0] as VariableDeclaration).name)
    expect(switchtypeCase1.getVariable('Var_07')).toBeNull()

    const switchtypeCase2 = result.scopeManager.scopes[7]
    expect(switchtypeCase2).toBe(result.scopeManager.getScope(switchtypeNode.cases[2]))
    expect(switchtypeCase2).toBe(result.scopeManager.getScope(switchtypeNode.cases[2].consequent))
    expect(switchtypeCase2.type).toBe(ScopeType.SwitchCase)

    expect(switchtypeCase2.variables.size).toBe(1)
    expect(switchtypeCase2.getVariable('Var_01')).not.toBeNull()
    expect(switchtypeCase2.getVariable('Var_02')).toBeNull()
    expect(switchtypeCase2.getVariable('Var_03')).toBeNull()
    expect(switchtypeCase2.getVariable('Var_04')).toBeNull()
    expect(switchtypeCase2.getVariable('Var_05')).toBeNull()
    expect(switchtypeCase2.getVariable('Var_06')).toBeNull()
    expect(switchtypeCase2.getVariable('Var_07')).not.toBeNull()
    expect(switchtypeCase2.getVariable('Var_07')?.node).toBe((switchtypeNode.cases[2].consequent as VariableDeclaration).name)
  })

  test('find conditional branches variables', async () => {
    const result = await parse(`
      main() {
        if (True) {
          declare Integer Var_01;
        } else if (True) declare Integer Var_02;
        else {
          declare Integer Var_03;
        }
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(5)

    const conditionalNode = result.ast.program?.main?.body.body[0] as ConditionalStatement
    expect(conditionalNode).toBeDefined()
    expect(conditionalNode.kind).toBe(Kind.ConditionalStatement)

    const conditionalBranch0 = result.scopeManager.scopes[2]
    expect(conditionalBranch0).toBe(result.scopeManager.getScope(conditionalNode.branches[0]))
    expect(conditionalBranch0).toBe(result.scopeManager.getScope(conditionalNode.branches[0].consequent))
    expect(conditionalBranch0).toBe(result.scopeManager.getScope((conditionalNode.branches[0].consequent as BlockStatement).body[0]))
    expect(conditionalBranch0.type).toBe(ScopeType.ConditionalBranch)

    expect(conditionalBranch0.variables.size).toBe(1)
    expect(conditionalBranch0.getVariable('Var_01')).not.toBeNull()
    expect(conditionalBranch0.getVariable('Var_01')?.node).toBe(((conditionalNode.branches[0].consequent as BlockStatement).body[0] as VariableDeclaration).name)
    expect(conditionalBranch0.getVariable('Var_02')).toBeNull()
    expect(conditionalBranch0.getVariable('Var_03')).toBeNull()

    const conditionalBranch1 = result.scopeManager.scopes[3]
    expect(conditionalBranch1).toBe(result.scopeManager.getScope(conditionalNode.branches[1]))
    expect(conditionalBranch1).toBe(result.scopeManager.getScope(conditionalNode.branches[1].consequent))
    expect(conditionalBranch1.type).toBe(ScopeType.ConditionalBranch)

    expect(conditionalBranch1.variables.size).toBe(1)
    expect(conditionalBranch1.getVariable('Var_01')).toBeNull()
    expect(conditionalBranch1.getVariable('Var_02')).not.toBeNull()
    expect(conditionalBranch1.getVariable('Var_02')?.node).toBe((conditionalNode.branches[1].consequent as VariableDeclaration).name)
    expect(conditionalBranch1.getVariable('Var_03')).toBeNull()

    const conditionalBranch2 = result.scopeManager.scopes[4]
    expect(conditionalBranch2).toBe(result.scopeManager.getScope(conditionalNode.branches[2]))
    expect(conditionalBranch2).toBe(result.scopeManager.getScope(conditionalNode.branches[2].consequent))
    expect(conditionalBranch2).toBe(result.scopeManager.getScope((conditionalNode.branches[2].consequent as BlockStatement).body[0]))
    expect(conditionalBranch2.type).toBe(ScopeType.ConditionalBranch)

    expect(conditionalBranch2.variables.size).toBe(1)
    expect(conditionalBranch2.getVariable('Var_01')).toBeNull()
    expect(conditionalBranch2.getVariable('Var_02')).toBeNull()
    expect(conditionalBranch2.getVariable('Var_03')).not.toBeNull()
    expect(conditionalBranch2.getVariable('Var_03')?.node).toBe(((conditionalNode.branches[2].consequent as BlockStatement).body[0] as VariableDeclaration).name)
  })

  test('can trace back to the global scope', async () => {
    const result = await parse(`
      declare Integer Var_00;
      main() {
        declare Boolean Var_01;
        if (Var_01) {
          declare Integer Var_02;
          for (Var_03, 0, 10) {
            declare Integer Var_04;
            {
              declare Integer Var_05;
              {
                declare Integer Var_06;
              }
              declare Integer Var_07;
            }
            declare Integer Var_08;
          }
          declare Integer Var_09;
        }
        declare Integer Var_10;
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    const program = result.ast.program
    expect(program).toBeDefined()
    const main = program?.main
    expect(main).toBeDefined()
    const ifBranch0 = (main?.body.body[1] as ConditionalStatement).branches[0]
    expect(ifBranch0).toBeDefined()
    const forStatement = (ifBranch0.consequent as BlockStatement).body[1] as ForStatement
    expect(forStatement).toBeDefined()
    const outerBlock = (forStatement.body as BlockStatement).body[1] as BlockStatement
    expect(outerBlock).toBeDefined()
    const innerBlock = outerBlock.body[1] as BlockStatement
    expect(innerBlock).toBeDefined()

    if (
      program !== undefined &&
      main !== undefined
    ) {
      expect(program.kind).toBe(Kind.Program)
      expect(main.kind).toBe(Kind.Main)
      expect(ifBranch0.kind).toBe(Kind.ConditionalBranch)
      expect(forStatement.kind).toBe(Kind.ForStatement)
      expect(outerBlock.kind).toBe(Kind.BlockStatement)
      expect(innerBlock.kind).toBe(Kind.BlockStatement)

      expect(result.scopeManager.scopes).toHaveLength(6)

      expect(result.scopeManager.getScope(program)?.variables.size).toBe(1)
      expect(result.scopeManager.getScope(program)?.getVariable('Var_00')).not.toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_01')).toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_02')).toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_03')).toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_04')).toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_05')).toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_06')).toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_07')).toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_08')).toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_09')).toBeNull()
      expect(result.scopeManager.getScope(program)?.getVariable('Var_10')).toBeNull()

      expect(result.scopeManager.getScope(main)?.variables.size).toBe(2)
      expect(result.scopeManager.getScope(main)?.getVariable('Var_00')).not.toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_01')).not.toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_02')).toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_03')).toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_04')).toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_05')).toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_06')).toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_07')).toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_08')).toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_09')).toBeNull()
      expect(result.scopeManager.getScope(main)?.getVariable('Var_10')).not.toBeNull()

      expect(result.scopeManager.getScope(ifBranch0)?.variables.size).toBe(2)
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_00')).not.toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_01')).not.toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_02')).not.toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_03')).toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_04')).toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_05')).toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_06')).toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_07')).toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_08')).toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_09')).not.toBeNull()
      expect(result.scopeManager.getScope(ifBranch0)?.getVariable('Var_10')).not.toBeNull()

      expect(result.scopeManager.getScope(forStatement)?.variables.size).toBe(3)
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_00')).not.toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_01')).not.toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_02')).not.toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_03')).not.toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_04')).not.toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_05')).toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_06')).toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_07')).toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_08')).not.toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_09')).not.toBeNull()
      expect(result.scopeManager.getScope(forStatement)?.getVariable('Var_10')).not.toBeNull()

      expect(result.scopeManager.getScope(outerBlock)?.variables.size).toBe(2)
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_00')).not.toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_01')).not.toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_02')).not.toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_03')).not.toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_04')).not.toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_05')).not.toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_06')).toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_07')).not.toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_08')).not.toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_09')).not.toBeNull()
      expect(result.scopeManager.getScope(outerBlock)?.getVariable('Var_10')).not.toBeNull()

      expect(result.scopeManager.getScope(innerBlock)?.variables.size).toBe(1)
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_00')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_01')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_02')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_03')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_04')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_05')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_06')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_07')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_08')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_09')).not.toBeNull()
      expect(result.scopeManager.getScope(innerBlock)?.getVariable('Var_10')).not.toBeNull()
    }
  })

  test('manage variable alias', async () => {
    const result = await parse(`
      main() {
        declare Boolean Var_01 as Var_02 for This;
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(2)
    expect(result.scopeManager.scopes[1].getVariable('Var_01')).toBeNull()
    expect(result.scopeManager.scopes[1].getVariable('Var_02')).not.toBeNull()
  })

  test('manage variable references', async () => {
    const result = await parse(`
      main() {
        declare Integer Var_01;
        Var_01 = 10;
        declare Integer Var_02;
        Var_02 = Var_01 + 10;
        Var_03 = [10];
        declare Integer[] Var_03;
        foreach (Var_04 in Var_03) {
          Var_01 += 1;
        }
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(3)

    const mainScope = result.scopeManager.scopes[1]
    expect(mainScope).toBeDefined()
    expect(mainScope.references).toHaveLength(4)

    expect(mainScope.references[0]?.variable).toBe(mainScope.getVariable('Var_01'))
    expect(mainScope.references[0]?.node).toBe((result.ast.program?.main?.body.body[1] as AssignmentStatement).left)
    expect(mainScope.references[1]?.variable).toBe(mainScope.getVariable('Var_02'))
    expect(mainScope.references[1]?.node).toBe((result.ast.program?.main?.body.body[3] as AssignmentStatement).left)
    expect(mainScope.references[2]?.variable).toBe(mainScope.getVariable('Var_01'))
    expect(mainScope.references[2]?.node).toBe(((result.ast.program?.main?.body.body[3] as AssignmentStatement).right as BinaryExpression).left)
    expect(mainScope.references[3]?.variable).toBeNull()
    expect(mainScope.references[3]?.node).toBe((result.ast.program?.main?.body.body[4] as AssignmentStatement).left)

    expect(mainScope.getVariable('Var_01')?.references).toHaveLength(3)
    expect(mainScope.getVariable('Var_02')?.references).toHaveLength(1)
    expect(mainScope.getVariable('Var_03')?.references).toHaveLength(1)

    const forScope = result.scopeManager.scopes[2]
    expect(forScope).toBeDefined()
    expect(forScope.references).toHaveLength(2)

    expect(forScope.references[0]?.variable).toBe(forScope.getVariable('Var_03'))
    expect(forScope.references[0]?.node).toBe((result.ast.program?.main?.body.body[6] as ForeachStatement).expression)
    expect(forScope.references[1]?.variable).toBe(forScope.getVariable('Var_01'))
    expect(forScope.references[1]?.node).toBe((((result.ast.program?.main?.body.body[6] as ForeachStatement).body as BlockStatement).body[0] as AssignmentStatement).left)

    expect(forScope.getVariable('Var_04')?.references).toHaveLength(0)
    expect(forScope.getVariable('Var_01')?.references).toHaveLength(3)
  })

  test('manage label references', async () => {
    const result = await parse(`
      Void Function_01() {
        +++Label_01+++
      }
      Void Function_02() {

      }
      main() {
        {
          ---Label_02---
          {

          }
        }
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(6)

    expect(result.scopeManager.scopes[0].couldReferenceVariableInLabel).toBe(true)
    expect(result.scopeManager.scopes[1].couldReferenceVariableInLabel).toBe(true)
    expect(result.scopeManager.scopes[2].couldReferenceVariableInLabel).toBe(false)
    expect(result.scopeManager.scopes[3].couldReferenceVariableInLabel).toBe(true)
    expect(result.scopeManager.scopes[4].couldReferenceVariableInLabel).toBe(true)
    expect(result.scopeManager.scopes[5].couldReferenceVariableInLabel).toBe(false)
  })

  test('find trait variables', async () => {
    const result = await parse(`
      main() {
        declare Integer Var_01;
        declare Integer Var_02 for This;
      }
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(2)

    expect(result.scopeManager.scopes[1].getVariable('Var_01')).not.toBeNull()
    expect(result.scopeManager.scopes[1].getVariable('Var_01')?.isTrait).toBe(false)
    expect(result.scopeManager.scopes[1].getVariable('Var_02')).not.toBeNull()
    expect(result.scopeManager.scopes[1].getVariable('Var_02')?.isTrait).toBe(true)
  })

  test('find label declaration variables', async () => {
    const result = await parse(`
      declare Integer G_A;

      ***Label_01***
      ***
      declare Integer B;
      {
        declare Integer C;
      }
      ***

      Void FunctionD() {}
    `, { buildScopes: true })

    expect(result.success).toBe(true)
    expect(result.scopeManager.scopes).toHaveLength(4)
    expect(result.scopeManager.scopes[0].variables.size).toBe(1)
    expect(result.scopeManager.scopes[1].variables.size).toBe(1)
    expect(result.scopeManager.scopes[2].variables.size).toBe(1)
    expect(result.scopeManager.scopes[3].variables.size).toBe(0)

    expect(result.scopeManager.scopes[1].node).toBe(result.ast.program?.declarations[1])
    expect(result.scopeManager.scopes[1].children).toHaveLength(1)
    expect(result.scopeManager.scopes[1].children[0].node).toBe((result.ast.program?.declarations[1] as LabelDeclaration).body[1])
  })
})
