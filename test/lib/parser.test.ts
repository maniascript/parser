import { describe, test, expect, beforeAll } from 'vitest'
import { parse, type ParseResult } from '../../src/lib/parser'
import { ProgramContext } from '../../src/antlr/ManiaScriptParser'
import { SourceLocationRange } from '../../src/lib/position'

const inputCorrect = `
main() {
  declare Integer Test = 0;
  log(Now^"> Test : "^Test);
  assert(Test == 0);
}
`
const inputIncorrect = `
main() {
  declare Integer Test = ;
  declare Integer Test = 0
  switchtype (Test) {
    case CFakeClass: log("error");
  }
}
`

let resultCorrect: ParseResult
let resultIncorrect: ParseResult

describe('lib/parser', () => {
  beforeAll(async () => {
    [resultCorrect, resultIncorrect] = await Promise.all([
      parse(inputCorrect),
      parse(inputIncorrect)
    ])
  })

  describe('parse()', () => {
    test('returns a parsing report', () => {
      for (const result of [resultCorrect, resultIncorrect]) {
        expect('success' in result).toBe(true)
        expect(typeof result.success === 'boolean').toBe(true)
        expect('errors' in result).toBe(true)
        expect(Array.isArray(result.errors)).toBe(true)
        expect('tree' in result).toBe(true)
        expect(result.tree instanceof ProgramContext).toBe(true)
      }
    })

    test('reports success status', () => {
      expect(resultCorrect.success).toBe(true)
      expect(resultIncorrect.success).toBe(false)
    })

    test('reports errors', () => {
      expect(resultCorrect.errors.length).toBe(0)
      expect(resultIncorrect.errors.length).toBe(4)
    })

    test('has valid error object', () => {
      const error = resultIncorrect.errors[0]
      expect('source' in error).toBe(true)
      expect(error.source instanceof SourceLocationRange).toBe(true)
      expect('message' in error).toBe(true)
      expect(typeof error.message === 'string').toBe(true)
    })

    test('reports correct source location, range and message', () => {
      let error = resultIncorrect.errors[0]
      expect(error.source.loc.start.line).toBe(3)
      expect(error.source.loc.start.column).toBe(23)
      expect(error.source.loc.end.line).toBe(3)
      expect(error.source.loc.end.column).toBe(26)
      expect(error.source.range.start).toBe(33)
      expect(error.source.range.end).toBe(35)
      expect(error.message).toMatch(/mismatched input ';' expecting/)

      error = resultIncorrect.errors[1]
      expect(error.source.loc.start.line).toBe(4)
      expect(error.source.loc.start.column).toBe(2)
      expect(error.source.loc.end.line).toBe(5)
      expect(error.source.loc.end.column).toBe(12)
      expect(error.source.range.start).toBe(39)
      expect(error.source.range.end).toBe(75)
      expect(error.message).toMatch(/missing ';' at 'switchtype'/)

      error = resultIncorrect.errors[2]
      expect(error.source.loc.start.line).toBe(6)
      expect(error.source.loc.start.column).toBe(4)
      expect(error.source.loc.end.line).toBe(6)
      expect(error.source.loc.end.column).toBe(19)
      expect(error.source.range.start).toBe(90)
      expect(error.source.range.end).toBe(104)
      expect(error.message).toMatch(/no viable alternative at input 'case CFakeClass'/)

      error = resultIncorrect.errors[3]
      expect(error.source.loc.start.line).toBe(2)
      expect(error.source.loc.start.column).toBe(0)
      expect(error.source.loc.end.line).toBe(8)
      expect(error.source.loc.end.column).toBe(1)
      expect(error.source.range.start).toBe(1)
      expect(error.source.range.end).toBe(125)
      expect(error.message).toMatch(/extraneous input '}' expecting <EOF>/)
    })

    test('parse with custom classes', async () => {
      const result = await parse(`
        main() {
          declare CNod Test1;
          declare CCustomClass Test2 = Test1 as CCustomClass;
        }
      `, {
        lexerClasses: new Set(['CCustomClass', 'CAnotherClass'])
      })
      expect(result.success).toBe(true)
    })

    test('parse with a custom doc.h file', async () => {
      const result = await parse(`
        main() {
          declare CNod Test1;
          declare CCustomClass Test2 = Test1 as CCustomClass;
        }
      `, {
        msApiPath: './test/doc.h'
      })
      expect(result.success).toBe(true)
    })
  })
})
