import { describe, test, expect } from 'vitest'
import { parse } from '../../src/lib/parser'
import { SettingSymbol, CommandSymbol, StructureSymbol, ConstantSymbol, VariableSymbol, NamespaceSymbol, FunctionSymbol, SymbolTable, ParameterSymbol, BlockSymbol, LabelBlockSymbol, LabelSymbol, ControlFlowSymbol, SymbolKind } from '../../src/lib/symbol-table'
import { getUnknownType } from '../../src/lib/type'

describe('lib/symbol-table.ts', () => {
  describe('Generate symbol for constant', () => {
    test('Constant declaration', async () => {
      const result = await parse(`
        #Const C_Test_01 "1"
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const symbol0 = result.symbolTable.children[0]
        expect(symbol0).toBeInstanceOf(ConstantSymbol)
        expect(symbol0.kind).toBe(SymbolKind.Constant)
        if (symbol0 instanceof ConstantSymbol) {
          expect(symbol0.name).toBe('C_Test_01')
          expect(symbol0.source.range.start).toBe(9)
          expect(symbol0.source.range.end).toBe(28)
          expect(symbol0.type).toEqual(getUnknownType())
        }
      }
    })

    test('Constant aliasing', async () => {
      const result = await parse(`
        #Const Lib1::C_TestLib_01 as C_Test_01
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const symbol0 = result.symbolTable.children[0]
        expect(symbol0).toBeInstanceOf(ConstantSymbol)
        expect(symbol0.kind).toBe(SymbolKind.Constant)
        if (symbol0 instanceof ConstantSymbol) {
          expect(symbol0.name).toBe('C_Test_01')
          expect(symbol0.source.range.start).toBe(9)
          expect(symbol0.source.range.end).toBe(46)
          expect(symbol0.type).toEqual(getUnknownType())
        }
      }
    })
  })

  describe('Generate symbol for structure', () => {
    test('Structure declaration', async () => {
      const result = await parse(`
        #Struct K_Struct_01 { Integer Member_01; }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const struct_01 = result.symbolTable.children[0]
        expect(struct_01).toBeInstanceOf(StructureSymbol)
        expect(struct_01.kind).toBe(SymbolKind.Structure)
        if (struct_01 instanceof StructureSymbol) {
          expect(struct_01.name).toBe('K_Struct_01')
          expect(struct_01.source.range.start).toBe(9)
          expect(struct_01.source.range.end).toBe(50)
        }
      }
    })

    test('Structure aliasing', async () => {
      const result = await parse(`
        #Struct Lib1::K_Struct_01 as K_StructAlias_01
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const structAlias_01 = result.symbolTable.children[0]
        expect(structAlias_01).toBeInstanceOf(StructureSymbol)
        expect(structAlias_01.kind).toBe(SymbolKind.Structure)
        if (structAlias_01 instanceof StructureSymbol) {
          expect(structAlias_01.name).toBe('K_StructAlias_01')
          expect(structAlias_01.source.range.start).toBe(9)
          expect(structAlias_01.source.range.end).toBe(53)
        }
      }
    })
  })

  describe('Generate symbol for setting', () => {
    test('Setting declaration', async () => {
      const result = await parse(`
        #Setting S_Setting_01 1
        #Setting S_Setting_02 "2" as _("Setting_02 description")
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(2)

        const setting_01 = result.symbolTable.children[0]
        expect(setting_01).toBeInstanceOf(SettingSymbol)
        expect(setting_01.kind).toBe(SymbolKind.Setting)
        if (setting_01 instanceof SettingSymbol) {
          expect(setting_01.name).toBe('S_Setting_01')
          expect(setting_01.source.range.start).toBe(9)
          expect(setting_01.source.range.end).toBe(31)
          expect(setting_01.type).toEqual(getUnknownType())
        }

        const setting_02 = result.symbolTable.children[1]
        expect(setting_02).toBeInstanceOf(SettingSymbol)
        expect(setting_02.kind).toBe(SymbolKind.Setting)
        if (setting_02 instanceof SettingSymbol) {
          expect(setting_02.name).toBe('S_Setting_02')
          expect(setting_02.source.range.start).toBe(41)
          expect(setting_02.source.range.end).toBe(96)
          expect(setting_02.type).toEqual(getUnknownType())
        }
      }
    })
  })

  describe('Generate symbol for command', () => {
    test('Command declaration', async () => {
      const result = await parse(`
        #Command Command_01 (Integer)
        #Command Command_02 (Text) as _("Command_02 description")
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(2)

        const command_01 = result.symbolTable.children[0]
        expect(command_01).toBeInstanceOf(CommandSymbol)
        expect(command_01.kind).toBe(SymbolKind.Command)
        if (command_01 instanceof CommandSymbol) {
          expect(command_01.name).toBe('Command_01')
          expect(command_01.source.range.start).toBe(9)
          expect(command_01.source.range.end).toBe(37)
          expect(command_01.type).toEqual(getUnknownType())
        }

        const command_02 = result.symbolTable.children[1]
        expect(command_02).toBeInstanceOf(CommandSymbol)
        expect(command_02.kind).toBe(SymbolKind.Command)
        if (command_02 instanceof CommandSymbol) {
          expect(command_02.name).toBe('Command_02')
          expect(command_02.source.range.start).toBe(47)
          expect(command_02.source.range.end).toBe(103)
          expect(command_02.type).toEqual(getUnknownType())
        }
      }
    })
  })

  describe('Generate symbol for namespace', () => {
    test('Include with namespace', async () => {
      const result = await parse(`
        #Include "TextLib" as TL
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const symbol0 = result.symbolTable.children[0]
        expect(symbol0).toBeInstanceOf(NamespaceSymbol)
        expect(symbol0.kind).toBe(SymbolKind.Namespace)
        if (symbol0 instanceof NamespaceSymbol) {
          expect(symbol0.name).toBe('TL')
          expect(symbol0.source.range.start).toBe(9)
          expect(symbol0.source.range.end).toBe(32)
        }
      }
    })
    test('Include without namespace', async () => {
      const result = await parse(`
        #Include "TextLib"
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(0)
      }
    })
  })

  describe('Generate symbol for variable', () => {
    test('Variables with explicit type', async () => {
      const result = await parse(`
        declare Boolean Test_01;
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const symbol0 = result.symbolTable.children[0]
        expect(symbol0).toBeInstanceOf(VariableSymbol)
        expect(symbol0.kind).toBe(SymbolKind.Variable)
        if (symbol0 instanceof VariableSymbol) {
          expect(symbol0.name).toBe('Test_01')
          expect(symbol0.source.range.start).toBe(9)
          expect(symbol0.source.range.end).toBe(32)
          expect(symbol0.type).toEqual(getUnknownType())
        }
      }
    })

    test('Variable with implicit type', async () => {
      const result = await parse(`
        declare Test_01 = 1;
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const symbol0 = result.symbolTable.children[0]
        expect(symbol0).toBeInstanceOf(VariableSymbol)
        expect(symbol0.kind).toBe(SymbolKind.Variable)
        if (symbol0 instanceof VariableSymbol) {
          expect(symbol0.name).toBe('Test_01')
          expect(symbol0.source.range.start).toBe(9)
          expect(symbol0.source.range.end).toBe(28)
          expect(symbol0.type).toEqual(getUnknownType())
        }
      }
    })

    test('Variable declared with let', async () => {
      const result = await parse(`
        let Test_01 = 1;
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const symbol0 = result.symbolTable.children[0]
        expect(symbol0).toBeInstanceOf(VariableSymbol)
        expect(symbol0.kind).toBe(SymbolKind.Variable)
        if (symbol0 instanceof VariableSymbol) {
          expect(symbol0.name).toBe('Test_01')
          expect(symbol0.source.range.start).toBe(9)
          expect(symbol0.source.range.end).toBe(24)
          expect(symbol0.type).toEqual(getUnknownType())
        }
      }
    })

    test('Trait variables', async () => {
      const result = await parse(`
        declare Integer Test_01 for This;
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const symbol0 = result.symbolTable.children[0]
        expect(symbol0).toBeInstanceOf(VariableSymbol)
        expect(symbol0.kind).toBe(SymbolKind.Variable)
        if (symbol0 instanceof VariableSymbol) {
          expect(symbol0.name).toBe('Test_01')
          expect(symbol0.source.range.start).toBe(9)
          expect(symbol0.source.range.end).toBe(41)
          expect(symbol0.type).toEqual(getUnknownType())
        }
      }
    })
  })

  describe('Generate symbol for function', () => {
    test('Function without parameters', async () => {
      const result = await parse(`
        Void Function_01() {}
        Integer Function_02() {}
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(2)

        const function_01 = result.symbolTable.children[0]
        expect(function_01).toBeInstanceOf(FunctionSymbol)
        expect(function_01.kind).toBe(SymbolKind.Function)
        if (function_01 instanceof FunctionSymbol) {
          expect(function_01.name).toBe('Function_01')
          expect(function_01.source.range.start).toBe(9)
          expect(function_01.source.range.end).toBe(29)
          expect(function_01.type).toEqual(getUnknownType())
        }

        const function_02 = result.symbolTable.children[1]
        expect(function_02).toBeInstanceOf(FunctionSymbol)
        expect(function_02.kind).toBe(SymbolKind.Function)
        if (function_02 instanceof FunctionSymbol) {
          expect(function_02.name).toBe('Function_02')
          expect(function_02.source.range.start).toBe(39)
          expect(function_02.source.range.end).toBe(62)
          expect(function_02.type).toEqual(getUnknownType())
        }
      }
    })

    test('Function with parameters', async () => {
      const result = await parse(`
        Void Function_01(Integer _Param_01) {}
        Integer Function_02(Real _Param_02, Text _Param_03, Vec2 _Param_04) {}
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(2)

        const function_01 = result.symbolTable.children[0]
        expect(function_01).toBeInstanceOf(FunctionSymbol)
        expect(function_01.kind).toBe(SymbolKind.Function)
        if (function_01 instanceof FunctionSymbol) {
          expect(function_01.children).toHaveLength(1)

          const param_01 = function_01.children[0]
          expect(param_01).toBeInstanceOf(ParameterSymbol)
          expect(param_01.kind).toBe(SymbolKind.Parameter)
          if (param_01 instanceof ParameterSymbol) {
            expect(param_01.name).toBe('_Param_01')
            expect(param_01.source.range.start).toBe(26)
            expect(param_01.source.range.end).toBe(42)
            expect(param_01.type).toEqual(getUnknownType())
          }
        }

        const function_02 = result.symbolTable.children[1]
        expect(function_02).toBeInstanceOf(FunctionSymbol)
        expect(function_02.kind).toBe(SymbolKind.Function)
        if (function_02 instanceof FunctionSymbol) {
          expect(function_02.children).toHaveLength(3)

          const param_02 = function_02.children[0]
          expect(param_02).toBeInstanceOf(ParameterSymbol)
          expect(param_02.kind).toBe(SymbolKind.Parameter)
          if (param_02 instanceof ParameterSymbol) {
            expect(param_02.name).toBe('_Param_02')
            expect(param_02.source.range.start).toBe(76)
            expect(param_02.source.range.end).toBe(89)
            expect(param_02.type).toEqual(getUnknownType())
          }

          const param_03 = function_02.children[1]
          expect(param_03).toBeInstanceOf(ParameterSymbol)
          expect(param_03.kind).toBe(SymbolKind.Parameter)
          if (param_03 instanceof ParameterSymbol) {
            expect(param_03.name).toBe('_Param_03')
            expect(param_03.source.range.start).toBe(92)
            expect(param_03.source.range.end).toBe(105)
            expect(param_03.type).toEqual(getUnknownType())
          }

          const param_04 = function_02.children[2]
          expect(param_04).toBeInstanceOf(ParameterSymbol)
          expect(param_04.kind).toBe(SymbolKind.Parameter)
          if (param_04 instanceof ParameterSymbol) {
            expect(param_04.name).toBe('_Param_04')
            expect(param_04.source.range.start).toBe(108)
            expect(param_04.source.range.end).toBe(121)
            expect(param_04.type).toEqual(getUnknownType())
          }
        }
      }
    })

    test('Function with body', async () => {
      const result = await parse(`
        Void Function_01() {
          declare Integer Variable_01;
        }
        Void Function_02(Real _Param_01, Text _Param_02) {
          declare Integer Variable_02;
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(2)

        const function_01 = result.symbolTable.children[0]
        expect(function_01).toBeInstanceOf(FunctionSymbol)
        expect(function_01.kind).toBe(SymbolKind.Function)
        if (function_01 instanceof FunctionSymbol) {
          expect(function_01.children).toHaveLength(1)

          const variable_01 = function_01.children[0]
          expect(variable_01).toBeInstanceOf(VariableSymbol)
          expect(variable_01.kind).toBe(SymbolKind.Variable)
          if (variable_01 instanceof VariableSymbol) {
            expect(variable_01.name).toBe('Variable_01')
            expect(variable_01.source.range.start).toBe(40)
            expect(variable_01.source.range.end).toBe(67)
            expect(variable_01.type).toEqual(getUnknownType())
          }
        }

        const function_02 = result.symbolTable.children[1]
        expect(function_02).toBeInstanceOf(FunctionSymbol)
        expect(function_02.kind).toBe(SymbolKind.Function)
        if (function_02 instanceof FunctionSymbol) {
          expect(function_02.children).toHaveLength(3)

          const param_01 = function_02.children[0]
          expect(param_01).toBeInstanceOf(ParameterSymbol)
          expect(param_01.kind).toBe(SymbolKind.Parameter)
          if (param_01 instanceof ParameterSymbol) {
            expect(param_01.name).toBe('_Param_01')
            expect(param_01.source.range.start).toBe(104)
            expect(param_01.source.range.end).toBe(117)
            expect(param_01.type).toEqual(getUnknownType())
          }

          const param_02 = function_02.children[1]
          expect(param_02).toBeInstanceOf(ParameterSymbol)
          expect(param_02.kind).toBe(SymbolKind.Parameter)
          if (param_02 instanceof ParameterSymbol) {
            expect(param_02.name).toBe('_Param_02')
            expect(param_02.source.range.start).toBe(120)
            expect(param_02.source.range.end).toBe(133)
            expect(param_02.type).toEqual(getUnknownType())
          }

          const variable_01 = function_02.children[2]
          expect(variable_01).toBeInstanceOf(VariableSymbol)
          expect(variable_01.kind).toBe(SymbolKind.Variable)
          if (variable_01 instanceof VariableSymbol) {
            expect(variable_01.name).toBe('Variable_02')
            expect(variable_01.source.range.start).toBe(148)
            expect(variable_01.source.range.end).toBe(175)
            expect(variable_01.type).toEqual(getUnknownType())
          }
        }
      }
    })

    test('main function', async () => {
      const result = await parse(`
        main() {
          declare Integer Variable_01;
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const variable_01 = result.symbolTable.children[0]
        expect(variable_01).toBeInstanceOf(FunctionSymbol)
        expect(variable_01.kind).toBe(SymbolKind.Function)
        if (variable_01 instanceof FunctionSymbol) {
          expect(variable_01.name).toBe('main')
          expect(variable_01.source.range.start).toBe(9)
          expect(variable_01.source.range.end).toBe(65)
          expect(variable_01.type).toEqual(getUnknownType())
        }
      }
    })
  })

  describe('Generate symbol for block', () => {
    test('Imbricated blocks', async () => {
      const result = await parse(`
        main() {
          declare Integer Variable_01;
          {
            declare Integer Variable_02;
            {
              declare Integer Variable_03;
            }
            declare Integer Variable_04;
            {
              declare Integer Variable_05;
            }
            declare Integer Variable_06;
          }
          declare Integer Variable_07;
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const main = result.symbolTable.children[0]
        expect(main).toBeInstanceOf(FunctionSymbol)
        expect(main.kind).toBe(SymbolKind.Function)
        if (main instanceof FunctionSymbol) {
          expect(main.children).toHaveLength(3)

          const block_01 = main.children[1]
          expect(block_01).toBeInstanceOf(BlockSymbol)
          expect(block_01.kind).toBe(SymbolKind.Block)
          if (block_01 instanceof BlockSymbol) {
            expect(block_01.name).toBe('')
            expect(block_01.source.range.start).toBe(67)
            expect(block_01.source.range.end).toBe(344)
            expect(block_01.children).toHaveLength(5)

            const block_02 = block_01.children[1]
            expect(block_02).toBeInstanceOf(BlockSymbol)
            expect(block_02.kind).toBe(SymbolKind.Block)
            if (block_02 instanceof BlockSymbol) {
              expect(block_02.name).toBe('')
              expect(block_02.source.range.start).toBe(122)
              expect(block_02.source.range.end).toBe(179)
              expect(block_02.children).toHaveLength(1)
            }

            const block_03 = block_01.children[3]
            expect(block_03).toBeInstanceOf(BlockSymbol)
            expect(block_03.kind).toBe(SymbolKind.Block)
            if (block_03 instanceof BlockSymbol) {
              expect(block_03.name).toBe('')
              expect(block_03.source.range.start).toBe(234)
              expect(block_03.source.range.end).toBe(291)
              expect(block_03.children).toHaveLength(1)
            }
          }
        }
      }
    })
  })

  describe('Generate symbol for label block', () => {
    test('Label declaration', async () => {
      const result = await parse(`
        ***Label_01***
        ***
        declare Integer Variable_01;
        ***

        ***Label_02***
        ***
        declare Integer Variable_02;
        ***
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(2)

        const label_01 = result.symbolTable.children[0]
        expect(label_01).toBeInstanceOf(LabelBlockSymbol)
        expect(label_01.kind).toBe(SymbolKind.LabelBlock)
        if (label_01 instanceof LabelBlockSymbol) {
          expect(label_01.name).toBe('Label_01')
          expect(label_01.source.range.start).toBe(9)
          expect(label_01.source.range.end).toBe(83)
          expect(label_01.children).toHaveLength(1)
          expect(label_01.children[0].name).toBe('Variable_01')
        }

        const label_02 = result.symbolTable.children[1]
        expect(label_02).toBeInstanceOf(LabelBlockSymbol)
        expect(label_02.kind).toBe(SymbolKind.LabelBlock)
        if (label_02 instanceof LabelBlockSymbol) {
          expect(label_02.name).toBe('Label_02')
          expect(label_02.source.range.start).toBe(94)
          expect(label_02.source.range.end).toBe(168)
          expect(label_02.children).toHaveLength(1)
          expect(label_02.children[0].name).toBe('Variable_02')
        }
      }
    })
  })

  describe('Generate symbol for label statement', () => {
    test('Insert and overwrite labels', async () => {
      const result = await parse(`
        main() {
          +++Label_01+++
          ---Label_02---
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.labels).toHaveLength(2)
        expect(result.symbolTable.children).toHaveLength(1)

        const main = result.symbolTable.children[0]
        expect(main).toBeInstanceOf(FunctionSymbol)
        expect(main.kind).toBe(SymbolKind.Function)
        if (main instanceof FunctionSymbol) {
          expect(main.name).toBe('main')
          expect(main.source.range.start).toBe(9)
          expect(main.source.range.end).toBe(76)
          expect(main.children).toHaveLength(0)
        }

        const label_01 = result.symbolTable.labels[0]
        expect(label_01).toBeInstanceOf(LabelSymbol)
        expect(label_01.kind).toBe(SymbolKind.Label)
        if (label_01 instanceof LabelSymbol) {
          expect(label_01.name).toBe('Label_01')
          expect(label_01.source.range.start).toBe(28)
          expect(label_01.source.range.end).toBe(41)
        }

        const label_02 = result.symbolTable.labels[1]
        expect(label_02).toBeInstanceOf(LabelSymbol)
        expect(label_02.kind).toBe(SymbolKind.Label)
        if (label_02 instanceof LabelSymbol) {
          expect(label_02.name).toBe('Label_02')
          expect(label_02.source.range.start).toBe(53)
          expect(label_02.source.range.end).toBe(66)
        }
      }
    })
  })

  describe('Generate symbol for flow control', () => {
    test('Foreach loop', async () => {
      const result = await parse(`
        main() {
          foreach (Value_01 in Source_01) {
            declare Integer Variable_01;
          }
          foreach (Key_02 => Value_02 in Source_02) {
            declare Integer Variable_02;
          }
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const main = result.symbolTable.children[0]
        expect(main).toBeInstanceOf(FunctionSymbol)
        expect(main.kind).toBe(SymbolKind.Function)
        if (main instanceof FunctionSymbol) {
          expect(main.name).toBe('main')
          expect(main.source.range.start).toBe(9)
          expect(main.source.range.end).toBe(230)
          expect(main.children).toHaveLength(2)

          const foreach_01 = main.children[0]
          expect(foreach_01).toBeInstanceOf(ControlFlowSymbol)
          expect(foreach_01.kind).toBe(SymbolKind.ControlFlow)
          if (foreach_01 instanceof ControlFlowSymbol) {
            expect(foreach_01.name).toBe('')
            expect(foreach_01.source.range.start).toBe(28)
            expect(foreach_01.source.range.end).toBe(113)
            expect(foreach_01.children).toHaveLength(2)

            const value_01 = foreach_01.children[0]
            expect(value_01).toBeInstanceOf(ParameterSymbol)
            expect(value_01.kind).toBe(SymbolKind.Parameter)
            if (value_01 instanceof ParameterSymbol) {
              expect(value_01.name).toBe('Value_01')
              expect(value_01.source.range.start).toBe(37)
              expect(value_01.source.range.end).toBe(44)
            }

            const variable_01 = foreach_01.children[1]
            expect(variable_01).toBeInstanceOf(VariableSymbol)
            expect(variable_01.kind).toBe(SymbolKind.Variable)
            if (variable_01 instanceof VariableSymbol) {
              expect(variable_01.name).toBe('Variable_01')
              expect(variable_01.source.range.start).toBe(74)
              expect(variable_01.source.range.end).toBe(101)
            }
          }

          const foreach_02 = main.children[1]
          expect(foreach_02).toBeInstanceOf(ControlFlowSymbol)
          expect(foreach_02.kind).toBe(SymbolKind.ControlFlow)
          if (foreach_02 instanceof ControlFlowSymbol) {
            expect(foreach_02.name).toBe('')
            expect(foreach_02.source.range.start).toBe(125)
            expect(foreach_02.source.range.end).toBe(220)
            expect(foreach_02.children).toHaveLength(3)

            const key_02 = foreach_02.children[0]
            expect(key_02).toBeInstanceOf(ParameterSymbol)
            expect(key_02.kind).toBe(SymbolKind.Parameter)
            if (key_02 instanceof ParameterSymbol) {
              expect(key_02.name).toBe('Key_02')
              expect(key_02.source.range.start).toBe(134)
              expect(key_02.source.range.end).toBe(139)
            }

            const value_02 = foreach_02.children[1]
            expect(value_02).toBeInstanceOf(ParameterSymbol)
            expect(value_02.kind).toBe(SymbolKind.Parameter)
            if (value_02 instanceof ParameterSymbol) {
              expect(value_02.name).toBe('Value_02')
              expect(value_02.source.range.start).toBe(144)
              expect(value_02.source.range.end).toBe(151)
            }

            const variable_02 = foreach_02.children[2]
            expect(variable_02).toBeInstanceOf(VariableSymbol)
            expect(variable_02.kind).toBe(SymbolKind.Variable)
            if (variable_02 instanceof VariableSymbol) {
              expect(variable_02.name).toBe('Variable_02')
              expect(variable_02.source.range.start).toBe(181)
              expect(variable_02.source.range.end).toBe(208)
            }
          }
        }
      }
    })

    test('For loop', async () => {
      const result = await parse(`
        main() {
          for (Value_01, 0, 9) {
            declare Integer Variable_01;
          }
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const main = result.symbolTable.children[0]
        expect(main).toBeInstanceOf(FunctionSymbol)
        expect(main.kind).toBe(SymbolKind.Function)
        if (main instanceof FunctionSymbol) {
          expect(main.name).toBe('main')
          expect(main.source.range.start).toBe(9)
          expect(main.source.range.end).toBe(112)
          expect(main.children).toHaveLength(1)

          const for_01 = main.children[0]
          expect(for_01).toBeInstanceOf(ControlFlowSymbol)
          expect(for_01.kind).toBe(SymbolKind.ControlFlow)
          if (for_01 instanceof ControlFlowSymbol) {
            expect(for_01.name).toBe('')
            expect(for_01.source.range.start).toBe(28)
            expect(for_01.source.range.end).toBe(102)
            expect(for_01.children).toHaveLength(2)

            const value_01 = for_01.children[0]
            expect(value_01).toBeInstanceOf(ParameterSymbol)
            expect(value_01.kind).toBe(SymbolKind.Parameter)
            if (value_01 instanceof ParameterSymbol) {
              expect(value_01.name).toBe('Value_01')
              expect(value_01.source.range.start).toBe(33)
              expect(value_01.source.range.end).toBe(40)
            }

            const variable_01 = for_01.children[1]
            expect(variable_01).toBeInstanceOf(VariableSymbol)
            expect(variable_01.kind).toBe(SymbolKind.Variable)
            if (variable_01 instanceof VariableSymbol) {
              expect(variable_01.name).toBe('Variable_01')
              expect(variable_01.source.range.start).toBe(63)
              expect(variable_01.source.range.end).toBe(90)
            }
          }
        }
      }
    })

    test('Switchtype', async () => {
      const result = await parse(`
        main() {
          switchtype (Value_01) {
            case CSmMode: {
              declare Integer Variable_01;
            }
            case CSmPlayer: {
              declare Integer Variable_02;
            }
          }
          switchtype (Value_02 as Value_03) {
            case CSmMode: {
              declare Integer Variable_03;
            }
            case CSmPlayer: {
              declare Integer Variable_04;
            }
          }
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(1)

        const main = result.symbolTable.children[0]
        expect(main).toBeInstanceOf(FunctionSymbol)
        expect(main.kind).toBe(SymbolKind.Function)
        if (main instanceof FunctionSymbol) {
          expect(main.name).toBe('main')
          expect(main.source.range.start).toBe(9)
          expect(main.source.range.end).toBe(474)
          expect(main.children).toHaveLength(2)

          const switchtype_01 = main.children[0]
          expect(switchtype_01).toBeInstanceOf(ControlFlowSymbol)
          expect(switchtype_01.kind).toBe(SymbolKind.ControlFlow)
          if (switchtype_01 instanceof ControlFlowSymbol) {
            expect(switchtype_01.name).toBe('')
            expect(switchtype_01.source.range.start).toBe(28)
            expect(switchtype_01.source.range.end).toBe(234)
            expect(switchtype_01.children).toHaveLength(2)

            const block_01 = switchtype_01.children[0]
            expect(block_01).toBeInstanceOf(BlockSymbol)
            expect(block_01.kind).toBe(SymbolKind.Block)
            if (block_01 instanceof BlockSymbol) {
              expect(block_01.name).toBe('')
              expect(block_01.source.range.start).toBe(78)
              expect(block_01.source.range.end).toBe(135)
              expect(block_01.children).toHaveLength(1)

              const variable_01 = block_01.children[0]
              expect(variable_01).toBeInstanceOf(VariableSymbol)
              expect(variable_01.kind).toBe(SymbolKind.Variable)
              if (variable_01 instanceof VariableSymbol) {
                expect(variable_01.name).toBe('Variable_01')
                expect(variable_01.source.range.start).toBe(94)
                expect(variable_01.source.range.end).toBe(121)
              }
            }

            const block_02 = switchtype_01.children[1]
            expect(block_02).toBeInstanceOf(BlockSymbol)
            expect(block_02.kind).toBe(SymbolKind.Block)
            if (block_02 instanceof BlockSymbol) {
              expect(block_02.name).toBe('')
              expect(block_02.source.range.start).toBe(165)
              expect(block_02.source.range.end).toBe(222)
              expect(block_02.children).toHaveLength(1)

              const variable_02 = block_02.children[0]
              expect(variable_02).toBeInstanceOf(VariableSymbol)
              expect(variable_02.kind).toBe(SymbolKind.Variable)
              if (variable_02 instanceof VariableSymbol) {
                expect(variable_02.name).toBe('Variable_02')
                expect(variable_02.source.range.start).toBe(181)
                expect(variable_02.source.range.end).toBe(208)
              }
            }
          }

          const switchtype_02 = main.children[1]
          expect(switchtype_02).toBeInstanceOf(ControlFlowSymbol)
          expect(switchtype_02.kind).toBe(SymbolKind.ControlFlow)
          if (switchtype_02 instanceof ControlFlowSymbol) {
            expect(switchtype_02.name).toBe('')
            expect(switchtype_02.source.range.start).toBe(246)
            expect(switchtype_02.source.range.end).toBe(464)
            expect(switchtype_02.children).toHaveLength(3)

            const value_03 = switchtype_02.children[0]
            expect(value_03).toBeInstanceOf(ParameterSymbol)
            expect(value_03.kind).toBe(SymbolKind.Parameter)
            if (value_03 instanceof ParameterSymbol) {
              expect(value_03.name).toBe('Value_03')
              expect(value_03.source.range.start).toBe(270)
              expect(value_03.source.range.end).toBe(277)
            }

            const block_03 = switchtype_02.children[1]
            expect(block_03).toBeInstanceOf(BlockSymbol)
            expect(block_03.kind).toBe(SymbolKind.Block)
            if (block_03 instanceof BlockSymbol) {
              expect(block_03.name).toBe('')
              expect(block_03.source.range.start).toBe(308)
              expect(block_03.source.range.end).toBe(365)
              expect(block_03.children).toHaveLength(1)

              const variable_03 = block_03.children[0]
              expect(variable_03).toBeInstanceOf(VariableSymbol)
              expect(variable_03.kind).toBe(SymbolKind.Variable)
              if (variable_03 instanceof VariableSymbol) {
                expect(variable_03.name).toBe('Variable_03')
                expect(variable_03.source.range.start).toBe(324)
                expect(variable_03.source.range.end).toBe(351)
              }
            }

            const block_04 = switchtype_02.children[2]
            expect(block_04).toBeInstanceOf(BlockSymbol)
            expect(block_04.kind).toBe(SymbolKind.Block)
            if (block_04 instanceof BlockSymbol) {
              expect(block_04.name).toBe('')
              expect(block_04.source.range.start).toBe(395)
              expect(block_04.source.range.end).toBe(452)
              expect(block_04.children).toHaveLength(1)

              const variable_04 = block_04.children[0]
              expect(variable_04).toBeInstanceOf(VariableSymbol)
              expect(variable_04.kind).toBe(SymbolKind.Variable)
              if (variable_04 instanceof VariableSymbol) {
                expect(variable_04.name).toBe('Variable_04')
                expect(variable_04.source.range.start).toBe(411)
                expect(variable_04.source.range.end).toBe(438)
              }
            }
          }
        }
      }
    })
  })

  describe('Get symbols available at position', () => {
    test('In global space', async () => {
      const result = await parse(`
        #Include "TextLib" as TL

        #Const C_Test_01 "1"

        declare Boolean Test_02;
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.getSymbolsAtPosition(1, 0)).toHaveLength(0)
        expect(result.symbolTable.getSymbolsAtPosition(2, 0)).toHaveLength(0)
        expect(result.symbolTable.getSymbolsAtPosition(2, 7)).toHaveLength(0)

        expect(result.symbolTable.getSymbolsAtPosition(2, 8)).toHaveLength(1)
        expect(result.symbolTable.getSymbolsAtPosition(4, 8)).toHaveLength(2)

        const symbols_6_8 = result.symbolTable.getSymbolsAtPosition(6, 8);
        expect(symbols_6_8).toHaveLength(3)
        expect(symbols_6_8.map(symbol => symbol.name)).toStrictEqual(['TL', 'C_Test_01', 'Test_02'])
      }
    })

    test('In functions', async () => {
      const result = await parse(`
        declare Integer Globale_01;
        Void Function_01() {
          declare Integer Variable_01;
        }
        Void Function_02(Real _Param_01, Text _Param_02) {
          declare Integer Variable_02;
        }
        main() {
          declare Integer Variable_03;
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.getSymbolsAtPosition(1, 0)).toHaveLength(0)

        expect(result.symbolTable.getSymbolsAtPosition(2, 36).map(symbol => symbol.name)).toStrictEqual(['Globale_01'])
        expect(result.symbolTable.getSymbolsAtPosition(3, 29).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01'])
        expect(result.symbolTable.getSymbolsAtPosition(4, 39).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(5, 8).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(5, 9).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01'])
        expect(result.symbolTable.getSymbolsAtPosition(6, 14).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Function_02'])
        expect(result.symbolTable.getSymbolsAtPosition(6, 26).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Function_02', '_Param_01'])
        expect(result.symbolTable.getSymbolsAtPosition(6, 42).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Function_02', '_Param_01', '_Param_02'])
        expect(result.symbolTable.getSymbolsAtPosition(7, 39).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Function_02', '_Param_01', '_Param_02', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(8, 8).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Function_02', '_Param_01', '_Param_02', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(8, 9).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Function_02'])
        expect(result.symbolTable.getSymbolsAtPosition(9, 17).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Function_02', 'main'])
        expect(result.symbolTable.getSymbolsAtPosition(10, 39).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Function_02', 'main', 'Variable_03'])
        expect(result.symbolTable.getSymbolsAtPosition(11, 8).map(symbol => symbol.name)).toStrictEqual(['Globale_01', 'Function_01', 'Function_02', 'main', 'Variable_03'])
        expect(result.symbolTable.getSymbolsAtPosition(11, 9).map(symbol => symbol.name)).toStrictEqual([])
      }
    })

    test('In blocks', async () => {
      const result = await parse(`
        main() {
          declare Integer Variable_01;
          {
            declare Integer Variable_02;
            {
              declare Integer Variable_03;
            }
            declare Integer Variable_04;
            {
              declare Integer Variable_05;
            }
            declare Integer Variable_06;
          }
          declare Integer Variable_07;
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.getSymbolsAtPosition(1, 0)).toHaveLength(0)

        expect(result.symbolTable.getSymbolsAtPosition(2, 17).map(symbol => symbol.name)).toStrictEqual(['main'])
        expect(result.symbolTable.getSymbolsAtPosition(3, 39).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(4, 12).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', ''])
        expect(result.symbolTable.getSymbolsAtPosition(5, 41).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(6, 14).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02', ''])
        expect(result.symbolTable.getSymbolsAtPosition(7, 43).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02', '', 'Variable_03'])
        expect(result.symbolTable.getSymbolsAtPosition(8, 12).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02', '', 'Variable_03'])
        expect(result.symbolTable.getSymbolsAtPosition(8, 13).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02', ''])
        expect(result.symbolTable.getSymbolsAtPosition(9, 41).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02', '', 'Variable_04'])
        expect(result.symbolTable.getSymbolsAtPosition(10, 14).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02', '', 'Variable_04', ''])
        expect(result.symbolTable.getSymbolsAtPosition(11, 43).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02', '', 'Variable_04', '', 'Variable_05'])
        expect(result.symbolTable.getSymbolsAtPosition(12, 14).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02', '', 'Variable_04', ''])
        expect(result.symbolTable.getSymbolsAtPosition(13, 41).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_02', '', 'Variable_04', '', 'Variable_06'])
        expect(result.symbolTable.getSymbolsAtPosition(14, 12).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', ''])
        expect(result.symbolTable.getSymbolsAtPosition(15, 39).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_07'])
        expect(result.symbolTable.getSymbolsAtPosition(16, 8).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', '', 'Variable_07'])
        expect(result.symbolTable.getSymbolsAtPosition(16, 9).map(symbol => symbol.name)).toStrictEqual([])
      }
    })

    test('In control flow', async () => {
      const result = await parse(`
        main() {
          foreach (Value_01 in Source_01) {
            declare Integer Variable_01;
          }
          for (Value_02, 0, 9) {
            declare Integer Variable_02;
          }
          switchtype (Value_03 as Value_04) {
            case CSmMode: {
              declare Integer Variable_03;
            }
            case CSmPlayer: {
              declare Integer Variable_04;
            }
          }
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.getSymbolsAtPosition(1, 0)).toHaveLength(0)

        expect(result.symbolTable.getSymbolsAtPosition(2, 16).map(symbol => symbol.name)).toStrictEqual(['main'])
        expect(result.symbolTable.getSymbolsAtPosition(3, 43).map(symbol => symbol.name)).toStrictEqual(['main', '', 'Value_01'])
        expect(result.symbolTable.getSymbolsAtPosition(4, 40).map(symbol => symbol.name)).toStrictEqual(['main', '', 'Value_01', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(5, 11).map(symbol => symbol.name)).toStrictEqual(['main', ''])
        expect(result.symbolTable.getSymbolsAtPosition(6, 32).map(symbol => symbol.name)).toStrictEqual(['main', '', '', 'Value_02'])
        expect(result.symbolTable.getSymbolsAtPosition(7, 40).map(symbol => symbol.name)).toStrictEqual(['main', '', '', 'Value_02', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(8, 11).map(symbol => symbol.name)).toStrictEqual(['main', '', ''])
        expect(result.symbolTable.getSymbolsAtPosition(9, 45).map(symbol => symbol.name)).toStrictEqual(['main', '', '', '', 'Value_04'])
        expect(result.symbolTable.getSymbolsAtPosition(10, 27).map(symbol => symbol.name)).toStrictEqual(['main', '', '', '', 'Value_04', ''])
        expect(result.symbolTable.getSymbolsAtPosition(11, 42).map(symbol => symbol.name)).toStrictEqual(['main', '', '', '', 'Value_04', '', 'Variable_03'])
        expect(result.symbolTable.getSymbolsAtPosition(12, 13).map(symbol => symbol.name)).toStrictEqual(['main', '', '', '', 'Value_04', ''])
        expect(result.symbolTable.getSymbolsAtPosition(13, 30).map(symbol => symbol.name)).toStrictEqual(['main', '', '', '', 'Value_04', '', ''])
        expect(result.symbolTable.getSymbolsAtPosition(14, 43).map(symbol => symbol.name)).toStrictEqual(['main', '', '', '', 'Value_04', '', '', 'Variable_04'])
        expect(result.symbolTable.getSymbolsAtPosition(15, 13).map(symbol => symbol.name)).toStrictEqual(['main', '', '', '', 'Value_04', '', ''])
        expect(result.symbolTable.getSymbolsAtPosition(16, 11).map(symbol => symbol.name)).toStrictEqual(['main', '', '', ''])
        expect(result.symbolTable.getSymbolsAtPosition(17, 9).map(symbol => symbol.name)).toStrictEqual([])
      }
    })

    test('With label symbol', async () => {
      const result = await parse(`
        declare Integer Variable_01;
        main() {
          +++Label_01+++
          declare Integer Variable_02;
          ---Label_02---
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.getSymbolsAtPosition(1, 0).map(symbol => symbol.name)).toStrictEqual(['Label_01', 'Label_02'])
        expect(result.symbolTable.getSymbolsAtPosition(2, 36).map(symbol => symbol.name)).toStrictEqual(['Label_01', 'Label_02', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(3, 16).map(symbol => symbol.name)).toStrictEqual(['Label_01', 'Label_02', 'Variable_01', 'main'])
        expect(result.symbolTable.getSymbolsAtPosition(4, 24).map(symbol => symbol.name)).toStrictEqual(['Label_01', 'Label_02', 'Variable_01', 'main'])
        expect(result.symbolTable.getSymbolsAtPosition(5, 38).map(symbol => symbol.name)).toStrictEqual(['Label_01', 'Label_02', 'Variable_01', 'main', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(6, 24).map(symbol => symbol.name)).toStrictEqual(['Label_01', 'Label_02', 'Variable_01', 'main', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(7, 9).map(symbol => symbol.name)).toStrictEqual(['Label_01', 'Label_02'])
      }
    })

    test('After variable declaration', async () => {
      const result = await parse(`
        main() {
          declare Integer Variable_01;
          declare Integer Variable_02;
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(true)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.getSymbolsAtPosition(3, 9).map(symbol => symbol.name)).toStrictEqual(['main'])
        expect(result.symbolTable.getSymbolsAtPosition(3, 10).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(3, 37).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(3, 38).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(4, 0).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(4, 9).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01'])
        expect(result.symbolTable.getSymbolsAtPosition(4, 10).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(4, 37).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(4, 38).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(5, 8).map(symbol => symbol.name)).toStrictEqual(['main', 'Variable_01', 'Variable_02'])
        expect(result.symbolTable.getSymbolsAtPosition(5, 9).map(symbol => symbol.name)).toStrictEqual([])
      }
    })
  })

  describe('Generate symbol for incomplete code', () => {
    test('Incomplete variable declaration', async () => {
      const result = await parse(`
        #Struct K_Struct_01 {
          Integer Member_01;
        }
        main() {
          declare Integer Variable_01;
          declare
          declare Integer Variable_02;
        }
      `, { buildSymbolTable: true })

      expect(result.success).toBe(false)
      expect(result.symbolTable).toBeInstanceOf(SymbolTable)

      if (result.symbolTable !== null) {
        expect(result.symbolTable.children).toHaveLength(2)

        const struct_01 = result.symbolTable.children[0]
        expect(struct_01).toBeInstanceOf(StructureSymbol)
        expect(struct_01.kind).toBe(SymbolKind.Structure)
        if (struct_01 instanceof StructureSymbol) {
          expect(struct_01.name).toBe('K_Struct_01')
          expect(struct_01.source.range.start).toBe(9)
          expect(struct_01.source.range.end).toBe(68)
        }

        const main = result.symbolTable.children[1]
        expect(main).toBeInstanceOf(FunctionSymbol)
        expect(main.kind).toBe(SymbolKind.Function)
        if (main instanceof FunctionSymbol) {
          expect(main.name).toBe('main')
          expect(main.source.range.start).toBe(78)
          expect(main.source.range.end).toBe(191)
          expect(main.type).toEqual(getUnknownType())

          expect(main.children).toHaveLength(2)

          const variable_01 = main.children[0]
          expect(variable_01).toBeInstanceOf(VariableSymbol)
          expect(variable_01.kind).toBe(SymbolKind.Variable)
          if (variable_01 instanceof VariableSymbol) {
            expect(variable_01.name).toBe('Variable_01')
            expect(variable_01.source.range.start).toBe(97)
            expect(variable_01.source.range.end).toBe(124)
          }

          const variable_02 = main.children[1]
          expect(variable_02).toBeInstanceOf(VariableSymbol)
          expect(variable_02.kind).toBe(SymbolKind.Variable)
          if (variable_02 instanceof VariableSymbol) {
            expect(variable_02.name).toBe('Variable_02')
            expect(variable_02.source.range.start).toBe(154)
            expect(variable_02.source.range.end).toBe(181)
          }
        }
      }
    })
  })
})
