/* eslint @typescript-eslint/no-unnecessary-condition: off */
import { describe, test, expect, beforeAll } from 'vitest'
import { parse } from '../../src/lib/parser'
import {
  Kind,
  UnaryOperator,
  BinaryOperator,
  LogicalOperator,
  TextLiteral,
  IntegerLiteral,
  RealLiteral,
  BooleanLiteral,
  NullLiteral,
  NullIdLiteral,
  EnumLiteral,
  ExtendsDirective,
  RequireContextDirective,
  IncludeDirective,
  SettingDirective,
  CommandDirective,
  StructDirective,
  StructDeclaration,
  StructMemberDeclaration,
  StructAliasing,
  ConstDirective,
  ConstDeclaration,
  ConstAliasing,
  VectorExpression,
  ArrayExpression,
  StructExpression,
  AsExpression,
  IsExpression,
  DotAccessExpression,
  IndexAccessExpression,
  UnaryExpression,
  BinaryExpression,
  LogicalExpression,
  TemplateTextLiteral,
  DumptypeExpression,
  DumpExpression,
  CastExpression,
  ThisExpression,
  NowExpression,
  TypeName,
  ClassType,
  SimpleType,
  Identifier,
  CustomType,
  VariableDeclaration,
  FunctionParameterDeclaration,
  FunctionDeclaration,
  BlockStatement,
  ExpressionStatement,
  Storage,
  InitializerSign,
  LabelDeclaration,
  AssignmentStatement,
  AssignmentOperator,
  ReturnStatement,
  LabelStatement,
  AssertStatement,
  ForeachStatement,
  ForStatement,
  WhileStatement,
  MeanwhileStatement,
  ContinueStatement,
  BreakStatement,
  SwitchStatement,
  SwitchCase,
  SwitchtypeStatement,
  SwitchtypeCase,
  ConditionalStatement,
  ConditionalBranch,
  LogStatement,
  SleepStatement,
  TuningstartStatement,
  TuningendStatement,
  TuningmarkStatement,
  WaitStatement,
  YieldStatement,
  FunctionCallExpression,
  isBaseType,
  isInitializerType,
  isType,
  isLiteral,
  isDirective,
  isDeclaration,
  isStatement,
  isExpression,
  InvalidDirective,
  type AST
} from '../../src/lib/ast'

const inputDeclarations = `
#RequireContext CSmMode
#Extends "Path/To/File.Script.txt"
#Include "TextLib" as TL
#Include "MathLib"
#Setting S_SettingA 123 as _("Setting description")
#Setting S_SettingB <1, 2, 3>
#Command Command_CommandA (Integer) as _("Command description")
#Struct K_StructA {
  Integer A1;
}
#Struct K_StructB {
  CSmMode B1;
  K_StructA B2;
  Vec2[Int2] B3;
}
#Struct Namespace::K_StructC as K_StructE
#Const C_ConstA 1
#Const C_ConstB [1, 2]
#Const C_ConstC ["A" => 1, "B" => 2]
#Const C_ConstD K_StructA { A1 = 0 }
#Const Namespace::C_ConstE as C_ConstF

declare cloud Integer VarA as VarB for This = 1;
declare metadata VarC as VarD for VarE <=> Real;

Void FuncA() {}
Integer FuncB(Integer _A, CSmMode _B) {

}

***LabelA***
***
declare Integer A;
***
`

let astDeclarations: AST

describe('lib/ast part 1', () => {
  beforeAll(async () => {
    astDeclarations = (await parse(inputDeclarations, { buildAst: true })).ast
  })

  test('generate Program node', () => {
    expect('program' in astDeclarations).toBe(true)
    const program = astDeclarations.program
    expect(program).toBeDefined()

    if (program !== undefined) {
      expect(program.kind).toBe(Kind.Program)
      expect(program.parent).toBeUndefined()
      expect(program.source.range.start).toBe(1)
      expect(program.source.range.end).toBe(749)
      expect(program.source.loc.start.line).toBe(2)
      expect(program.source.loc.start.column).toBe(0)
      expect(program.source.loc.end.line).toBe(35)
      expect(program.source.loc.end.column).toBe(3)

      expect('directives' in program).toBe(true)
      expect(program.directives.length).toBe(15)
      expect('declarations' in program).toBe(true)
      expect(program.declarations.length).toBe(5)
      expect(program.main).toBeUndefined()
    }
  })

  test('generate RequireContextDirective node', () => {
    const requireContextDirective = astDeclarations.program?.directives[0] as RequireContextDirective
    expect(requireContextDirective).toBeDefined()

    expect(requireContextDirective.kind).toBe(Kind.RequireContextDirective)
    expect(requireContextDirective.parent).toBe(astDeclarations.program)
    expect(requireContextDirective.source.range.start).toBe(1)
    expect(requireContextDirective.source.range.end).toBe(23)
    expect(requireContextDirective.source.loc.start.line).toBe(2)
    expect(requireContextDirective.source.loc.start.column).toBe(0)
    expect(requireContextDirective.source.loc.end.line).toBe(2)
    expect(requireContextDirective.source.loc.end.column).toBe(23)

    expect('class' in requireContextDirective).toBe(true)
    expect(requireContextDirective.class instanceof ClassType).toBe(true)

    expect(requireContextDirective.class.kind).toBe(Kind.ClassType)
    expect(requireContextDirective.class.parent).toBe(requireContextDirective)
    expect(requireContextDirective.class.source.range.start).toBe(17)
    expect(requireContextDirective.class.source.range.end).toBe(23)
    expect(requireContextDirective.class.source.loc.start.line).toBe(2)
    expect(requireContextDirective.class.source.loc.start.column).toBe(16)
    expect(requireContextDirective.class.source.loc.end.line).toBe(2)
    expect(requireContextDirective.class.source.loc.end.column).toBe(23)

    expect(requireContextDirective.class.name).toBe('CSmMode')
  })

  test('generate ExtendsDirective node', () => {
    const extendsDirective = astDeclarations.program?.directives[1] as ExtendsDirective
    expect(extendsDirective).toBeDefined()

    expect(extendsDirective.kind).toBe(Kind.ExtendsDirective)
    expect(extendsDirective.parent).toBe(astDeclarations.program)
    expect(extendsDirective.source.range.start).toBe(25)
    expect(extendsDirective.source.range.end).toBe(58)
    expect(extendsDirective.source.loc.start.line).toBe(3)
    expect(extendsDirective.source.loc.start.column).toBe(0)
    expect(extendsDirective.source.loc.end.line).toBe(3)
    expect(extendsDirective.source.loc.end.column).toBe(34)

    expect('path' in extendsDirective).toBe(true)
    expect(extendsDirective.path instanceof TextLiteral).toBe(true)

    expect(extendsDirective.path.kind).toBe(Kind.TextLiteral)
    expect(extendsDirective.path.parent).toBe(extendsDirective)
    expect(extendsDirective.path.source.range.start).toBe(34)
    expect(extendsDirective.path.source.range.end).toBe(58)
    expect(extendsDirective.path.source.loc.start.line).toBe(3)
    expect(extendsDirective.path.source.loc.start.column).toBe(9)
    expect(extendsDirective.path.source.loc.end.line).toBe(3)
    expect(extendsDirective.path.source.loc.end.column).toBe(34)

    expect(extendsDirective.path.isMultiline).toBe(false)
    expect(extendsDirective.path.isTranslated).toBe(false)
    expect(extendsDirective.path.value).toBe('Path/To/File.Script.txt')
    expect(extendsDirective.path.raw).toBe('"Path/To/File.Script.txt"')
  })

  test('generate IncludeDirective node', () => {
    let includeDirective = astDeclarations.program?.directives[2] as IncludeDirective
    expect(includeDirective).toBeDefined()

    if (includeDirective !== undefined) {
      expect(includeDirective.kind).toBe(Kind.IncludeDirective)
      expect(includeDirective.parent).toBe(astDeclarations.program)
      expect(includeDirective.source.range.start).toBe(60)
      expect(includeDirective.source.range.end).toBe(83)
      expect(includeDirective.source.loc.start.line).toBe(4)
      expect(includeDirective.source.loc.start.column).toBe(0)
      expect(includeDirective.source.loc.end.line).toBe(4)
      expect(includeDirective.source.loc.end.column).toBe(24)

      expect('path' in includeDirective).toBe(true)
      expect(includeDirective.path instanceof TextLiteral).toBe(true)

      expect(includeDirective.path.kind).toBe(Kind.TextLiteral)
      expect(includeDirective.path.parent).toBe(includeDirective)
      expect(includeDirective.path.source.range.start).toBe(69)
      expect(includeDirective.path.source.range.end).toBe(77)
      expect(includeDirective.path.source.loc.start.line).toBe(4)
      expect(includeDirective.path.source.loc.start.column).toBe(9)
      expect(includeDirective.path.source.loc.end.line).toBe(4)
      expect(includeDirective.path.source.loc.end.column).toBe(18)

      expect(includeDirective.path.isMultiline).toBe(false)
      expect(includeDirective.path.isTranslated).toBe(false)
      expect(includeDirective.path.value).toBe('TextLib')
      expect(includeDirective.path.raw).toBe('"TextLib"')

      expect('alias' in includeDirective).toBe(true)
      expect(includeDirective.alias instanceof Identifier).toBe(true)

      if (includeDirective.alias !== undefined) {
        expect(includeDirective.alias.kind).toBe(Kind.Identifier)
        expect(includeDirective.alias.parent).toBe(includeDirective)
        expect(includeDirective.alias.source.range.start).toBe(82)
        expect(includeDirective.alias.source.range.end).toBe(83)
        expect(includeDirective.alias.source.loc.start.line).toBe(4)
        expect(includeDirective.alias.source.loc.start.column).toBe(22)
        expect(includeDirective.alias.source.loc.end.line).toBe(4)
        expect(includeDirective.alias.source.loc.end.column).toBe(24)

        expect(includeDirective.alias.name).toBe('TL')
        expect(includeDirective.alias.isExpression).toBe(false)
      }
    }

    includeDirective = astDeclarations.program?.directives[3] as IncludeDirective
    expect(includeDirective).toBeDefined()

    if (includeDirective !== undefined) {
      expect(includeDirective.kind).toBe(Kind.IncludeDirective)
      expect(includeDirective.parent).toBe(astDeclarations.program)
      expect(includeDirective.source.range.start).toBe(85)
      expect(includeDirective.source.range.end).toBe(102)
      expect(includeDirective.source.loc.start.line).toBe(5)
      expect(includeDirective.source.loc.start.column).toBe(0)
      expect(includeDirective.source.loc.end.line).toBe(5)
      expect(includeDirective.source.loc.end.column).toBe(18)

      expect('path' in includeDirective).toBe(true)
      expect(includeDirective.path instanceof TextLiteral).toBe(true)

      expect(includeDirective.path.kind).toBe(Kind.TextLiteral)
      expect(includeDirective.path.parent).toBe(includeDirective)
      expect(includeDirective.path.source.range.start).toBe(94)
      expect(includeDirective.path.source.range.end).toBe(102)
      expect(includeDirective.path.source.loc.start.line).toBe(5)
      expect(includeDirective.path.source.loc.start.column).toBe(9)
      expect(includeDirective.path.source.loc.end.line).toBe(5)
      expect(includeDirective.path.source.loc.end.column).toBe(18)

      expect(includeDirective.path.isMultiline).toBe(false)
      expect(includeDirective.path.isTranslated).toBe(false)
      expect(includeDirective.path.value).toBe('MathLib')
      expect(includeDirective.path.raw).toBe('"MathLib"')

      expect(includeDirective.alias).toBeUndefined()
    }
  })

  test('generate SettingDirective node', () => {
    let settingDirective = astDeclarations.program?.directives[4] as SettingDirective
    expect(settingDirective).toBeDefined()

    if (settingDirective !== undefined) {
      expect(settingDirective.kind).toBe(Kind.SettingDirective)
      expect(settingDirective.parent).toBe(astDeclarations.program)
      expect(settingDirective.source.range.start).toBe(104)
      expect(settingDirective.source.range.end).toBe(154)
      expect(settingDirective.source.loc.start.line).toBe(6)
      expect(settingDirective.source.loc.start.column).toBe(0)
      expect(settingDirective.source.loc.end.line).toBe(6)
      expect(settingDirective.source.loc.end.column).toBe(51)

      expect('name' in settingDirective).toBe(true)
      expect(settingDirective.name instanceof Identifier).toBe(true)

      if (settingDirective.name !== undefined) {
        expect(settingDirective.name.kind).toBe(Kind.Identifier)
        expect(settingDirective.name.parent).toBe(settingDirective)
        expect(settingDirective.name.source.range.start).toBe(113)
        expect(settingDirective.name.source.range.end).toBe(122)
        expect(settingDirective.name.source.loc.start.line).toBe(6)
        expect(settingDirective.name.source.loc.start.column).toBe(9)
        expect(settingDirective.name.source.loc.end.line).toBe(6)
        expect(settingDirective.name.source.loc.end.column).toBe(19)

        expect(settingDirective.name.name).toBe('S_SettingA')
        expect(settingDirective.name.isExpression).toBe(false)
      }

      expect('value' in settingDirective).toBe(true)
      expect(settingDirective.value instanceof IntegerLiteral).toBe(true)

      if (settingDirective.value !== undefined && settingDirective.value instanceof IntegerLiteral) {
        expect(settingDirective.value.kind).toBe(Kind.IntegerLiteral)
        expect(settingDirective.value.parent).toBe(settingDirective)
        expect(settingDirective.value.source.range.start).toBe(124)
        expect(settingDirective.value.source.range.end).toBe(126)
        expect(settingDirective.value.source.loc.start.line).toBe(6)
        expect(settingDirective.value.source.loc.start.column).toBe(20)
        expect(settingDirective.value.source.loc.end.line).toBe(6)
        expect(settingDirective.value.source.loc.end.column).toBe(23)

        expect(settingDirective.value.value).toBe(123)
      }

      expect('description' in settingDirective).toBe(true)
      expect(settingDirective.description instanceof TextLiteral).toBe(true)

      if (settingDirective.description !== undefined) {
        expect(settingDirective.description.kind).toBe(Kind.TextLiteral)
        expect(settingDirective.description.parent).toBe(settingDirective)
        expect(settingDirective.description.source.range.start).toBe(131)
        expect(settingDirective.description.source.range.end).toBe(154)
        expect(settingDirective.description.source.loc.start.line).toBe(6)
        expect(settingDirective.description.source.loc.start.column).toBe(27)
        expect(settingDirective.description.source.loc.end.line).toBe(6)
        expect(settingDirective.description.source.loc.end.column).toBe(51)

        expect(settingDirective.description.isMultiline).toBe(false)
        expect(settingDirective.description.isTranslated).toBe(true)
        expect(settingDirective.description.value).toBe('Setting description')
        expect(settingDirective.description.raw).toBe('_("Setting description")')
      }
    }

    settingDirective = astDeclarations.program?.directives[5] as SettingDirective
    expect(settingDirective).toBeDefined()

    if (settingDirective !== undefined) {
      expect(settingDirective.kind).toBe(Kind.SettingDirective)
      expect(settingDirective.parent).toBe(astDeclarations.program)
      expect(settingDirective.source.range.start).toBe(156)
      expect(settingDirective.source.range.end).toBe(184)
      expect(settingDirective.source.loc.start.line).toBe(7)
      expect(settingDirective.source.loc.start.column).toBe(0)
      expect(settingDirective.source.loc.end.line).toBe(7)
      expect(settingDirective.source.loc.end.column).toBe(29)

      expect('name' in settingDirective).toBe(true)
      expect(settingDirective.name instanceof Identifier).toBe(true)

      if (settingDirective.name !== undefined) {
        expect(settingDirective.name.kind).toBe(Kind.Identifier)
        expect(settingDirective.name.parent).toBe(settingDirective)
        expect(settingDirective.name.source.range.start).toBe(165)
        expect(settingDirective.name.source.range.end).toBe(174)
        expect(settingDirective.name.source.loc.start.line).toBe(7)
        expect(settingDirective.name.source.loc.start.column).toBe(9)
        expect(settingDirective.name.source.loc.end.line).toBe(7)
        expect(settingDirective.name.source.loc.end.column).toBe(19)

        expect(settingDirective.name.name).toBe('S_SettingB')
        expect(settingDirective.name.isExpression).toBe(false)
      }

      expect('value' in settingDirective).toBe(true)
      expect(settingDirective.value instanceof VectorExpression).toBe(true)

      const vector = settingDirective.value as VectorExpression
      if (vector !== undefined) {
        expect(vector.kind).toBe(Kind.VectorExpression)
        expect(vector.parent).toBe(settingDirective)
        expect(vector.source.range.start).toBe(176)
        expect(vector.source.range.end).toBe(184)
        expect(vector.source.loc.start.line).toBe(7)
        expect(vector.source.loc.start.column).toBe(20)
        expect(vector.source.loc.end.line).toBe(7)
        expect(vector.source.loc.end.column).toBe(29)

        expect(vector.values.length).toBe(3)
        const correctValues = [1, 2, 3]
        let index = 0
        for (const value of vector.values) {
          expect(value instanceof IntegerLiteral)
          expect((value as IntegerLiteral).value).toBe(correctValues[index])
          index++
        }
      }

      expect(settingDirective.description).toBeUndefined()
    }
  })

  test('generate CommandDirective node', () => {
    const commandDirective = astDeclarations.program?.directives[6] as CommandDirective
    expect(commandDirective).toBeDefined()

    if (commandDirective !== undefined) {
      expect(commandDirective.kind).toBe(Kind.CommandDirective)
      expect(commandDirective.parent).toBe(astDeclarations.program)
      expect(commandDirective.source.range.start).toBe(186)
      expect(commandDirective.source.range.end).toBe(248)
      expect(commandDirective.source.loc.start.line).toBe(8)
      expect(commandDirective.source.loc.start.column).toBe(0)
      expect(commandDirective.source.loc.end.line).toBe(8)
      expect(commandDirective.source.loc.end.column).toBe(63)

      expect('name' in commandDirective).toBe(true)
      expect(commandDirective.name instanceof Identifier).toBe(true)

      if (commandDirective.name !== undefined) {
        expect(commandDirective.name.kind).toBe(Kind.Identifier)
        expect(commandDirective.name.parent).toBe(commandDirective)
        expect(commandDirective.name.source.range.start).toBe(195)
        expect(commandDirective.name.source.range.end).toBe(210)
        expect(commandDirective.name.source.loc.start.line).toBe(8)
        expect(commandDirective.name.source.loc.start.column).toBe(9)
        expect(commandDirective.name.source.loc.end.line).toBe(8)
        expect(commandDirective.name.source.loc.end.column).toBe(25)

        expect(commandDirective.name.name).toBe('Command_CommandA')
        expect(commandDirective.name.isExpression).toBe(false)
      }

      expect('type' in commandDirective).toBe(true)
      expect(commandDirective.type instanceof SimpleType).toBe(true)

      if (commandDirective.type !== undefined) {
        const type = (commandDirective.type as SimpleType)
        expect(type.kind).toBe(Kind.SimpleType)
        expect(type.parent).toBe(commandDirective)
        expect(type.source.range.start).toBe(213)
        expect(type.source.range.end).toBe(219)
        expect(type.source.loc.start.line).toBe(8)
        expect(type.source.loc.start.column).toBe(27)
        expect(type.source.loc.end.line).toBe(8)
        expect(type.source.loc.end.column).toBe(34)

        expect(type.name).toBe(TypeName.Integer)
      }

      expect('description' in commandDirective).toBe(true)
      expect(commandDirective.description instanceof TextLiteral).toBe(true)

      if (commandDirective.description !== undefined) {
        expect(commandDirective.description.kind).toBe(Kind.TextLiteral)
        expect(commandDirective.description.parent).toBe(commandDirective)
        expect(commandDirective.description.source.range.start).toBe(225)
        expect(commandDirective.description.source.range.end).toBe(248)
        expect(commandDirective.description.source.loc.start.line).toBe(8)
        expect(commandDirective.description.source.loc.start.column).toBe(39)
        expect(commandDirective.description.source.loc.end.line).toBe(8)
        expect(commandDirective.description.source.loc.end.column).toBe(63)

        expect(commandDirective.description.isMultiline).toBe(false)
        expect(commandDirective.description.isTranslated).toBe(true)
        expect(commandDirective.description.value).toBe('Command description')
        expect(commandDirective.description.raw).toBe('_("Command description")')
      }
    }
  })

  test('generate StructDeclaration node', () => {
    let structDirective = astDeclarations.program?.directives[7] as StructDirective
    expect(structDirective).toBeDefined()

    if (structDirective !== undefined) {
      expect(structDirective.kind).toBe(Kind.StructDirective)
      expect(structDirective.parent).toBe(astDeclarations.program)
      expect(structDirective.source.range.start).toBe(250)
      expect(structDirective.source.range.end).toBe(284)
      expect(structDirective.source.loc.start.line).toBe(9)
      expect(structDirective.source.loc.start.column).toBe(0)
      expect(structDirective.source.loc.end.line).toBe(11)
      expect(structDirective.source.loc.end.column).toBe(1)

      expect('declaration' in structDirective).toBe(true)
      expect(structDirective.declaration instanceof StructDeclaration).toBe(true)
      expect(structDirective.aliasing).toBeUndefined()

      if (structDirective.declaration !== undefined) {
        expect(structDirective.declaration.kind).toBe(Kind.StructDeclaration)
        expect(structDirective.declaration.parent).toBe(structDirective)
        expect('name' in structDirective.declaration).toBe(true)
        expect(structDirective.declaration.name instanceof Identifier).toBe(true)

        if (structDirective.declaration.name !== undefined) {
          expect(structDirective.declaration.name.kind).toBe(Kind.Identifier)
          expect(structDirective.declaration.name.source.range.start).toBe(258)
          expect(structDirective.declaration.name.source.range.end).toBe(266)
          expect(structDirective.declaration.name.source.loc.start.line).toBe(9)
          expect(structDirective.declaration.name.source.loc.start.column).toBe(8)
          expect(structDirective.declaration.name.source.loc.end.line).toBe(9)
          expect(structDirective.declaration.name.source.loc.end.column).toBe(17)

          expect(structDirective.declaration.name.name).toBe('K_StructA')
          expect(structDirective.declaration.name.isExpression).toBe(false)
        }

        expect('members' in structDirective.declaration).toBe(true)
        expect(structDirective.declaration.members.length).toBe(1)

        const members = structDirective.declaration.members
        expect(members[0] instanceof StructMemberDeclaration).toBe(true)
        expect(members[0].kind).toBe(Kind.StructMemberDeclaration)
        expect(members[0].parent).toBe(structDirective.declaration)
        expect(members[0].source.range.start).toBe(272)
        expect(members[0].source.range.end).toBe(282)
        expect(members[0].source.loc.start.line).toBe(10)
        expect(members[0].source.loc.start.column).toBe(2)
        expect(members[0].source.loc.end.line).toBe(10)
        expect(members[0].source.loc.end.column).toBe(13)

        expect(members[0].type instanceof SimpleType).toBe(true)
        const type = (members[0].type as SimpleType)
        expect(type.kind).toBe(Kind.SimpleType)
        expect(type.parent).toBe(members[0])
        expect(type.source.range.start).toBe(272)
        expect(type.source.range.end).toBe(278)
        expect(type.source.loc.start.line).toBe(10)
        expect(type.source.loc.start.column).toBe(2)
        expect(type.source.loc.end.line).toBe(10)
        expect(type.source.loc.end.column).toBe(9)
        expect(type.name).toBe(TypeName.Integer)
      }
    }

    structDirective = astDeclarations.program?.directives[8] as StructDirective
    expect(structDirective).toBeDefined()

    if (structDirective !== undefined) {
      expect(structDirective.kind).toBe(Kind.StructDirective)
      expect(structDirective.parent).toBe(astDeclarations.program)
      expect(structDirective.source.range.start).toBe(286)
      expect(structDirective.source.range.end).toBe(353)
      expect(structDirective.source.loc.start.line).toBe(12)
      expect(structDirective.source.loc.start.column).toBe(0)
      expect(structDirective.source.loc.end.line).toBe(16)
      expect(structDirective.source.loc.end.column).toBe(1)

      expect('declaration' in structDirective).toBe(true)
      expect(structDirective.declaration instanceof StructDeclaration).toBe(true)
      expect(structDirective.aliasing).toBeUndefined()

      if (structDirective.declaration !== undefined) {
        expect(structDirective.declaration.kind).toBe(Kind.StructDeclaration)
        expect(structDirective.declaration.parent).toBe(structDirective)
        expect('name' in structDirective.declaration).toBe(true)
        expect(structDirective.declaration.name instanceof Identifier).toBe(true)

        if (structDirective.declaration.name !== undefined) {
          expect(structDirective.declaration.name.kind).toBe(Kind.Identifier)
          expect(structDirective.declaration.name.parent).toBe(structDirective.declaration)
          expect(structDirective.declaration.name.source.range.start).toBe(294)
          expect(structDirective.declaration.name.source.range.end).toBe(302)
          expect(structDirective.declaration.name.source.loc.start.line).toBe(12)
          expect(structDirective.declaration.name.source.loc.start.column).toBe(8)
          expect(structDirective.declaration.name.source.loc.end.line).toBe(12)
          expect(structDirective.declaration.name.source.loc.end.column).toBe(17)

          expect(structDirective.declaration.name.name).toBe('K_StructB')
          expect(structDirective.declaration.name.isExpression).toBe(false)
        }

        expect('members' in structDirective.declaration).toBe(true)
        expect(structDirective.declaration.members.length).toBe(3)

        const members = structDirective.declaration.members
        expect(members[1] instanceof StructMemberDeclaration).toBe(true)
        expect(members[1].kind).toBe(Kind.StructMemberDeclaration)
        expect(members[1].parent).toBe(structDirective.declaration)
        expect(members[1].source.range.start).toBe(322)
        expect(members[1].source.range.end).toBe(334)
        expect(members[1].source.loc.start.line).toBe(14)
        expect(members[1].source.loc.start.column).toBe(2)
        expect(members[1].source.loc.end.line).toBe(14)
        expect(members[1].source.loc.end.column).toBe(15)

        expect(members[1].type instanceof CustomType).toBe(true)
        const type = (members[1].type as CustomType)
        expect(type.kind).toBe(Kind.CustomType)
        expect(type.parent).toBe(members[1])
        expect(type.source.range.start).toBe(322)
        expect(type.source.range.end).toBe(330)
        expect(type.source.loc.start.line).toBe(14)
        expect(type.source.loc.start.column).toBe(2)
        expect(type.source.loc.end.line).toBe(14)
        expect(type.source.loc.end.column).toBe(11)
        expect(type.name.name).toBe('K_StructA')
      }
    }
  })

  test('generate StructAliasing node', () => {
    const structDirective = astDeclarations.program?.directives[9] as StructDirective
    expect(structDirective).toBeDefined()

    if (structDirective !== undefined) {
      expect(structDirective.kind).toBe(Kind.StructDirective)
      expect(structDirective.parent).toBe(astDeclarations.program)
      expect(structDirective.source.range.start).toBe(355)
      expect(structDirective.source.range.end).toBe(395)
      expect(structDirective.source.loc.start.line).toBe(17)
      expect(structDirective.source.loc.start.column).toBe(0)
      expect(structDirective.source.loc.end.line).toBe(17)
      expect(structDirective.source.loc.end.column).toBe(41)

      expect('aliasing' in structDirective).toBe(true)
      expect(structDirective.aliasing instanceof StructAliasing).toBe(true)
      expect(structDirective.declaration).toBeUndefined()

      if (structDirective.aliasing !== undefined) {
        expect(structDirective.aliasing.kind).toBe(Kind.StructAliasing)
        expect(structDirective.aliasing.parent).toBe(structDirective)
        expect('name' in structDirective.aliasing).toBe(true)
        expect(structDirective.aliasing.name instanceof Identifier).toBe(true)

        if (structDirective.aliasing.name !== undefined) {
          expect(structDirective.aliasing.name.kind).toBe(Kind.Identifier)
          expect(structDirective.aliasing.name.parent).toBe(structDirective.aliasing)
          expect(structDirective.aliasing.name.source.range.start).toBe(363)
          expect(structDirective.aliasing.name.source.range.end).toBe(382)
          expect(structDirective.aliasing.name.source.loc.start.line).toBe(17)
          expect(structDirective.aliasing.name.source.loc.start.column).toBe(8)
          expect(structDirective.aliasing.name.source.loc.end.line).toBe(17)
          expect(structDirective.aliasing.name.source.loc.end.column).toBe(28)

          expect(structDirective.aliasing.name.name).toBe('K_StructC')
          expect(structDirective.aliasing.name.namespace).toBe('Namespace')
          expect(structDirective.aliasing.name.isExpression).toBe(false)
        }

        expect('alias' in structDirective.aliasing).toBe(true)
        expect(structDirective.aliasing.alias instanceof Identifier).toBe(true)

        if (structDirective.aliasing.alias !== undefined) {
          expect(structDirective.aliasing.alias.kind).toBe(Kind.Identifier)
          expect(structDirective.aliasing.alias.parent).toBe(structDirective.aliasing)
          expect(structDirective.aliasing.alias.source.range.start).toBe(387)
          expect(structDirective.aliasing.alias.source.range.end).toBe(395)
          expect(structDirective.aliasing.alias.source.loc.start.line).toBe(17)
          expect(structDirective.aliasing.alias.source.loc.start.column).toBe(32)
          expect(structDirective.aliasing.alias.source.loc.end.line).toBe(17)
          expect(structDirective.aliasing.alias.source.loc.end.column).toBe(41)

          expect(structDirective.aliasing.alias.name).toBe('K_StructE')
          expect(structDirective.aliasing.alias.isExpression).toBe(false)
        }
      }
    }
  })

  test('generate ConstDeclaration node', () => {
    let constDirective = astDeclarations.program?.directives[10] as ConstDirective
    expect(constDirective).toBeDefined()

    if (constDirective !== undefined) {
      expect(constDirective.kind).toBe(Kind.ConstDirective)
      expect(constDirective.parent).toBe(astDeclarations.program)
      expect(constDirective.source.range.start).toBe(397)
      expect(constDirective.source.range.end).toBe(413)
      expect(constDirective.source.loc.start.line).toBe(18)
      expect(constDirective.source.loc.start.column).toBe(0)
      expect(constDirective.source.loc.end.line).toBe(18)
      expect(constDirective.source.loc.end.column).toBe(17)

      expect('declaration' in constDirective).toBe(true)
      expect(constDirective.declaration instanceof ConstDeclaration).toBe(true)
      expect(constDirective.aliasing).toBeUndefined()

      if (constDirective.declaration !== undefined) {
        expect('name' in constDirective.declaration).toBe(true)
        expect(constDirective.declaration.name instanceof Identifier).toBe(true)
        expect(constDirective.declaration.name.kind).toBe(Kind.Identifier)
        expect(constDirective.declaration.name.parent).toBe(constDirective.declaration)
        expect(constDirective.declaration.name.name).toBe('C_ConstA')
        expect(constDirective.declaration.name.isExpression).toBe(false)

        expect('value' in constDirective.declaration).toBe(true)
        expect(constDirective.declaration.value instanceof IntegerLiteral).toBe(true)
        expect((constDirective.declaration.value as IntegerLiteral).value).toBe(1)
      }
    }

    constDirective = astDeclarations.program?.directives[11] as ConstDirective
    expect(constDirective).toBeDefined()

    if (constDirective !== undefined) {
      expect(constDirective.kind).toBe(Kind.ConstDirective)
      expect(constDirective.parent).toBe(astDeclarations.program)
      expect(constDirective.source.range.start).toBe(415)
      expect(constDirective.source.range.end).toBe(436)
      expect(constDirective.source.loc.start.line).toBe(19)
      expect(constDirective.source.loc.start.column).toBe(0)
      expect(constDirective.source.loc.end.line).toBe(19)
      expect(constDirective.source.loc.end.column).toBe(22)

      expect('declaration' in constDirective).toBe(true)
      expect(constDirective.declaration instanceof ConstDeclaration).toBe(true)
      expect(constDirective.aliasing).toBeUndefined()

      if (constDirective.declaration !== undefined) {
        expect(constDirective.declaration.kind).toBe(Kind.ConstDeclaration)
        expect(constDirective.declaration.parent).toBe(constDirective)
        expect('name' in constDirective.declaration).toBe(true)
        expect(constDirective.declaration.name instanceof Identifier).toBe(true)
        expect(constDirective.declaration.name.kind).toBe(Kind.Identifier)
        expect(constDirective.declaration.name.parent).toBe(constDirective.declaration)
        expect(constDirective.declaration.name.name).toBe('C_ConstB')
        expect(constDirective.declaration.name.isExpression).toBe(false)

        expect('value' in constDirective.declaration).toBe(true)
        expect(constDirective.declaration.value instanceof ArrayExpression).toBe(true)

        const array = constDirective.declaration.value as ArrayExpression
        expect(array.kind).toBe(Kind.ArrayExpression)
        expect(array.parent).toBe(constDirective.declaration)
        expect(array.source.range.start).toBe(431)
        expect(array.source.range.end).toBe(436)
        expect(array.source.loc.start.line).toBe(19)
        expect(array.source.loc.start.column).toBe(16)
        expect(array.source.loc.end.line).toBe(19)
        expect(array.source.loc.end.column).toBe(22)

        expect(array.isAssociative).toBe(false)

        expect('values' in array).toBe(true)
        expect(array.values.length).toBe(2)

        expect('value' in array.values[0]).toBe(true)
        expect('key' in array.values[0]).toBe(false)
        expect(array.values[0].value instanceof IntegerLiteral).toBe(true)
        expect((array.values[0].value as IntegerLiteral).value).toBe(1)

        expect('value' in array.values[1]).toBe(true)
        expect('key' in array.values[1]).toBe(false)
        expect(array.values[1].value instanceof IntegerLiteral).toBe(true)
        expect((array.values[1].value as IntegerLiteral).value).toBe(2)
      }
    }

    constDirective = astDeclarations.program?.directives[12] as ConstDirective
    expect(constDirective).toBeDefined()

    if (constDirective !== undefined) {
      expect(constDirective.kind).toBe(Kind.ConstDirective)
      expect(constDirective.parent).toBe(astDeclarations.program)
      expect(constDirective.source.range.start).toBe(438)
      expect(constDirective.source.range.end).toBe(473)
      expect(constDirective.source.loc.start.line).toBe(20)
      expect(constDirective.source.loc.start.column).toBe(0)
      expect(constDirective.source.loc.end.line).toBe(20)
      expect(constDirective.source.loc.end.column).toBe(36)

      expect('declaration' in constDirective).toBe(true)
      expect(constDirective.declaration instanceof ConstDeclaration).toBe(true)
      expect(constDirective.aliasing).toBeUndefined()

      if (constDirective.declaration !== undefined) {
        expect(constDirective.declaration.kind).toBe(Kind.ConstDeclaration)
        expect(constDirective.declaration.parent).toBe(constDirective)
        expect('name' in constDirective.declaration).toBe(true)
        expect(constDirective.declaration.name instanceof Identifier).toBe(true)
        expect(constDirective.declaration.name.kind).toBe(Kind.Identifier)
        expect(constDirective.declaration.name.parent).toBe(constDirective.declaration)
        expect(constDirective.declaration.name.name).toBe('C_ConstC')
        expect(constDirective.declaration.name.isExpression).toBe(false)

        expect('value' in constDirective.declaration).toBe(true)
        expect(constDirective.declaration.value instanceof ArrayExpression).toBe(true)

        const array = constDirective.declaration.value as ArrayExpression
        expect(array.kind).toBe(Kind.ArrayExpression)
        expect(array.parent).toBe(constDirective.declaration)
        expect(array.source.range.start).toBe(454)
        expect(array.source.range.end).toBe(473)
        expect(array.source.loc.start.line).toBe(20)
        expect(array.source.loc.start.column).toBe(16)
        expect(array.source.loc.end.line).toBe(20)
        expect(array.source.loc.end.column).toBe(36)

        expect(array.isAssociative).toBe(true)

        expect('values' in array).toBe(true)
        expect(array.values.length).toBe(2)

        expect('value' in array.values[0]).toBe(true)
        expect('key' in array.values[0]).toBe(true)
        expect(array.values[0].key instanceof TextLiteral).toBe(true)
        expect((array.values[0].key as TextLiteral).value).toBe('A')
        expect(array.values[0].value instanceof IntegerLiteral).toBe(true)
        expect((array.values[0].value as IntegerLiteral).value).toBe(1)

        expect('value' in array.values[1]).toBe(true)
        expect('key' in array.values[1]).toBe(true)
        expect(array.values[1].key instanceof TextLiteral).toBe(true)
        expect((array.values[1].key as TextLiteral).value).toBe('B')
        expect(array.values[1].value instanceof IntegerLiteral).toBe(true)
        expect((array.values[1].value as IntegerLiteral).value).toBe(2)
      }
    }

    constDirective = astDeclarations.program?.directives[13] as ConstDirective
    expect(constDirective).toBeDefined()

    if (constDirective !== undefined) {
      expect(constDirective.kind).toBe(Kind.ConstDirective)
      expect(constDirective.parent).toBe(astDeclarations.program)
      expect(constDirective.source.range.start).toBe(475)
      expect(constDirective.source.range.end).toBe(510)
      expect(constDirective.source.loc.start.line).toBe(21)
      expect(constDirective.source.loc.start.column).toBe(0)
      expect(constDirective.source.loc.end.line).toBe(21)
      expect(constDirective.source.loc.end.column).toBe(36)

      expect('declaration' in constDirective).toBe(true)
      expect(constDirective.declaration instanceof ConstDeclaration).toBe(true)
      expect(constDirective.aliasing).toBeUndefined()

      if (constDirective.declaration !== undefined) {
        expect('name' in constDirective.declaration).toBe(true)
        expect(constDirective.declaration.name instanceof Identifier).toBe(true)
        expect(constDirective.declaration.name.name).toBe('C_ConstD')
        expect(constDirective.declaration.name.isExpression).toBe(false)

        expect('value' in constDirective.declaration).toBe(true)
        expect(constDirective.declaration.value instanceof StructExpression).toBe(true)

        const struct = constDirective.declaration.value as StructExpression
        expect(struct.kind).toBe(Kind.StructExpression)
        expect(struct.parent).toBe(constDirective.declaration)
        expect(struct.source.range.start).toBe(491)
        expect(struct.source.range.end).toBe(510)
        expect(struct.source.loc.start.line).toBe(21)
        expect(struct.source.loc.start.column).toBe(16)
        expect(struct.source.loc.end.line).toBe(21)
        expect(struct.source.loc.end.column).toBe(36)

        expect('name' in struct).toBe(true)
        expect(struct.name instanceof Identifier).toBe(true)
        expect(struct.name.name).toBe('K_StructA')
        expect(struct.name.isExpression).toBe(false)

        expect('members' in struct).toBe(true)
        expect(struct.members.length).toBe(1)

        expect(struct.members[0].name.name).toBe('A1')
        expect(struct.members[0].value instanceof IntegerLiteral).toBe(true)
        expect((struct.members[0].value as IntegerLiteral).value).toBe(0)
      }
    }

    constDirective = astDeclarations.program?.directives[14] as ConstDirective
    expect(constDirective).toBeDefined()

    if (constDirective !== undefined) {
      expect(constDirective.kind).toBe(Kind.ConstDirective)
      expect(constDirective.parent).toBe(astDeclarations.program)
      expect(constDirective.source.range.start).toBe(512)
      expect(constDirective.source.range.end).toBe(549)
      expect(constDirective.source.loc.start.line).toBe(22)
      expect(constDirective.source.loc.start.column).toBe(0)
      expect(constDirective.source.loc.end.line).toBe(22)
      expect(constDirective.source.loc.end.column).toBe(38)

      expect(constDirective.declaration).toBeUndefined()
      expect('aliasing' in constDirective).toBe(true)
      expect(constDirective.aliasing instanceof ConstAliasing).toBe(true)

      if (constDirective.aliasing !== undefined) {
        expect(constDirective.aliasing.kind).toBe(Kind.ConstAliasing)
        expect(constDirective.aliasing.parent).toBe(constDirective)
        expect('name' in constDirective.aliasing).toBe(true)
        expect(constDirective.aliasing.name instanceof Identifier).toBe(true)
        expect(constDirective.aliasing.name.namespace).toBe('Namespace')
        expect(constDirective.aliasing.name.name).toBe('C_ConstE')
        expect(constDirective.aliasing.name.isExpression).toBe(false)

        expect('alias' in constDirective.aliasing).toBe(true)
        expect(constDirective.aliasing.alias instanceof Identifier).toBe(true)
        expect(constDirective.aliasing.alias.name).toBe('C_ConstF')
        expect(constDirective.aliasing.alias.isExpression).toBe(false)
      }
    }
  })

  test('generate VariableDeclaration node', () => {
    let variableDeclaration = astDeclarations.program?.declarations[0] as VariableDeclaration
    expect(variableDeclaration).toBeDefined()

    if (variableDeclaration !== undefined) {
      expect(variableDeclaration.kind).toBe(Kind.VariableDeclaration)
      expect(variableDeclaration.parent).toBe(astDeclarations.program)
      expect(variableDeclaration.source.range.start).toBe(552)
      expect(variableDeclaration.source.range.end).toBe(599)
      expect(variableDeclaration.source.loc.start.line).toBe(24)
      expect(variableDeclaration.source.loc.start.column).toBe(0)
      expect(variableDeclaration.source.loc.end.line).toBe(24)
      expect(variableDeclaration.source.loc.end.column).toBe(48)

      expect(variableDeclaration.storage).toBeDefined()
      expect(variableDeclaration.storage).toBe(Storage.cloud)

      expect(variableDeclaration.type).toBeDefined()
      expect(variableDeclaration.type instanceof SimpleType).toBe(true)
      if (variableDeclaration.type !== undefined) {
        expect(variableDeclaration.type.kind).toBe(Kind.SimpleType)
        expect(variableDeclaration.type.parent).toBe(variableDeclaration)
        expect((variableDeclaration.type as SimpleType).name).toBe(TypeName.Integer)
      }

      expect(variableDeclaration.name).toBeDefined()
      expect(variableDeclaration.name.name).toBe('VarA')

      expect(variableDeclaration.alias).toBeDefined()
      expect(variableDeclaration.alias?.name).toBe('VarB')

      expect(variableDeclaration.forTarget).toBeDefined()
      if (variableDeclaration.forTarget !== undefined) {
        expect(variableDeclaration.forTarget instanceof ThisExpression).toBe(true)
        expect(variableDeclaration.forTarget.kind).toBe(Kind.ThisExpression)
        expect(variableDeclaration.forTarget.parent).toBe(variableDeclaration)
      }

      expect(variableDeclaration.initializerSign).toBeDefined()
      expect(variableDeclaration.initializerSign).toBe(InitializerSign['='])

      expect(variableDeclaration.initializerExpression).toBeDefined()
      if (variableDeclaration.initializerExpression !== undefined) {
        expect(variableDeclaration.initializerExpression instanceof IntegerLiteral).toBe(true)
        expect(variableDeclaration.initializerExpression.kind).toBe(Kind.IntegerLiteral)
        expect(variableDeclaration.initializerExpression.parent).toBe(variableDeclaration)
        expect((variableDeclaration.initializerExpression as IntegerLiteral).value).toBe(1)
      }

      expect(variableDeclaration.initializerType).toBeUndefined()
    }

    variableDeclaration = astDeclarations.program?.declarations[1] as VariableDeclaration
    expect(variableDeclaration).toBeDefined()

    if (variableDeclaration !== undefined) {
      expect(variableDeclaration.kind).toBe(Kind.VariableDeclaration)
      expect(variableDeclaration.parent).toBe(astDeclarations.program)
      expect(variableDeclaration.source.range.start).toBe(601)
      expect(variableDeclaration.source.range.end).toBe(648)
      expect(variableDeclaration.source.loc.start.line).toBe(25)
      expect(variableDeclaration.source.loc.start.column).toBe(0)
      expect(variableDeclaration.source.loc.end.line).toBe(25)
      expect(variableDeclaration.source.loc.end.column).toBe(48)

      expect(variableDeclaration.storage).toBeDefined()
      expect(variableDeclaration.storage).toBe(Storage.metadata)

      expect(variableDeclaration.type).toBeUndefined()

      expect(variableDeclaration.name).toBeDefined()
      expect(variableDeclaration.name.name).toBe('VarC')

      expect(variableDeclaration.alias).toBeDefined()
      expect(variableDeclaration.alias?.name).toBe('VarD')

      expect(variableDeclaration.forTarget).toBeDefined()
      if (variableDeclaration.forTarget !== undefined) {
        expect(variableDeclaration.forTarget instanceof Identifier).toBe(true)
        expect(variableDeclaration.forTarget.kind).toBe(Kind.Identifier)
        expect(variableDeclaration.forTarget.parent).toBe(variableDeclaration)
        expect((variableDeclaration.forTarget as Identifier).isExpression).toBe(true)
      }

      expect(variableDeclaration.initializerSign).toBeDefined()
      expect(variableDeclaration.initializerSign).toBe(InitializerSign['<=>'])

      expect(variableDeclaration.initializerExpression).toBeUndefined()

      expect(variableDeclaration.initializerType).toBeDefined()
      if (variableDeclaration.initializerType !== undefined) {
        expect(variableDeclaration.initializerType instanceof SimpleType).toBe(true)
        expect(variableDeclaration.initializerType.kind).toBe(Kind.SimpleType)
        expect(variableDeclaration.initializerType.parent).toBe(variableDeclaration)
        expect((variableDeclaration.initializerType as SimpleType).name).toBe(TypeName.Real)
      }
    }
  })

  test('generate FunctionDeclaration node', () => {
    let functionDeclaration = astDeclarations.program?.declarations[2] as FunctionDeclaration
    expect(functionDeclaration).toBeDefined()

    if (functionDeclaration !== undefined) {
      expect(functionDeclaration.kind).toBe(Kind.FunctionDeclaration)
      expect(functionDeclaration.parent).toBe(astDeclarations.program)
      expect(functionDeclaration.source.range.start).toBe(651)
      expect(functionDeclaration.source.range.end).toBe(665)
      expect(functionDeclaration.source.loc.start.line).toBe(27)
      expect(functionDeclaration.source.loc.start.column).toBe(0)
      expect(functionDeclaration.source.loc.end.line).toBe(27)
      expect(functionDeclaration.source.loc.end.column).toBe(15)

      expect(functionDeclaration.type).toBeDefined()
      expect(functionDeclaration.type instanceof SimpleType).toBe(true)
      if (functionDeclaration.type !== undefined) {
        expect((functionDeclaration.type as SimpleType).name).toBe(TypeName.Void)
      }

      expect(functionDeclaration.name).toBeDefined()
      expect(functionDeclaration.name.name).toBe('FuncA')

      expect(functionDeclaration.parameters).toBeDefined()
      expect(functionDeclaration.parameters.length).toBe(0)

      expect(functionDeclaration.body).toBeDefined()
      expect(functionDeclaration.body instanceof BlockStatement).toBe(true)
      expect(functionDeclaration.body.kind).toBe(Kind.BlockStatement)
      expect(functionDeclaration.body.parent).toBe(functionDeclaration)
      expect(functionDeclaration.body.source.range.start).toBe(664)
      expect(functionDeclaration.body.source.range.end).toBe(665)
      expect(functionDeclaration.body.source.loc.start.line).toBe(27)
      expect(functionDeclaration.body.source.loc.start.column).toBe(13)
      expect(functionDeclaration.body.source.loc.end.line).toBe(27)
      expect(functionDeclaration.body.source.loc.end.column).toBe(15)
    }

    functionDeclaration = astDeclarations.program?.declarations[3] as FunctionDeclaration
    expect(functionDeclaration).toBeDefined()

    if (functionDeclaration !== undefined) {
      expect(functionDeclaration.kind).toBe(Kind.FunctionDeclaration)
      expect(functionDeclaration.parent).toBe(astDeclarations.program)
      expect(functionDeclaration.source.range.start).toBe(667)
      expect(functionDeclaration.source.range.end).toBe(708)
      expect(functionDeclaration.source.loc.start.line).toBe(28)
      expect(functionDeclaration.source.loc.start.column).toBe(0)
      expect(functionDeclaration.source.loc.end.line).toBe(30)
      expect(functionDeclaration.source.loc.end.column).toBe(1)

      expect(functionDeclaration.type).toBeDefined()
      expect(functionDeclaration.type instanceof SimpleType).toBe(true)
      if (functionDeclaration.type !== undefined) {
        expect((functionDeclaration.type as SimpleType).name).toBe(TypeName.Integer)
      }

      expect(functionDeclaration.name).toBeDefined()
      expect(functionDeclaration.name.name).toBe('FuncB')

      expect(functionDeclaration.parameters).toBeDefined()
      expect(functionDeclaration.parameters.length).toBe(2)

      expect(functionDeclaration.parameters[0]).toBeDefined()
      expect(functionDeclaration.parameters[0] instanceof FunctionParameterDeclaration).toBe(true)
      expect(functionDeclaration.parameters[0].kind).toBe(Kind.FunctionParameterDeclaration)
      expect(functionDeclaration.parameters[0].parent).toBe(functionDeclaration)
      expect(functionDeclaration.parameters[0].type instanceof SimpleType).toBe(true)
      expect((functionDeclaration.parameters[0].type as SimpleType).name).toBe(TypeName.Integer)

      expect(functionDeclaration.parameters[1]).toBeDefined()
      expect(functionDeclaration.parameters[1] instanceof FunctionParameterDeclaration).toBe(true)
      expect(functionDeclaration.parameters[1].kind).toBe(Kind.FunctionParameterDeclaration)
      expect(functionDeclaration.parameters[1].parent).toBe(functionDeclaration)
      expect(functionDeclaration.parameters[1].type instanceof ClassType).toBe(true)
      expect((functionDeclaration.parameters[1].type as ClassType).name).toBe('CSmMode')

      expect(functionDeclaration.body).toBeDefined()
      expect(functionDeclaration.body instanceof BlockStatement).toBe(true)
      expect(functionDeclaration.body.kind).toBe(Kind.BlockStatement)
      expect(functionDeclaration.body.parent).toBe(functionDeclaration)
      expect(functionDeclaration.body.source.range.start).toBe(705)
      expect(functionDeclaration.body.source.range.end).toBe(708)
      expect(functionDeclaration.body.source.loc.start.line).toBe(28)
      expect(functionDeclaration.body.source.loc.start.column).toBe(38)
      expect(functionDeclaration.body.source.loc.end.line).toBe(30)
      expect(functionDeclaration.body.source.loc.end.column).toBe(1)
    }
  })

  test('generate LabelDeclaration node', () => {
    const labelDeclaration = astDeclarations.program?.declarations[4] as LabelDeclaration
    expect(labelDeclaration).toBeDefined()

    if (labelDeclaration !== undefined) {
      expect(labelDeclaration.kind).toBe(Kind.LabelDeclaration)
      expect(labelDeclaration.parent).toBe(astDeclarations.program)
      expect(labelDeclaration.source.range.start).toBe(711)
      expect(labelDeclaration.source.range.end).toBe(749)
      expect(labelDeclaration.source.loc.start.line).toBe(32)
      expect(labelDeclaration.source.loc.start.column).toBe(0)
      expect(labelDeclaration.source.loc.end.line).toBe(35)
      expect(labelDeclaration.source.loc.end.column).toBe(3)

      expect(labelDeclaration.name).toBeDefined()
      expect(labelDeclaration.name.name).toBe('LabelA')

      expect(labelDeclaration.body).toBeDefined()
      expect(labelDeclaration.body.length).toBe(1)
    }
  })
})

const inputMain = `
main() {
  {

  }
  declare Integer BlockStatement;
  1 + 1;
  A as CMode;
  A.B;
  C.D();
  A[1];
  -1;
  !A;
  1 * 2;
  3 / 4;
  5 % 6;
  7 + 8;
  9 - 10;
  "A" ^ "B";
  11 < 12;
  13 > 14;
  15 <= 16;
  17 >= 18;
  A != B;
  C == D;
  True || False;
  False && True;
  A is CMode;
  """A {{{B}}} C {{{D}}}{{{E}}} F""";
  """{{{F}}}""";
  dumptype(K_StructA);
  dump("DUMP");
  VarA;
  NamespaceB::VarC;
  123;
  456.789;
  0.1e+2;
  True;
  False;
  Null;
  NullId;
  CSmMode::EMedal::Gold;
  ::EMedal::Gold;
  TimeLib::EDateFormats::Time;
  This;
  (1);
  .5;
  cast(CSmMode, A);
  FunctionTest(0, Integer);
  Now;
  AAAA.Now;
}
`

let astMain: AST

describe('lib/ast part 2', () => {
  beforeAll(async () => {
    astMain = (await parse(inputMain, { buildAst: true })).ast
  })

  test('generate Main node', () => {
    const main = astMain.program?.main
    expect(main).toBeDefined()

    if (main !== undefined) {
      expect(main.kind).toBe(Kind.Main)
      expect(main.parent).toBe(astMain.program)
      expect(main.source.range.start).toBe(1)
      expect(main.source.range.end).toBe(632)
      expect(main.source.loc.start.line).toBe(2)
      expect(main.source.loc.start.column).toBe(0)
      expect(main.source.loc.end.line).toBe(52)
      expect(main.source.loc.end.column).toBe(1)

      expect(main.body).toBeDefined()
      expect(main.body instanceof BlockStatement).toBe(true)
    }
  })

  test('generate BlockStatement node for main function', () => {
    const blockStatement = astMain.program?.main?.body
    expect(blockStatement).toBeDefined()

    if (blockStatement !== undefined) {
      expect(blockStatement instanceof BlockStatement).toBe(true)
      expect(blockStatement.kind).toBe(Kind.BlockStatement)
      expect(blockStatement.parent).toBe(astMain.program?.main)
      expect(blockStatement.source.range.start).toBe(8)
      expect(blockStatement.source.range.end).toBe(632)
      expect(blockStatement.source.loc.start.line).toBe(2)
      expect(blockStatement.source.loc.start.column).toBe(7)
      expect(blockStatement.source.loc.end.line).toBe(52)
      expect(blockStatement.source.loc.end.column).toBe(1)

      expect(blockStatement.body).toBeDefined()
      expect(blockStatement.body.length).toBe(47)
    }
  })

  test('generate BlockStatement node', () => {
    const blockStatement = astMain.program?.main?.body.body[0] as BlockStatement
    expect(blockStatement).toBeDefined()

    if (blockStatement !== undefined) {
      expect(blockStatement instanceof BlockStatement).toBe(true)
      expect(blockStatement.kind).toBe(Kind.BlockStatement)
      expect(blockStatement.parent).toBe(astMain.program?.main?.body)
      expect(blockStatement.source.range.start).toBe(12)
      expect(blockStatement.source.range.end).toBe(17)
      expect(blockStatement.source.loc.start.line).toBe(3)
      expect(blockStatement.source.loc.start.column).toBe(2)
      expect(blockStatement.source.loc.end.line).toBe(5)
      expect(blockStatement.source.loc.end.column).toBe(3)

      expect(blockStatement.body).toBeDefined()
      expect(blockStatement.body.length).toBe(0)
    }
  })

  test('generate VariableDeclaration node', () => {
    const variableDeclaration = astMain.program?.main?.body.body[1] as VariableDeclaration
    expect(variableDeclaration).toBeDefined()

    if (variableDeclaration !== undefined) {
      expect(variableDeclaration instanceof VariableDeclaration).toBe(true)
      expect(variableDeclaration.kind).toBe(Kind.VariableDeclaration)
      expect(variableDeclaration.parent).toBe(astMain.program?.main?.body)
      expect(variableDeclaration.source.range.start).toBe(21)
      expect(variableDeclaration.source.range.end).toBe(51)
      expect(variableDeclaration.source.loc.start.line).toBe(6)
      expect(variableDeclaration.source.loc.start.column).toBe(2)
      expect(variableDeclaration.source.loc.end.line).toBe(6)
      expect(variableDeclaration.source.loc.end.column).toBe(33)

      expect((variableDeclaration.type as SimpleType).name).toBe(TypeName.Integer)
      expect(variableDeclaration.name.name).toBe('BlockStatement')
    }
  })

  test('generate ExpressionStatement node', () => {
    const expressionStatement = astMain.program?.main?.body.body[2] as ExpressionStatement
    expect(expressionStatement).toBeDefined()

    if (expressionStatement !== undefined) {
      expect(expressionStatement instanceof ExpressionStatement).toBe(true)
      expect(expressionStatement.kind).toBe(Kind.ExpressionStatement)
      expect(expressionStatement.parent).toBe(astMain.program?.main?.body)
      expect(expressionStatement.source.range.start).toBe(55)
      expect(expressionStatement.source.range.end).toBe(60)
      expect(expressionStatement.source.loc.start.line).toBe(7)
      expect(expressionStatement.source.loc.start.column).toBe(2)
      expect(expressionStatement.source.loc.end.line).toBe(7)
      expect(expressionStatement.source.loc.end.column).toBe(8)

      expect(expressionStatement.expression).toBeDefined()
      expect(expressionStatement.expression.kind).toBe(Kind.BinaryExpression)
      expect(expressionStatement.expression.parent).toBe(expressionStatement)
      expect(expressionStatement.expression.source.range.start).toBe(55)
      expect(expressionStatement.expression.source.range.end).toBe(59)
      expect(expressionStatement.expression.source.loc.start.line).toBe(7)
      expect(expressionStatement.expression.source.loc.start.column).toBe(2)
      expect(expressionStatement.expression.source.loc.end.line).toBe(7)
      expect(expressionStatement.expression.source.loc.end.column).toBe(7)
    }
  })

  test('generate AsExpression node', () => {
    const asExpression = (astMain.program?.main?.body.body[3] as ExpressionStatement).expression as AsExpression
    expect(asExpression).toBeDefined()

    if (asExpression !== undefined) {
      expect(asExpression instanceof AsExpression).toBe(true)
      expect(asExpression.kind).toBe(Kind.AsExpression)
      expect(asExpression.parent).toBe(astMain.program?.main?.body.body[3])
      expect(asExpression.source.range.start).toBe(64)
      expect(asExpression.source.range.end).toBe(73)
      expect(asExpression.source.loc.start.line).toBe(8)
      expect(asExpression.source.loc.start.column).toBe(2)
      expect(asExpression.source.loc.end.line).toBe(8)
      expect(asExpression.source.loc.end.column).toBe(12)

      expect(asExpression.expression).toBeDefined()
      expect(asExpression.expression.kind).toBe(Kind.Identifier)
      expect(asExpression.expression.parent).toBe(asExpression)
      expect((asExpression.expression as Identifier).name).toBe('A')
      expect((asExpression.expression as Identifier).isExpression).toBe(true)

      expect(asExpression.class).toBeDefined()
      expect(asExpression.class.kind).toBe(Kind.ClassType)
      expect(asExpression.class.parent).toBe(asExpression)
      expect(asExpression.class.name).toBe('CMode')
    }
  })

  test('generate IsExpression node', () => {
    const isExpression = (astMain.program?.main?.body.body[23] as ExpressionStatement).expression as IsExpression
    expect(isExpression).toBeDefined()

    if (isExpression !== undefined) {
      expect(isExpression instanceof IsExpression).toBe(true)
      expect(isExpression.kind).toBe(Kind.IsExpression)
      expect(isExpression.parent).toBe(astMain.program?.main?.body.body[23])
      expect(isExpression.source.range.start).toBe(273)
      expect(isExpression.source.range.end).toBe(282)
      expect(isExpression.source.loc.start.line).toBe(28)
      expect(isExpression.source.loc.start.column).toBe(2)
      expect(isExpression.source.loc.end.line).toBe(28)
      expect(isExpression.source.loc.end.column).toBe(12)

      expect(isExpression.expression).toBeDefined()
      expect(isExpression.expression.kind).toBe(Kind.Identifier)
      expect(isExpression.expression.parent).toBe(isExpression)
      expect((isExpression.expression as Identifier).name).toBe('A')
      expect((isExpression.expression as Identifier).isExpression).toBe(true)

      expect(isExpression.class).toBeDefined()
      expect(isExpression.class.kind).toBe(Kind.ClassType)
      expect(isExpression.class.parent).toBe(isExpression)
      expect(isExpression.class.name).toBe('CMode')
    }
  })

  test('generate FunctionCallExpression node', () => {
    const functionCallExpression = (astMain.program?.main?.body.body[44] as ExpressionStatement).expression as FunctionCallExpression
    expect(functionCallExpression).toBeDefined()

    if (functionCallExpression !== undefined) {
      expect(functionCallExpression instanceof FunctionCallExpression).toBe(true)
      expect(functionCallExpression.kind).toBe(Kind.FunctionCallExpression)
      expect(functionCallExpression.parent).toBe(astMain.program?.main?.body.body[44])
      expect(functionCallExpression.source.range.start).toBe(587)
      expect(functionCallExpression.source.range.end).toBe(610)
      expect(functionCallExpression.source.loc.start.line).toBe(49)
      expect(functionCallExpression.source.loc.start.column).toBe(2)
      expect(functionCallExpression.source.loc.end.line).toBe(49)
      expect(functionCallExpression.source.loc.end.column).toBe(26)

      expect(functionCallExpression.name).toBeDefined()
      expect(functionCallExpression.name.name).toBe('FunctionTest')

      expect(functionCallExpression.arguments).toBeDefined()
      expect(functionCallExpression.arguments.length).toBe(2)
      expect(functionCallExpression.arguments[0].kind).toBe(Kind.IntegerLiteral)
      expect(functionCallExpression.arguments[0].parent).toBe(functionCallExpression)
      expect(functionCallExpression.arguments[1].kind).toBe(Kind.SimpleType)
      expect(functionCallExpression.arguments[1].parent).toBe(functionCallExpression)
    }
  })

  test('generate DotAccessExpression node', () => {
    let dotAccessExpression = (astMain.program?.main?.body.body[4] as ExpressionStatement).expression as DotAccessExpression
    expect(dotAccessExpression).toBeDefined()

    if (dotAccessExpression !== undefined) {
      expect(dotAccessExpression instanceof DotAccessExpression).toBe(true)
      expect(dotAccessExpression.kind).toBe(Kind.DotAccessExpression)
      expect(dotAccessExpression.parent).toBe(astMain.program?.main?.body.body[4])
      expect(dotAccessExpression.source.range.start).toBe(78)
      expect(dotAccessExpression.source.range.end).toBe(80)
      expect(dotAccessExpression.source.loc.start.line).toBe(9)
      expect(dotAccessExpression.source.loc.start.column).toBe(2)
      expect(dotAccessExpression.source.loc.end.line).toBe(9)
      expect(dotAccessExpression.source.loc.end.column).toBe(5)

      expect(dotAccessExpression.object).toBeDefined()
      expect(dotAccessExpression.object.kind).toBe(Kind.Identifier)
      expect(dotAccessExpression.object.parent).toBe(dotAccessExpression)
      expect((dotAccessExpression.object as Identifier).name).toBe('A')
      expect((dotAccessExpression.object as Identifier).isExpression).toBe(true)

      expect(dotAccessExpression.functionCall).toBeUndefined()

      expect(dotAccessExpression.member).toBeDefined()
      if (dotAccessExpression.member !== undefined) {
        expect(dotAccessExpression.member.kind).toBe(Kind.Identifier)
        expect(dotAccessExpression.member.parent).toBe(dotAccessExpression)
        expect(dotAccessExpression.member.isExpression).toBe(false)
      }
    }

    dotAccessExpression = (astMain.program?.main?.body.body[5] as ExpressionStatement).expression as DotAccessExpression
    expect(dotAccessExpression).toBeDefined()

    if (dotAccessExpression !== undefined) {
      expect(dotAccessExpression instanceof DotAccessExpression).toBe(true)
      expect(dotAccessExpression.kind).toBe(Kind.DotAccessExpression)
      expect(dotAccessExpression.parent).toBe(astMain.program?.main?.body.body[5])
      expect(dotAccessExpression.source.range.start).toBe(85)
      expect(dotAccessExpression.source.range.end).toBe(89)
      expect(dotAccessExpression.source.loc.start.line).toBe(10)
      expect(dotAccessExpression.source.loc.start.column).toBe(2)
      expect(dotAccessExpression.source.loc.end.line).toBe(10)
      expect(dotAccessExpression.source.loc.end.column).toBe(7)

      expect(dotAccessExpression.object).toBeDefined()
      expect(dotAccessExpression.object.kind).toBe(Kind.Identifier)
      expect(dotAccessExpression.object.parent).toBe(dotAccessExpression)
      expect((dotAccessExpression.object as Identifier).isExpression).toBe(true)

      expect(dotAccessExpression.functionCall).toBeDefined()
      if (dotAccessExpression.functionCall !== undefined) {
        expect(dotAccessExpression.functionCall.kind).toBe(Kind.FunctionCallExpression)
        expect(dotAccessExpression.functionCall.parent).toBe(dotAccessExpression)
      }

      expect(dotAccessExpression.member).toBeUndefined()
    }
  })

  test('generate IndexAccessExpression node', () => {
    const indexAccessExpression = (astMain.program?.main?.body.body[6] as ExpressionStatement).expression as IndexAccessExpression
    expect(indexAccessExpression).toBeDefined()

    if (indexAccessExpression !== undefined) {
      expect(indexAccessExpression instanceof IndexAccessExpression).toBe(true)
      expect(indexAccessExpression.kind).toBe(Kind.IndexAccessExpression)
      expect(indexAccessExpression.parent).toBe(astMain.program?.main?.body.body[6])
      expect(indexAccessExpression.source.range.start).toBe(94)
      expect(indexAccessExpression.source.range.end).toBe(97)
      expect(indexAccessExpression.source.loc.start.line).toBe(11)
      expect(indexAccessExpression.source.loc.start.column).toBe(2)
      expect(indexAccessExpression.source.loc.end.line).toBe(11)
      expect(indexAccessExpression.source.loc.end.column).toBe(6)

      expect(indexAccessExpression.array).toBeDefined()
      expect(indexAccessExpression.array.kind).toBe(Kind.Identifier)
      expect(indexAccessExpression.array.parent).toBe(indexAccessExpression)
      expect((indexAccessExpression.array as Identifier).isExpression).toBe(true)

      expect(indexAccessExpression.index).toBeDefined()
      expect(indexAccessExpression.index.kind).toBe(Kind.IntegerLiteral)
      expect(indexAccessExpression.index.parent).toBe(indexAccessExpression)
    }
  })

  test('generate UnaryExpression node', () => {
    let unaryExpression = (astMain.program?.main?.body.body[7] as ExpressionStatement).expression as UnaryExpression
    expect(unaryExpression).toBeDefined()

    if (unaryExpression !== undefined) {
      expect(unaryExpression instanceof UnaryExpression).toBe(true)
      expect(unaryExpression.kind).toBe(Kind.UnaryExpression)
      expect(unaryExpression.parent).toBe(astMain.program?.main?.body.body[7])
      expect(unaryExpression.source.range.start).toBe(102)
      expect(unaryExpression.source.range.end).toBe(103)
      expect(unaryExpression.source.loc.start.line).toBe(12)
      expect(unaryExpression.source.loc.start.column).toBe(2)
      expect(unaryExpression.source.loc.end.line).toBe(12)
      expect(unaryExpression.source.loc.end.column).toBe(4)

      expect(unaryExpression.operator).toBeDefined()
      expect(unaryExpression.operator).toBe(UnaryOperator['-'])

      expect(unaryExpression.argument).toBeDefined()
      expect(unaryExpression.argument.kind).toBe(Kind.IntegerLiteral)
      expect(unaryExpression.argument.parent).toBe(unaryExpression)
    }

    unaryExpression = (astMain.program?.main?.body.body[8] as ExpressionStatement).expression as UnaryExpression
    expect(unaryExpression).toBeDefined()

    if (unaryExpression !== undefined) {
      expect(unaryExpression instanceof UnaryExpression).toBe(true)
      expect(unaryExpression.kind).toBe(Kind.UnaryExpression)
      expect(unaryExpression.parent).toBe(astMain.program?.main?.body.body[8])
      expect(unaryExpression.source.range.start).toBe(108)
      expect(unaryExpression.source.range.end).toBe(109)
      expect(unaryExpression.source.loc.start.line).toBe(13)
      expect(unaryExpression.source.loc.start.column).toBe(2)
      expect(unaryExpression.source.loc.end.line).toBe(13)
      expect(unaryExpression.source.loc.end.column).toBe(4)

      expect(unaryExpression.operator).toBeDefined()
      expect(unaryExpression.operator).toBe(UnaryOperator['!'])

      expect(unaryExpression.argument).toBeDefined()
      expect(unaryExpression.argument.kind).toBe(Kind.Identifier)
      expect(unaryExpression.argument.parent).toBe(unaryExpression)
      expect((unaryExpression.argument as Identifier).isExpression).toBe(true)
    }
  })

  test('generate BinaryExpression node', () => {
    let binaryExpression = (astMain.program?.main?.body.body[9] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[9])
      expect(binaryExpression.source.range.start).toBe(114)
      expect(binaryExpression.source.range.end).toBe(118)
      expect(binaryExpression.source.loc.start.line).toBe(14)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(14)
      expect(binaryExpression.source.loc.end.column).toBe(7)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['*'])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[10] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[10])
      expect(binaryExpression.source.range.start).toBe(123)
      expect(binaryExpression.source.range.end).toBe(127)
      expect(binaryExpression.source.loc.start.line).toBe(15)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(15)
      expect(binaryExpression.source.loc.end.column).toBe(7)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['/'])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[11] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[11])
      expect(binaryExpression.source.range.start).toBe(132)
      expect(binaryExpression.source.range.end).toBe(136)
      expect(binaryExpression.source.loc.start.line).toBe(16)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(16)
      expect(binaryExpression.source.loc.end.column).toBe(7)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['%'])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[12] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[12])
      expect(binaryExpression.source.range.start).toBe(141)
      expect(binaryExpression.source.range.end).toBe(145)
      expect(binaryExpression.source.loc.start.line).toBe(17)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(17)
      expect(binaryExpression.source.loc.end.column).toBe(7)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['+'])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[13] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[13])
      expect(binaryExpression.source.range.start).toBe(150)
      expect(binaryExpression.source.range.end).toBe(155)
      expect(binaryExpression.source.loc.start.line).toBe(18)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(18)
      expect(binaryExpression.source.loc.end.column).toBe(8)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['-'])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[14] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[14])
      expect(binaryExpression.source.range.start).toBe(160)
      expect(binaryExpression.source.range.end).toBe(168)
      expect(binaryExpression.source.loc.start.line).toBe(19)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(19)
      expect(binaryExpression.source.loc.end.column).toBe(11)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['^'])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.TextLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.TextLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[15] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[15])
      expect(binaryExpression.source.range.start).toBe(173)
      expect(binaryExpression.source.range.end).toBe(179)
      expect(binaryExpression.source.loc.start.line).toBe(20)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(20)
      expect(binaryExpression.source.loc.end.column).toBe(9)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['<'])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[16] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[16])
      expect(binaryExpression.source.range.start).toBe(184)
      expect(binaryExpression.source.range.end).toBe(190)
      expect(binaryExpression.source.loc.start.line).toBe(21)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(21)
      expect(binaryExpression.source.loc.end.column).toBe(9)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['>'])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[17] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[17])
      expect(binaryExpression.source.range.start).toBe(195)
      expect(binaryExpression.source.range.end).toBe(202)
      expect(binaryExpression.source.loc.start.line).toBe(22)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(22)
      expect(binaryExpression.source.loc.end.column).toBe(10)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['<='])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[18] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[18])
      expect(binaryExpression.source.range.start).toBe(207)
      expect(binaryExpression.source.range.end).toBe(214)
      expect(binaryExpression.source.loc.start.line).toBe(23)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(23)
      expect(binaryExpression.source.loc.end.column).toBe(10)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['>='])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.left.parent).toBe(binaryExpression)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.IntegerLiteral)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
    }

    binaryExpression = (astMain.program?.main?.body.body[19] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[19])
      expect(binaryExpression.source.range.start).toBe(219)
      expect(binaryExpression.source.range.end).toBe(224)
      expect(binaryExpression.source.loc.start.line).toBe(24)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(24)
      expect(binaryExpression.source.loc.end.column).toBe(8)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['!='])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.Identifier)
      expect(binaryExpression.left.parent).toBe(binaryExpression)
      expect((binaryExpression.left as Identifier).isExpression).toBe(true)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.Identifier)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
      expect((binaryExpression.right as Identifier).isExpression).toBe(true)
    }

    binaryExpression = (astMain.program?.main?.body.body[20] as ExpressionStatement).expression as BinaryExpression
    expect(binaryExpression).toBeDefined()

    if (binaryExpression !== undefined) {
      expect(binaryExpression instanceof BinaryExpression).toBe(true)
      expect(binaryExpression.kind).toBe(Kind.BinaryExpression)
      expect(binaryExpression.parent).toBe(astMain.program?.main?.body.body[20])
      expect(binaryExpression.source.range.start).toBe(229)
      expect(binaryExpression.source.range.end).toBe(234)
      expect(binaryExpression.source.loc.start.line).toBe(25)
      expect(binaryExpression.source.loc.start.column).toBe(2)
      expect(binaryExpression.source.loc.end.line).toBe(25)
      expect(binaryExpression.source.loc.end.column).toBe(8)

      expect(binaryExpression.operator).toBeDefined()
      expect(binaryExpression.operator).toBe(BinaryOperator['=='])

      expect(binaryExpression.left).toBeDefined()
      expect(binaryExpression.left.kind).toBe(Kind.Identifier)
      expect(binaryExpression.left.parent).toBe(binaryExpression)
      expect((binaryExpression.left as Identifier).isExpression).toBe(true)

      expect(binaryExpression.right).toBeDefined()
      expect(binaryExpression.right.kind).toBe(Kind.Identifier)
      expect(binaryExpression.right.parent).toBe(binaryExpression)
      expect((binaryExpression.right as Identifier).isExpression).toBe(true)
    }
  })

  test('generate LogicalExpression node', () => {
    let logicalExpression = (astMain.program?.main?.body.body[21] as ExpressionStatement).expression as LogicalExpression
    expect(logicalExpression).toBeDefined()

    if (logicalExpression !== undefined) {
      expect(logicalExpression instanceof LogicalExpression).toBe(true)
      expect(logicalExpression.kind).toBe(Kind.LogicalExpression)
      expect(logicalExpression.parent).toBe(astMain.program?.main?.body.body[21])
      expect(logicalExpression.source.range.start).toBe(239)
      expect(logicalExpression.source.range.end).toBe(251)
      expect(logicalExpression.source.loc.start.line).toBe(26)
      expect(logicalExpression.source.loc.start.column).toBe(2)
      expect(logicalExpression.source.loc.end.line).toBe(26)
      expect(logicalExpression.source.loc.end.column).toBe(15)

      expect(logicalExpression.operator).toBeDefined()
      expect(logicalExpression.operator).toBe(LogicalOperator['||'])

      expect(logicalExpression.left).toBeDefined()
      expect(logicalExpression.left.kind).toBe(Kind.BooleanLiteral)
      expect(logicalExpression.left.parent).toBe(logicalExpression)

      expect(logicalExpression.right).toBeDefined()
      expect(logicalExpression.right.kind).toBe(Kind.BooleanLiteral)
      expect(logicalExpression.right.parent).toBe(logicalExpression)
    }

    logicalExpression = (astMain.program?.main?.body.body[22] as ExpressionStatement).expression as LogicalExpression
    expect(logicalExpression).toBeDefined()

    if (logicalExpression !== undefined) {
      expect(logicalExpression instanceof LogicalExpression).toBe(true)
      expect(logicalExpression.kind).toBe(Kind.LogicalExpression)
      expect(logicalExpression.parent).toBe(astMain.program?.main?.body.body[22])
      expect(logicalExpression.source.range.start).toBe(256)
      expect(logicalExpression.source.range.end).toBe(268)
      expect(logicalExpression.source.loc.start.line).toBe(27)
      expect(logicalExpression.source.loc.start.column).toBe(2)
      expect(logicalExpression.source.loc.end.line).toBe(27)
      expect(logicalExpression.source.loc.end.column).toBe(15)

      expect(logicalExpression.operator).toBeDefined()
      expect(logicalExpression.operator).toBe(LogicalOperator['&&'])

      expect(logicalExpression.left).toBeDefined()
      expect(logicalExpression.left.kind).toBe(Kind.BooleanLiteral)
      expect(logicalExpression.left.parent).toBe(logicalExpression)

      expect(logicalExpression.right).toBeDefined()
      expect(logicalExpression.right.kind).toBe(Kind.BooleanLiteral)
      expect(logicalExpression.right.parent).toBe(logicalExpression)
    }
  })

  test('generate TemplateTextLiteral node', () => {
    let templateTextLiteral = (astMain.program?.main?.body.body[24] as ExpressionStatement).expression as TemplateTextLiteral
    expect(templateTextLiteral).toBeDefined()

    if (templateTextLiteral !== undefined) {
      expect(templateTextLiteral instanceof TemplateTextLiteral).toBe(true)
      expect(templateTextLiteral.kind).toBe(Kind.TemplateTextLiteral)
      expect(templateTextLiteral.parent).toBe(astMain.program?.main?.body.body[24])
      expect(templateTextLiteral.source.range.start).toBe(287)
      expect(templateTextLiteral.source.range.end).toBe(320)
      expect(templateTextLiteral.source.loc.start.line).toBe(29)
      expect(templateTextLiteral.source.loc.start.column).toBe(2)
      expect(templateTextLiteral.source.loc.end.line).toBe(29)
      expect(templateTextLiteral.source.loc.end.column).toBe(36)

      expect(templateTextLiteral.expressions).toBeDefined()
      expect(templateTextLiteral.expressions.length).toBe(3)

      expect(templateTextLiteral.texts).toBeDefined()
      expect(templateTextLiteral.texts.length).toBe(3)
      expect(templateTextLiteral.texts[0].value).toBe('A ')
      expect(templateTextLiteral.texts[0].source.range.start).toBe(290)
      expect(templateTextLiteral.texts[0].source.range.end).toBe(291)
      expect(templateTextLiteral.texts[1].value).toBe(' C ')
      expect(templateTextLiteral.texts[1].source.range.start).toBe(299)
      expect(templateTextLiteral.texts[1].source.range.end).toBe(301)
      expect(templateTextLiteral.texts[2].value).toBe(' F')
      expect(templateTextLiteral.texts[2].source.range.start).toBe(316)
      expect(templateTextLiteral.texts[2].source.range.end).toBe(317)
    }

    templateTextLiteral = (astMain.program?.main?.body.body[25] as ExpressionStatement).expression as TemplateTextLiteral
    expect(templateTextLiteral).toBeDefined()

    if (templateTextLiteral !== undefined) {
      expect(templateTextLiteral instanceof TemplateTextLiteral).toBe(true)
      expect(templateTextLiteral.kind).toBe(Kind.TemplateTextLiteral)
      expect(templateTextLiteral.parent).toBe(astMain.program?.main?.body.body[25])
      expect(templateTextLiteral.source.range.start).toBe(325)
      expect(templateTextLiteral.source.range.end).toBe(337)
      expect(templateTextLiteral.source.loc.start.line).toBe(30)
      expect(templateTextLiteral.source.loc.start.column).toBe(2)
      expect(templateTextLiteral.source.loc.end.line).toBe(30)
      expect(templateTextLiteral.source.loc.end.column).toBe(15)

      expect(templateTextLiteral.expressions).toBeDefined()
      expect(templateTextLiteral.expressions.length).toBe(1)

      expect(templateTextLiteral.texts).toBeDefined()
      expect(templateTextLiteral.texts.length).toBe(0)
    }
  })

  test('generate DumptypeExpression node', () => {
    const dumptypeExpression = (astMain.program?.main?.body.body[26] as ExpressionStatement).expression as DumptypeExpression
    expect(dumptypeExpression).toBeDefined()

    if (dumptypeExpression !== undefined) {
      expect(dumptypeExpression instanceof DumptypeExpression).toBe(true)
      expect(dumptypeExpression.kind).toBe(Kind.DumptypeExpression)
      expect(dumptypeExpression.parent).toBe(astMain.program?.main?.body.body[26])
      expect(dumptypeExpression.source.range.start).toBe(342)
      expect(dumptypeExpression.source.range.end).toBe(360)
      expect(dumptypeExpression.source.loc.start.line).toBe(31)
      expect(dumptypeExpression.source.loc.start.column).toBe(2)
      expect(dumptypeExpression.source.loc.end.line).toBe(31)
      expect(dumptypeExpression.source.loc.end.column).toBe(21)

      expect(dumptypeExpression.type).toBeDefined()
      expect(dumptypeExpression.type.kind).toBe(Kind.Identifier)
      expect(dumptypeExpression.type.name).toBe('K_StructA')
      expect(dumptypeExpression.type.isExpression).toBe(false)
    }
  })

  test('generate DumpExpression node', () => {
    const dumpExpression = (astMain.program?.main?.body.body[27] as ExpressionStatement).expression as DumpExpression
    expect(dumpExpression).toBeDefined()

    if (dumpExpression !== undefined) {
      expect(dumpExpression instanceof DumpExpression).toBe(true)
      expect(dumpExpression.kind).toBe(Kind.DumpExpression)
      expect(dumpExpression.parent).toBe(astMain.program?.main?.body.body[27])
      expect(dumpExpression.source.range.start).toBe(365)
      expect(dumpExpression.source.range.end).toBe(376)
      expect(dumpExpression.source.loc.start.line).toBe(32)
      expect(dumpExpression.source.loc.start.column).toBe(2)
      expect(dumpExpression.source.loc.end.line).toBe(32)
      expect(dumpExpression.source.loc.end.column).toBe(14)

      expect(dumpExpression.expression).toBeDefined()
      expect(dumpExpression.expression.kind).toBe(Kind.TextLiteral)
      expect(dumpExpression.expression.parent).toBe(dumpExpression)
      expect((dumpExpression.expression as TextLiteral).value).toBe('DUMP')
    }
  })

  test('generate CastExpression node', () => {
    const castExpression = (astMain.program?.main?.body.body[43] as ExpressionStatement).expression as CastExpression
    expect(castExpression).toBeDefined()

    if (castExpression !== undefined) {
      expect(castExpression instanceof CastExpression).toBe(true)
      expect(castExpression.kind).toBe(Kind.CastExpression)
      expect(castExpression.parent).toBe(astMain.program?.main?.body.body[43])
      expect(castExpression.source.range.start).toBe(567)
      expect(castExpression.source.range.end).toBe(582)
      expect(castExpression.source.loc.start.line).toBe(48)
      expect(castExpression.source.loc.start.column).toBe(2)
      expect(castExpression.source.loc.end.line).toBe(48)
      expect(castExpression.source.loc.end.column).toBe(18)

      expect(castExpression.expression).toBeDefined()
      expect(castExpression.expression.kind).toBe(Kind.Identifier)
      expect(castExpression.expression.parent).toBe(castExpression)
      expect((castExpression.expression as Identifier).name).toBe('A')
      expect((castExpression.expression as Identifier).isExpression).toBe(true)

      expect(castExpression.class).toBeDefined()
      expect(castExpression.class.kind).toBe(Kind.ClassType)
      expect(castExpression.class.parent).toBe(castExpression)
      expect(castExpression.class.name).toBe('CSmMode')
    }
  })

  test('generate Identifier node', () => {
    let identifier = (astMain.program?.main?.body.body[28] as ExpressionStatement).expression as Identifier
    expect(identifier).toBeDefined()

    if (identifier !== undefined) {
      expect(identifier instanceof Identifier).toBe(true)
      expect(identifier.kind).toBe(Kind.Identifier)
      expect(identifier.parent).toBe(astMain.program?.main?.body.body[28])
      expect(identifier.source.range.start).toBe(381)
      expect(identifier.source.range.end).toBe(384)
      expect(identifier.source.loc.start.line).toBe(33)
      expect(identifier.source.loc.start.column).toBe(2)
      expect(identifier.source.loc.end.line).toBe(33)
      expect(identifier.source.loc.end.column).toBe(6)

      expect(identifier.name).toBe('VarA')
      expect(identifier.namespace).toBeUndefined()
      expect(identifier.isExpression).toBe(true)
    }

    identifier = (astMain.program?.main?.body.body[29] as ExpressionStatement).expression as Identifier
    expect(identifier).toBeDefined()

    if (identifier !== undefined) {
      expect(identifier instanceof Identifier).toBe(true)
      expect(identifier.kind).toBe(Kind.Identifier)
      expect(identifier.parent).toBe(astMain.program?.main?.body.body[29])
      expect(identifier.source.range.start).toBe(389)
      expect(identifier.source.range.end).toBe(404)
      expect(identifier.source.loc.start.line).toBe(34)
      expect(identifier.source.loc.start.column).toBe(2)
      expect(identifier.source.loc.end.line).toBe(34)
      expect(identifier.source.loc.end.column).toBe(18)

      expect(identifier.name).toBe('VarC')
      expect(identifier.namespace).toBe('NamespaceB')
      expect(identifier.isExpression).toBe(true)
    }
  })

  test('generate IntegerLiteral node', () => {
    const integerLiteral = (astMain.program?.main?.body.body[30] as ExpressionStatement).expression as IntegerLiteral
    expect(integerLiteral).toBeDefined()

    if (integerLiteral !== undefined) {
      expect(integerLiteral instanceof IntegerLiteral).toBe(true)
      expect(integerLiteral.kind).toBe(Kind.IntegerLiteral)
      expect(integerLiteral.parent).toBe(astMain.program?.main?.body.body[30])
      expect(integerLiteral.source.range.start).toBe(409)
      expect(integerLiteral.source.range.end).toBe(411)
      expect(integerLiteral.source.loc.start.line).toBe(35)
      expect(integerLiteral.source.loc.start.column).toBe(2)
      expect(integerLiteral.source.loc.end.line).toBe(35)
      expect(integerLiteral.source.loc.end.column).toBe(5)

      expect(integerLiteral.value).toBeDefined()
      expect(integerLiteral.value).toBe(123)
      expect(integerLiteral.raw).toBeDefined()
      expect(integerLiteral.raw).toBe('123')
    }
  })

  test('generate RealLiteral node', () => {
    let realLiteral = (astMain.program?.main?.body.body[31] as ExpressionStatement).expression as RealLiteral
    expect(realLiteral).toBeDefined()

    if (realLiteral !== undefined) {
      expect(realLiteral instanceof RealLiteral).toBe(true)
      expect(realLiteral.kind).toBe(Kind.RealLiteral)
      expect(realLiteral.parent).toBe(astMain.program?.main?.body.body[31])
      expect(realLiteral.source.range.start).toBe(416)
      expect(realLiteral.source.range.end).toBe(422)
      expect(realLiteral.source.loc.start.line).toBe(36)
      expect(realLiteral.source.loc.start.column).toBe(2)
      expect(realLiteral.source.loc.end.line).toBe(36)
      expect(realLiteral.source.loc.end.column).toBe(9)

      expect(realLiteral.value).toBeDefined()
      expect(realLiteral.value).toBe(456.789)
      expect(realLiteral.raw).toBeDefined()
      expect(realLiteral.raw).toBe('456.789')
    }

    realLiteral = (astMain.program?.main?.body.body[32] as ExpressionStatement).expression as RealLiteral
    expect(realLiteral).toBeDefined()

    if (realLiteral !== undefined) {
      expect(realLiteral instanceof RealLiteral).toBe(true)
      expect(realLiteral.kind).toBe(Kind.RealLiteral)
      expect(realLiteral.parent).toBe(astMain.program?.main?.body.body[32])
      expect(realLiteral.source.range.start).toBe(427)
      expect(realLiteral.source.range.end).toBe(432)
      expect(realLiteral.source.loc.start.line).toBe(37)
      expect(realLiteral.source.loc.start.column).toBe(2)
      expect(realLiteral.source.loc.end.line).toBe(37)
      expect(realLiteral.source.loc.end.column).toBe(8)

      expect(realLiteral.value).toBeDefined()
      expect(realLiteral.value).toBe(0.1e+2)
      expect(realLiteral.raw).toBeDefined()
      expect(realLiteral.raw).toBe('0.1e+2')
    }

    realLiteral = (astMain.program?.main?.body.body[42] as ExpressionStatement).expression as RealLiteral
    expect(realLiteral).toBeDefined()

    if (realLiteral !== undefined) {
      expect(realLiteral instanceof RealLiteral).toBe(true)
      expect(realLiteral.kind).toBe(Kind.RealLiteral)
      expect(realLiteral.parent).toBe(astMain.program?.main?.body.body[42])
      expect(realLiteral.source.range.start).toBe(561)
      expect(realLiteral.source.range.end).toBe(562)
      expect(realLiteral.source.loc.start.line).toBe(47)
      expect(realLiteral.source.loc.start.column).toBe(2)
      expect(realLiteral.source.loc.end.line).toBe(47)
      expect(realLiteral.source.loc.end.column).toBe(4)

      expect(realLiteral.value).toBeDefined()
      expect(realLiteral.value).toBe(0.5)
      expect(realLiteral.raw).toBeDefined()
      expect(realLiteral.raw).toBe('.5')
    }
  })

  test('generate BooleanLiteral node', () => {
    let booleanLiteral = (astMain.program?.main?.body.body[33] as ExpressionStatement).expression as BooleanLiteral
    expect(booleanLiteral).toBeDefined()

    if (booleanLiteral !== undefined) {
      expect(booleanLiteral instanceof BooleanLiteral).toBe(true)
      expect(booleanLiteral.kind).toBe(Kind.BooleanLiteral)
      expect(booleanLiteral.parent).toBe(astMain.program?.main?.body.body[33])
      expect(booleanLiteral.source.range.start).toBe(437)
      expect(booleanLiteral.source.range.end).toBe(440)
      expect(booleanLiteral.source.loc.start.line).toBe(38)
      expect(booleanLiteral.source.loc.start.column).toBe(2)
      expect(booleanLiteral.source.loc.end.line).toBe(38)
      expect(booleanLiteral.source.loc.end.column).toBe(6)

      expect(booleanLiteral.value).toBeDefined()
      expect(booleanLiteral.value).toBe(true)
    }

    booleanLiteral = (astMain.program?.main?.body.body[34] as ExpressionStatement).expression as BooleanLiteral
    expect(booleanLiteral).toBeDefined()

    if (booleanLiteral !== undefined) {
      expect(booleanLiteral instanceof BooleanLiteral).toBe(true)
      expect(booleanLiteral.kind).toBe(Kind.BooleanLiteral)
      expect(booleanLiteral.parent).toBe(astMain.program?.main?.body.body[34])
      expect(booleanLiteral.source.range.start).toBe(445)
      expect(booleanLiteral.source.range.end).toBe(449)
      expect(booleanLiteral.source.loc.start.line).toBe(39)
      expect(booleanLiteral.source.loc.start.column).toBe(2)
      expect(booleanLiteral.source.loc.end.line).toBe(39)
      expect(booleanLiteral.source.loc.end.column).toBe(7)

      expect(booleanLiteral.value).toBeDefined()
      expect(booleanLiteral.value).toBe(false)
    }
  })

  test('generate NullLiteral node', () => {
    const nullLiteral = (astMain.program?.main?.body.body[35] as ExpressionStatement).expression as NullLiteral
    expect(nullLiteral).toBeDefined()

    if (nullLiteral !== undefined) {
      expect(nullLiteral instanceof NullLiteral).toBe(true)
      expect(nullLiteral.kind).toBe(Kind.NullLiteral)
      expect(nullLiteral.parent).toBe(astMain.program?.main?.body.body[35])
      expect(nullLiteral.source.range.start).toBe(454)
      expect(nullLiteral.source.range.end).toBe(457)
      expect(nullLiteral.source.loc.start.line).toBe(40)
      expect(nullLiteral.source.loc.start.column).toBe(2)
      expect(nullLiteral.source.loc.end.line).toBe(40)
      expect(nullLiteral.source.loc.end.column).toBe(6)
    }
  })

  test('generate NullIdLiteral node', () => {
    const nullIdLiteral = (astMain.program?.main?.body.body[36] as ExpressionStatement).expression as NullIdLiteral
    expect(nullIdLiteral).toBeDefined()

    if (nullIdLiteral !== undefined) {
      expect(nullIdLiteral instanceof NullIdLiteral).toBe(true)
      expect(nullIdLiteral.kind).toBe(Kind.NullIdLiteral)
      expect(nullIdLiteral.parent).toBe(astMain.program?.main?.body.body[36])
      expect(nullIdLiteral.source.range.start).toBe(462)
      expect(nullIdLiteral.source.range.end).toBe(467)
      expect(nullIdLiteral.source.loc.start.line).toBe(41)
      expect(nullIdLiteral.source.loc.start.column).toBe(2)
      expect(nullIdLiteral.source.loc.end.line).toBe(41)
      expect(nullIdLiteral.source.loc.end.column).toBe(8)
    }
  })

  test('generate EnumLiteral node', () => {
    let enumLiteral = (astMain.program?.main?.body.body[37] as ExpressionStatement).expression as EnumLiteral
    expect(enumLiteral).toBeDefined()

    if (enumLiteral !== undefined) {
      expect(enumLiteral instanceof EnumLiteral).toBe(true)
      expect(enumLiteral.kind).toBe(Kind.EnumLiteral)
      expect(enumLiteral.parent).toBe(astMain.program?.main?.body.body[37])
      expect(enumLiteral.source.range.start).toBe(472)
      expect(enumLiteral.source.range.end).toBe(492)
      expect(enumLiteral.source.loc.start.line).toBe(42)
      expect(enumLiteral.source.loc.start.column).toBe(2)
      expect(enumLiteral.source.loc.end.line).toBe(42)
      expect(enumLiteral.source.loc.end.column).toBe(23)

      expect(enumLiteral.class).toBeDefined()
      if (enumLiteral.class !== undefined) {
        expect(enumLiteral.class.kind).toBe(Kind.ClassType)
        expect(enumLiteral.class.parent).toBe(enumLiteral)
        expect((enumLiteral.class as ClassType).name).toBe('CSmMode')
      }

      expect(enumLiteral.name.kind).toBe(Kind.Identifier)
      expect(enumLiteral.name.parent).toBe(enumLiteral)
      expect(enumLiteral.name.name).toBe('EMedal')
      expect(enumLiteral.name.isExpression).toBe(false)

      expect(enumLiteral.value.kind).toBe(Kind.Identifier)
      expect(enumLiteral.value.parent).toBe(enumLiteral)
      expect(enumLiteral.value.name).toBe('Gold')
      expect(enumLiteral.value.isExpression).toBe(false)
    }

    enumLiteral = (astMain.program?.main?.body.body[38] as ExpressionStatement).expression as EnumLiteral
    expect(enumLiteral).toBeDefined()

    if (enumLiteral !== undefined) {
      expect(enumLiteral instanceof EnumLiteral).toBe(true)
      expect(enumLiteral.kind).toBe(Kind.EnumLiteral)
      expect(enumLiteral.parent).toBe(astMain.program?.main?.body.body[38])
      expect(enumLiteral.source.range.start).toBe(497)
      expect(enumLiteral.source.range.end).toBe(510)
      expect(enumLiteral.source.loc.start.line).toBe(43)
      expect(enumLiteral.source.loc.start.column).toBe(2)
      expect(enumLiteral.source.loc.end.line).toBe(43)
      expect(enumLiteral.source.loc.end.column).toBe(16)

      expect(enumLiteral.class).toBeUndefined()

      expect(enumLiteral.name.kind).toBe(Kind.Identifier)
      expect(enumLiteral.name.parent).toBe(enumLiteral)
      expect(enumLiteral.name.name).toBe('EMedal')
      expect(enumLiteral.name.isExpression).toBe(false)

      expect(enumLiteral.value.kind).toBe(Kind.Identifier)
      expect(enumLiteral.value.parent).toBe(enumLiteral)
      expect(enumLiteral.value.name).toBe('Gold')
      expect(enumLiteral.value.isExpression).toBe(false)
    }

    enumLiteral = (astMain.program?.main?.body.body[39] as ExpressionStatement).expression as EnumLiteral
    expect(enumLiteral).toBeDefined()

    if (enumLiteral !== undefined) {
      expect(enumLiteral instanceof EnumLiteral).toBe(true)
      expect(enumLiteral.kind).toBe(Kind.EnumLiteral)
      expect(enumLiteral.parent).toBe(astMain.program?.main?.body.body[39])
      expect(enumLiteral.source.range.start).toBe(515)
      expect(enumLiteral.source.range.end).toBe(541)
      expect(enumLiteral.source.loc.start.line).toBe(44)
      expect(enumLiteral.source.loc.start.column).toBe(2)
      expect(enumLiteral.source.loc.end.line).toBe(44)
      expect(enumLiteral.source.loc.end.column).toBe(29)

      expect(enumLiteral.class).toBeDefined()
      if (enumLiteral.class !== undefined) {
        expect(enumLiteral.class.kind).toBe(Kind.Identifier)
        expect(enumLiteral.class.parent).toBe(enumLiteral)
        expect((enumLiteral.class as Identifier).name).toBe('TimeLib')
        expect((enumLiteral.class as Identifier).isExpression).toBe(false)
      }

      expect(enumLiteral.name.kind).toBe(Kind.Identifier)
      expect(enumLiteral.name.parent).toBe(enumLiteral)
      expect(enumLiteral.name.name).toBe('EDateFormats')
      expect(enumLiteral.name.isExpression).toBe(false)

      expect(enumLiteral.value.kind).toBe(Kind.Identifier)
      expect(enumLiteral.value.parent).toBe(enumLiteral)
      expect(enumLiteral.value.name).toBe('Time')
      expect(enumLiteral.value.isExpression).toBe(false)
    }
  })

  test('generate ThisExpression node', () => {
    const thisExpression = (astMain.program?.main?.body.body[40] as ExpressionStatement).expression as ThisExpression
    expect(thisExpression).toBeDefined()

    if (thisExpression !== undefined) {
      expect(thisExpression instanceof ThisExpression).toBe(true)
      expect(thisExpression.kind).toBe(Kind.ThisExpression)
      expect(thisExpression.parent).toBe(astMain.program?.main?.body.body[40])
      expect(thisExpression.source.range.start).toBe(546)
      expect(thisExpression.source.range.end).toBe(549)
      expect(thisExpression.source.loc.start.line).toBe(45)
      expect(thisExpression.source.loc.start.column).toBe(2)
      expect(thisExpression.source.loc.end.line).toBe(45)
      expect(thisExpression.source.loc.end.column).toBe(6)
    }
  })

  test('remove parenthesis expression', () => {
    const literalExpression = (astMain.program?.main?.body.body[41] as ExpressionStatement).expression as IntegerLiteral
    expect(literalExpression).toBeDefined()

    if (literalExpression !== undefined) {
      expect(literalExpression instanceof IntegerLiteral).toBe(true)
      expect(literalExpression.kind).toBe(Kind.IntegerLiteral)
      expect(literalExpression.parent).toBe(astMain.program?.main?.body.body[41])
      expect(literalExpression.source.range.start).toBe(555)
      expect(literalExpression.source.range.end).toBe(555)
      expect(literalExpression.source.loc.start.line).toBe(46)
      expect(literalExpression.source.loc.start.column).toBe(3)
      expect(literalExpression.source.loc.end.line).toBe(46)
      expect(literalExpression.source.loc.end.column).toBe(4)
    }
  })

  test('generate NowExpression node', () => {
    const nowExpression = (astMain.program?.main?.body.body[45] as ExpressionStatement).expression as NowExpression
    expect(nowExpression).toBeDefined()

    if (nowExpression !== undefined) {
      expect(nowExpression instanceof NowExpression).toBe(true)
      expect(nowExpression.kind).toBe(Kind.NowExpression)
      expect(nowExpression.parent).toBe(astMain.program?.main?.body.body[45])
      expect(nowExpression.source.range.start).toBe(615)
      expect(nowExpression.source.range.end).toBe(617)
      expect(nowExpression.source.loc.start.line).toBe(50)
      expect(nowExpression.source.loc.start.column).toBe(2)
      expect(nowExpression.source.loc.end.line).toBe(50)
      expect(nowExpression.source.loc.end.column).toBe(5)
    }
  })

  test('generate Now member node', () => {
    const dotAccessExpression = (astMain.program?.main?.body.body[46] as ExpressionStatement).expression as DotAccessExpression
    expect(dotAccessExpression).toBeDefined()

    if (dotAccessExpression !== undefined) {
      expect(dotAccessExpression instanceof DotAccessExpression).toBe(true)
      expect(dotAccessExpression.kind).toBe(Kind.DotAccessExpression)
      expect(dotAccessExpression.parent).toBe(astMain.program?.main?.body.body[46])
      expect(dotAccessExpression.source.range.start).toBe(622)
      expect(dotAccessExpression.source.range.end).toBe(629)
      expect(dotAccessExpression.source.loc.start.line).toBe(51)
      expect(dotAccessExpression.source.loc.start.column).toBe(2)
      expect(dotAccessExpression.source.loc.end.line).toBe(51)
      expect(dotAccessExpression.source.loc.end.column).toBe(10)

      expect(dotAccessExpression.object).toBeDefined()
      expect(dotAccessExpression.object.kind).toBe(Kind.Identifier)
      expect(dotAccessExpression.object.parent).toBe(dotAccessExpression)
      expect((dotAccessExpression.object as Identifier).name).toBe('AAAA')
      expect((dotAccessExpression.object as Identifier).isExpression).toBe(true)

      expect(dotAccessExpression.functionCall).toBeUndefined()

      expect(dotAccessExpression.member).toBeDefined()
      if (dotAccessExpression.member !== undefined) {
        expect(dotAccessExpression.member.kind).toBe(Kind.Identifier)
        expect(dotAccessExpression.member.parent).toBe(dotAccessExpression)
        expect(dotAccessExpression.member.isExpression).toBe(false)
        expect(dotAccessExpression.member.name).toBe('Now')
      }
    }
  })
})

const inputStatement = `
main() {
  A = 1;
  B <=> 2;
  C *= 3;
  D /= 4;
  E += 5;
  F -= 6;
  G %= 7;
  H ^= 8;
  I = Integer;
  J <=> CSmMode;
  return 1;
  return CSmMode;
  return;
  +++LabelA+++
  ---LabelB---
  assert(True);
  assert(False, Message);
  foreach (ValueA, ContentB) {}
  foreach (KeyC => ValueD in ContentE) {

  }
  for (ValueA, 1, 2) {}
  for (ValueB, 3, 4, 5) {

  }
  while (True) {}
  meanwhile (False) {}
  break;
  continue;
  switch (A) {
    case 1: {}
    case 2,3: {}
    default: {}
  }
  switchtype (A) {
    case CSmMode: {}
    case CUIConfig,CMlControl: {}
    default: {}
  }
  if (A) {

  } else if (B) {}
  else if (C) D();
  else {

  }
  log(Now);
  sleep(1000);
  tuningstart();
  tuningend();
  tuningmark("A");
  wait(True);
  yield;
}
`

let astStatement: AST

describe('lib/ast part 3', () => {
  beforeAll(async () => {
    astStatement = (await parse(inputStatement, { buildAst: true })).ast
  })

  test('generate AssignmentStatement node', () => {
    let assignmentStatement = astStatement.program?.main?.body.body[0] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(12)
      expect(assignmentStatement.source.range.end).toBe(17)
      expect(assignmentStatement.source.loc.start.line).toBe(3)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(3)
      expect(assignmentStatement.source.loc.end.column).toBe(8)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.IntegerLiteral)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['='])
      expect(assignmentStatement.isInitializedByType).toBe(false)
    }

    assignmentStatement = astStatement.program?.main?.body.body[1] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(21)
      expect(assignmentStatement.source.range.end).toBe(28)
      expect(assignmentStatement.source.loc.start.line).toBe(4)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(4)
      expect(assignmentStatement.source.loc.end.column).toBe(10)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.IntegerLiteral)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['<=>'])
      expect(assignmentStatement.isInitializedByType).toBe(false)
    }

    assignmentStatement = astStatement.program?.main?.body.body[2] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(32)
      expect(assignmentStatement.source.range.end).toBe(38)
      expect(assignmentStatement.source.loc.start.line).toBe(5)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(5)
      expect(assignmentStatement.source.loc.end.column).toBe(9)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.IntegerLiteral)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['*='])
      expect(assignmentStatement.isInitializedByType).toBe(false)
    }

    assignmentStatement = astStatement.program?.main?.body.body[3] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(42)
      expect(assignmentStatement.source.range.end).toBe(48)
      expect(assignmentStatement.source.loc.start.line).toBe(6)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(6)
      expect(assignmentStatement.source.loc.end.column).toBe(9)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.IntegerLiteral)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['/='])
      expect(assignmentStatement.isInitializedByType).toBe(false)
    }

    assignmentStatement = astStatement.program?.main?.body.body[4] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(52)
      expect(assignmentStatement.source.range.end).toBe(58)
      expect(assignmentStatement.source.loc.start.line).toBe(7)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(7)
      expect(assignmentStatement.source.loc.end.column).toBe(9)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.IntegerLiteral)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['+='])
      expect(assignmentStatement.isInitializedByType).toBe(false)
    }

    assignmentStatement = astStatement.program?.main?.body.body[5] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(62)
      expect(assignmentStatement.source.range.end).toBe(68)
      expect(assignmentStatement.source.loc.start.line).toBe(8)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(8)
      expect(assignmentStatement.source.loc.end.column).toBe(9)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.IntegerLiteral)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['-='])
      expect(assignmentStatement.isInitializedByType).toBe(false)
    }

    assignmentStatement = astStatement.program?.main?.body.body[6] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(72)
      expect(assignmentStatement.source.range.end).toBe(78)
      expect(assignmentStatement.source.loc.start.line).toBe(9)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(9)
      expect(assignmentStatement.source.loc.end.column).toBe(9)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.IntegerLiteral)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['%='])
      expect(assignmentStatement.isInitializedByType).toBe(false)
    }

    assignmentStatement = astStatement.program?.main?.body.body[7] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(82)
      expect(assignmentStatement.source.range.end).toBe(88)
      expect(assignmentStatement.source.loc.start.line).toBe(10)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(10)
      expect(assignmentStatement.source.loc.end.column).toBe(9)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.IntegerLiteral)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['^='])
      expect(assignmentStatement.isInitializedByType).toBe(false)
    }

    assignmentStatement = astStatement.program?.main?.body.body[8] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(92)
      expect(assignmentStatement.source.range.end).toBe(103)
      expect(assignmentStatement.source.loc.start.line).toBe(11)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(11)
      expect(assignmentStatement.source.loc.end.column).toBe(14)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.SimpleType)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['='])
      expect(assignmentStatement.isInitializedByType).toBe(true)
    }

    assignmentStatement = astStatement.program?.main?.body.body[9] as AssignmentStatement
    expect(assignmentStatement).toBeDefined()

    if (assignmentStatement !== undefined) {
      expect(assignmentStatement instanceof AssignmentStatement).toBe(true)
      expect(assignmentStatement.kind).toBe(Kind.AssignmentStatement)
      expect(assignmentStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assignmentStatement.source.range.start).toBe(107)
      expect(assignmentStatement.source.range.end).toBe(120)
      expect(assignmentStatement.source.loc.start.line).toBe(12)
      expect(assignmentStatement.source.loc.start.column).toBe(2)
      expect(assignmentStatement.source.loc.end.line).toBe(12)
      expect(assignmentStatement.source.loc.end.column).toBe(16)

      expect(assignmentStatement.left.kind).toBe(Kind.Identifier)
      expect(assignmentStatement.left.parent).toBe(assignmentStatement)
      expect((assignmentStatement.left as Identifier).isExpression).toBe(true)
      expect(assignmentStatement.right.kind).toBe(Kind.ClassType)
      expect(assignmentStatement.right.parent).toBe(assignmentStatement)
      expect(assignmentStatement.operator).toBe(AssignmentOperator['<=>'])
      expect(assignmentStatement.isInitializedByType).toBe(true)
    }
  })

  test('generate ReturnStatement node', () => {
    let returnStatement = astStatement.program?.main?.body.body[10] as ReturnStatement
    expect(returnStatement).toBeDefined()

    if (returnStatement !== undefined) {
      expect(returnStatement instanceof ReturnStatement).toBe(true)
      expect(returnStatement.kind).toBe(Kind.ReturnStatement)
      expect(returnStatement.parent).toBe(astStatement.program?.main?.body)
      expect(returnStatement.source.range.start).toBe(124)
      expect(returnStatement.source.range.end).toBe(132)
      expect(returnStatement.source.loc.start.line).toBe(13)
      expect(returnStatement.source.loc.start.column).toBe(2)
      expect(returnStatement.source.loc.end.line).toBe(13)
      expect(returnStatement.source.loc.end.column).toBe(11)

      expect(returnStatement.argument).toBeDefined()
      if (returnStatement.argument !== undefined) {
        expect(returnStatement.argument.kind).toBe(Kind.IntegerLiteral)
        expect(returnStatement.argument.parent).toBe(returnStatement)
      }

      expect(returnStatement.isReturningType).toBeDefined()
      expect(returnStatement.isReturningType).toBe(false)
    }

    returnStatement = astStatement.program?.main?.body.body[11] as ReturnStatement
    expect(returnStatement).toBeDefined()

    if (returnStatement !== undefined) {
      expect(returnStatement instanceof ReturnStatement).toBe(true)
      expect(returnStatement.kind).toBe(Kind.ReturnStatement)
      expect(returnStatement.parent).toBe(astStatement.program?.main?.body)
      expect(returnStatement.source.range.start).toBe(136)
      expect(returnStatement.source.range.end).toBe(150)
      expect(returnStatement.source.loc.start.line).toBe(14)
      expect(returnStatement.source.loc.start.column).toBe(2)
      expect(returnStatement.source.loc.end.line).toBe(14)
      expect(returnStatement.source.loc.end.column).toBe(17)

      expect(returnStatement.argument).toBeDefined()
      if (returnStatement.argument !== undefined) {
        expect(returnStatement.argument.kind).toBe(Kind.ClassType)
        expect(returnStatement.argument.parent).toBe(returnStatement)
      }

      expect(returnStatement.isReturningType).toBeDefined()
      expect(returnStatement.isReturningType).toBe(true)
    }

    returnStatement = astStatement.program?.main?.body.body[12] as ReturnStatement
    expect(returnStatement).toBeDefined()

    if (returnStatement !== undefined) {
      expect(returnStatement instanceof ReturnStatement).toBe(true)
      expect(returnStatement.kind).toBe(Kind.ReturnStatement)
      expect(returnStatement.parent).toBe(astStatement.program?.main?.body)
      expect(returnStatement.source.range.start).toBe(154)
      expect(returnStatement.source.range.end).toBe(160)
      expect(returnStatement.source.loc.start.line).toBe(15)
      expect(returnStatement.source.loc.start.column).toBe(2)
      expect(returnStatement.source.loc.end.line).toBe(15)
      expect(returnStatement.source.loc.end.column).toBe(9)

      expect(returnStatement.argument).toBeUndefined()

      expect(returnStatement.isReturningType).toBeDefined()
      expect(returnStatement.isReturningType).toBe(false)
    }
  })

  test('generate LabelStatement node', () => {
    let labelStatement = astStatement.program?.main?.body.body[13] as LabelStatement
    expect(labelStatement).toBeDefined()

    if (labelStatement !== undefined) {
      expect(labelStatement instanceof LabelStatement).toBe(true)
      expect(labelStatement.kind).toBe(Kind.LabelStatement)
      expect(labelStatement.parent).toBe(astStatement.program?.main?.body)
      expect(labelStatement.source.range.start).toBe(164)
      expect(labelStatement.source.range.end).toBe(175)
      expect(labelStatement.source.loc.start.line).toBe(16)
      expect(labelStatement.source.loc.start.column).toBe(2)
      expect(labelStatement.source.loc.end.line).toBe(16)
      expect(labelStatement.source.loc.end.column).toBe(14)

      expect(labelStatement.isOverwrite).toBe(false)
      expect(labelStatement.name.kind).toBe(Kind.Identifier)
      expect(labelStatement.name.parent).toBe(labelStatement)
      expect((labelStatement.name as Identifier).isExpression).toBe(false)
    }

    labelStatement = astStatement.program?.main?.body.body[14] as LabelStatement
    expect(labelStatement).toBeDefined()

    if (labelStatement !== undefined) {
      expect(labelStatement instanceof LabelStatement).toBe(true)
      expect(labelStatement.kind).toBe(Kind.LabelStatement)
      expect(labelStatement.parent).toBe(astStatement.program?.main?.body)
      expect(labelStatement.source.range.start).toBe(179)
      expect(labelStatement.source.range.end).toBe(190)
      expect(labelStatement.source.loc.start.line).toBe(17)
      expect(labelStatement.source.loc.start.column).toBe(2)
      expect(labelStatement.source.loc.end.line).toBe(17)
      expect(labelStatement.source.loc.end.column).toBe(14)

      expect(labelStatement.isOverwrite).toBe(true)
      expect(labelStatement.name.kind).toBe(Kind.Identifier)
      expect(labelStatement.name.parent).toBe(labelStatement)
      expect((labelStatement.name as Identifier).isExpression).toBe(false)
    }
  })

  test('generate AssertStatement node', () => {
    let assertStatement = astStatement.program?.main?.body.body[15] as AssertStatement
    expect(assertStatement).toBeDefined()

    if (assertStatement !== undefined) {
      expect(assertStatement instanceof AssertStatement).toBe(true)
      expect(assertStatement.kind).toBe(Kind.AssertStatement)
      expect(assertStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assertStatement.source.range.start).toBe(194)
      expect(assertStatement.source.range.end).toBe(206)
      expect(assertStatement.source.loc.start.line).toBe(18)
      expect(assertStatement.source.loc.start.column).toBe(2)
      expect(assertStatement.source.loc.end.line).toBe(18)
      expect(assertStatement.source.loc.end.column).toBe(15)

      expect(assertStatement.test.kind).toBe(Kind.BooleanLiteral)
      expect(assertStatement.test.parent).toBe(assertStatement)
      expect(assertStatement.message).toBeUndefined()
    }

    assertStatement = astStatement.program?.main?.body.body[16] as AssertStatement
    expect(assertStatement).toBeDefined()

    if (assertStatement !== undefined) {
      expect(assertStatement instanceof AssertStatement).toBe(true)
      expect(assertStatement.kind).toBe(Kind.AssertStatement)
      expect(assertStatement.parent).toBe(astStatement.program?.main?.body)
      expect(assertStatement.source.range.start).toBe(210)
      expect(assertStatement.source.range.end).toBe(232)
      expect(assertStatement.source.loc.start.line).toBe(19)
      expect(assertStatement.source.loc.start.column).toBe(2)
      expect(assertStatement.source.loc.end.line).toBe(19)
      expect(assertStatement.source.loc.end.column).toBe(25)

      expect(assertStatement.test.kind).toBe(Kind.BooleanLiteral)
      expect(assertStatement.test.parent).toBe(assertStatement)
      expect(assertStatement.message).toBeDefined()
      if (assertStatement.message !== undefined) {
        expect(assertStatement.message.kind).toBe(Kind.Identifier)
        expect(assertStatement.message.parent).toBe(assertStatement)
        expect((assertStatement.message as Identifier).isExpression).toBe(true)
      }
    }
  })

  test('generate ForeachStatement node', () => {
    let foreachStatement = astStatement.program?.main?.body.body[17] as ForeachStatement
    expect(foreachStatement).toBeDefined()

    if (foreachStatement !== undefined) {
      expect(foreachStatement instanceof ForeachStatement).toBe(true)
      expect(foreachStatement.kind).toBe(Kind.ForeachStatement)
      expect(foreachStatement.parent).toBe(astStatement.program?.main?.body)
      expect(foreachStatement.source.range.start).toBe(236)
      expect(foreachStatement.source.range.end).toBe(264)
      expect(foreachStatement.source.loc.start.line).toBe(20)
      expect(foreachStatement.source.loc.start.column).toBe(2)
      expect(foreachStatement.source.loc.end.line).toBe(20)
      expect(foreachStatement.source.loc.end.column).toBe(31)

      expect(foreachStatement.key).toBeUndefined()
      expect(foreachStatement.value.kind).toBe(Kind.Identifier)
      expect(foreachStatement.value.parent).toBe(foreachStatement)
      expect(foreachStatement.value.name).toBe('ValueA')
      expect(foreachStatement.value.isExpression).toBe(false)
      expect(foreachStatement.expression.kind).toBe(Kind.Identifier)
      expect(foreachStatement.expression.parent).toBe(foreachStatement)
      expect((foreachStatement.expression as Identifier).name).toBe('ContentB')
      expect((foreachStatement.expression as Identifier).isExpression).toBe(true)
      expect(foreachStatement.body.kind).toBe(Kind.BlockStatement)
      expect(foreachStatement.body.parent).toBe(foreachStatement)
    }

    foreachStatement = astStatement.program?.main?.body.body[18] as ForeachStatement
    expect(foreachStatement).toBeDefined()

    if (foreachStatement !== undefined) {
      expect(foreachStatement instanceof ForeachStatement).toBe(true)
      expect(foreachStatement.kind).toBe(Kind.ForeachStatement)
      expect(foreachStatement.parent).toBe(astStatement.program?.main?.body)
      expect(foreachStatement.source.range.start).toBe(268)
      expect(foreachStatement.source.range.end).toBe(310)
      expect(foreachStatement.source.loc.start.line).toBe(21)
      expect(foreachStatement.source.loc.start.column).toBe(2)
      expect(foreachStatement.source.loc.end.line).toBe(23)
      expect(foreachStatement.source.loc.end.column).toBe(3)

      expect(foreachStatement.key).toBeDefined()
      if (foreachStatement.key !== undefined) {
        expect(foreachStatement.key.kind).toBe(Kind.Identifier)
        expect(foreachStatement.value.parent).toBe(foreachStatement)
        expect(foreachStatement.key.name).toBe('KeyC')
        expect(foreachStatement.key.isExpression).toBe(false)
      }
      expect(foreachStatement.value.kind).toBe(Kind.Identifier)
      expect(foreachStatement.value.parent).toBe(foreachStatement)
      expect(foreachStatement.value.name).toBe('ValueD')
      expect(foreachStatement.value.isExpression).toBe(false)
      expect(foreachStatement.expression.kind).toBe(Kind.Identifier)
      expect(foreachStatement.expression.parent).toBe(foreachStatement)
      expect((foreachStatement.expression as Identifier).name).toBe('ContentE')
      expect((foreachStatement.expression as Identifier).isExpression).toBe(true)
      expect(foreachStatement.body.kind).toBe(Kind.BlockStatement)
      expect(foreachStatement.body.parent).toBe(foreachStatement)
    }
  })

  test('generate ForStatement node', () => {
    let forStatement = astStatement.program?.main?.body.body[19] as ForStatement
    expect(forStatement).toBeDefined()

    if (forStatement !== undefined) {
      expect(forStatement instanceof ForStatement).toBe(true)
      expect(forStatement.kind).toBe(Kind.ForStatement)
      expect(forStatement.parent).toBe(astStatement.program?.main?.body)
      expect(forStatement.source.range.start).toBe(314)
      expect(forStatement.source.range.end).toBe(334)
      expect(forStatement.source.loc.start.line).toBe(24)
      expect(forStatement.source.loc.start.column).toBe(2)
      expect(forStatement.source.loc.end.line).toBe(24)
      expect(forStatement.source.loc.end.column).toBe(23)

      expect(forStatement.value.kind).toBe(Kind.Identifier)
      expect(forStatement.value.parent).toBe(forStatement)
      expect(forStatement.value.name).toBe('ValueA')
      expect(forStatement.value.isExpression).toBe(false)
      expect(forStatement.loopStart.kind).toBe(Kind.IntegerLiteral)
      expect(forStatement.loopStart.parent).toBe(forStatement)
      expect((forStatement.loopStart as IntegerLiteral).value).toBe(1)
      expect(forStatement.loopStop.kind).toBe(Kind.IntegerLiteral)
      expect(forStatement.loopStop.parent).toBe(forStatement)
      expect((forStatement.loopStop as IntegerLiteral).value).toBe(2)
      expect(forStatement.increment).toBeUndefined()
      expect(forStatement.body.kind).toBe(Kind.BlockStatement)
      expect(forStatement.body.parent).toBe(forStatement)
    }

    forStatement = astStatement.program?.main?.body.body[20] as ForStatement
    expect(forStatement).toBeDefined()

    if (forStatement !== undefined) {
      expect(forStatement instanceof ForStatement).toBe(true)
      expect(forStatement.kind).toBe(Kind.ForStatement)
      expect(forStatement.parent).toBe(astStatement.program?.main?.body)
      expect(forStatement.source.range.start).toBe(338)
      expect(forStatement.source.range.end).toBe(365)
      expect(forStatement.source.loc.start.line).toBe(25)
      expect(forStatement.source.loc.start.column).toBe(2)
      expect(forStatement.source.loc.end.line).toBe(27)
      expect(forStatement.source.loc.end.column).toBe(3)

      expect(forStatement.value.kind).toBe(Kind.Identifier)
      expect(forStatement.value.parent).toBe(forStatement)
      expect(forStatement.value.name).toBe('ValueB')
      expect(forStatement.value.isExpression).toBe(false)
      expect(forStatement.loopStart.kind).toBe(Kind.IntegerLiteral)
      expect(forStatement.loopStart.parent).toBe(forStatement)
      expect((forStatement.loopStart as IntegerLiteral).value).toBe(3)
      expect(forStatement.loopStop.kind).toBe(Kind.IntegerLiteral)
      expect(forStatement.loopStop.parent).toBe(forStatement)
      expect((forStatement.loopStop as IntegerLiteral).value).toBe(4)
      expect(forStatement.increment).toBeDefined()
      if (forStatement.increment !== undefined) {
        expect(forStatement.increment.kind).toBe(Kind.IntegerLiteral)
        expect(forStatement.increment.parent).toBe(forStatement)
        expect((forStatement.increment as IntegerLiteral).value).toBe(5)
      }
      expect(forStatement.body.kind).toBe(Kind.BlockStatement)
      expect(forStatement.body.parent).toBe(forStatement)
    }
  })

  test('generate WhileStatement node', () => {
    const whileStatement = astStatement.program?.main?.body.body[21] as WhileStatement
    expect(whileStatement).toBeDefined()

    if (whileStatement !== undefined) {
      expect(whileStatement instanceof WhileStatement).toBe(true)
      expect(whileStatement.kind).toBe(Kind.WhileStatement)
      expect(whileStatement.parent).toBe(astStatement.program?.main?.body)
      expect(whileStatement.source.range.start).toBe(369)
      expect(whileStatement.source.range.end).toBe(383)
      expect(whileStatement.source.loc.start.line).toBe(28)
      expect(whileStatement.source.loc.start.column).toBe(2)
      expect(whileStatement.source.loc.end.line).toBe(28)
      expect(whileStatement.source.loc.end.column).toBe(17)

      expect(whileStatement.test.kind).toBe(Kind.BooleanLiteral)
      expect(whileStatement.test.parent).toBe(whileStatement)
      expect((whileStatement.test as BooleanLiteral).value).toBe(true)
      expect(whileStatement.body.kind).toBe(Kind.BlockStatement)
      expect(whileStatement.body.parent).toBe(whileStatement)
    }
  })

  test('generate MeanwhileStatement node', () => {
    const meanwhileStatement = astStatement.program?.main?.body.body[22] as MeanwhileStatement
    expect(meanwhileStatement).toBeDefined()

    if (meanwhileStatement !== undefined) {
      expect(meanwhileStatement instanceof MeanwhileStatement).toBe(true)
      expect(meanwhileStatement.kind).toBe(Kind.MeanwhileStatement)
      expect(meanwhileStatement.parent).toBe(astStatement.program?.main?.body)
      expect(meanwhileStatement.source.range.start).toBe(387)
      expect(meanwhileStatement.source.range.end).toBe(406)
      expect(meanwhileStatement.source.loc.start.line).toBe(29)
      expect(meanwhileStatement.source.loc.start.column).toBe(2)
      expect(meanwhileStatement.source.loc.end.line).toBe(29)
      expect(meanwhileStatement.source.loc.end.column).toBe(22)

      expect(meanwhileStatement.test.kind).toBe(Kind.BooleanLiteral)
      expect(meanwhileStatement.test.parent).toBe(meanwhileStatement)
      expect((meanwhileStatement.test as BooleanLiteral).value).toBe(false)
      expect(meanwhileStatement.body.kind).toBe(Kind.BlockStatement)
      expect(meanwhileStatement.body.parent).toBe(meanwhileStatement)
    }
  })

  test('generate BreakStatement node', () => {
    const breakStatement = astStatement.program?.main?.body.body[23] as BreakStatement
    expect(breakStatement).toBeDefined()

    if (breakStatement !== undefined) {
      expect(breakStatement instanceof BreakStatement).toBe(true)
      expect(breakStatement.kind).toBe(Kind.BreakStatement)
      expect(breakStatement.parent).toBe(astStatement.program?.main?.body)
      expect(breakStatement.source.range.start).toBe(410)
      expect(breakStatement.source.range.end).toBe(415)
      expect(breakStatement.source.loc.start.line).toBe(30)
      expect(breakStatement.source.loc.start.column).toBe(2)
      expect(breakStatement.source.loc.end.line).toBe(30)
      expect(breakStatement.source.loc.end.column).toBe(8)
    }
  })

  test('generate ContinueStatement node', () => {
    const continueStatement = astStatement.program?.main?.body.body[24] as ContinueStatement
    expect(continueStatement).toBeDefined()

    if (continueStatement !== undefined) {
      expect(continueStatement instanceof ContinueStatement).toBe(true)
      expect(continueStatement.kind).toBe(Kind.ContinueStatement)
      expect(continueStatement.parent).toBe(astStatement.program?.main?.body)
      expect(continueStatement.source.range.start).toBe(419)
      expect(continueStatement.source.range.end).toBe(427)
      expect(continueStatement.source.loc.start.line).toBe(31)
      expect(continueStatement.source.loc.start.column).toBe(2)
      expect(continueStatement.source.loc.end.line).toBe(31)
      expect(continueStatement.source.loc.end.column).toBe(11)
    }
  })

  test('generate SwitchStatement node', () => {
    const switchStatement = astStatement.program?.main?.body.body[25] as SwitchStatement
    expect(switchStatement).toBeDefined()

    if (switchStatement !== undefined) {
      expect(switchStatement instanceof SwitchStatement).toBe(true)
      expect(switchStatement.kind).toBe(Kind.SwitchStatement)
      expect(switchStatement.parent).toBe(astStatement.program?.main?.body)
      expect(switchStatement.source.range.start).toBe(431)
      expect(switchStatement.source.range.end).toBe(494)
      expect(switchStatement.source.loc.start.line).toBe(32)
      expect(switchStatement.source.loc.start.column).toBe(2)
      expect(switchStatement.source.loc.end.line).toBe(36)
      expect(switchStatement.source.loc.end.column).toBe(3)

      expect(switchStatement.discriminant).toBeDefined()
      expect(switchStatement.discriminant.kind).toBe(Kind.Identifier)
      expect(switchStatement.discriminant.parent).toBe(switchStatement)
      expect((switchStatement.discriminant as Identifier).isExpression).toBe(true)
      expect(switchStatement.cases).toBeDefined()
      expect(switchStatement.cases.length).toBe(3)

      const case0 = switchStatement.cases[0]
      expect(case0 instanceof SwitchCase).toBe(true)
      expect(case0.kind).toBe(Kind.SwitchCase)
      expect(case0.parent).toBe(switchStatement)
      expect(case0.source.range.start).toBe(448)
      expect(case0.source.range.end).toBe(457)
      expect(case0.source.loc.start.line).toBe(33)
      expect(case0.source.loc.start.column).toBe(4)
      expect(case0.source.loc.end.line).toBe(33)
      expect(case0.source.loc.end.column).toBe(14)

      expect(case0.tests).toBeDefined()
      expect(case0.tests.length).toBe(1)
      if (case0.tests !== undefined) {
        expect(case0.tests[0].kind).toBe(Kind.IntegerLiteral)
        expect(case0.tests[0].parent).toBe(case0)
      }
      expect(case0.consequent.kind).toBe(Kind.BlockStatement)
      expect(case0.consequent.parent).toBe(case0)

      const case1 = switchStatement.cases[1]
      expect(case1.tests).toBeDefined()
      expect(case1.tests.length).toBe(2)
      if (case1.tests !== undefined) {
        expect(case1.tests[0].kind).toBe(Kind.IntegerLiteral)
        expect(case1.tests[0].parent).toBe(case1)
        expect(case1.tests[1].kind).toBe(Kind.IntegerLiteral)
        expect(case1.tests[1].parent).toBe(case1)
      }
      expect(case1.consequent.kind).toBe(Kind.BlockStatement)
      expect(case1.consequent.parent).toBe(case1)

      expect(switchStatement.cases[2].tests).toBeDefined()
      expect(switchStatement.cases[2].tests.length).toBe(0)
      expect(switchStatement.cases[2].consequent.kind).toBe(Kind.BlockStatement)
      expect(switchStatement.cases[2].consequent.parent).toBe(switchStatement.cases[2])
    }
  })

  test('generate SwitchtypeStatement node', () => {
    const switchtypeStatement = astStatement.program?.main?.body.body[26] as SwitchtypeStatement
    expect(switchtypeStatement).toBeDefined()

    if (switchtypeStatement !== undefined) {
      expect(switchtypeStatement instanceof SwitchtypeStatement).toBe(true)
      expect(switchtypeStatement.kind).toBe(Kind.SwitchtypeStatement)
      expect(switchtypeStatement.parent).toBe(astStatement.program?.main?.body)
      expect(switchtypeStatement.source.range.start).toBe(498)
      expect(switchtypeStatement.source.range.end).toBe(588)
      expect(switchtypeStatement.source.loc.start.line).toBe(37)
      expect(switchtypeStatement.source.loc.start.column).toBe(2)
      expect(switchtypeStatement.source.loc.end.line).toBe(41)
      expect(switchtypeStatement.source.loc.end.column).toBe(3)

      expect(switchtypeStatement.discriminant).toBeDefined()
      expect(switchtypeStatement.discriminant.kind).toBe(Kind.Identifier)
      expect(switchtypeStatement.discriminant.parent).toBe(switchtypeStatement)
      expect((switchtypeStatement.discriminant as Identifier).isExpression).toBe(true)
      expect(switchtypeStatement.alias).toBeUndefined()
      expect(switchtypeStatement.cases).toBeDefined()
      expect(switchtypeStatement.cases.length).toBe(3)

      const case0 = switchtypeStatement.cases[0]
      expect(case0 instanceof SwitchtypeCase).toBe(true)
      expect(case0.kind).toBe(Kind.SwitchtypeCase)
      expect(case0.parent).toBe(switchtypeStatement)
      expect(case0.source.range.start).toBe(519)
      expect(case0.source.range.end).toBe(534)
      expect(case0.source.loc.start.line).toBe(38)
      expect(case0.source.loc.start.column).toBe(4)
      expect(case0.source.loc.end.line).toBe(38)
      expect(case0.source.loc.end.column).toBe(20)

      expect(case0.tests).toBeDefined()
      expect(case0.tests.length).toBe(1)
      if (case0.tests !== undefined) {
        expect(case0.tests[0].kind).toBe(Kind.ClassType)
        expect(case0.tests[0].parent).toBe(case0)
        expect(case0.tests[0].name).toBe('CSmMode')
      }
      expect(case0.consequent.kind).toBe(Kind.BlockStatement)
      expect(case0.consequent.parent).toBe(case0)

      const case1 = switchtypeStatement.cases[1]
      expect(case1.tests).toBeDefined()
      expect(case1.tests.length).toBe(2)
      if (case1.tests !== undefined) {
        expect(case1.tests[0].kind).toBe(Kind.ClassType)
        expect(case1.tests[0].parent).toBe(case1)
        expect(case1.tests[0].name).toBe('CUIConfig')
        expect(case1.tests[1].kind).toBe(Kind.ClassType)
        expect(case1.tests[1].parent).toBe(case1)
        expect(case1.tests[1].name).toBe('CMlControl')
      }
      expect(case1.consequent.kind).toBe(Kind.BlockStatement)
      expect(case1.consequent.parent).toBe(case1)

      expect(switchtypeStatement.cases[2].tests).toBeDefined()
      expect(switchtypeStatement.cases[2].tests.length).toBe(0)
      expect(switchtypeStatement.cases[2].consequent.kind).toBe(Kind.BlockStatement)
      expect(switchtypeStatement.cases[2].consequent.parent).toBe(switchtypeStatement.cases[2])
    }
  })

  test('generate ConditionalStatement node', () => {
    const conditionalStatement = astStatement.program?.main?.body.body[27] as ConditionalStatement
    expect(conditionalStatement).toBeDefined()

    if (conditionalStatement !== undefined) {
      expect(conditionalStatement instanceof ConditionalStatement).toBe(true)
      expect(conditionalStatement.kind).toBe(Kind.ConditionalStatement)
      expect(conditionalStatement.parent).toBe(astStatement.program?.main?.body)
      expect(conditionalStatement.source.range.start).toBe(592)
      expect(conditionalStatement.source.range.end).toBe(652)
      expect(conditionalStatement.source.loc.start.line).toBe(42)
      expect(conditionalStatement.source.loc.start.column).toBe(2)
      expect(conditionalStatement.source.loc.end.line).toBe(48)
      expect(conditionalStatement.source.loc.end.column).toBe(3)

      expect(conditionalStatement.branches).toBeDefined()
      expect(conditionalStatement.branches.length).toBe(4)

      const branch0 = conditionalStatement.branches[0]
      expect(branch0 instanceof ConditionalBranch).toBe(true)
      expect(branch0.kind).toBe(Kind.ConditionalBranch)
      expect(branch0.parent).toBe(conditionalStatement)
      expect(branch0.source.range.start).toBe(592)
      expect(branch0.source.range.end).toBe(604)
      expect(branch0.source.loc.start.line).toBe(42)
      expect(branch0.source.loc.start.column).toBe(2)
      expect(branch0.source.loc.end.line).toBe(44)
      expect(branch0.source.loc.end.column).toBe(3)

      expect(branch0.test).toBeDefined()
      if (branch0.test !== undefined) {
        expect(branch0.test.kind).toBe(Kind.Identifier)
        expect(branch0.test.parent).toBe(branch0)
        expect((branch0.test as Identifier).isExpression).toBe(true)
      }
      expect(branch0.consequent.kind).toBe(Kind.BlockStatement)
      expect(branch0.consequent.parent).toBe(branch0)

      const branch1 = conditionalStatement.branches[1]
      expect(branch1.test).toBeDefined()
      if (branch1.test !== undefined) {
        expect(branch1.test.kind).toBe(Kind.Identifier)
        expect(branch1.test.parent).toBe(branch1)
        expect((branch1.test as Identifier).isExpression).toBe(true)
      }
      expect(branch1.consequent.kind).toBe(Kind.BlockStatement)
      expect(branch1.consequent.parent).toBe(branch1)

      const branch2 = conditionalStatement.branches[2]
      expect(branch2.test).toBeDefined()
      if (branch2.test !== undefined) {
        expect(branch2.test.kind).toBe(Kind.Identifier)
        expect(branch2.test.parent).toBe(branch2)
        expect((branch2.test as Identifier).isExpression).toBe(true)
      }
      expect(branch2.consequent.kind).toBe(Kind.ExpressionStatement)
      expect(branch2.consequent.parent).toBe(branch2)

      const branch3 = conditionalStatement.branches[3]
      expect(branch3.test).toBeUndefined()
      expect(branch3.consequent.kind).toBe(Kind.BlockStatement)
      expect(branch3.consequent.parent).toBe(branch3)
    }
  })

  test('generate LogStatement node', () => {
    const logStatement = astStatement.program?.main?.body.body[28] as LogStatement
    expect(logStatement).toBeDefined()

    if (logStatement !== undefined) {
      expect(logStatement instanceof LogStatement).toBe(true)
      expect(logStatement.kind).toBe(Kind.LogStatement)
      expect(logStatement.parent).toBe(astStatement.program?.main?.body)
      expect(logStatement.source.range.start).toBe(656)
      expect(logStatement.source.range.end).toBe(664)
      expect(logStatement.source.loc.start.line).toBe(49)
      expect(logStatement.source.loc.start.column).toBe(2)
      expect(logStatement.source.loc.end.line).toBe(49)
      expect(logStatement.source.loc.end.column).toBe(11)

      expect(logStatement.expression).toBeDefined()
      expect(logStatement.expression.kind).toBe(Kind.NowExpression)
      expect(logStatement.expression.parent).toBe(logStatement)
    }
  })

  test('generate SleepStatement node', () => {
    const sleepStatement = astStatement.program?.main?.body.body[29] as SleepStatement
    expect(sleepStatement).toBeDefined()

    if (sleepStatement !== undefined) {
      expect(sleepStatement instanceof SleepStatement).toBe(true)
      expect(sleepStatement.kind).toBe(Kind.SleepStatement)
      expect(sleepStatement.parent).toBe(astStatement.program?.main?.body)
      expect(sleepStatement.source.range.start).toBe(668)
      expect(sleepStatement.source.range.end).toBe(679)
      expect(sleepStatement.source.loc.start.line).toBe(50)
      expect(sleepStatement.source.loc.start.column).toBe(2)
      expect(sleepStatement.source.loc.end.line).toBe(50)
      expect(sleepStatement.source.loc.end.column).toBe(14)

      expect(sleepStatement.expression).toBeDefined()
      expect(sleepStatement.expression.kind).toBe(Kind.IntegerLiteral)
      expect(sleepStatement.expression.parent).toBe(sleepStatement)
    }
  })

  test('generate TuningstartStatement node', () => {
    const tuningstartStatement = astStatement.program?.main?.body.body[30] as TuningstartStatement
    expect(tuningstartStatement).toBeDefined()

    if (tuningstartStatement !== undefined) {
      expect(tuningstartStatement instanceof TuningstartStatement).toBe(true)
      expect(tuningstartStatement.kind).toBe(Kind.TuningstartStatement)
      expect(tuningstartStatement.parent).toBe(astStatement.program?.main?.body)
      expect(tuningstartStatement.source.range.start).toBe(683)
      expect(tuningstartStatement.source.range.end).toBe(696)
      expect(tuningstartStatement.source.loc.start.line).toBe(51)
      expect(tuningstartStatement.source.loc.start.column).toBe(2)
      expect(tuningstartStatement.source.loc.end.line).toBe(51)
      expect(tuningstartStatement.source.loc.end.column).toBe(16)
    }
  })

  test('generate TuningendStatement node', () => {
    const tuningendStatement = astStatement.program?.main?.body.body[31] as TuningendStatement
    expect(tuningendStatement).toBeDefined()

    if (tuningendStatement !== undefined) {
      expect(tuningendStatement instanceof TuningendStatement).toBe(true)
      expect(tuningendStatement.kind).toBe(Kind.TuningendStatement)
      expect(tuningendStatement.parent).toBe(astStatement.program?.main?.body)
      expect(tuningendStatement.source.range.start).toBe(700)
      expect(tuningendStatement.source.range.end).toBe(711)
      expect(tuningendStatement.source.loc.start.line).toBe(52)
      expect(tuningendStatement.source.loc.start.column).toBe(2)
      expect(tuningendStatement.source.loc.end.line).toBe(52)
      expect(tuningendStatement.source.loc.end.column).toBe(14)
    }
  })

  test('generate TuningmarkStatement node', () => {
    const tuningmarkStatement = astStatement.program?.main?.body.body[32] as TuningmarkStatement
    expect(tuningmarkStatement).toBeDefined()

    if (tuningmarkStatement !== undefined) {
      expect(tuningmarkStatement instanceof TuningmarkStatement).toBe(true)
      expect(tuningmarkStatement.kind).toBe(Kind.TuningmarkStatement)
      expect(tuningmarkStatement.parent).toBe(astStatement.program?.main?.body)
      expect(tuningmarkStatement.source.range.start).toBe(715)
      expect(tuningmarkStatement.source.range.end).toBe(730)
      expect(tuningmarkStatement.source.loc.start.line).toBe(53)
      expect(tuningmarkStatement.source.loc.start.column).toBe(2)
      expect(tuningmarkStatement.source.loc.end.line).toBe(53)
      expect(tuningmarkStatement.source.loc.end.column).toBe(18)

      expect(tuningmarkStatement.label).toBeDefined()
      expect(tuningmarkStatement.label.kind).toBe(Kind.TextLiteral)
      expect(tuningmarkStatement.label.parent).toBe(tuningmarkStatement)
    }
  })

  test('generate WaitStatement node', () => {
    const waitStatement = astStatement.program?.main?.body.body[33] as WaitStatement
    expect(waitStatement).toBeDefined()

    if (waitStatement !== undefined) {
      expect(waitStatement instanceof WaitStatement).toBe(true)
      expect(waitStatement.kind).toBe(Kind.WaitStatement)
      expect(waitStatement.parent).toBe(astStatement.program?.main?.body)
      expect(waitStatement.source.range.start).toBe(734)
      expect(waitStatement.source.range.end).toBe(744)
      expect(waitStatement.source.loc.start.line).toBe(54)
      expect(waitStatement.source.loc.start.column).toBe(2)
      expect(waitStatement.source.loc.end.line).toBe(54)
      expect(waitStatement.source.loc.end.column).toBe(13)

      expect(waitStatement.expression).toBeDefined()
      expect(waitStatement.expression.kind).toBe(Kind.BooleanLiteral)
      expect(waitStatement.expression.parent).toBe(waitStatement)
    }
  })

  test('generate YieldStatement node', () => {
    const yieldStatement = astStatement.program?.main?.body.body[34] as YieldStatement
    expect(yieldStatement).toBeDefined()

    if (yieldStatement !== undefined) {
      expect(yieldStatement instanceof YieldStatement).toBe(true)
      expect(yieldStatement.kind).toBe(Kind.YieldStatement)
      expect(yieldStatement.parent).toBe(astStatement.program?.main?.body)
      expect(yieldStatement.source.range.start).toBe(748)
      expect(yieldStatement.source.range.end).toBe(753)
      expect(yieldStatement.source.loc.start.line).toBe(55)
      expect(yieldStatement.source.loc.start.column).toBe(2)
      expect(yieldStatement.source.loc.end.line).toBe(55)
      expect(yieldStatement.source.loc.end.column).toBe(8)
    }
  })
})

describe('lib/ast part 4', () => {
  test('start and end token index', async () => {
    const input = `
    #Const C_Const_01 1
    main() {
      DoSomething();
    }
    `
    const result = await parse(input, { buildAst: true })
    const program = result.ast.program
    expect(program).toBeDefined()
    if (program !== undefined) {
      expect(program.source.token.start).toBe(2)
      expect(program.source.token.end).toBe(22)
      expect(result.tokens.get(program.source.token.start).text).toBe('#Const')
      expect(result.tokens.get(program.source.token.end).text).toBe('}')

      const constantDirective = (program.directives[0] as ConstDirective)
      expect(constantDirective.source.token.start).toBe(2)
      expect(constantDirective.source.token.end).toBe(6)
      expect(result.tokens.get(constantDirective.source.token.start).text).toBe('#Const')
      expect(result.tokens.get(constantDirective.source.token.end).text).toBe('1')
    }
  })

  test('isBaseType()', async () => {
    const inputPredicate = `
    #Const C_Const_01 1
    declare Integer Variable_01 = Integer;
    declare Integer Variable_02 = 1;
    `
    const astTypePredicate = (await parse(inputPredicate, { buildAst: true })).ast

    const type = (astTypePredicate.program?.declarations[0] as VariableDeclaration).type
    const variableDeclaration = (astTypePredicate.program?.declarations[0] as VariableDeclaration)
    const initializerType = (astTypePredicate.program?.declarations[0] as VariableDeclaration).initializerType
    const literal = (astTypePredicate.program?.declarations[1] as VariableDeclaration).initializerExpression
    const constantDirective = (astTypePredicate.program?.directives[0] as ConstDirective)
    expect(type).toBeDefined()
    expect(initializerType).toBeDefined()
    expect(literal).toBeDefined()
    expect(constantDirective).toBeDefined()

    if (type !== undefined) {
      expect(isBaseType(type)).toBe(true)
      expect(isInitializerType(type)).toBe(true)
      expect(isType(type)).toBe(true)
      expect(isLiteral(type)).toBe(false)
      expect(isDirective(type)).toBe(false)
      expect(isDeclaration(type)).toBe(false)
      expect(isStatement(type)).toBe(false)
      expect(isExpression(type)).toBe(false)
    }

    if (initializerType !== undefined) {
      expect(isBaseType(initializerType)).toBe(true)
      expect(isInitializerType(initializerType)).toBe(true)
      expect(isType(initializerType)).toBe(true)
      expect(isLiteral(initializerType)).toBe(false)
      expect(isDirective(initializerType)).toBe(false)
      expect(isDeclaration(initializerType)).toBe(false)
      expect(isStatement(initializerType)).toBe(false)
      expect(isExpression(initializerType)).toBe(false)
    }

    if (literal !== undefined) {
      expect(isBaseType(literal)).toBe(false)
      expect(isInitializerType(literal)).toBe(false)
      expect(isType(literal)).toBe(false)
      expect(isLiteral(literal)).toBe(true)
      expect(isDirective(literal)).toBe(false)
      expect(isDeclaration(literal)).toBe(false)
      expect(isStatement(literal)).toBe(false)
      expect(isExpression(literal)).toBe(true)
    }

    if (constantDirective !== undefined) {
      expect(isBaseType(constantDirective)).toBe(false)
      expect(isInitializerType(constantDirective)).toBe(false)
      expect(isType(constantDirective)).toBe(false)
      expect(isLiteral(constantDirective)).toBe(false)
      expect(isDirective(constantDirective)).toBe(true)
      expect(isDeclaration(constantDirective)).toBe(false)
      expect(isStatement(constantDirective)).toBe(false)
      expect(isExpression(constantDirective)).toBe(false)

      expect(constantDirective.declaration?.name).toBeDefined()
      if (constantDirective.declaration?.name !== undefined) {
        expect(isExpression(constantDirective.declaration?.name)).toBe(false)
      }
    }

    if (variableDeclaration !== undefined) {
      expect(isBaseType(variableDeclaration)).toBe(false)
      expect(isInitializerType(variableDeclaration)).toBe(false)
      expect(isType(variableDeclaration)).toBe(false)
      expect(isLiteral(variableDeclaration)).toBe(false)
      expect(isDirective(variableDeclaration)).toBe(false)
      expect(isDeclaration(variableDeclaration)).toBe(true)
      expect(isStatement(variableDeclaration)).toBe(true)
      expect(isExpression(variableDeclaration)).toBe(false)

      expect(isExpression(variableDeclaration.name)).toBe(false)
    }
  })

  test('mark variables as global or not', async () => {
    const input = `
    declare Integer G_GlobalVariableA;

    Void Function() {
      declare Integer LocalVariableB;
    }

    main() {
      declare Integer LocalVariableC;
    }
    `
    const result = await parse(input, { buildAst: true })

    const program = result.ast.program
    expect(program).toBeDefined()
    if (program !== undefined) {
      expect(program.declarations[0].kind).toBe(Kind.VariableDeclaration)
      expect(program.declarations[0].parent).toBe(program)
      expect((program.declarations[0] as VariableDeclaration).isGlobal).toBe(true)

      expect(program.declarations[1].kind).toBe(Kind.FunctionDeclaration)
      expect(program.declarations[1].parent).toBe(program)
      expect((program.declarations[1] as FunctionDeclaration).body.body[0].kind).toBe(Kind.VariableDeclaration)
      expect((program.declarations[1] as FunctionDeclaration).body.body[0].parent).toBe((program.declarations[1] as FunctionDeclaration).body)
      expect(((program.declarations[1] as FunctionDeclaration).body.body[0] as VariableDeclaration).isGlobal).toBe(false)

      expect(program.main).toBeDefined()
      if (program.main !== undefined) {
        expect(program.main.body.body[0].kind).toBe(Kind.VariableDeclaration)
        expect(program.main.body.body[0].parent).toBe(program.main.body)
        expect((program.main.body.body[0] as VariableDeclaration).isGlobal).toBe(false)
      }
    }
  })

  test('mark variables as declared with let or not', async () => {
    const input = `
    main() {
      declare Integer LocalVariableA;
      let LocalVariableB = 10;
    }
    `
    const result = await parse(input, { buildAst: true })

    const program = result.ast.program
    expect(program).toBeDefined()
    if (program !== undefined) {
      expect(program.main).toBeDefined()
      if (program.main !== undefined) {
        expect(program.main.body.body[0].kind).toBe(Kind.VariableDeclaration)
        expect(program.main.body.body[0].parent).toBe(program.main.body)
        expect((program.main.body.body[0] as VariableDeclaration).isLet).toBe(false)

        expect(program.main.body.body[1].kind).toBe(Kind.VariableDeclaration)
        expect(program.main.body.body[1].parent).toBe(program.main.body)
        expect((program.main.body.body[1] as VariableDeclaration).isLet).toBe(true)
      }
    }
  })

  test('set let variable initializer', async () => {
    const input = `
    main() {
      let Variable_01 = 10;
    }
    `
    const result = await parse(input, { buildAst: true })

    const program = result.ast.program
    expect(program).toBeDefined()
    if (program !== undefined) {
      expect(program.main).toBeDefined()
      if (program.main !== undefined) {
        const letVariableDeclaration = program.main.body.body[0]
        expect(letVariableDeclaration.kind).toBe(Kind.VariableDeclaration)
        expect(letVariableDeclaration.parent).toBe(program.main.body)
        expect(letVariableDeclaration instanceof VariableDeclaration)

        if (letVariableDeclaration instanceof VariableDeclaration) {
          expect(letVariableDeclaration.isLet).toBe(true)
          expect(letVariableDeclaration.initializerSign).toBeDefined()
          expect(letVariableDeclaration.initializerSign).toBe(InitializerSign['='])
          expect(letVariableDeclaration.initializerExpression).toBeDefined()
          if (letVariableDeclaration.initializerExpression !== undefined) {
            expect(letVariableDeclaration.initializerExpression instanceof IntegerLiteral).toBe(true)
            expect(letVariableDeclaration.initializerExpression.kind).toBe(Kind.IntegerLiteral)
            expect(letVariableDeclaration.initializerExpression.parent).toBe(letVariableDeclaration)
            expect((letVariableDeclaration.initializerExpression as IntegerLiteral).value).toBe(10)
          }
          expect(letVariableDeclaration.initializerType).toBeUndefined()
        }
      }
    }
  })

  describe('create ast containing parse tree errors', () => {
    test('parsing does not throw an error', async () => {
      const result = await parse('aaa', { buildAst: true })
      expect(result.success).toBe(false)
      expect(result.ast.program).toBeDefined()

      if (result.ast.program !== undefined) {
        expect(result.ast.program.directives).toHaveLength(0)
        expect(result.ast.program.declarations).toHaveLength(1)
        expect(result.ast.program.declarations[0].kind).toBe(Kind.InvalidDeclaration)
        expect(result.ast.program.main).toBeUndefined()
      }
    })

    test('Incomplete main function', async () => {
      const result = await parse('main()', { buildAst: true })
      expect(result.success).toBe(false)
      expect(result.ast.program).toBeDefined()

      if (result.ast.program !== undefined) {
        expect(result.ast.program.directives).toHaveLength(0)
        expect(result.ast.program.declarations).toHaveLength(0)
        expect(result.ast.program.main?.body).toBeDefined()
      }
    })

    test('Incomplete const directive', async () => {
      const result = await parse(`
        #Const C_Test_01 1
        #Const C_Test_02
        #Const C_Test_03 3
      `, { buildAst: true })

      expect(result.success).toBe(false)

      const program = result.ast.program
      expect(program).toBeDefined()
      if (program !== undefined) {
        expect(program.kind).toBe(Kind.Program)
        expect(program.parent).toBeUndefined()
        expect(program.source.range.start).toBe(9)
        expect(program.source.range.end).toBe(78)
        expect(program.source.loc.start.line).toBe(2)
        expect(program.source.loc.start.column).toBe(8)
        expect(program.source.loc.end.line).toBe(4)
        expect(program.source.loc.end.column).toBe(26)

        expect(program.directives).toHaveLength(3)

        const const01 = program.directives[0]
        expect(const01 instanceof ConstDirective).toBe(true)
        if (const01 instanceof ConstDirective) {
          expect(const01.declaration?.name.name).toBe('C_Test_01')
        }

        expect(program.directives[1] instanceof InvalidDirective).toBe(true)
        expect(program.directives[2] instanceof InvalidDirective).toBe(true)
      }
    })

    test('Incomplete variable declaration', async () => {
      const result = await parse(`
        main() {
          declare Integer Test_01;
          declare
          declare Integer Test_02;
        }
      `, { buildAst: true })

      expect(result.success).toBe(false)

      const program = result.ast.program
      expect(program).toBeDefined()
      if (program !== undefined) {
        expect(program.kind).toBe(Kind.Program)
        expect(program.parent).toBeUndefined()
        expect(program.source.range.start).toBe(9)
        expect(program.source.range.end).toBe(114)
        expect(program.source.loc.start.line).toBe(2)
        expect(program.source.loc.start.column).toBe(8)
        expect(program.source.loc.end.line).toBe(6)
        expect(program.source.loc.end.column).toBe(9)

        expect(result.ast.program?.main?.body.body).toBeDefined()
        if (result.ast.program?.main?.body.body !== undefined) {
          expect(result.ast.program.main.body.body).toHaveLength(4);
          expect(result.ast.program.main.body.body[0].kind).toBe(Kind.VariableDeclaration)
          expect(result.ast.program.main.body.body[1].kind).toBe(Kind.InvalidStatement)
          expect(result.ast.program.main.body.body[2].kind).toBe(Kind.InvalidStatement)
          expect(result.ast.program.main.body.body[3].kind).toBe(Kind.VariableDeclaration)
        }
      }
    })
  })

  describe('quick tests', () => {
    test('produces ArrayExpression nodes', async () => {
      const input = `
      main() {
        declare Integer[] A = [
          1,
          2,
          3
        ];
      }
      `
      const result = await parse(input, { buildAst: true })

      const program = result.ast.program
      expect(program).toBeDefined()
      if (program !== undefined) {
        expect(program.main).toBeDefined()
        if (program.main !== undefined) {
          const variableDeclaration = program.main.body.body[0]
          expect(variableDeclaration.kind).toBe(Kind.VariableDeclaration)
          expect(variableDeclaration.parent).toBe(program.main.body)
          if (variableDeclaration instanceof VariableDeclaration) {
            expect(variableDeclaration.initializerExpression).toBeDefined()
            if (variableDeclaration.initializerExpression !== undefined) {
              expect(variableDeclaration.initializerExpression.kind).toBe(Kind.ArrayExpression)
              expect(variableDeclaration.initializerExpression.parent).toBe(variableDeclaration)
            }
          }
        }
      }
    })

    test('avoid octal conversion', async () => {
      const input = `
      main() {
        0777;
        0777.7;
      }
      `
      const result = await parse(input, { buildAst: true })

      const statements = result.ast.program?.main?.body.body
      expect(statements).toBeDefined()
      if (statements !== undefined) {
        const integer = (statements[0] as ExpressionStatement).expression as IntegerLiteral
        const real = (statements[1] as ExpressionStatement).expression as RealLiteral
        expect(integer.value).toBe(777)
        expect(real.value).toBe(777.7)
      }
    })

    test('parse structure initialization', async () => {
      const input = `
      main() {
        declare K_StructA StructA = K_StructA {
          G = _G,
          H = _H,
          I = _I,
          J = _J,
          K = _K
        };
      }
      `
      const result = await parse(input, { buildAst: true })

      const statements = result.ast.program?.main?.body.body
      expect(statements).toBeDefined()
      if (statements !== undefined) {
        const variableDeclaration = statements[0] as VariableDeclaration
        expect(variableDeclaration).toBeDefined()
        expect(variableDeclaration.initializerExpression).toBeDefined()
        if (variableDeclaration.initializerExpression !== undefined) {
          expect(variableDeclaration.initializerExpression instanceof StructExpression).toBe(true)
        }
      }
    })

    test('support alias on switchtype statement', async () => {
      const input = `
      main() {
        switchtype (A as B) {
          case CSmMode: {}
        }
        switchtype (A as CSmMode) {
          case CSmMode: {}
        }
      }
      `
      const result = await parse(input, { buildAst: true })

      const statements = result.ast.program?.main?.body.body
      expect(statements).toBeDefined()
      if (statements !== undefined) {
        const switchtypeStatement0 = statements[0] as SwitchtypeStatement
        expect(switchtypeStatement0).toBeDefined()
        expect(switchtypeStatement0.alias).toBeDefined()
        if (switchtypeStatement0.alias !== undefined) {
          expect(switchtypeStatement0.alias instanceof Identifier).toBe(true)
          expect(switchtypeStatement0.alias.name).toBe('B')
          expect(switchtypeStatement0.alias.source.range.start).toBe(41)
          expect(switchtypeStatement0.alias.source.range.end).toBe(41)
        }

        const switchtypeStatement1 = statements[1] as SwitchtypeStatement
        expect(switchtypeStatement1).toBeDefined()
        expect(switchtypeStatement1.alias).toBeUndefined()
        expect(switchtypeStatement1.discriminant.kind).toBe(Kind.AsExpression)
      }
    })

    test('support empty struct declaration', async () => {
      const input = `
      #Struct K_Struct_01 {}
      `
      const result = await parse(input, { buildAst: true })

      const structDirective = result.ast.program?.directives[0]
      expect(structDirective).toBeDefined()
      expect(structDirective instanceof StructDirective).toBe(true)
      if (structDirective instanceof StructDirective) {
        expect(structDirective.kind).toBe(Kind.StructDirective)

        expect('declaration' in structDirective).toBe(true)
        expect(structDirective.declaration instanceof StructDeclaration).toBe(true)
        expect(structDirective.aliasing).toBeUndefined()

        if (structDirective.declaration !== undefined) {
          expect(structDirective.declaration.kind).toBe(Kind.StructDeclaration)

          expect('name' in structDirective.declaration).toBe(true)
          expect(structDirective.declaration.name instanceof Identifier).toBe(true)
          if (structDirective.declaration.name !== undefined) {
            expect(structDirective.declaration.name.kind).toBe(Kind.Identifier)
            expect(structDirective.declaration.name.name).toBe('K_Struct_01')
          }

          expect('members' in structDirective.declaration).toBe(true)
          expect(structDirective.declaration.members.length).toBe(0)
        }
      }
    })
  })
})
