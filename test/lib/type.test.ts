import { describe, test, expect } from 'vitest'
import { getParser } from '../tools'
import { TypeKind, deduceTypeFromContext } from '../../src/lib/type'

describe('lib/type.ts', () => {
  describe('Find constant types', () => {
    describe('Constant declaration', () => {
      test('Text', () => {
        const parser = getParser('#Const C_Test_01 "1"')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Text, name: '' })
      })
      test('Integer', () => {
        const parser = getParser('#Const C_Test_02 2')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Integer, name: '' })
      })
      test('Real', () => {
        const parser = getParser('#Const C_Test_03 3.0')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Real, name: '' })
      })
      test('Boolean', () => {
        const parser = getParser('#Const C_Test_04 True')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Boolean, name: '' })
      })
      test('Class', () => {
        const parser = getParser('#Const C_Test_05 Null')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Class, name: '' })
      })
      test('Ident', () => {
        const parser = getParser('#Const C_Test_06 NullId')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Ident, name: '' })
      })
      test('Enum', () => {
        const parser = getParser('#Const C_Test_07 CSmMode::EMedal::Gold')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Enum, name: '' })
      })
      test('Int2', () => {
        const parser = getParser('#Const C_Test_08 <8, 8>')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Int2, name: '' })
      })
      test('Vec2', () => {
        const parser = getParser('#Const C_Test_09 <9., 9.>')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Vec2, name: '' })
      })
      test('Int3', () => {
        const parser = getParser('#Const C_Test_10 <10, 10, 10>')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Int3, name: '' })
      })
      test('Vec3', () => {
        const parser = getParser('#Const C_Test_11 <11., 11., 11.>')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Vec3, name: '' })
      })
      test('Array', () => {
        const parser = getParser('#Const C_Test_12 [1, 2]')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Array, name: '' })
      })
      test('Local structure', () => {
        const parser = getParser('#Const C_Test_13 K_Test_13 { A = "13", B = "13" }')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Structure, name: 'K_Test_13'})
      })
      test('Imported structure', () => {
        const parser = getParser('#Const C_Test_14 Lib14::K_Test_14 { A = "14", B = "14" }')
        const type = deduceTypeFromContext(parser.constDeclaration())
        expect(type).toEqual({ kind: TypeKind.Structure, name: 'Lib14::K_Test_14'})
      })
    })

    test('Constant aliasing', () => {
      const parser = getParser('#Const Lib1::C_TestLib_01 as C_Test_01')
      const type = deduceTypeFromContext(parser.constAliasing())
      expect(type).toEqual({ kind: TypeKind.Unknown, name: ''})
    })
  })

  describe('Find variable declaration type', () => {
    describe('Variable declaration with explicit type', () => {
      test('Boolean', () => {
        const parser = getParser('declare Boolean Test_01;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Boolean, name: ''})
      })
      test('Ident', () => {
        const parser = getParser('declare Ident Test_02;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Ident, name: ''})
      })
      test('Int2', () => {
        const parser = getParser('declare Int2 Test_03;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Int2, name: ''})
      })
      test('Int3', () => {
        const parser = getParser('declare Int3 Test_04;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Int3, name: ''})
      })
      test('Integer', () => {
        const parser = getParser('declare Integer Test_05;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Integer, name: ''})
      })
      test('Real', () => {
        const parser = getParser('declare Real Test_06;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Real, name: ''})
      })
      test('Text', () => {
        const parser = getParser('declare Text Test_07;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Text, name: ''})
      })
      test('Vec2', () => {
        const parser = getParser('declare Vec2 Test_08;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Vec2, name: ''})
      })
      test('Vec3', () => {
        const parser = getParser('declare Vec3 Test_09;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Vec3, name: ''})
      })
      test('Array', () => {
        const parser = getParser('declare Text[] Test_10;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Array, name: ''})
      })
    })

    describe('Variable declaration with implicit type', () => {
      test('Integer', () => {
        const parser = getParser('declare Test_01 = 1;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Unknown, name: ''})
      })
    })

    describe('Variable declaration with let', () => {
      test('Integer', () => {
        const parser = getParser('let Test_01 = 1;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Unknown, name: ''})
      })
    })

    describe('Trait declaration', () => {
      test('Integer', () => {
        const parser = getParser('declare Integer Test_01 for This;')
        const type = deduceTypeFromContext(parser.variableDeclaration())
        expect(type).toEqual({ kind: TypeKind.Integer, name: ''})
      })
    })
  })
})
