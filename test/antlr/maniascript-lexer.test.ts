import { describe, test, expect } from 'vitest'
import { CharStream, Token } from 'antlr4ng'
import { ManiaScriptLexer } from '../../src/antlr/ManiaScriptLexer'

const getTokensManiaScript = (input: string, classes: string[] = []): Token[] => {
  const chars = CharStream.fromString(input)
  const lexer = new ManiaScriptLexer(chars)
  lexer.removeErrorListeners()
  if (classes.length > 0) {
    lexer.setClasses(classes)
  }
  return lexer.getAllTokens()
}

const getTokensManiaScriptWithCustomSet = (input: string, classes: Set<string>): Token[] => {
  const chars = CharStream.fromString(input)
  const lexer = new ManiaScriptLexer(chars)
  lexer.removeErrorListeners()
  lexer.setClasses(classes)
  return lexer.getAllTokens()
}

const getTokensManiaScriptWithCustomApi = async (input: string, msApiPath = ''): Promise<Token[]> => {
  const chars = CharStream.fromString(input)
  const lexer = new ManiaScriptLexer(chars)
  lexer.removeErrorListeners()
  if (msApiPath.length > 0) {
    await lexer.setMSApi(msApiPath)
  }
  return lexer.getAllTokens()
}

const validateToken = (token: Token, type: number, start: number, text: string): void => {
  expect(token.type).toBe(type)
  expect(token.start).toBe(start)
  expect(token.text).toBe(text)
}

describe('Lexer', function () {
  describe('find comments', function () {
    test('single line', function () {
      const tokens = getTokensManiaScript('// Comment1')
      validateToken(tokens[0], ManiaScriptLexer.SINGLE_LINE_COMMENT, 0, '// Comment1')
    })
    test('multi lines', function () {
      const tokens = getTokensManiaScript('/* Comment1 */' + '\n' + '/*\nComment2\n*/')
      validateToken(tokens[0], ManiaScriptLexer.MULTI_LINES_COMMENT, 0, '/* Comment1 */')
      validateToken(tokens[2], ManiaScriptLexer.MULTI_LINES_COMMENT, 15, '/*\nComment2\n*/')
    })
  })

  describe('find keywords', function () {
    test('assert', function () {
      const tokens = getTokensManiaScript('assert')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_ASSERT, 0, 'assert')
    })
    test('as', function () {
      const tokens = getTokensManiaScript('as')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_AS, 0, 'as')
    })
    test('break', function () {
      const tokens = getTokensManiaScript('break')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_BREAK, 0, 'break')
    })
    test('case', function () {
      const tokens = getTokensManiaScript('case')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_CASE, 0, 'case')
    })
    test('cast', function () {
      const tokens = getTokensManiaScript('cast')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_CAST, 0, 'cast')
    })
    test('continue', function () {
      const tokens = getTokensManiaScript('continue')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_CONTINUE, 0, 'continue')
    })
    test('declare', function () {
      const tokens = getTokensManiaScript('declare')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_DECLARE, 0, 'declare')
    })
    test('default', function () {
      const tokens = getTokensManiaScript('default')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_DEFAULT, 0, 'default')
    })
    test('dumptype', function () {
      const tokens = getTokensManiaScript('dumptype')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_DUMPTYPE, 0, 'dumptype')
    })
    test('dump', function () {
      const tokens = getTokensManiaScript('dump')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_DUMP, 0, 'dump')
    })
    test('else', function () {
      const tokens = getTokensManiaScript('else')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_ELSE, 0, 'else')
    })
    test('foreach', function () {
      const tokens = getTokensManiaScript('foreach')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_FOREACH, 0, 'foreach')
    })
    test('for', function () {
      const tokens = getTokensManiaScript('for')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_FOR, 0, 'for')
    })
    test('if', function () {
      const tokens = getTokensManiaScript('if')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_IF, 0, 'if')
    })
    test('in', function () {
      const tokens = getTokensManiaScript('in')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_IN, 0, 'in')
    })
    test('is', function () {
      const tokens = getTokensManiaScript('is')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_IS, 0, 'is')
    })
    test('log', function () {
      const tokens = getTokensManiaScript('log')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_LOG, 0, 'log')
    })
    test('main', function () {
      const tokens = getTokensManiaScript('main')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_MAIN, 0, 'main')
    })
    test('meanwhile', function () {
      const tokens = getTokensManiaScript('meanwhile')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_MEANWHILE, 0, 'meanwhile')
    })
    test('return', function () {
      const tokens = getTokensManiaScript('return')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_RETURN, 0, 'return')
    })
    test('reverse', function () {
      const tokens = getTokensManiaScript('reverse')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_REVERSE, 0, 'reverse')
    })
    test('sleep', function () {
      const tokens = getTokensManiaScript('sleep')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_SLEEP, 0, 'sleep')
    })
    test('switchtype', function () {
      const tokens = getTokensManiaScript('switchtype')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_SWITCHTYPE, 0, 'switchtype')
    })
    test('switch', function () {
      const tokens = getTokensManiaScript('switch')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_SWITCH, 0, 'switch')
    })
    test('tuningend', function () {
      const tokens = getTokensManiaScript('tuningend')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_TUNINGEND, 0, 'tuningend')
    })
    test('tuningmark', function () {
      const tokens = getTokensManiaScript('tuningmark')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_TUNINGMARK, 0, 'tuningmark')
    })
    test('tuningstart', function () {
      const tokens = getTokensManiaScript('tuningstart')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_TUNINGSTART, 0, 'tuningstart')
    })
    test('wait', function () {
      const tokens = getTokensManiaScript('wait')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_WAIT, 0, 'wait')
    })
    test('while', function () {
      const tokens = getTokensManiaScript('while')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_WHILE, 0, 'while')
    })
    test('yield', function () {
      const tokens = getTokensManiaScript('yield')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_YIELD, 0, 'yield')
    })
    test('This', function () {
      const tokens = getTokensManiaScript('This')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_THIS, 0, 'This')
    })
    test('Now', function () {
      const tokens = getTokensManiaScript('Now')
      validateToken(tokens[0], ManiaScriptLexer.KEYWORD_NOW, 0, 'Now')
    })
  })

  describe('find labels', function () {
    test('+++', function () {
      const tokens = getTokensManiaScript('+++')
      validateToken(tokens[0], ManiaScriptLexer.LABEL_PLUS, 0, '+++')
    })
    test('---', function () {
      const tokens = getTokensManiaScript('---')
      validateToken(tokens[0], ManiaScriptLexer.LABEL_MINUS, 0, '---')
    })
    test('***', function () {
      const tokens = getTokensManiaScript('***')
      validateToken(tokens[0], ManiaScriptLexer.LABEL_STAR, 0, '***')
    })
    test('all labels together', function () {
      const input = '+++ + --- - *** *'
      const tokens = getTokensManiaScript(input)
      validateToken(tokens[0], ManiaScriptLexer.LABEL_PLUS, 0, '+++')
      validateToken(tokens[2], ManiaScriptLexer.OPERATOR_PLUS, 4, '+')
      validateToken(tokens[4], ManiaScriptLexer.LABEL_MINUS, 6, '---')
      validateToken(tokens[6], ManiaScriptLexer.OPERATOR_MINUS, 10, '-')
      validateToken(tokens[8], ManiaScriptLexer.LABEL_STAR, 12, '***')
      validateToken(tokens[10], ManiaScriptLexer.OPERATOR_MULTIPLY, 16, '*')
    })
  })

  describe('find operators', function () {
    test('=', function () {
      const tokens = getTokensManiaScript('=')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_ASSIGN, 0, '=')
    })
    test(';', function () {
      const tokens = getTokensManiaScript(';')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_SEMICOLON, 0, ';')
    })
    test('+', function () {
      const tokens = getTokensManiaScript('+')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_PLUS, 0, '+')
    })
    test('-', function () {
      const tokens = getTokensManiaScript('-')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_MINUS, 0, '-')
    })
    test('*', function () {
      const tokens = getTokensManiaScript('*')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_MULTIPLY, 0, '*')
    })
    test('/', function () {
      const tokens = getTokensManiaScript('/')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_DIVIDE, 0, '/')
    })
    test('%', function () {
      const tokens = getTokensManiaScript('%')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_MODULO, 0, '%')
    })
    test('()', function () {
      const tokens = getTokensManiaScript('()')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_PAREN, 0, '(')
      validateToken(tokens[1], ManiaScriptLexer.OPERATOR_CLOSE_PAREN, 1, ')')
    })
    test('[]', function () {
      const tokens = getTokensManiaScript('[]')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_BRACKET, 0, '[')
      validateToken(tokens[1], ManiaScriptLexer.OPERATOR_CLOSE_BRACKET, 1, ']')
    })
    test('{}', function () {
      const tokens = getTokensManiaScript('{}')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_BRACE, 0, '{')
      validateToken(tokens[1], ManiaScriptLexer.OPERATOR_CLOSE_BRACE, 1, '}')
    })
    test('^', function () {
      const tokens = getTokensManiaScript('^')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_CONCAT, 0, '^')
    })
    test(',', function () {
      const tokens = getTokensManiaScript(',')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_COMMA, 0, ',')
    })
    test(':', function () {
      const tokens = getTokensManiaScript(':')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_COLON, 0, ':')
    })
    test('.', function () {
      const tokens = getTokensManiaScript('.')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_DOT, 0, '.')
    })
    test('!', function () {
      const tokens = getTokensManiaScript('!')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_NOT, 0, '!')
    })
    test('<', function () {
      const tokens = getTokensManiaScript('<')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_LESSTHAN, 0, '<')
    })
    test('>', function () {
      const tokens = getTokensManiaScript('>')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_MORETHAN, 0, '>')
    })
    test('<=', function () {
      const tokens = getTokensManiaScript('<=')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_LESSTHANEQUAL, 0, '<=')
    })
    test('>=', function () {
      const tokens = getTokensManiaScript('>=')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_MORETHANEQUAL, 0, '>=')
    })
    test('==', function () {
      const tokens = getTokensManiaScript('==')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_EQUAL, 0, '==')
    })
    test('!=', function () {
      const tokens = getTokensManiaScript('!=')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_NOTEQUAL, 0, '!=')
    })
    test('&&', function () {
      const tokens = getTokensManiaScript('&&')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_AND, 0, '&&')
    })
    test('||', function () {
      const tokens = getTokensManiaScript('||')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OR, 0, '||')
    })
    test('*=', function () {
      const tokens = getTokensManiaScript('*=')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_MULTIPLYASSIGN, 0, '*=')
    })
    test('/=', function () {
      const tokens = getTokensManiaScript('/=')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_DIVIDEASSIGN, 0, '/=')
    })
    test('+=', function () {
      const tokens = getTokensManiaScript('+=')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_PLUSASSIGN, 0, '+=')
    })
    test('-=', function () {
      const tokens = getTokensManiaScript('-=')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_MINUSASSIGN, 0, '-=')
    })
    test('%=', function () {
      const tokens = getTokensManiaScript('%=')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_MODULOASSIGN, 0, '%=')
    })
    test('<=>', function () {
      const tokens = getTokensManiaScript('<=>')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_ARROWASSIGN, 0, '<=>')
    })
  })

  describe('find types', function () {
    test('Boolean', function () {
      const tokens = getTokensManiaScript('Boolean')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_BOOLEAN, 0, 'Boolean')
    })
    test('Ident', function () {
      const tokens = getTokensManiaScript('Ident')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_IDENT, 0, 'Ident')
    })
    test('Int2', function () {
      const tokens = getTokensManiaScript('Int2')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_INT2, 0, 'Int2')
    })
    test('Int3', function () {
      const tokens = getTokensManiaScript('Int3')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_INT3, 0, 'Int3')
    })
    test('Integer', function () {
      const tokens = getTokensManiaScript('Integer')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_INTEGER, 0, 'Integer')
    })
    test('Real', function () {
      const tokens = getTokensManiaScript('Real')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_REAL, 0, 'Real')
    })
    test('Text', function () {
      const tokens = getTokensManiaScript('Text')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_TEXT, 0, 'Text')
    })
    test('Vec2', function () {
      const tokens = getTokensManiaScript('Vec2')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_VEC2, 0, 'Vec2')
    })
    test('Vec3', function () {
      const tokens = getTokensManiaScript('Vec3')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_VEC3, 0, 'Vec3')
    })
    test('Void', function () {
      const tokens = getTokensManiaScript('Void')
      validateToken(tokens[0], ManiaScriptLexer.TYPE_VOID, 0, 'Void')
    })
  })

  describe('find storage specifiers', function () {
    test('cloud', function () {
      const tokens = getTokensManiaScript('cloud')
      validateToken(tokens[0], ManiaScriptLexer.STORAGESPECIFIER_CLOUD, 0, 'cloud')
    })
    test('metadata', function () {
      const tokens = getTokensManiaScript('metadata')
      validateToken(tokens[0], ManiaScriptLexer.STORAGESPECIFIER_METADATA, 0, 'metadata')
    })
    test('netread', function () {
      const tokens = getTokensManiaScript('netread')
      validateToken(tokens[0], ManiaScriptLexer.STORAGESPECIFIER_NETREAD, 0, 'netread')
    })
    test('netwrite', function () {
      const tokens = getTokensManiaScript('netwrite')
      validateToken(tokens[0], ManiaScriptLexer.STORAGESPECIFIER_NETWRITE, 0, 'netwrite')
    })
    test('persistent', function () {
      const tokens = getTokensManiaScript('persistent')
      validateToken(tokens[0], ManiaScriptLexer.STORAGESPECIFIER_PERSISTENT, 0, 'persistent')
    })
  })

  describe('find literals', function () {
    test('Integer', function () {
      const input = '10 01 10e10 1e+1 1e-1 1234567890 00000001 123e456 123e+456 123e-456'
      const tokens = getTokensManiaScript(input)
      const words = input.split(' ')
      let start = 0
      for (let key = 0; key < words.length; key++) {
        const word = words[key]
        validateToken(tokens[key * 2], ManiaScriptLexer.LITERAL_INTEGER, start, word)
        start += word.length + ' '.length
      }
    })
    test('Real', function () {
      const input = '1.0 1. .1 1.0e1 1.e1 .1e1 1.0e+1 1.0e-1 123.456 .123 123.'
      const tokens = getTokensManiaScript(input)
      const words = input.split(' ')
      let start = 0
      for (let key = 0; key < words.length; key++) {
        const word = words[key]
        validateToken(tokens[key * 2], ManiaScriptLexer.LITERAL_REAL, start, word)
        start += word.length + ' '.length
      }
    })
    test('Boolean', function () {
      const input = 'True False'
      const tokens = getTokensManiaScript(input)
      const words = input.split(' ')
      let start = 0
      for (let key = 0; key < words.length; key++) {
        const word = words[key]
        validateToken(tokens[key * 2], ManiaScriptLexer.LITERAL_BOOLEAN, start, word)
        start += word.length + ' '.length
      }
    })
    test('Null', function () {
      const tokens = getTokensManiaScript('Null')
      validateToken(tokens[0], ManiaScriptLexer.LITERAL_NULL, 0, 'Null')
    })
    test('NullId', function () {
      const tokens = getTokensManiaScript('NullId')
      validateToken(tokens[0], ManiaScriptLexer.LITERAL_NULLID, 0, 'NullId')
    })
  })

  describe('find Text', function () {
    test('Text single quote', function () {
      const tokens = getTokensManiaScript('"Test" "AAA \\n BBB" "CCC \\" DDD" "EEE \\\\ FFF"')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_SINGLE, 0, '"')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_SINGLE, 1, 'Test')
      validateToken(tokens[2], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_SINGLE, 5, '"')
      validateToken(tokens[4], ManiaScriptLexer.OPERATOR_OPEN_TEXT_SINGLE, 7, '"')
      validateToken(tokens[5], ManiaScriptLexer.PART_TEXT_SINGLE, 8, 'AAA \\n BBB')
      validateToken(tokens[6], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_SINGLE, 18, '"')
      validateToken(tokens[8], ManiaScriptLexer.OPERATOR_OPEN_TEXT_SINGLE, 20, '"')
      validateToken(tokens[9], ManiaScriptLexer.PART_TEXT_SINGLE, 21, 'CCC \\" DDD')
      validateToken(tokens[10], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_SINGLE, 31, '"')
      validateToken(tokens[12], ManiaScriptLexer.OPERATOR_OPEN_TEXT_SINGLE, 33, '"')
      validateToken(tokens[13], ManiaScriptLexer.PART_TEXT_SINGLE, 34, 'EEE \\\\ FFF')
      validateToken(tokens[14], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_SINGLE, 44, '"')
    })

    test('Text triple quote simple', function () {
      const tokens = getTokensManiaScript('"""Test""" """AAA \\n BBB""" """CCC \\" DDD""" """A " "" B""" """A { } {{ }} B"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'Test')
      validateToken(tokens[2], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 7, '"""')
      validateToken(tokens[3], ManiaScriptLexer.WHITESPACES, 10, ' ')
      validateToken(tokens[4], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 11, '"""')
      validateToken(tokens[5], ManiaScriptLexer.PART_TEXT_TRIPLE, 14, 'AAA \\n BBB')
      validateToken(tokens[6], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 24, '"""')
      validateToken(tokens[7], ManiaScriptLexer.WHITESPACES, 27, ' ')
      validateToken(tokens[8], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 28, '"""')
      validateToken(tokens[9], ManiaScriptLexer.PART_TEXT_TRIPLE, 31, 'CCC \\')
      validateToken(tokens[10], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 36, '"')
      validateToken(tokens[11], ManiaScriptLexer.PART_TEXT_TRIPLE, 37, ' DDD')
      validateToken(tokens[12], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 41, '"""')
      validateToken(tokens[13], ManiaScriptLexer.WHITESPACES, 44, ' ')
      validateToken(tokens[14], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 45, '"""')
      validateToken(tokens[15], ManiaScriptLexer.PART_TEXT_TRIPLE, 48, 'A ')
      validateToken(tokens[16], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 50, '"')
      validateToken(tokens[17], ManiaScriptLexer.PART_TEXT_TRIPLE, 51, ' ')
      validateToken(tokens[18], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 52, '"')
      validateToken(tokens[19], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 53, '"')
      validateToken(tokens[20], ManiaScriptLexer.PART_TEXT_TRIPLE, 54, ' B')
      validateToken(tokens[21], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 56, '"""')
      validateToken(tokens[22], ManiaScriptLexer.WHITESPACES, 59, ' ')
      validateToken(tokens[23], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 60, '"""')
      validateToken(tokens[24], ManiaScriptLexer.PART_TEXT_TRIPLE, 63, 'A ')
      validateToken(tokens[25], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 65, '{')
      validateToken(tokens[26], ManiaScriptLexer.PART_TEXT_TRIPLE, 66, ' } ')
      validateToken(tokens[27], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 69, '{')
      validateToken(tokens[28], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 70, '{')
      validateToken(tokens[29], ManiaScriptLexer.PART_TEXT_TRIPLE, 71, ' }} B')
      validateToken(tokens[30], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 76, '"""')
    })

    test('Text triple quote multilines', function () {
      const tokens = getTokensManiaScript(`"""AAA
BBB"""`)
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AAA\nBBB')
      validateToken(tokens[2], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 10, '"""')
    })

    test('Text triple quote interpolation', function () {
      let tokens = getTokensManiaScript('"""AA{{ }}}BB"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AA')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 5, '{')
      validateToken(tokens[3], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 6, '{')
      validateToken(tokens[4], ManiaScriptLexer.PART_TEXT_TRIPLE, 7, ' }}}BB')
      validateToken(tokens[5], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 13, '"""')

      tokens = getTokensManiaScript('"""AA{"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AA')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 5, '{')
      validateToken(tokens[3], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 6, '"""')

      tokens = getTokensManiaScript('"""AA{{"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AA')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 5, '{')
      validateToken(tokens[3], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 6, '{')
      validateToken(tokens[4], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 7, '"""')

      tokens = getTokensManiaScript('"""""AA{{"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 3, '"')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 4, '"')
      validateToken(tokens[3], ManiaScriptLexer.PART_TEXT_TRIPLE, 5, 'AA')
      validateToken(tokens[4], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 7, '{')
      validateToken(tokens[5], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 8, '{')
      validateToken(tokens[6], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 9, '"""')

      tokens = getTokensManiaScript('"""{}AA{{"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 3, '{')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE, 4, '}AA')
      validateToken(tokens[3], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 7, '{')
      validateToken(tokens[4], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 8, '{')
      validateToken(tokens[5], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 9, '"""')

      tokens = getTokensManiaScript('"""AA{{""""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AA')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 5, '{')
      validateToken(tokens[3], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 6, '{')
      validateToken(tokens[4], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 7, '"""')
      validateToken(tokens[5], ManiaScriptLexer.OPERATOR_OPEN_TEXT_SINGLE, 10, '"')

      tokens = getTokensManiaScript('"""AA{{" """')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AA')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 5, '{')
      validateToken(tokens[3], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 6, '{')
      validateToken(tokens[4], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 7, '"')
      validateToken(tokens[5], ManiaScriptLexer.PART_TEXT_TRIPLE, 8, ' ')
      validateToken(tokens[6], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 9, '"""')

      tokens = getTokensManiaScript('"""AA{{"" """')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AA')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 5, '{')
      validateToken(tokens[3], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 6, '{')
      validateToken(tokens[4], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 7, '"')
      validateToken(tokens[5], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 8, '"')
      validateToken(tokens[6], ManiaScriptLexer.PART_TEXT_TRIPLE, 9, ' ')
      validateToken(tokens[7], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 10, '"""')

      tokens = getTokensManiaScript('"""AA{"{"{" "{{""{{" """')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AA')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 5, '{')
      validateToken(tokens[3], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 6, '"')
      validateToken(tokens[4], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 7, '{')
      validateToken(tokens[5], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 8, '"')
      validateToken(tokens[6], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 9, '{')
      validateToken(tokens[7], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 10, '"')
      validateToken(tokens[8], ManiaScriptLexer.PART_TEXT_TRIPLE, 11, ' ')
      validateToken(tokens[9], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 12, '"')
      validateToken(tokens[10], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 13, '{')
      validateToken(tokens[11], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 14, '{')
      validateToken(tokens[12], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 15, '"')
      validateToken(tokens[13], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 16, '"')
      validateToken(tokens[14], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 17, '{')
      validateToken(tokens[15], ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE, 18, '{')
      validateToken(tokens[16], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 19, '"')
      validateToken(tokens[17], ManiaScriptLexer.PART_TEXT_TRIPLE, 20, ' ')
      validateToken(tokens[18], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 21, '"""')

      tokens = getTokensManiaScript('"""AA{{{ }}}BB"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AA')
      validateToken(tokens[2], ManiaScriptLexer.OPERATOR_OPEN_INTERPOLATION, 5, '{{{')
      validateToken(tokens[3], ManiaScriptLexer.WHITESPACES, 8, ' ')
      validateToken(tokens[4], ManiaScriptLexer.OPERATOR_CLOSE_INTERPOLATION, 9, '}}}')
      validateToken(tokens[5], ManiaScriptLexer.PART_TEXT_TRIPLE, 12, 'BB')
      validateToken(tokens[6], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 14, '"""')

      tokens = getTokensManiaScript('"""AAA {{{BBB + CCC}}} DDD"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, 'AAA ')
      validateToken(tokens[2], ManiaScriptLexer.OPERATOR_OPEN_INTERPOLATION, 7, '{{{')
      validateToken(tokens[3], ManiaScriptLexer.IDENTIFIER, 10, 'BBB')
      validateToken(tokens[4], ManiaScriptLexer.WHITESPACES, 13, ' ')
      validateToken(tokens[5], ManiaScriptLexer.OPERATOR_PLUS, 14, '+')
      validateToken(tokens[6], ManiaScriptLexer.WHITESPACES, 15, ' ')
      validateToken(tokens[7], ManiaScriptLexer.IDENTIFIER, 16, 'CCC')
      validateToken(tokens[8], ManiaScriptLexer.OPERATOR_CLOSE_INTERPOLATION, 19, '}}}')
      validateToken(tokens[9], ManiaScriptLexer.PART_TEXT_TRIPLE, 22, ' DDD')
      validateToken(tokens[10], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 26, '"""')

      tokens = getTokensManiaScript('"""{{{AAA}}}"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.OPERATOR_OPEN_INTERPOLATION, 3, '{{{')
      validateToken(tokens[2], ManiaScriptLexer.IDENTIFIER, 6, 'AAA')
      validateToken(tokens[3], ManiaScriptLexer.OPERATOR_CLOSE_INTERPOLATION, 9, '}}}')
      validateToken(tokens[4], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 12, '"""')

      tokens = getTokensManiaScript('"""{{{AAA"""{{{BBB}}}"""CCC}}}"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.OPERATOR_OPEN_INTERPOLATION, 3, '{{{')
      validateToken(tokens[2], ManiaScriptLexer.IDENTIFIER, 6, 'AAA')
      validateToken(tokens[3], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 9, '"""')
      validateToken(tokens[4], ManiaScriptLexer.OPERATOR_OPEN_INTERPOLATION, 12, '{{{')
      validateToken(tokens[5], ManiaScriptLexer.IDENTIFIER, 15, 'BBB')
      validateToken(tokens[6], ManiaScriptLexer.OPERATOR_CLOSE_INTERPOLATION, 18, '}}}')
      validateToken(tokens[7], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 21, '"""')
      validateToken(tokens[8], ManiaScriptLexer.IDENTIFIER, 24, 'CCC')
      validateToken(tokens[9], ManiaScriptLexer.OPERATOR_CLOSE_INTERPOLATION, 27, '}}}')
      validateToken(tokens[10], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 30, '"""')

      tokens = getTokensManiaScript('"""<label text="{{{Value}}}" />"""')
      validateToken(tokens[0], ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE, 0, '"""')
      validateToken(tokens[1], ManiaScriptLexer.PART_TEXT_TRIPLE, 3, '<label text=')
      validateToken(tokens[2], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 15, '"')
      validateToken(tokens[3], ManiaScriptLexer.OPERATOR_OPEN_INTERPOLATION, 16, '{{{')
      validateToken(tokens[4], ManiaScriptLexer.IDENTIFIER, 19, 'Value')
      validateToken(tokens[5], ManiaScriptLexer.OPERATOR_CLOSE_INTERPOLATION, 24, '}}}')
      validateToken(tokens[6], ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE, 27, '"')
      validateToken(tokens[7], ManiaScriptLexer.PART_TEXT_TRIPLE, 28, ' />')
      validateToken(tokens[8], ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE, 31, '"""')
    })
  })

  describe('find directives', function () {
    test('#RequireContext', function () {
      const tokens = getTokensManiaScript('#RequireContext')
      validateToken(tokens[0], ManiaScriptLexer.DIRECTIVE_REQUIRE_CONTEXT, 0, '#RequireContext')
    })
    test('#Extends', function () {
      const tokens = getTokensManiaScript('#Extends')
      validateToken(tokens[0], ManiaScriptLexer.DIRECTIVE_EXTENDS, 0, '#Extends')
    })
    test('#Include', function () {
      const tokens = getTokensManiaScript('#Include')
      validateToken(tokens[0], ManiaScriptLexer.DIRECTIVE_INCLUDE, 0, '#Include')
    })
    test('#Setting', function () {
      const tokens = getTokensManiaScript('#Setting')
      validateToken(tokens[0], ManiaScriptLexer.DIRECTIVE_SETTING, 0, '#Setting')
    })
    test('#Command', function () {
      const tokens = getTokensManiaScript('#Command')
      validateToken(tokens[0], ManiaScriptLexer.DIRECTIVE_COMMAND, 0, '#Command')
    })
    test('#Struct', function () {
      const tokens = getTokensManiaScript('#Struct')
      validateToken(tokens[0], ManiaScriptLexer.DIRECTIVE_STRUCT, 0, '#Struct')
    })
    test('#Const', function () {
      const tokens = getTokensManiaScript('#Const')
      validateToken(tokens[0], ManiaScriptLexer.DIRECTIVE_CONST, 0, '#Const')
    })
  })

  describe('find others', function () {
    test('identifier', function () {
      let tokens = getTokensManiaScript('Identifier Name Test _Test Test0 0Test')
      validateToken(tokens[0], ManiaScriptLexer.IDENTIFIER, 0, 'Identifier')
      validateToken(tokens[2], ManiaScriptLexer.IDENTIFIER, 11, 'Name')
      validateToken(tokens[4], ManiaScriptLexer.IDENTIFIER, 16, 'Test')
      validateToken(tokens[6], ManiaScriptLexer.IDENTIFIER, 21, '_Test')
      validateToken(tokens[8], ManiaScriptLexer.IDENTIFIER, 27, 'Test0')
      validateToken(tokens[10], ManiaScriptLexer.LITERAL_INTEGER, 33, '0')
      validateToken(tokens[11], ManiaScriptLexer.IDENTIFIER, 34, 'Test')
      tokens = getTokensManiaScript('#Command Command_Test (Integer)')
      validateToken(tokens[0], ManiaScriptLexer.DIRECTIVE_COMMAND, 0, '#Command')
      validateToken(tokens[2], ManiaScriptLexer.IDENTIFIER, 9, 'Command_Test')
      validateToken(tokens[4], ManiaScriptLexer.OPERATOR_OPEN_PAREN, 22, '(')
      validateToken(tokens[5], ManiaScriptLexer.TYPE_INTEGER, 23, 'Integer')
      validateToken(tokens[6], ManiaScriptLexer.OPERATOR_CLOSE_PAREN, 30, ')')
    })
    test('classes', function () {
      let tokens = getTokensManiaScript('CNotAClass CSmMode Identifier')
      validateToken(tokens[0], ManiaScriptLexer.IDENTIFIER, 0, 'CNotAClass')
      validateToken(tokens[2], ManiaScriptLexer.CLASS, 11, 'CSmMode')
      validateToken(tokens[4], ManiaScriptLexer.IDENTIFIER, 19, 'Identifier')

      tokens = getTokensManiaScript('CMlScript CSmMapLandmark CUser')
      validateToken(tokens[0], ManiaScriptLexer.CLASS, 0, 'CMlScript')
      validateToken(tokens[2], ManiaScriptLexer.CLASS, 10, 'CSmMapLandmark')
      validateToken(tokens[4], ManiaScriptLexer.CLASS, 25, 'CUser')
    })
    test('white spaces', function () {
      const tokens = getTokensManiaScript(' aaa	bbb\tccc 	\t ddd')
      validateToken(tokens[0], ManiaScriptLexer.WHITESPACES, 0, ' ')
      validateToken(tokens[2], ManiaScriptLexer.WHITESPACES, 4, '	')
      validateToken(tokens[4], ManiaScriptLexer.WHITESPACES, 8, '\t')
      validateToken(tokens[6], ManiaScriptLexer.WHITESPACES, 12, ' 	\t ')
    })
    test('line endings', function () {
      const tokens = getTokensManiaScript('aaa\nbbb\rccc\r\nddd\n\neee')
      validateToken(tokens[1], ManiaScriptLexer.LINE_TERMINATOR, 3, '\n')
      validateToken(tokens[3], ManiaScriptLexer.LINE_TERMINATOR, 7, '\r')
      validateToken(tokens[5], ManiaScriptLexer.LINE_TERMINATOR, 11, '\r\n')
      validateToken(tokens[7], ManiaScriptLexer.LINE_TERMINATOR, 16, '\n')
      validateToken(tokens[8], ManiaScriptLexer.LINE_TERMINATOR, 17, '\n')
    })
    test('unknown', function () {
      const tokens = getTokensManiaScript('& é # @ 😛')
      validateToken(tokens[0], ManiaScriptLexer.UNKNOWN, 0, '&')
      validateToken(tokens[2], ManiaScriptLexer.UNKNOWN, 2, 'é')
      validateToken(tokens[4], ManiaScriptLexer.UNKNOWN, 4, '#')
      validateToken(tokens[6], ManiaScriptLexer.UNKNOWN, 6, '@')
      validateToken(tokens[8], ManiaScriptLexer.UNKNOWN, 8, '😛')
    })
  })

  test('read custom classes from Array', function () {
    const tokens = getTokensManiaScript('CNotAClass CSmMode CCustomClass', ['CCustomClass', 'CAnotherClass'])
    validateToken(tokens[0], ManiaScriptLexer.IDENTIFIER, 0, 'CNotAClass')
    validateToken(tokens[2], ManiaScriptLexer.IDENTIFIER, 11, 'CSmMode')
    validateToken(tokens[4], ManiaScriptLexer.CLASS, 19, 'CCustomClass')
  })

  test('read custom classes from Set', function () {
    const tokens = getTokensManiaScriptWithCustomSet('CNotAClass CSmMode CCustomClass', new Set(['CCustomClass', 'CAnotherClass']))
    validateToken(tokens[0], ManiaScriptLexer.IDENTIFIER, 0, 'CNotAClass')
    validateToken(tokens[2], ManiaScriptLexer.IDENTIFIER, 11, 'CSmMode')
    validateToken(tokens[4], ManiaScriptLexer.CLASS, 19, 'CCustomClass')
  })

  test('read custom classes from Maniascript API doc file', async function () {
    const tokens = await getTokensManiaScriptWithCustomApi('CNotAClass CSmMode CCustomClass', './test/doc.h')
    validateToken(tokens[0], ManiaScriptLexer.IDENTIFIER, 0, 'CNotAClass')
    validateToken(tokens[2], ManiaScriptLexer.IDENTIFIER, 11, 'CSmMode')
    validateToken(tokens[4], ManiaScriptLexer.CLASS, 19, 'CCustomClass')
  })

  test('read custom classes from Set or Array only', function () {
    // @ts-expect-error -- Check that the error message is correctly triggered
    expect(function () { getTokensManiaScriptWithCustomSet('CNotAClass CSmMode CCustomClass', 'Error') }).toThrow('classes parameter must be an Array or Set')
  })
})
