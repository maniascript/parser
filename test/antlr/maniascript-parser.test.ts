import { describe, test, expect } from 'vitest'
import { getParser, getParseTree } from '../tools'

describe('Parser', () => {
  describe('program', () => {
    test('parse empty file', () => {
      const parser = getParser('')
      expect(getParseTree(parser, parser.program())).toBe('(program programContent <EOF>)')
    })
  })

  describe('directives', () => {
    describe('directive', () => {
      test('parse a directive', () => {
        let script = '#RequireContext CSmMode'
        let tree = '(directive (requireContextDirective #RequireContext (className CSmMode)))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.directive())).toBe(tree)
        script = '#Const C_Test 0'
        tree = ''
        // const parser = getParser(script)
        // expect(getParseTree(parser, parser.directive())).toBe(tree)
      })
    })

    describe('requireContextDirective', () => {
      test('parse #RequireContext', () => {
        let script = '#RequireContext CSmMode'
        let tree = '(requireContextDirective #RequireContext (className CSmMode))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.requireContextDirective())).toBe(tree)
        script = '#RequireContext CManiaApp'
        tree = '(requireContextDirective #RequireContext (className CManiaApp))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.requireContextDirective())).toBe(tree)
      })
      test('fail on invalid class', () => {
        const script = '#RequireContext CFakeClass'
        const error = /ParserError \(1:16\): mismatched input 'CFakeClass' expecting CLASS/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.requireContextDirective())).toThrow(error)
      })
    })

    describe('extendsDirective', () => {
      test('parse #Extends', () => {
        const script = '#Extends "Path/To/File.Script.txt"'
        const tree = '(extendsDirective #Extends (extendsPath (textLiteral (textBaseLiteral (textSingleQuoteLiteral " Path/To/File.Script.txt ")))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.extendsDirective())).toBe(tree)
      })
    })

    describe('includeDirective', () => {
      test('parse #Include without alias', () => {
        const script = '#Include "A/B/C.Script.txt"'
        const tree = '(includeDirective #Include (includePath (textLiteral (textBaseLiteral (textSingleQuoteLiteral " A/B/C.Script.txt ")))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.includeDirective())).toBe(tree)
      })
      test('parse #Include with alias', () => {
        const script = '#Include "A/B/C.Script.txt" as D'
        const tree = '(includeDirective #Include (includePath (textLiteral (textBaseLiteral (textSingleQuoteLiteral " A/B/C.Script.txt ")))) (includeAlias as (namespaceDefinition D)))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.includeDirective())).toBe(tree)
      })
    })

    describe('settingDirective', () => {
      test('parse #Setting without alias', () => {
        let script = '#Setting S_A 1'
        let tree = '(settingDirective #Setting (settingName S_A) (literal 1))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.settingDirective())).toBe(tree)
        script = '#Setting S_A -1'
        tree = '(settingDirective #Setting (settingName S_A) (literal - 1))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.settingDirective())).toBe(tree)
        script = '#Setting S_B "C"'
        tree = '(settingDirective #Setting (settingName S_B) (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " C ")))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.settingDirective())).toBe(tree)
        script = '#Setting S_C <1, 2>'
        tree = '(settingDirective #Setting (settingName S_C) (vector < (expression (literal 1)) , (expression (literal 2)) >))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.settingDirective())).toBe(tree)
      })
      test('parse #Setting with alias', () => {
        let script = '#Setting S_D 1 as "E"'
        let tree = '(settingDirective #Setting (settingName S_D) (literal 1) (textAlias as (textLiteral (textBaseLiteral (textSingleQuoteLiteral " E ")))))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.settingDirective())).toBe(tree)
        script = '#Setting S_E "F" as _("G")'
        tree = '(settingDirective #Setting (settingName S_E) (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " F ")))) (textAlias as (textLiteral (textTranslatedLiteral _( (textBaseLiteral (textSingleQuoteLiteral " G ")) )))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.settingDirective())).toBe(tree)
      })
    })

    describe('commandDirective', () => {
      test('parse #Command without alias', () => {
        let script = '#Command Command_A (Integer)'
        let tree = '(commandDirective #Command (commandName Command_A) ( (type (baseType (simpleType Integer))) ))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.commandDirective())).toBe(tree)
        script = '#Command Command_B (Integer[])'
        tree = '(commandDirective #Command (commandName Command_B) ( (type (arrayType (baseType (simpleType Integer)) (arrayTypeDimension [ ]))) ))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.commandDirective())).toBe(tree)
      })
      test('parse #Command with alias', () => {
        let script = '#Command Command_C (Real) as "Test"'
        let tree = '(commandDirective #Command (commandName Command_C) ( (type (baseType (simpleType Real))) ) (textAlias as (textLiteral (textBaseLiteral (textSingleQuoteLiteral " Test ")))))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.commandDirective())).toBe(tree)
        script = '#Command Command_D (Vec3) as _("Test")'
        tree = '(commandDirective #Command (commandName Command_D) ( (type (baseType (simpleType Vec3))) ) (textAlias as (textLiteral (textTranslatedLiteral _( (textBaseLiteral (textSingleQuoteLiteral " Test ")) )))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.commandDirective())).toBe(tree)
      })
    })

    describe('structDirective', () => {
      test('parse #Struct declaration', () => {
        const script = `
        #Struct K_A {
          Integer B;
          Real[] C;
          S_D E;
        }
        `
        const tree = '(structDirective (structDeclaration #Struct K_A { (structMembers (structMember (type (baseType (simpleType Integer))) B ;) (structMember (type (arrayType (baseType (simpleType Real)) (arrayTypeDimension [ ]))) C ;) (structMember (type (baseType (customType (structName S_D)))) E ;)) }))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.structDirective())).toBe(tree)
      })
      test('parse empty #Struct declaration', () => {
        const script = '#Struct K_A {}'
        const tree = '(structDirective (structDeclaration #Struct K_A { }))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.structDirective())).toBe(tree)
      })
    })

    describe('structAliasing', () => {
      test('parse #Struct aliasing', () => {
        const script = '#Struct A::S_B as S_C'
        const tree = '(structAliasing #Struct (namespaceName A) :: (structName S_B) (identifierAlias as S_C))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.structAliasing())).toBe(tree)
      })
    })

    describe('constDirective', () => {
      test('parse #Const without an alias', () => {
        let script = '#Const C_A 1'
        let tree = '(constDirective (constDeclaration #Const C_A (literal 1)))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.constDirective())).toBe(tree)
        script = '#Const C_B ["C", "D"]'
        tree = '(constDirective (constDeclaration #Const C_B (array (simpleArray [ (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " C "))))) , (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " D "))))) ]))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.constDirective())).toBe(tree)
        script = '#Const C_E K_F { G = 1 }'
        tree = '(constDirective (constDeclaration #Const C_E (structure (structIdentifier (structName K_F)) { (structureMemberInitializationList (structureMemberInitialization (structMemberName G) = (expression (literal 1)))) })))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.constDirective())).toBe(tree)
        script = '#Const C_H <1, 2>'
        tree = '(constDirective (constDeclaration #Const C_H (vector < (expression (literal 1)) , (expression (literal 2)) >)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.constDirective())).toBe(tree)
        script = '#Const C_I -1'
        tree = '(constDirective (constDeclaration #Const C_I (literal - 1)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.constDirective())).toBe(tree)
      })
    })

    describe('constAliasing', () => {
      test('parse #Const aliasing', () => {
        const script = '#Const A::C_B as C_C'
        const tree = '(constAliasing #Const (namespaceName A) :: (constName C_B) (identifierAlias as C_C))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.constAliasing())).toBe(tree)
      })
    })
  })

  describe('declarations', () => {
    describe('functionDeclaration', () => {
      test('parse a function without parameters', () => {
        const script = 'Void A() {}'
        const tree = '(functionDeclaration (type (baseType (simpleType Void))) (functionName A) ( ) (block { }))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.functionDeclaration())).toBe(tree)
      })
      test('parse a function with parameters', () => {
        let script = 'Void B(Integer C) {}'
        let tree = '(functionDeclaration (type (baseType (simpleType Void))) (functionName B) ( (parameters (parameter (type (baseType (simpleType Integer))) C)) ) (block { }))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.functionDeclaration())).toBe(tree)
        script = 'Vec2 D(Vec3 E, Int2 F) {}'
        tree = '(functionDeclaration (type (baseType (simpleType Vec2))) (functionName D) ( (parameters (parameter (type (baseType (simpleType Vec3))) E) , (parameter (type (baseType (simpleType Int2))) F)) ) (block { }))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.functionDeclaration())).toBe(tree)
      })
    })
    describe('labelDeclaration', () => {
      test('parse a label declaration', () => {
        const script = '***A*** ***B;***'
        const tree = '(labelDeclaration *** (labelName A) *** *** (statements (statement (expressionStatement (expression (identifier (variableName B))) ;))) ***)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.labelDeclaration())).toBe(tree)
      })
      test('throw an error on empty label declaration', () => {
        const script = '***A*** *** ***'
        const error = /ParserError \(1:12\): mismatched input '\*\*\*' expecting {.*}/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.labelDeclaration())).toThrow(error)
      })
    })
  })

  describe('statements', () => {
    describe('mainStatement', () => {
      test('parse an empty main function', () => {
        const script = 'main() {}'
        const tree = '(mainStatement main ( ) (block { }))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.mainStatement())).toBe(tree)
      })
      test('parse a main function with a statement', () => {
        const script = 'main() { 1 + 1; }'
        const tree = '(mainStatement main ( ) (block { (statements (statement (expressionStatement (expression (expression (literal 1)) + (expression (literal 1))) ;))) }))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.mainStatement())).toBe(tree)
      })
    })
    describe('expressionStatement', () => {
      test('parse an expression statement', () => {
        let script = '1 + 1;'
        let tree = '(expressionStatement (expression (expression (literal 1)) + (expression (literal 1))) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expressionStatement())).toBe(tree)
        script = 'A;'
        tree = '(expressionStatement (expression (identifier (variableName A))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expressionStatement())).toBe(tree)
      })
    })
    describe('block', () => {
      test('parse an empty block statement', () => {
        const script = '{}'
        const tree = '(block { })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.block())).toBe(tree)
      })
      test('parse a block with a statement', () => {
        const script = '{ 1 + 1; }'
        const tree = '(block { (statements (statement (expressionStatement (expression (expression (literal 1)) + (expression (literal 1))) ;))) })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.block())).toBe(tree)
      })
      test('parse a block with multiple statements', () => {
        const script = '{ A; B; }'
        const tree = '(block { (statements (statement (expressionStatement (expression (identifier (variableName A))) ;)) (statement (expressionStatement (expression (identifier (variableName B))) ;))) })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.block())).toBe(tree)
      })
    })
    describe('variableDeclaration', () => {
      test('parse some basic variable declarations', () => {
        let script = 'declare Integer A;'
        let tree = '(variableDeclaration declare (type (baseType (simpleType Integer))) (variableName A) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare K_B C;'
        tree = '(variableDeclaration declare (type (baseType (customType (structName K_B)))) (variableName C) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare CSmMode D;'
        tree = '(variableDeclaration declare (type (baseType (className CSmMode))) (variableName D) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare Integer[] E;'
        tree = '(variableDeclaration declare (type (arrayType (baseType (simpleType Integer)) (arrayTypeDimension [ ]))) (variableName E) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
      })
      test('parse variable declarations with an initializer', () => {
        let script = 'declare Integer F = 1;'
        let tree = '(variableDeclaration declare (type (baseType (simpleType Integer))) (variableName F) (initializer = (expression (literal 1))) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare G = 1;'
        tree = '(variableDeclaration declare (variableName G) (initializer = (expression (literal 1))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare CSmMode H <=> This;'
        tree = '(variableDeclaration declare (type (baseType (className CSmMode))) (variableName H) (initializer <=> (expression This)) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare I <=> This;'
        tree = '(variableDeclaration declare (variableName I) (initializer <=> (expression This)) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare J = [1];'
        tree = '(variableDeclaration declare (variableName J) (initializer = (expression (array (simpleArray [ (expression (literal 1)) ])))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare K = [];'
        tree = '(variableDeclaration declare (variableName K) (initializer = (expression (array (emptyArray [ ])))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare L = K_M {};'
        tree = '(variableDeclaration declare (variableName L) (initializer = (expression (structure (structIdentifier (structName K_M)) { }))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
      })
      test('parse variable declarations with an deprecated type initializer', () => {
        let script = 'declare N = Int3;'
        let tree = '(variableDeclaration declare (variableName N) (initializer = (initializerType (simpleType Int3))) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare O <=> CSmMode;'
        tree = '(variableDeclaration declare (variableName O) (initializer <=> (initializerType (className CSmMode))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare P = Integer[];'
        tree = '(variableDeclaration declare (variableName P) (initializer = (initializerType (arrayType (baseType (simpleType Integer)) (arrayTypeDimension [ ])))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
      })
      test('parse variable declarations with an alias', () => {
        let script = 'declare Integer Q as R;'
        let tree = '(variableDeclaration declare (type (baseType (simpleType Integer))) (variableName Q) (identifierAlias as R) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare S as T = 1;'
        tree = '(variableDeclaration declare (variableName S) (identifierAlias as T) (initializer = (expression (literal 1))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
      })
      test('parse variable declarations for', () => {
        let script = 'declare Integer U for This;'
        let tree = '(variableDeclaration declare (type (baseType (simpleType Integer))) (variableName U) for (expression This) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare V for This = 1;'
        tree = '(variableDeclaration declare (variableName V) for (expression This) (initializer = (expression (literal 1))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare Integer W for This = 1;'
        tree = '(variableDeclaration declare (type (baseType (simpleType Integer))) (variableName W) for (expression This) (initializer = (expression (literal 1))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare Integer X as Y for This = 1;'
        tree = '(variableDeclaration declare (type (baseType (simpleType Integer))) (variableName X) (identifierAlias as Y) for (expression This) (initializer = (expression (literal 1))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare Integer Z for AA.BB(CC[0]);'
        tree = '(variableDeclaration declare (type (baseType (simpleType Integer))) (variableName Z) for (expression (expression (identifier (variableName AA))) . (functionCall (functionName BB) ( (arguments (argument (expression (expression (identifier (variableName CC))) [ (expression (literal 0)) ]))) ))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
      })
      test('parse variable declarations with a storage specifier', () => {
        let script = 'declare netwrite DD = 1;'
        let tree = '(variableDeclaration declare (storageSpecifier netwrite) (variableName DD) (initializer = (expression (literal 1))) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare netread Integer EE;'
        tree = '(variableDeclaration declare (storageSpecifier netread) (type (baseType (simpleType Integer))) (variableName EE) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare cloud Integer FF = 1;'
        tree = '(variableDeclaration declare (storageSpecifier cloud) (type (baseType (simpleType Integer))) (variableName FF) (initializer = (expression (literal 1))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare metadata Integer GG for HH;'
        tree = '(variableDeclaration declare (storageSpecifier metadata) (type (baseType (simpleType Integer))) (variableName GG) for (expression (identifier (variableName HH))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'declare persistent Integer II for JJ = 1;'
        tree = '(variableDeclaration declare (storageSpecifier persistent) (type (baseType (simpleType Integer))) (variableName II) for (expression (identifier (variableName JJ))) (initializer = (expression (literal 1))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
      })
      test('parse variable declarations with let keyword', () => {
        let script = 'let Test1 = 10;'
        let tree = '(variableDeclaration let Test1 = (expression (literal 10)) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'let Test2 = "OK";'
        tree = '(variableDeclaration let Test2 = (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " OK "))))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
        script = 'let Test3 = Integer[Real];'
        tree = '(variableDeclaration let Test3 = (arrayType (baseType (simpleType Integer)) (arrayTypeDimension [ (baseType (simpleType Real)) ])) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.variableDeclaration())).toBe(tree)
      })
      test('faile to parse invalid variable declarations', () => {
        let script = 'declare netwrite = 1;'
        let error = /ParserError \(1:17\): no viable alternative at input 'declare netwrite ='/
        let parser = getParser(script)
        expect(() => getParseTree(parser, parser.variableDeclaration())).toThrow(error)
        script = 'declare netwrite netwrite = 1;'
        error = /ParserError \(1:17\): no viable alternative at input 'declare netwrite netwrite'/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.variableDeclaration())).toThrow(error)
        script = 'declare netwrite netwrite KK = 1;'
        error = /ParserError \(1:17\): no viable alternative at input 'declare netwrite netwrite'/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.variableDeclaration())).toThrow(error)
        script = 'let Integer LL = 1;'
        error = /ParserError \(1:4\): extraneous input 'Integer' expecting IDENTIFIER/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.variableDeclaration())).toThrow(error)
        script = 'let MM = Integer;'
        error = /ParserError \(1:16\): mismatched input ';' expecting '\['/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.variableDeclaration())).toThrow(error)
      })
    })
    describe('assignmentStatement', () => {
      test('assign an expression', () => {
        let script = 'A = 1;'
        let tree = '(assignmentStatement (expression (identifier (variableName A))) (assignmentOperator =) (expression (literal 1)) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
        script = 'B *= 2;'
        tree = '(assignmentStatement (expression (identifier (variableName B))) (assignmentOperator *=) (expression (literal 2)) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
        script = 'C /= 3;'
        tree = '(assignmentStatement (expression (identifier (variableName C))) (assignmentOperator /=) (expression (literal 3)) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
        script = 'D += 4;'
        tree = '(assignmentStatement (expression (identifier (variableName D))) (assignmentOperator +=) (expression (literal 4)) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
        script = 'E -= 5;'
        tree = '(assignmentStatement (expression (identifier (variableName E))) (assignmentOperator -=) (expression (literal 5)) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
        script = 'F %= 6;'
        tree = '(assignmentStatement (expression (identifier (variableName F))) (assignmentOperator %=) (expression (literal 6)) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
        script = 'G <=> 7;'
        tree = '(assignmentStatement (expression (identifier (variableName G))) (assignmentOperator <=>) (expression (literal 7)) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
        script = 'H ^= "8";'
        tree = '(assignmentStatement (expression (identifier (variableName H))) (assignmentOperator ^=) (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " 8 "))))) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
      })
      test('assign an initializer type', () => {
        let script = 'I <=> Integer[];'
        let tree = '(assignmentStatement (expression (identifier (variableName I))) <=> (initializerType (arrayType (baseType (simpleType Integer)) (arrayTypeDimension [ ]))) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
        script = 'J = CSmMode;'
        tree = '(assignmentStatement (expression (identifier (variableName J))) = (initializerType (className CSmMode)) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.assignmentStatement())).toBe(tree)
        script = 'K *= Real;'
        const error = /ParserError \(1:5\): mismatched input 'Real' expecting {.*}/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.assignmentStatement())).toThrow(error)
      })
    })
    describe('returnStatement', () => {
      test('return nothing', () => {
        const script = 'return;'
        const tree = '(returnStatement return ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.returnStatement())).toBe(tree)
      })
      test('return an expression', () => {
        const script = 'return 1;'
        const tree = '(returnStatement return (expression (literal 1)) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.returnStatement())).toBe(tree)
      })
      test('return an initializer type', () => {
        const script = 'return Integer;'
        const tree = '(returnStatement return (initializerType (simpleType Integer)) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.returnStatement())).toBe(tree)
      })
    })
    describe('labelStatement', () => {
      test('parse insert label', () => {
        const script = '+++A+++'
        const tree = '(labelStatement (labelInsert +++ A +++))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.labelStatement())).toBe(tree)
      })
      test('parse overwrite label', () => {
        const script = '---B---'
        const tree = '(labelStatement (labelOverwrite --- B ---))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.labelStatement())).toBe(tree)
      })
    })
    describe('assertStatement', () => {
      test('parse assert statement without comment', () => {
        const script = 'assert(A);'
        const tree = '(assertStatement assert ( (expression (identifier (variableName A))) ) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.assertStatement())).toBe(tree)
      })
      test('parse assert statement with a comment', () => {
        const script = 'assert(B, "C");'
        const tree = '(assertStatement assert ( (expression (identifier (variableName B))) , (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " C "))))) ) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.assertStatement())).toBe(tree)
      })
      test('throw an error when using incorrect number of arguments', () => {
        let script = 'assert();'
        let error = /ParserError \(1:7\): mismatched input '\)' expecting {.*}/
        let parser = getParser(script)
        expect(() => getParseTree(parser, parser.assertStatement())).toThrow(error)
        script = 'assert(D, E, F);'
        error = /ParserError \(1:11\): mismatched input ',' expecting '\)'/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.assertStatement())).toThrow(error)
      })
    })
    describe('foreachStatement', () => {
      test('parse basic foreach', () => {
        let script = 'foreach (A, B) {}'
        let tree = '(foreachStatement foreach ( A , (expression (identifier (variableName B))) ) (statement (block { })))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
        script = 'foreach (C in D) {}'
        tree = '(foreachStatement foreach ( C in (expression (identifier (variableName D))) ) (statement (block { })))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
      })
      test('parse foreach with a key', () => {
        let script = 'foreach (E => F, G) {}'
        let tree = '(foreachStatement foreach ( E => F , (expression (identifier (variableName G))) ) (statement (block { })))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
        script = 'foreach (H => I in J) {}'
        tree = '(foreachStatement foreach ( H => I in (expression (identifier (variableName J))) ) (statement (block { })))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
      })
      test('parse foreach with reverse', () => {
        let script = 'foreach (A, B reverse) {}'
        let tree = '(foreachStatement foreach ( A , (expression (identifier (variableName B))) reverse ) (statement (block { })))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
        script = 'foreach (H => I in J reverse) {}'
        tree = '(foreachStatement foreach ( H => I in (expression (identifier (variableName J))) reverse ) (statement (block { })))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
      })
      test('parse for as an alias of basic foreach', () => {
        let script = 'for (A, B) {}'
        const error = /ParserError \(1:6\): no viable alternative at input 'A,'/
        let parser = getParser(script)
        expect(() => getParseTree(parser, parser.foreachStatement())).toThrow(error)
        script = 'for (C in D) {}'
        const tree = '(foreachStatement for ( C in (expression (identifier (variableName D))) ) (statement (block { })))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
      })
      test('parse for as an alias of foreach with a key', () => {
        let script = 'for (E => F, G) {}'
        const error = /ParserError \(1:11\): mismatched input ',' expecting 'in'/
        let parser = getParser(script)
        expect(() => getParseTree(parser, parser.foreachStatement())).toThrow(error)
        script = 'for (H => I in J) {}'
        const tree = '(foreachStatement for ( H => I in (expression (identifier (variableName J))) ) (statement (block { })))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
      })
      test('parse for as an alias of foreach with reverse', () => {
        let script = 'for (C in reverse D) {}'
        let tree = '(foreachStatement for ( C in reverse (expression (identifier (variableName D))) ) (statement (block { })))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
        script = 'for (H => I in reverse J) {}'
        tree = '(foreachStatement for ( H => I in reverse (expression (identifier (variableName J))) ) (statement (block { })))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.foreachStatement())).toBe(tree)
      })
    })
    describe('forStatement', () => {
      test('parse for', () => {
        let script = 'for (A, 1, 2) {}'
        let tree = '(forStatement for ( A , (expression (literal 1)) , (expression (literal 2)) ) (statement (block { })))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.forStatement())).toBe(tree)
        script = 'for (B, 3, 4, 5) {}'
        tree = '(forStatement for ( B , (expression (literal 3)) , (expression (literal 4)) , (expression (literal 5)) ) (statement (block { })))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.forStatement())).toBe(tree)
      })
    })
    describe('whileStatement', () => {
      test('parse while', () => {
        const script = 'while (A) {}'
        const tree = '(whileStatement while ( (expression (identifier (variableName A))) ) (statement (block { })))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.whileStatement())).toBe(tree)
      })
    })
    describe('meanwhileStatement', () => {
      test('parse meanwhile', () => {
        const script = 'meanwhile (A) {}'
        const tree = '(meanwhileStatement meanwhile ( (expression (identifier (variableName A))) ) (statement (block { })))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.meanwhileStatement())).toBe(tree)
      })
    })
    describe('breakStatement', () => {
      test('parse break', () => {
        const script = 'break;'
        const tree = '(breakStatement break ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.breakStatement())).toBe(tree)
      })
    })
    describe('continueStatement', () => {
      test('parse continue', () => {
        const script = 'continue;'
        const tree = '(continueStatement continue ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.continueStatement())).toBe(tree)
      })
    })
    describe('switchStatement', () => {
      test('parse switch with cases', () => {
        let script = 'switch (A) { case B: C; }'
        let tree = '(switchStatement switch ( (expression (identifier (variableName A))) ) { (switchClauseList (switchCase case (expression (identifier (variableName B))) : (statement (expressionStatement (expression (identifier (variableName C))) ;)))) })'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.switchStatement())).toBe(tree)
        script = 'switch (D) { case E: { F; } case G: { H; } };'
        tree = '(switchStatement switch ( (expression (identifier (variableName D))) ) { (switchClauseList (switchCase case (expression (identifier (variableName E))) : (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName F))) ;))) }))) (switchCase case (expression (identifier (variableName G))) : (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName H))) ;))) })))) })'
        parser = getParser(script)
        expect(getParseTree(parser, parser.switchStatement())).toBe(tree)
      })
      test('parse switch with defaults', () => {
        let script = 'switch (I) { default: J; }'
        let tree = '(switchStatement switch ( (expression (identifier (variableName I))) ) { (switchClauseList (switchDefault default : (statement (expressionStatement (expression (identifier (variableName J))) ;)))) })'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.switchStatement())).toBe(tree)
        script = 'switch (K) { default: { L; } default: { M; } }'
        tree = '(switchStatement switch ( (expression (identifier (variableName K))) ) { (switchClauseList (switchDefault default : (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName L))) ;))) }))) (switchDefault default : (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName M))) ;))) })))) })'
        parser = getParser(script)
        expect(getParseTree(parser, parser.switchStatement())).toBe(tree)
      })
      test('parse switch with cases and defaults', () => {
        let script = 'switch (N) { case O: P; default: Q; }'
        let tree = '(switchStatement switch ( (expression (identifier (variableName N))) ) { (switchClauseList (switchCase case (expression (identifier (variableName O))) : (statement (expressionStatement (expression (identifier (variableName P))) ;))) (switchDefault default : (statement (expressionStatement (expression (identifier (variableName Q))) ;)))) })'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.switchStatement())).toBe(tree)
        script = 'switch (R) { case S: T; case U: V; default: W; default: X; };'
        tree = '(switchStatement switch ( (expression (identifier (variableName R))) ) { (switchClauseList (switchCase case (expression (identifier (variableName S))) : (statement (expressionStatement (expression (identifier (variableName T))) ;))) (switchCase case (expression (identifier (variableName U))) : (statement (expressionStatement (expression (identifier (variableName V))) ;))) (switchDefault default : (statement (expressionStatement (expression (identifier (variableName W))) ;))) (switchDefault default : (statement (expressionStatement (expression (identifier (variableName X))) ;)))) })'
        parser = getParser(script)
        expect(getParseTree(parser, parser.switchStatement())).toBe(tree)
      })
      test('parse cases with multiple expressions', () => {
        const script = 'switch (A) { case B,C, D: E; }'
        const tree = '(switchStatement switch ( (expression (identifier (variableName A))) ) { (switchClauseList (switchCase case (expression (identifier (variableName B))) , (expression (identifier (variableName C))) , (expression (identifier (variableName D))) : (statement (expressionStatement (expression (identifier (variableName E))) ;)))) })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.switchStatement())).toBe(tree)
      })
      test('throw an error if default before case', () => {
        const script = 'switch (Y) { default: Z; case AA: BB; }'
        const error = /ParserError \(1:25\): mismatched input 'case' expecting '}'/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.switchStatement())).toThrow(error)
      })
      test('throw an error if empty switch', () => {
        const script = 'switch (Y) {}'
        const error = /ParserError \(1:12\): mismatched input '}' expecting {'case', 'default'}/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.switchStatement())).toThrow(error)
      })
    })
    describe('switchtypeStatement', () => {
      test('parse switchtype with cases', () => {
        let script = 'switchtype (A) { case CSmMode: B; }'
        let tree = '(switchtypeStatement switchtype ( (expression (identifier (variableName A))) ) { (switchtypeClauseList (switchtypeCase case (className CSmMode) : (statement (expressionStatement (expression (identifier (variableName B))) ;)))) })'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.switchtypeStatement())).toBe(tree)
        script = 'switchtype (C) { case CSmMode: { D; } case CSmPlayer: { E; } };'
        tree = '(switchtypeStatement switchtype ( (expression (identifier (variableName C))) ) { (switchtypeClauseList (switchtypeCase case (className CSmMode) : (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName D))) ;))) }))) (switchtypeCase case (className CSmPlayer) : (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName E))) ;))) })))) })'
        parser = getParser(script)
        expect(getParseTree(parser, parser.switchtypeStatement())).toBe(tree)
      })
      test('parse switchtype with defaults', () => {
        let script = 'switchtype (F) { default: G; }'
        let tree = '(switchtypeStatement switchtype ( (expression (identifier (variableName F))) ) { (switchtypeClauseList (switchtypeDefault default : (statement (expressionStatement (expression (identifier (variableName G))) ;)))) })'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.switchtypeStatement())).toBe(tree)
        script = 'switchtype (H) { default: { I; } default: { J; } };'
        tree = '(switchtypeStatement switchtype ( (expression (identifier (variableName H))) ) { (switchtypeClauseList (switchtypeDefault default : (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName I))) ;))) }))) (switchtypeDefault default : (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName J))) ;))) })))) })'
        parser = getParser(script)
        expect(getParseTree(parser, parser.switchtypeStatement())).toBe(tree)
      })
      test('parse switchtype with cases and defaults', () => {
        let script = 'switchtype (K) { case CSmMode: L; default: M; }'
        let tree = '(switchtypeStatement switchtype ( (expression (identifier (variableName K))) ) { (switchtypeClauseList (switchtypeCase case (className CSmMode) : (statement (expressionStatement (expression (identifier (variableName L))) ;))) (switchtypeDefault default : (statement (expressionStatement (expression (identifier (variableName M))) ;)))) })'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.switchtypeStatement())).toBe(tree)
        script = 'switchtype (N) { case CSmMode: O; case CSmPlayer: P; default: Q; default: R; };'
        tree = '(switchtypeStatement switchtype ( (expression (identifier (variableName N))) ) { (switchtypeClauseList (switchtypeCase case (className CSmMode) : (statement (expressionStatement (expression (identifier (variableName O))) ;))) (switchtypeCase case (className CSmPlayer) : (statement (expressionStatement (expression (identifier (variableName P))) ;))) (switchtypeDefault default : (statement (expressionStatement (expression (identifier (variableName Q))) ;))) (switchtypeDefault default : (statement (expressionStatement (expression (identifier (variableName R))) ;)))) })'
        parser = getParser(script)
        expect(getParseTree(parser, parser.switchtypeStatement())).toBe(tree)
      })
      test('parse cases with multiple classes', () => {
        const script = 'switchtype (A) { case CSmMode,CSmPlayer, CMode: B; }'
        const tree = '(switchtypeStatement switchtype ( (expression (identifier (variableName A))) ) { (switchtypeClauseList (switchtypeCase case (className CSmMode) , (className CSmPlayer) , (className CMode) : (statement (expressionStatement (expression (identifier (variableName B))) ;)))) })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.switchtypeStatement())).toBe(tree)
      })
      test('parse switchtype with alias', () => {
        const script = 'switchtype (A as B) { case CSmMode: B; }'
        const tree = '(switchtypeStatement switchtype ( (expression (identifier (variableName A))) (identifierAlias as B) ) { (switchtypeClauseList (switchtypeCase case (className CSmMode) : (statement (expressionStatement (expression (identifier (variableName B))) ;)))) })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.switchtypeStatement())).toBe(tree)
      })
      test('parse switchtype with cast', () => {
        const script = 'switchtype (A as CSmMode) { case CSmMode: A; }'
        const tree = '(switchtypeStatement switchtype ( (expression (expression (identifier (variableName A))) as (className CSmMode)) ) { (switchtypeClauseList (switchtypeCase case (className CSmMode) : (statement (expressionStatement (expression (identifier (variableName A))) ;)))) })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.switchtypeStatement())).toBe(tree)
      })
      test('throw an error if default before case', () => {
        const script = 'switchtype (S) { default: T; case CSmMode: U; }'
        const error = /ParserError \(1:29\): mismatched input 'case' expecting '}'/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.switchtypeStatement())).toThrow(error)
      })
      test('throw an error if empty switch', () => {
        const script = 'switchtype (V) {}'
        const error = /ParserError \(1:16\): mismatched input '}' expecting {'case', 'default'}/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.switchtypeStatement())).toThrow(error)
      })
    })
    describe('conditionalStatement', () => {
      test('parse if', () => {
        let script = 'if (A) B;'
        let tree = '(conditionalStatement (ifBranch if ( (expression (identifier (variableName A))) ) (statement (expressionStatement (expression (identifier (variableName B))) ;))))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.conditionalStatement())).toBe(tree)
        script = 'if (C) { D; }'
        tree = '(conditionalStatement (ifBranch if ( (expression (identifier (variableName C))) ) (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName D))) ;))) }))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.conditionalStatement())).toBe(tree)
      })
      test('parse if / else', () => {
        let script = 'if (E) F; else G;'
        let tree = '(conditionalStatement (ifBranch if ( (expression (identifier (variableName E))) ) (statement (expressionStatement (expression (identifier (variableName F))) ;))) (elseBranch else (statement (expressionStatement (expression (identifier (variableName G))) ;))))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.conditionalStatement())).toBe(tree)
        script = 'if (H) { I; } else { J; }'
        tree = '(conditionalStatement (ifBranch if ( (expression (identifier (variableName H))) ) (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName I))) ;))) }))) (elseBranch else (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName J))) ;))) }))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.conditionalStatement())).toBe(tree)
      })
      test('parse if / else if / else', () => {
        let script = 'if (K) L; else if (M) N; else O;'
        let tree = '(conditionalStatement (ifBranch if ( (expression (identifier (variableName K))) ) (statement (expressionStatement (expression (identifier (variableName L))) ;))) (elseIfBranch else if ( (expression (identifier (variableName M))) ) (statement (expressionStatement (expression (identifier (variableName N))) ;))) (elseBranch else (statement (expressionStatement (expression (identifier (variableName O))) ;))))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.conditionalStatement())).toBe(tree)
        script = 'if (P) { Q; } else if (R) { S; } else if (T) { U; } else { V; }'
        tree = '(conditionalStatement (ifBranch if ( (expression (identifier (variableName P))) ) (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName Q))) ;))) }))) (elseIfBranch else if ( (expression (identifier (variableName R))) ) (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName S))) ;))) }))) (elseIfBranch else if ( (expression (identifier (variableName T))) ) (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName U))) ;))) }))) (elseBranch else (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName V))) ;))) }))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.conditionalStatement())).toBe(tree)
      })
      test('parse if / else if', () => {
        let script = 'if (W) X; else if (Y) Z;'
        let tree = '(conditionalStatement (ifBranch if ( (expression (identifier (variableName W))) ) (statement (expressionStatement (expression (identifier (variableName X))) ;))) (elseIfBranch else if ( (expression (identifier (variableName Y))) ) (statement (expressionStatement (expression (identifier (variableName Z))) ;))))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.conditionalStatement())).toBe(tree)
        script = 'if (AA) { BB; } else if (CC) { DD; } else if (EE) { FF; }'
        tree = '(conditionalStatement (ifBranch if ( (expression (identifier (variableName AA))) ) (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName BB))) ;))) }))) (elseIfBranch else if ( (expression (identifier (variableName CC))) ) (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName DD))) ;))) }))) (elseIfBranch else if ( (expression (identifier (variableName EE))) ) (statement (block { (statements (statement (expressionStatement (expression (identifier (variableName FF))) ;))) }))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.conditionalStatement())).toBe(tree)
      })
      test('throw an error if else or else if before if', () => {
        let script = 'else {} if (GG) {} '
        let error = /ParserError \(1:0\): mismatched input 'else' expecting 'if'/
        let parser = getParser(script)
        expect(() => getParseTree(parser, parser.conditionalStatement())).toThrow(error)
        script = 'else if (HH) {} if (II) {} '
        error = /ParserError \(1:0\): extraneous input 'else' expecting 'if'/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.conditionalStatement())).toThrow(error)
      })
    })
    describe('logStatement', () => {
      test('parse log()', () => {
        const script = 'log(A);'
        const tree = '(logStatement log ( (expression (identifier (variableName A))) ) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.logStatement())).toBe(tree)
      })
      test('throw an error on empty log()', () => {
        const script = 'log();'
        const error = /ParserError \(1:4\): mismatched input '\)' expecting {.*}/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.logStatement())).toThrow(error)
      })
    })
    describe('sleepStatement', () => {
      test('parse sleep()', () => {
        const script = 'sleep(A);'
        const tree = '(sleepStatement sleep ( (expression (identifier (variableName A))) ) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.sleepStatement())).toBe(tree)
      })
      test('throw an error on empty sleep()', () => {
        const script = 'sleep();'
        const error = /ParserError \(1:6\): mismatched input '\)' expecting {.*}/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.sleepStatement())).toThrow(error)
      })
    })
    describe('tuningstartStatement', () => {
      test('parse tuningstart()', () => {
        const script = 'tuningstart();'
        const tree = '(tuningstartStatement tuningstart ( ) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.tuningstartStatement())).toBe(tree)
      })
      test('throw an error when giving an argument to tuningstart()', () => {
        const script = 'tuningstart(A);'
        const error = /ParserError \(1:12\): extraneous input 'A' expecting '\)'/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.tuningstartStatement())).toThrow(error)
      })
    })
    describe('tuningendStatement', () => {
      test('parse tuningend()', () => {
        const script = 'tuningend();'
        const tree = '(tuningendStatement tuningend ( ) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.tuningendStatement())).toBe(tree)
      })
      test('throw an error when giving an argument to tuningend()', () => {
        const script = 'tuningend(A);'
        const error = /ParserError \(1:10\): extraneous input 'A' expecting '\)'/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.tuningendStatement())).toThrow(error)
      })
    })
    describe('tuningmarkStatement', () => {
      test('parse tuningmark()', () => {
        let script = 'tuningmark("A");'
        let tree = '(tuningmarkStatement tuningmark ( (textLiteral (textBaseLiteral (textSingleQuoteLiteral " A "))) ) ;)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.tuningmarkStatement())).toBe(tree)
        script = 'tuningmark("""B""");'
        tree = '(tuningmarkStatement tuningmark ( (textLiteral (textBaseLiteral (textTripleQuoteLiteral """ B """))) ) ;)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.tuningmarkStatement())).toBe(tree)
      })
      test('throw an error when the argument is not a Text', () => {
        const script = 'tuningmark(A);'
        const error = /ParserError \(1:11\): mismatched input 'A' expecting {.*}/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.tuningmarkStatement())).toThrow(error)
      })
    })
    describe('waitStatement', () => {
      test('parse wait()', () => {
        const script = 'wait(A);'
        const tree = '(waitStatement wait ( (expression (identifier (variableName A))) ) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.waitStatement())).toBe(tree)
      })
      test('throw an error on empty wait()', () => {
        const script = 'wait();'
        const error = /ParserError \(1:5\): mismatched input '\)' expecting {.*}/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.waitStatement())).toThrow(error)
      })
    })
    describe('yieldStatement', () => {
      test('parse yield', () => {
        const script = 'yield;'
        const tree = '(yieldStatement yield ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.yieldStatement())).toBe(tree)
      })
      test('parse yield(x)', () => {
        const script = 'yield(10);'
        const tree = '(yieldStatement yield ( 10 ) ;)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.yieldStatement())).toBe(tree)
      })
      test('yield() must be called with an Integer as parameter', () => {
        const script = 'yield(VarError);'
        const error = /ParserError \(1:6\): mismatched input 'VarError' expecting LITERAL_INTEGER/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.yieldStatement())).toThrow(error)
      })
    })
  })

  describe('expressions', () => {
    describe('functionCall', () => {
      test('parse a function call without arguments', () => {
        const script = 'Test()'
        const tree = '(functionCall (functionName Test) ( ))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.functionCall())).toBe(tree)
      })
      test('parse a function call with one argument', () => {
        const script = 'Test(1)'
        const tree = '(functionCall (functionName Test) ( (arguments (argument (expression (literal 1)))) ))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.functionCall())).toBe(tree)
      })
      test('parse a function call with arguments', () => {
        const script = 'Test(1, 2)'
        const tree = '(functionCall (functionName Test) ( (arguments (argument (expression (literal 1))) , (argument (expression (literal 2)))) ))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.functionCall())).toBe(tree)
      })
      test('parse a function call in a namespace', () => {
        const script = 'A::B()'
        const tree = '(expression (namespaceName A) :: (functionCall (functionName B) ( )))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse a function call with types', () => {
        const script = 'Test(Integer, 0, Real[], [1.])'
        const tree = '(functionCall (functionName Test) ( (arguments (argument (initializerType (simpleType Integer))) , (argument (expression (literal 0))) , (argument (initializerType (arrayType (baseType (simpleType Real)) (arrayTypeDimension [ ])))) , (argument (expression (array (simpleArray [ (expression (literal 1.)) ]))))) ))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.functionCall())).toBe(tree)
      })
    })

    describe('asExpression', () => {
      test('parse a type cast', () => {
        const script = 'Test as CSmMode'
        const tree = '(expression (expression (identifier (variableName Test))) as (className CSmMode))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse multiple levels type cast', () => {
        const script = 'Test as CMapEditorPlugin as CManiaApp'
        const tree = '(expression (expression (expression (identifier (variableName Test))) as (className CMapEditorPlugin)) as (className CManiaApp))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse a type cast with the correct precedence', () => {
        const script = 'Test as CMapEditorPlugin.Map'
        const tree = '(expression (expression (expression (identifier (variableName Test))) as (className CMapEditorPlugin)) . (objectMemberName Map))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('type must be a class', () => {
        const script = 'Test as CFakeClass;'
        const error = /ParserError \(1:8\): no viable alternative at input 'Test as CFakeClass'/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.statement())).toThrow(error)
      })
    })

    describe('castExpression', () => {
      test('parse a type cast', () => {
        const script = 'cast(CSmMode, Test)'
        const tree = '(expression cast ( (className CSmMode) , (expression (identifier (variableName Test))) ))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('type must be a class', () => {
        const script = 'cast(CFakeClass, Test)'
        const error = /ParserError \(1:5\): mismatched input 'CFakeClass' expecting CLASS/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.expression())).toThrow(error)
      })
    })

    describe('equalityExpression', () => {
      test('parse equality expression', () => {
        const script = 'A == B'
        const tree = '(expression (expression (identifier (variableName A))) == (expression (identifier (variableName B))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse inequality expression', () => {
        const script = 'A != B'
        const tree = '(expression (expression (identifier (variableName A))) != (expression (identifier (variableName B))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('orExpression', () => {
      test('parse one or', () => {
        const script = 'A || B'
        const tree = '(expression (expression (identifier (variableName A))) || (expression (identifier (variableName B))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse multiple or', () => {
        const script = 'A || B || C || D'
        const tree = '(expression (expression (expression (expression (identifier (variableName A))) || (expression (identifier (variableName B)))) || (expression (identifier (variableName C)))) || (expression (identifier (variableName D))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse with the correct precedence', () => {
        const script = 'A || B && C || D'
        const tree = '(expression (expression (expression (identifier (variableName A))) || (expression (expression (identifier (variableName B))) && (expression (identifier (variableName C))))) || (expression (identifier (variableName D))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('andExpression', () => {
      test('parse one and', () => {
        const script = 'A && B'
        const tree = '(expression (expression (identifier (variableName A))) && (expression (identifier (variableName B))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse multiple and', () => {
        const script = 'A && B && C && D'
        const tree = '(expression (expression (expression (expression (identifier (variableName A))) && (expression (identifier (variableName B)))) && (expression (identifier (variableName C)))) && (expression (identifier (variableName D))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse with the correct precedence', () => {
        const script = 'A && B || C && D'
        const tree = '(expression (expression (expression (identifier (variableName A))) && (expression (identifier (variableName B)))) || (expression (expression (identifier (variableName C))) && (expression (identifier (variableName D)))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('isExpression', () => {
      test('parse a type check', () => {
        const script = 'Test is CSmMode'
        const tree = '(expression (expression (identifier (variableName Test))) is (className CSmMode))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse multiple levels type check', () => {
        const script = 'Test is CMapEditorPlugin is CManiaApp'
        const tree = '(expression (expression (expression (identifier (variableName Test))) is (className CMapEditorPlugin)) is (className CManiaApp))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse a type check with the correct precedence', () => {
        const script = 'Test is CMapEditorPlugin.Map'
        const error = /ParserError \(1:24\): rule expression (TestisCMapEditorPlugin|undefined) is not an object/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.expression())).toThrow(error)
      })
      test('type must be a class', () => {
        const script = 'Test is CFakeClass'
        const error = /ParserError \(1:8\): mismatched input 'CFakeClass' expecting CLASS/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.expression())).toThrow(error)
      })
    })

    describe('unaryMinusExpression', () => {
      test('parse one unary minus', () => {
        const script = '-1'
        const tree = '(expression - (expression (literal 1)))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse multiple unary minus', () => {
        const script = '--1'
        const tree = '(expression - (expression - (expression (literal 1))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('notExpression', () => {
      test('parse one not', () => {
        const script = '!A'
        const tree = '(expression ! (expression (identifier (variableName A))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse multiple not', () => {
        const script = '!!A'
        const tree = '(expression ! (expression ! (expression (identifier (variableName A)))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('multiplicativeExpression', () => {
      test('parse a simple multiplication', () => {
        let script = '1 * 2'
        let tree = '(expression (expression (literal 1)) * (expression (literal 2)))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'A * B.C'
        tree = '(expression (expression (identifier (variableName A))) * (expression (expression (identifier (variableName B))) . (objectMemberName C)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse a simple division', () => {
        let script = '1 / 2'
        let tree = '(expression (expression (literal 1)) / (expression (literal 2)))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'A / B.C'
        tree = '(expression (expression (identifier (variableName A))) / (expression (expression (identifier (variableName B))) . (objectMemberName C)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse a simple modulo', () => {
        let script = '1 % 2'
        let tree = '(expression (expression (literal 1)) % (expression (literal 2)))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'A % B.C'
        tree = '(expression (expression (identifier (variableName A))) % (expression (expression (identifier (variableName B))) . (objectMemberName C)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse several multiplications, divisions and modulo', () => {
        let script = '1 * 2 / 3 % 4 * 5 / 6 % 7'
        let tree = '(expression (expression (expression (expression (expression (expression (expression (literal 1)) * (expression (literal 2))) / (expression (literal 3))) % (expression (literal 4))) * (expression (literal 5))) / (expression (literal 6))) % (expression (literal 7)))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'A * B.C / D() % <E, F> * G::H / I % J'
        tree = '(expression (expression (expression (expression (expression (expression (expression (identifier (variableName A))) * (expression (expression (identifier (variableName B))) . (objectMemberName C))) / (expression (functionCall (functionName D) ( )))) % (expression (vector < (expression (identifier (variableName E))) , (expression (identifier (variableName F))) >))) * (expression (identifier (namespaceName G) :: (variableName H)))) / (expression (identifier (variableName I)))) % (expression (identifier (variableName J))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('additiveExpression', () => {
      test('parse a simple addition', () => {
        let script = '1 + 2'
        let tree = '(expression (expression (literal 1)) + (expression (literal 2)))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'A + B.C'
        tree = '(expression (expression (identifier (variableName A))) + (expression (expression (identifier (variableName B))) . (objectMemberName C)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse a simple subtraction', () => {
        let script = '1 - 2'
        let tree = '(expression (expression (literal 1)) - (expression (literal 2)))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'A - B.C'
        tree = '(expression (expression (identifier (variableName A))) - (expression (expression (identifier (variableName B))) . (objectMemberName C)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse several additions and subtractions', () => {
        let script = '1 + 2 - 3 + 4 - 5'
        let tree = '(expression (expression (expression (expression (expression (literal 1)) + (expression (literal 2))) - (expression (literal 3))) + (expression (literal 4))) - (expression (literal 5)))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'A + B.C - D() + (E + F) - G::H'
        tree = '(expression (expression (expression (expression (expression (identifier (variableName A))) + (expression (expression (identifier (variableName B))) . (objectMemberName C))) - (expression (functionCall (functionName D) ( )))) + (expression (parenthesis ( (expression (expression (identifier (variableName E))) + (expression (identifier (variableName F)))) )))) - (expression (identifier (namespaceName G) :: (variableName H))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse several additions, subtractions, multiplication, division and module with the right precedence', () => {
        const script = '1 + 2 * 3 - 4 / 5 + 6 % 7'
        const tree = '(expression (expression (expression (expression (literal 1)) + (expression (expression (literal 2)) * (expression (literal 3)))) - (expression (expression (literal 4)) / (expression (literal 5)))) + (expression (expression (literal 6)) % (expression (literal 7))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('concatenationExpression', () => {
      test('parse a simple concatenation', () => {
        const script = '"A" ^ "B"'
        const tree = '(expression (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " A "))))) ^ (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " B "))))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse several concatenations', () => {
        const script = 'A ^ "B" ^ """C"""'
        const tree = '(expression (expression (expression (identifier (variableName A))) ^ (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " B ")))))) ^ (expression (literal (textLiteral (textBaseLiteral (textTripleQuoteLiteral """ C """))))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('relationalExpression', () => {
      test('parse relational expression', () => {
        let script = '1 < 2'
        let tree = '(expression (expression (literal 1)) < (expression (literal 2)))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = '1 > 2'
        tree = '(expression (expression (literal 1)) > (expression (literal 2)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = '1 <= 2'
        tree = '(expression (expression (literal 1)) <= (expression (literal 2)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = '1 >= 2'
        tree = '(expression (expression (literal 1)) >= (expression (literal 2)))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('textInterpolationExpression', () => {
      test('parse a single text interpolation', () => {
        let script = '"""A {{{B}}} C"""'
        let tree = '(textInterpolation """ A  {{{ (expression (identifier (variableName B))) }}}  C """)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
        script = '"""{{{B}}} C"""'
        tree = '(textInterpolation """ {{{ (expression (identifier (variableName B))) }}}  C """)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
        script = '"""A {{{B}}}"""'
        tree = '(textInterpolation """ A  {{{ (expression (identifier (variableName B))) }}} """)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
        script = '"""{{{B}}}"""'
        tree = '(textInterpolation """ {{{ (expression (identifier (variableName B))) }}} """)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
      })
      test('parse multiple text interpolation', () => {
        let script = '"""A {{{B}}} C {{{D}}} E"""'
        let tree = '(textInterpolation """ A  {{{ (expression (identifier (variableName B))) }}}  C  {{{ (expression (identifier (variableName D))) }}}  E """)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
        script = '"""{{{B}}} C {{{D}}} E"""'
        tree = '(textInterpolation """ {{{ (expression (identifier (variableName B))) }}}  C  {{{ (expression (identifier (variableName D))) }}}  E """)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
        script = '"""A {{{B}}} {{{D}}} E"""'
        tree = '(textInterpolation """ A  {{{ (expression (identifier (variableName B))) }}}   {{{ (expression (identifier (variableName D))) }}}  E """)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
        script = '"""A {{{B}}} C {{{D}}}"""'
        tree = '(textInterpolation """ A  {{{ (expression (identifier (variableName B))) }}}  C  {{{ (expression (identifier (variableName D))) }}} """)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
        script = '"""A {{{B}}} {{{D}}}"""'
        tree = '(textInterpolation """ A  {{{ (expression (identifier (variableName B))) }}}   {{{ (expression (identifier (variableName D))) }}} """)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
        script = '"""{{{B}}} {{{D}}} E"""'
        tree = '(textInterpolation """ {{{ (expression (identifier (variableName B))) }}}   {{{ (expression (identifier (variableName D))) }}}  E """)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
        script = '"""{{{B}}} {{{D}}}"""'
        tree = '(textInterpolation """ {{{ (expression (identifier (variableName B))) }}}   {{{ (expression (identifier (variableName D))) }}} """)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
      })
      test('parse text interpolation inside text interpolation', () => {
        const script = '"""{{{"""{{{"""{{{A}}}"""}}}"""}}}"""'
        const tree = '(textInterpolation """ {{{ (expression (textInterpolation """ {{{ (expression (textInterpolation """ {{{ (expression (identifier (variableName A))) }}} """)) }}} """)) }}} """)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.textInterpolation())).toBe(tree)
      })
      test('parse quote and brace in text literal', () => {
        let script = '"""A " "" B"""'
        let tree = '(expression (literal (textLiteral (textBaseLiteral (textTripleQuoteLiteral """ A  "   " "  B """)))))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)

        script = '"""C { } {{ }} D"""'
        tree = '(expression (literal (textLiteral (textBaseLiteral (textTripleQuoteLiteral """ C  {  }  { {  }} D """)))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)

        script = '"""E{{ }}}F"""'
        tree = '(expression (literal (textLiteral (textBaseLiteral (textTripleQuoteLiteral """ E { {  }}}F """)))))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)

        script = '"""<label text="{{{Value}}}" />"""'
        tree = '(expression (textInterpolation """ <label text= " {{{ (expression (identifier (variableName Value))) }}} "  /> """))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse text interpolation in whole program', () => {
        const script = 'main () { declare Text Value = """<label id="some-label" {{{GetLabelClass("some-label-class")}}} pos="0 10.5" textsize="{{{TextSize}}}" color="ffffff" text="{{{GetValue("""AAA {{{Value}}} BBB""")}}}" />"""; }'
        const tree = '(program (programContent (mainStatement main ( ) (block { (statements (statement (variableDeclaration declare (type (baseType (simpleType Text))) (variableName Value) (initializer = (expression (textInterpolation """ <label id= " some-label "   {{{ (expression (functionCall (functionName GetLabelClass) ( (arguments (argument (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " some-label-class "))))))) ))) }}}  pos= " 0 10.5 "  textsize= " {{{ (expression (identifier (variableName TextSize))) }}} "  color= " ffffff "  text= " {{{ (expression (functionCall (functionName GetValue) ( (arguments (argument (expression (textInterpolation """ AAA  {{{ (expression (identifier (variableName Value))) }}}  BBB """)))) ))) }}} "  /> """))) ;))) }))) <EOF>)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.program())).toBe(tree)
      })
      test('throw error on empty interpolation', () => {
        const script = '"""{{{}}}"""'
        const error = /ParserError \(1:6\): mismatched input '}}}' expecting {.*}/
        const parser = getParser(script)
        expect(() => getParseTree(parser, parser.expression())).toThrow(error)
      })
    })

    describe('dumptypeExpression', () => {
      test('parse dumptype()', () => {
        const script = 'dumptype(K_A)'
        const tree = '(expression dumptype ( (structIdentifier (structName K_A)) ))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
      test('parse dumptype() from library', () => {
        const script = 'dumptype(A::K_B)'
        const tree = '(expression dumptype ( (structIdentifier (namespaceName A) :: (structName K_B)) ))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('dumpExpression', () => {
      test('parse dump()', () => {
        let script = 'dump(1)'
        let tree = '(expression dump ( (expression (literal 1)) ))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'dump(A)'
        tree = '(expression dump ( (expression (identifier (variableName A))) ))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'dump("A")'
        tree = '(expression dump ( (expression (literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " A "))))) ))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })

    describe('structureExpression', () => {
      test('parse simple structure', () => {
        const script = `
          K_A {
            B = 1,
            C = 2
          }
        `
        const tree = '(structure (structIdentifier (structName K_A)) { (structureMemberInitializationList (structureMemberInitialization (structMemberName B) = (expression (literal 1))) , (structureMemberInitialization (structMemberName C) = (expression (literal 2)))) })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.structure())).toBe(tree)
      })
      test('parse structure with dot', () => {
        const script = `
          K_A {
            .B = 1,
            C = 2,
            .D = 3
          }
        `
        const tree = '(structure (structIdentifier (structName K_A)) { (structureMemberInitializationList (structureMemberInitialization . (structMemberName B) = (expression (literal 1))) , (structureMemberInitialization (structMemberName C) = (expression (literal 2))) , (structureMemberInitialization . (structMemberName D) = (expression (literal 3)))) })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.structure())).toBe(tree)
      })
      test('parse empty structure', () => {
        const script = 'K_A {}'
        const tree = '(structure (structIdentifier (structName K_A)) { })'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.structure())).toBe(tree)
      })
      /*
      test('parse complex structure', () => {
        const script = `
          K_A {
            B = 1,
            C = CSmMode::EMedal::Gold,
            E = [],
            F = <0, -1>,
            G = H(),
            I = J::K_L {},
            M = Boolean,
            N = CSmMode,
            O = P[][Integer]
          }
        `
        const tree = ''
        const parser = getParser(script)
        expect(getParseTree(parser, parser.structure())).toBe(tree)
      })
      */
    })

    describe('arrayExpression', () => {
      test('parse simple array', () => {
        const script = '[1, A(), [], [2, 3]]'
        const tree = '(array (simpleArray [ (expression (literal 1)) , (expression (functionCall (functionName A) ( ))) , (expression (array (emptyArray [ ]))) , (expression (array (simpleArray [ (expression (literal 2)) , (expression (literal 3)) ]))) ]))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.array())).toBe(tree)
      })
      test('parse associative array', () => {
        const script = '[1 => 2, 2 => A(), 3 => [], 4 => [5 => 6]]'
        const tree = '(array (associativeArray [ (expression (literal 1)) => (expression (literal 2)) , (expression (literal 2)) => (expression (functionCall (functionName A) ( ))) , (expression (literal 3)) => (expression (array (emptyArray [ ]))) , (expression (literal 4)) => (expression (array (associativeArray [ (expression (literal 5)) => (expression (literal 6)) ]))) ]))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.array())).toBe(tree)
      })
      test('parse empty array', () => {
        const script = '[]'
        const tree = '(array (emptyArray [ ]))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.array())).toBe(tree)
      })
    })

    describe('elementAccess', () => {
      describe('index access', () => {
        test('parse one dimensional index access', () => {
          const script = 'Test[1]'
          const tree = '(expression (expression (identifier (variableName Test))) [ (expression (literal 1)) ])'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('parse multi dimensional index access', () => {
          const script = 'Test[1][2][3]'
          const tree = '(expression (expression (expression (expression (identifier (variableName Test))) [ (expression (literal 1)) ]) [ (expression (literal 2)) ]) [ (expression (literal 3)) ])'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('parse index access after a function', () => {
          const script = 'Test()[1]'
          const tree = '(expression (expression (functionCall (functionName Test) ( ))) [ (expression (literal 1)) ])'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('parse This index access', () => {
          const script = 'This[1]'
          const tree = '(expression (expression This) [ (expression (literal 1)) ])'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('parse index access after a dot access', () => {
          let script = 'Test.A[1]'
          let tree = '(expression (expression (expression (identifier (variableName Test))) . (objectMemberName A)) [ (expression (literal 1)) ])'
          let parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
          script = 'Test.A[1].B[2]'
          tree = '(expression (expression (expression (expression (expression (identifier (variableName Test))) . (objectMemberName A)) [ (expression (literal 1)) ]) . (objectMemberName B)) [ (expression (literal 2)) ])'
          parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('find proper precedence', () => {
          const script = 'A + B[1]'
          const tree = '(expression (expression (identifier (variableName A))) + (expression (expression (identifier (variableName B))) [ (expression (literal 1)) ]))'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('throw error on incorrect access', () => {
          let script = 'Test[]'
          let error = /ParserError \(1:5\): mismatched input '\]' expecting {.*}/
          let parser = getParser(script)
          expect(() => getParseTree(parser, parser.expression())).toThrow(error)
          script = '"A"[1]'
          error = /ParserError \(1:3\): rule expression ("A"|undefined) is not an array/
          parser = getParser(script)
          expect(() => getParseTree(parser, parser.expression())).toThrow(error)
          script = '<1, 1>[1]'
          error = /ParserError \(1:6\): rule expression (<1,1>|undefined) is not an array/
          parser = getParser(script)
          expect(() => getParseTree(parser, parser.expression())).toThrow(error)
          script = 'A::B[C]'
          error = /ParserError \(1:4\): rule expression (A::B|undefined) is not an array/
          parser = getParser(script)
          expect(() => getParseTree(parser, parser.expression())).toThrow(error)
        })
      })

      describe('dot access', () => {
        test('parse one dimensional dot access', () => {
          const script = 'Test.A'
          const tree = '(expression (expression (identifier (variableName Test))) . (objectMemberName A))'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('parse multi dimensional dot access', () => {
          const script = 'Test.A.B.C'
          const tree = '(expression (expression (expression (expression (identifier (variableName Test))) . (objectMemberName A)) . (objectMemberName B)) . (objectMemberName C))'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('parse function dot access', () => {
          const script = 'Test.A()'
          const tree = '(expression (expression (identifier (variableName Test))) . (functionCall (functionName A) ( )))'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('parse dot access after a function', () => {
          const script = 'Test().A'
          const tree = '(expression (expression (functionCall (functionName Test) ( ))) . (objectMemberName A))'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('parse This dot access', () => {
          const script = 'This.A'
          const tree = '(expression (expression This) . (objectMemberName A))'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('parse dot access after index access', () => {
          let script = 'Test[1].A'
          let tree = '(expression (expression (expression (identifier (variableName Test))) [ (expression (literal 1)) ]) . (objectMemberName A))'
          let parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
          script = 'Test.A[1].B'
          tree = '(expression (expression (expression (expression (identifier (variableName Test))) . (objectMemberName A)) [ (expression (literal 1)) ]) . (objectMemberName B))'
          parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('find proper precedence', () => {
          const script = 'A + B.C'
          const tree = '(expression (expression (identifier (variableName A))) + (expression (expression (identifier (variableName B))) . (objectMemberName C)))'
          const parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
        test('throw error on incorrect access', () => {
          let script = 'A."ce"'
          let error = /ParserError \(1:2\): mismatched input '"' expecting {'Now', IDENTIFIER}/
          let parser = getParser(script)
          expect(() => getParseTree(parser, parser.expression())).toThrow(error)
          script = 'A.<1, 1>'
          error = /ParserError \(1:2\): mismatched input '<' expecting {'Now', IDENTIFIER}/
          parser = getParser(script)
          expect(() => getParseTree(parser, parser.expression())).toThrow(error)
          script = '<1, 1>.X'
          error = /ParserError \(1:6\): rule expression (<1,1>|undefined) is not an object/
          parser = getParser(script)
          expect(() => getParseTree(parser, parser.expression())).toThrow(error)
          script = 'A::B.C'
          error = /ParserError \(1:4\): rule expression (A::B|undefined) is not an object/
          parser = getParser(script)
          expect(() => getParseTree(parser, parser.expression())).toThrow(error)
          script = '1.A'
          const tree = '(expression (literal 1.))' // Must match only 1. not 1.A
          parser = getParser(script)
          expect(getParseTree(parser, parser.expression())).toBe(tree)
        })
      })
    })

    describe('identifier', () => {
      test('parse IDENTIFIER', () => {
        const script = 'Test'
        const tree = '(identifier (variableName Test))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.identifier())).toBe(tree)
      })
      test('parse namespaced IDENTIFIER', () => {
        const script = 'Namespace::Test'
        const tree = '(identifier (namespaceName Namespace) :: (variableName Test))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.identifier())).toBe(tree)
      })
    })

    describe('literal', () => {
      test('parse LITERAL_REAL', () => {
        const script = '1.23'
        const tree = '(literal 1.23)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.literal())).toBe(tree)
      })
      test('parse LITERAL_INTEGER', () => {
        const script = '123'
        const tree = '(literal 123)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.literal())).toBe(tree)
      })
      test('parse LITERAL_BOOLEAN', () => {
        let script = 'True'
        let tree = '(literal True)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.literal())).toBe(tree)
        script = 'False'
        tree = '(literal False)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.literal())).toBe(tree)
      })
      test('parse LITERAL_NULL', () => {
        const script = 'Null'
        const tree = '(literal Null)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.literal())).toBe(tree)
      })
      test('parse LITERAL_NULLID', () => {
        const script = 'NullId'
        const tree = '(literal NullId)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.literal())).toBe(tree)
      })
      test('find textLiteral', () => {
        const script = '"Test"'
        const tree = '(literal (textLiteral (textBaseLiteral (textSingleQuoteLiteral " Test "))))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.literal())).toBe(tree)
      })
      test('find enumLiteral', () => {
        const script = 'CSmMode::EMedal::Gold'
        const tree = '(literal (enumLiteral (className CSmMode) :: (enumName EMedal) :: (enumValue Gold)))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.literal())).toBe(tree)
      })
    })

    describe('textLiteral', () => {
      test('find textTranslatedLiteral', () => {
        const script = '_("Test")'
        const tree = '(textLiteral (textTranslatedLiteral _( (textBaseLiteral (textSingleQuoteLiteral " Test ")) )))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.textLiteral())).toBe(tree)
      })
      test('find textBaseLiteral', () => {
        const script = '"""Test"""'
        const tree = '(textLiteral (textBaseLiteral (textTripleQuoteLiteral """ Test """)))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.textLiteral())).toBe(tree)
      })
    })

    describe('textTranslatedLiteral', () => {
      test('parse translated TEXT', () => {
        let script = '_("Test")'
        let tree = '(textTranslatedLiteral _( (textBaseLiteral (textSingleQuoteLiteral " Test ")) ))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.textTranslatedLiteral())).toBe(tree)
        script = '_("""Test""")'
        tree = '(textTranslatedLiteral _( (textBaseLiteral (textTripleQuoteLiteral """ Test """)) ))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.textTranslatedLiteral())).toBe(tree)
      })
    })

    describe('textBaseLiteral', () => {
      test('find textSingleQuoteLiteral', () => {
        const script = '"Test"'
        const tree = '(textBaseLiteral (textSingleQuoteLiteral " Test "))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.textBaseLiteral())).toBe(tree)
      })
      test('find textTripleQuoteLiteral', () => {
        const script = '"""Test"""'
        const tree = '(textBaseLiteral (textTripleQuoteLiteral """ Test """))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.textBaseLiteral())).toBe(tree)
      })
    })

    describe('textSingleQuoteLiteral', () => {
      test('parse single quote TEXT', () => {
        const script = '"Test"'
        const tree = '(textSingleQuoteLiteral " Test ")'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.textSingleQuoteLiteral())).toBe(tree)
      })
    })

    describe('textTripleQuoteLiteral', () => {
      test('parse triple quote TEXT', () => {
        const script = '"""Test"""'
        const tree = '(textTripleQuoteLiteral """ Test """)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.textTripleQuoteLiteral())).toBe(tree)
      })
    })

    describe('enumLiteral', () => {
      test('parse enum with class', () => {
        const script = 'CSmMode::EMedal::Gold'
        const tree = '(enumLiteral (className CSmMode) :: (enumName EMedal) :: (enumValue Gold))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.enumLiteral())).toBe(tree)
      })
      test('parse enum without class', () => {
        const script = '::EMedal::Gold'
        const tree = '(enumLiteral :: (enumName EMedal) :: (enumValue Gold))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.enumLiteral())).toBe(tree)
      })
      test('parse enum with library', () => {
        const script = 'TimeLib::EDateFormats::Time'
        const tree = '(enumLiteral (namespaceName TimeLib) :: (enumName EDateFormats) :: (enumValue Time))'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.enumLiteral())).toBe(tree)
      })
    })

    describe('vector', () => {
      test('parse a Vec2', () => {
        let script = '<1., 2.>'
        let tree = '(vector < (expression (literal 1.)) , (expression (literal 2.)) >)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.vector())).toBe(tree)
        script = '<1. + 2., A()>'
        tree = '(vector < (expression (expression (literal 1.)) + (expression (literal 2.))) , (expression (functionCall (functionName A) ( ))) >)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.vector())).toBe(tree)
      })
      test('parse a Vec3', () => {
        let script = '<1., 2., 3.>'
        let tree = '(vector < (expression (literal 1.)) , (expression (literal 2.)) , (expression (literal 3.)) >)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.vector())).toBe(tree)
        script = '<1. + 2., A(), <1., 2.>>'
        tree = '(vector < (expression (expression (literal 1.)) + (expression (literal 2.))) , (expression (functionCall (functionName A) ( ))) , (expression (vector < (expression (literal 1.)) , (expression (literal 2.)) >)) >)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.vector())).toBe(tree)
      })
      test('parse an Int2', () => {
        let script = '<1, 2>'
        let tree = '(vector < (expression (literal 1)) , (expression (literal 2)) >)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.vector())).toBe(tree)
        script = '<1 + 2, A()>'
        tree = '(vector < (expression (expression (literal 1)) + (expression (literal 2))) , (expression (functionCall (functionName A) ( ))) >)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.vector())).toBe(tree)
      })
      test('parse an Int3', () => {
        let script = '<1, 2, 3>'
        let tree = '(vector < (expression (literal 1)) , (expression (literal 2)) , (expression (literal 3)) >)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.vector())).toBe(tree)
        script = '<1 + 2, A(), <1, 2>>'
        tree = '(vector < (expression (expression (literal 1)) + (expression (literal 2))) , (expression (functionCall (functionName A) ( ))) , (expression (vector < (expression (literal 1)) , (expression (literal 2)) >)) >)'
        parser = getParser(script)
        expect(getParseTree(parser, parser.vector())).toBe(tree)
      })
      test('parse a substraction of vector', () => {
        let script = '<11, 22> - <33, 44>;'
        let tree = '(statement (expressionStatement (expression (expression (vector < (expression (literal 11)) , (expression (literal 22)) >)) - (expression (vector < (expression (literal 33)) , (expression (literal 44)) >))) ;))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.statement())).toBe(tree)
        script = 'A + B.C - D() + <E, F> - G::H;'
        tree = '(statement (expressionStatement (expression (expression (expression (expression (expression (identifier (variableName A))) + (expression (expression (identifier (variableName B))) . (objectMemberName C))) - (expression (functionCall (functionName D) ( )))) + (expression (vector < (expression (identifier (variableName E))) , (expression (identifier (variableName F))) >))) - (expression (identifier (namespaceName G) :: (variableName H)))) ;))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.statement())).toBe(tree)
      })
      test('throw error on invalid vector', () => {
        let script = '<1.>'
        let error = /ParserError \(1:4\): no viable alternative at input '<1.>'/
        let parser = getParser(script)
        expect(() => getParseTree(parser, parser.vector())).toThrow(error)
        script = '<1., 2., 3., 4.>'
        error = /ParserError \(1:11\): mismatched input ',' expecting '>'/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.vector())).toThrow(error)
        script = '<1>'
        error = /ParserError \(1:3\): no viable alternative at input '<1>'/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.vector())).toThrow(error)
        script = '<1, 2, 3, 4>'
        error = /ParserError \(1:8\): mismatched input ',' expecting '>'/
        parser = getParser(script)
        expect(() => getParseTree(parser, parser.vector())).toThrow(error)
      })
    })

    describe('parenthesisExpression', () => {
      test('parse parenthesis around expressions', () => {
        let script = '(Test)'
        let tree = '(parenthesis ( (expression (identifier (variableName Test))) ))'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.parenthesis())).toBe(tree)
        script = '((((Test).A).B).C)'
        tree = '(parenthesis ( (expression (expression (parenthesis ( (expression (expression (parenthesis ( (expression (expression (parenthesis ( (expression (identifier (variableName Test))) ))) . (objectMemberName A)) ))) . (objectMemberName B)) ))) . (objectMemberName C)) ))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.parenthesis())).toBe(tree)
      })
    })

    describe('nowExpression', () => {
      test('parse now expression', () => {
        let script = 'Now'
        let tree = '(expression Now)'
        let parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
        script = 'Now;'
        tree = '(statement (expressionStatement (expression Now) ;))'
        parser = getParser(script)
        expect(getParseTree(parser, parser.statement())).toBe(tree)
      })
      test('parse now dot access expression', () => {
        const script = 'This.Now'
        const tree = '(expression (expression This) . Now)'
        const parser = getParser(script)
        expect(getParseTree(parser, parser.expression())).toBe(tree)
      })
    })
  })

  describe('types', () => {
    test('parse simple types', () => {
      let script = 'Boolean'
      let tree = '(simpleType Boolean)'
      let parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'Ident'
      tree = '(simpleType Ident)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'Int2'
      tree = '(simpleType Int2)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'Int3'
      tree = '(simpleType Int3)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'Integer'
      tree = '(simpleType Integer)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'Real'
      tree = '(simpleType Real)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'Text'
      tree = '(simpleType Text)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'Vec2'
      tree = '(simpleType Vec2)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'Vec3'
      tree = '(simpleType Vec3)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'Void'
      tree = '(simpleType Void)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.simpleType())).toBe(tree)
      script = 'CMode'
      const error = /ParserError \(1:0\): mismatched input 'CMode' expecting {.*}/
      parser = getParser(script)
      expect(() => getParseTree(parser, parser.simpleType())).toThrow(error)
    })
    test('parse class type', () => {
      let script = 'CMode'
      let tree = '(className CMode)'
      let parser = getParser(script)
      expect(getParseTree(parser, parser.className())).toBe(tree)
      script = 'CUIConfig'
      tree = '(className CUIConfig)'
      parser = getParser(script)
      expect(getParseTree(parser, parser.className())).toBe(tree)
      script = 'Boolean'
      const error = /ParserError \(1:0\): mismatched input 'Boolean' expecting CLASS/
      parser = getParser(script)
      expect(() => getParseTree(parser, parser.className())).toThrow(error)
    })
    test('parse enum type', () => {
      let script = 'CMode::EMedal'
      let tree = '(enumType (className CMode) :: (enumName EMedal))'
      let parser = getParser(script)
      expect(getParseTree(parser, parser.enumType())).toBe(tree)
      script = '::EMedal'
      tree = '(enumType :: (enumName EMedal))'
      parser = getParser(script)
      expect(getParseTree(parser, parser.enumType())).toBe(tree)
      script = 'Boolean'
      let error = /ParserError \(1:0\): mismatched input 'Boolean' expecting {.*}/
      parser = getParser(script)
      expect(() => getParseTree(parser, parser.enumType())).toThrow(error)
      script = 'Lib::K_A'
      error = /ParserError \(1:0\): extraneous input 'Lib' expecting {.*}/
      parser = getParser(script)
      expect(() => getParseTree(parser, parser.enumType())).toThrow(error)
    })
    test('parse custom type', () => {
      let script = 'K_A'
      let tree = '(customType (structName K_A))'
      let parser = getParser(script)
      expect(getParseTree(parser, parser.customType())).toBe(tree)
      script = 'Lib::K_A'
      tree = '(customType (namespaceName Lib) :: (structName K_A))'
      parser = getParser(script)
      expect(getParseTree(parser, parser.customType())).toBe(tree)
      script = 'Boolean'
      let error = /ParserError \(1:0\): mismatched input 'Boolean' expecting IDENTIFIER/
      parser = getParser(script)
      expect(() => getParseTree(parser, parser.customType())).toThrow(error)
      script = 'CMode::EMedal'
      error = /ParserError \(1:0\): mismatched input 'CMode' expecting IDENTIFIER/
      parser = getParser(script)
      expect(() => getParseTree(parser, parser.customType())).toThrow(error)
    })
    test('parse array type', () => {
      let script = 'Integer[]'
      let tree = '(arrayType (baseType (simpleType Integer)) (arrayTypeDimension [ ]))'
      let parser = getParser(script)
      expect(getParseTree(parser, parser.arrayType())).toBe(tree)
      script = 'Integer[][]'
      tree = '(arrayType (baseType (simpleType Integer)) (arrayTypeDimension [ ]) (arrayTypeDimension [ ]))'
      parser = getParser(script)
      expect(getParseTree(parser, parser.arrayType())).toBe(tree)
      script = 'CSmMode[K_A][][CMode::EMedal]'
      tree = '(arrayType (baseType (className CSmMode)) (arrayTypeDimension [ (baseType (customType (structName K_A))) ]) (arrayTypeDimension [ ]) (arrayTypeDimension [ (baseType (enumType (className CMode) :: (enumName EMedal))) ]))'
      parser = getParser(script)
      expect(getParseTree(parser, parser.arrayType())).toBe(tree)
      script = 'Boolean'
      const error = /ParserError \(1:7\): mismatched input '<EOF>' expecting '\['/
      parser = getParser(script)
      expect(() => getParseTree(parser, parser.arrayType())).toThrow(error)
    })
  })
})
