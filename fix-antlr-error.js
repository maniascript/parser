import { readFileSync, writeFileSync } from 'node:fs'

let content = ''

content = readFileSync('./src/antlr/ManiaScriptParserListener.ts', 'utf8')
content = content.replace('import { ErrorNode, ParseTreeListener, ParserRuleContext, TerminalNode } from "antlr4ng";', 'import { ErrorNode, type ParseTreeListener, ParserRuleContext, TerminalNode } from "antlr4ng";')
content = content.replace('visitTerminal(node: TerminalNode): void {}', 'visitTerminal(_node: TerminalNode): void {}')
content = content.replace('visitErrorNode(node: ErrorNode): void {}', 'visitErrorNode(_node: ErrorNode): void {}')
content = content.replace('enterEveryRule(node: ParserRuleContext): void {}', 'enterEveryRule(_node: ParserRuleContext): void {}')
content = content.replace('exitEveryRule(node: ParserRuleContext): void {}', 'exitEveryRule(_node: ParserRuleContext): void {}')
writeFileSync('./src/antlr/ManiaScriptParserListener.ts', content)

content = readFileSync('./src/antlr/ManiaScriptLexer.ts', 'utf8')
content = content.replace('private OPERATOR_CLOSE_INTERPOLATION_action(localContext: antlr.ParserRuleContext | null, actionIndex: number): void {', 'private OPERATOR_CLOSE_INTERPOLATION_action(_localContext: antlr.ParserRuleContext | null, actionIndex: number): void {')
content = content.replace('private IDENTIFIER_action(localContext: antlr.ParserRuleContext | null, actionIndex: number): void {', 'private IDENTIFIER_action(_localContext: antlr.ParserRuleContext | null, actionIndex: number): void {')
content = content.replace('private OPERATOR_OPEN_INTERPOLATION_action(localContext: antlr.ParserRuleContext | null, actionIndex: number): void {', 'private OPERATOR_OPEN_INTERPOLATION_action(_localContext: antlr.ParserRuleContext | null, actionIndex: number): void {')
content = content.replace('private OPERATOR_CLOSE_INTERPOLATION_sempred(localContext: antlr.ParserRuleContext | null, predIndex: number): boolean {', 'private OPERATOR_CLOSE_INTERPOLATION_sempred(_localContext: antlr.ParserRuleContext | null, predIndex: number): boolean {')
content = content.replace('private TEXT2_sempred(localContext: antlr.ParserRuleContext | null, predIndex: number): boolean {', 'private TEXT2_sempred(_localContext: antlr.ParserRuleContext | null, predIndex: number): boolean {')
writeFileSync('./src/antlr/ManiaScriptLexer.ts', content)
